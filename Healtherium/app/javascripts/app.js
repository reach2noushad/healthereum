
// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";


 // Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract';
import uuidv4  from 'uuid/v4';
import sha1 from 'sha1';

const web3 = new Web3();

var timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();

//Initialisations

$("#loader").hide();



window.doctorsignup = function(candidate) {
  $("#loader").show();
  let DoctorFirstName = $("#doctor_fname").val();
  let DoctorSecondName = $("#doctor_sname").val();
  let DoctorGender = $("#doctor_gender").val();
  let DoctorBirthDate = $("#doctor_age").val();
  let DoctorAddressLineOne = $("#doctor_addresslineone").val();
  let DoctorAddressLineTwo = $("#doctor_addresslinetwo").val();
  let DoctorCity = $("#doctor_city").val();
  let DoctorState = $("#doctor_state").val();
  let DoctorPostalCode = $("#doctor_postalcode").val();
  let DoctorCountry = $("#doctor_country").val();
  let DoctorDepartment = $("#doctor_department").val();
  let DoctorHospital = $("#doctor_hospital").val();
  let DoctorAdhaar = $("#doctor_adhaar").val();
  var uuid = uuidv4();
  var DoctorUserId = uuid.replace(/-/g, '');
  var a = parseFloat(DoctorAdhaar);
  var b = timeStampInMs;
  var c = a+b;
  let blockinputoutcomplete = sha1(c);
  console.log("This is Sha1 "+blockinputoutcomplete);
  let DoctorInputPassword= $("#doctor_password").val();

  var body = {"InputPassword":DoctorInputPassword,"InputAdhaar":DoctorAdhaar,"user":"Doctor","UserID":DoctorUserId,"InputOutputIdentifier":blockinputoutcomplete,"Department":DoctorDepartment,"Hospital":DoctorHospital,"FirstName":DoctorFirstName,"SecondName":DoctorSecondName,"Gender":DoctorGender,"birthDate":DoctorBirthDate,"AddressLineOne":DoctorAddressLineOne,"AddressLineTwo":DoctorAddressLineTwo,"City":DoctorCity,"State":DoctorState,"PostalCode":DoctorPostalCode,"Country":DoctorCountry};

    $.ajax({type: "POST",url: 'http://localhost:4445/createaccount',data: JSON.stringify(body),success: function(result) {
      console.log(result);
      if(result=='User Already Exist'){
        $("#doctor_humanized").html("<p class='black-text'>User Is Already Present </p> <blockquote>");
        }else{
          $("#doctor_humanized").html("<p class='black-text'>Please note your Unique ID </p> <blockquote>"+ DoctorUserId + "</blockquote> <p class='black-text'>This is required for future access to your data</p>");
        }
      $("#loader").hide();
        },
        contentType: 'application/json'
      });

}

window.getDoctor = function(candidate) {
  let DoctorInputId = $("#getdoctorid").val();
  let Doctoradhaarnum = $("#getdoctoradhaar").val();
  let DoctorLoginPassword = $("#getdoctorpassword").val();

  var body = {"InputPassword":DoctorLoginPassword,"InputAdhaar":Doctoradhaarnum,"user":"Doctor","type":"doctordata","UserID":DoctorInputId};

    $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(result) {
      console.log(result);
      var mytry = result['openmrsbody'];
      var myresult = JSON.parse(mytry);
      var docfirstname = myresult['entry'][0]['resource']['name'][0]['given'][0];
      var docfamilysname = myresult['entry'][0]['resource']['name'][0]['family'];
      var docgender = myresult['entry'][0]['resource']['gender'];
      var docbirthdate = myresult['entry'][0]['resource']['birthDate'];
      var docaddresslineone = myresult['entry'][0]['resource']['address'][0]['line'][0];
      var docaddresslinetwo = myresult['entry'][0]['resource']['address'][0]['line'][1];
      var doccity = myresult['entry'][0]['resource']['address'][0]['city'];
      var docstate = myresult['entry'][0]['resource']['address'][0]['state'];
      var doccode = myresult['entry'][0]['resource']['address'][0]['postalCode'];
      var doccountry = myresult['entry'][0]['resource']['address'][0]['country'];
      var reqdepartment = result['department'];
      var reqhospital = result['hospital'];
      var data = "<table> <tr>    <td>Given Name</td>    <td>" + docfirstname + "</td>  </tr> <tr>    <td>Family Name</td>    <td>" + docfamilysname + "</td>  </tr> <tr>    <td>Gender</td>    <td>" + docgender + "</td>  </tr> <tr>    <td>Birth Date</td>    <td>" + docbirthdate + "</td>  </tr> <tr>    <td>Address Line 1</td>    <td>" + docaddresslineone + "</td>  </tr> <tr>    <td>Address Line 2</td>    <td>" + docaddresslinetwo + "</td>  </tr> <tr>    <td>City</td>    <td>" + doccity + "</td>  </tr> <tr>   <td>State</td>    <td>" + docstate + "</td>  </tr> <tr>    <td>Postal Code</td>    <td>" + doccode + "</td>  </tr> <tr>    <td>Country</td>    <td>" + doccountry + "</td>  </tr> <tr>    <td>Department</td>    <td>" + reqdepartment + "</td>  </tr> <tr>    <td>Hospital</td>    <td>" + reqhospital + "</td>  </tr>  </table>";
      $("#displaydoctor").html(data);
    },
    contentType: 'application/json'
  });
}

window.createPatient = function(candidate) {
  $("#loader").show();
    let PatientFirstName = $("#patient_gname").val();
    let PatientSecondName = $("#patient_fname").val();
    let PatientGender = $("#patient_gender").val();
    let PatientBirthDate = $("#patient_birthdate").val();
    let PatientAddressLineOne = $("#patient_addresslineone").val();
    let PatientAddressLineTwo = $("#patient_addresslinetwo").val();
    let PatientCity = $("#patient_city").val();
    let PatientState = $("#patient_state").val();
    let PatientPostalCode = $("#patient_postalcode").val();
    let PatientCountry = $("#patient_country").val();
    let PatientAdhaar = $("#patient_adhaar").val();
    var a = parseFloat(PatientAdhaar);
    var b = timeStampInMs;
    var c = a+b;
    let blockinputoutcompletepatient = sha1(c);
    console.log("This is sha 1 "+blockinputoutcompletepatient);
    var uuid = uuidv4();
    var PatientUserId = uuid.replace(/-/g, '');
    let PatientInputPassword= $("#patient_password").val();

    var body = {"InputPassword":PatientInputPassword,"InputAdhaar":PatientAdhaar,"user":"Patient","UserID":PatientUserId,"InputOutputIdentifier":blockinputoutcompletepatient,"FirstName":PatientFirstName,"SecondName":PatientSecondName,"Gender":PatientGender,"birthDate":PatientBirthDate,"AddressLineOne":PatientAddressLineOne,"AddressLineTwo":PatientAddressLineTwo,"City":PatientCity,"State":PatientState,"PostalCode":PatientPostalCode,"Country":PatientCountry};

    $.ajax({type: "POST",url: 'http://localhost:4445/createaccount',data: JSON.stringify(body),success: function(result) {
      console.log(result);
      if(result=='User Already Exist'){
        $("#patient_humanized").html("<p class='black-text'>User Is Already Present </p> <blockquote>");
        }else{
          $("#patient_humanized").html("<p class='black-text'>Please note your Unique ID </p> <blockquote>"+ PatientUserId + "</blockquote> <p class='black-text'>This is required for future access to your data</p>");
        }
      $("#loader").hide();
        },
        contentType: 'application/json'
      });
}




window.getPatient = function(candidate){
    let PatientId = $("#getpatientid").val();
    let Patientadhaarnum = $("#getpatientadhaar").val();
    let PatientGivenPassword = $("#getpatientpassword").val();

      var body = {"InputPassword":PatientGivenPassword,"InputAdhaar":Patientadhaarnum,"user":"Patient","type":"patientdata","UserID":PatientId};

      $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(list) {
        console.log(list);
        var result = JSON.parse(list);
           console.log(result);
           var firstname = result['entry'][0]['resource']['name'][0]['given'][0];
           var familysname = result['entry'][0]['resource']['name'][0]['family'];
           var patientsgender = result['entry'][0]['resource']['gender'];
           var patientsbirthdate = result['entry'][0]['resource']['birthDate'];
           var patientaddresslineone = result['entry'][0]['resource']['address'][0]['line'][0];
           var patientaddresslinetwo = result['entry'][0]['resource']['address'][0]['line'][1];
           var patientcity = result['entry'][0]['resource']['address'][0]['city'];
           var patientstate = result['entry'][0]['resource']['address'][0]['state'];
           var patientcode = result['entry'][0]['resource']['address'][0]['postalCode'];
           var patientscountry = result['entry'][0]['resource']['address'][0]['country'];
          var data = "<table> <tr>    <td>Given Name</td>    <td>" + firstname + "</td>  </tr>         <tr>    <td>Family Name</td>    <td>" + familysname + "</td>  </tr>         <tr>    <td>Gender</td>    <td>" + patientsgender + "</td>  </tr>         <tr>    <td>Birth Date</td>    <td>" + patientsbirthdate + "</td>  </tr>         <tr>    <td>Address Line 1</td>    <td>" + patientaddresslineone + "</td>  </tr>         <tr>    <td>Address Line 2</td>    <td>" + patientaddresslinetwo + "</td> </tr>         <tr>    <td>City</td>    <td>" + patientcity + "</td>  </tr>         <tr>    <td>State</td>    <td>" + patientstate + "</td>  </tr>        <tr>    <td>Postal Code</td>    <td>" + patientcode + "</td>  </tr>        <tr>    <td>Country</td>    <td>" + patientscountry + "</td>  </tr>  </table>";
          $("#display").html(data);
      },
      contentType: 'application/json'
    });

}

 window.transferownership = function(candidate) {
    $('#acesspost').removeClass("hide");
    let PatientId = $("#getpatientid").val();
    let Patientadhaarnum = $("#getpatientadhaar").val();
    let PatientGivenPassword = $("#getpatientpassword").val();
    let secondowneraddress = $("#doctorid").val();
    var body = {"InputPassword":PatientGivenPassword,"InputAdhaar":Patientadhaarnum,"type":"grant","UserID":PatientId,"SecondAddress":secondowneraddress};

    $.ajax({type: "POST",url: 'http://localhost:4445/ownership',data: JSON.stringify(body),success: function(result) {
      console.log(result);
      $('#acesspost').addClass("hide");
      $("#ownershipstatus").html("Access Granted");
    },
    contentType: 'application/json'
  });

}

window.cancelownership = function(candidate) {
  $('#cancelaccesspost').removeClass("hide");
  let PatientId = $("#getpatientid").val();
  let Patientadhaarnum = $("#getpatientadhaar").val();
  let PatientGivenPassword = $("#getpatientpassword").val();
  let secondowneraddress = $("#doctorids").val();

  var body = {"InputPassword":PatientGivenPassword,"InputAdhaar":Patientadhaarnum,"type":"cancel","UserID":PatientId,"SecondAddress":secondowneraddress};

  $.ajax({type: "POST",url: 'http://localhost:4445/ownership',data: JSON.stringify(body),success: function(result) {
    console.log(result);
    $('#cancelaccesspost').addClass("hide");
    $("#ownershipstatuscancelled").html("Access Cancelled");
  },
  contentType: 'application/json'
});

}

window.getvisits = function(candidate){
  let PatientId = $("#getpatientid").val();
  let Patientadhaarnum = $("#getpatientadhaar").val();
  let PatientGivenPassword = $("#getpatientpassword").val();

  var body = {"InputPassword":PatientGivenPassword,"InputAdhaar":Patientadhaarnum,"user":"Patient","type":"patientvisits","UserID":PatientId};

  $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(data) {
    // console.log(data);
    var resp = JSON.parse(data);
    var list =resp['entry'];
    console.log(list);
    $.each( list , function(i) {
      if (list[i].resource.hasOwnProperty("partOf")) {
        console.log(i);
        $('#displaycollapsible').append('<li><div class="collapsible-header"><i style="color:green;" class="material-icons">local_hospital</i>'+new Date(list[i].resource.period.start)+'</div><div class="collapsible-body"><span>Start:</span><span> '+new Date(list[i].resource.period.start)+'<br>End: '+new Date(list[i].resource.period.end)+'<br>Location: '+list[i].resource.location[0].location.display+'<br>Type: '+list[i].resource.partOf.display+'<br>Doctor: '+list[0].resource.participant[0].individual.display+'</span></div></li>');
      }
      else {
        $('#displaycollapsible').append('<li><div class="collapsible-header"><i style="color:orange;" class="material-icons">local_hospital</i>'+new Date(list[i].resource.period.start)+'</div><div class="collapsible-body"><span>Start: '+new Date(list[i].resource.period.start)+'<br>End: '+new Date(list[i].resource.period.end)+'<br>Location: '+list[i].resource.location[0].location.display+'</span></div></li>');
      }
    });
  },
  contentType: 'application/json'
  });
}



window.sharedoctoraddress=function(candidate){

  let DoctorInputId = $("#getdoctorid").val();
  let Doctoradhaarnum = $("#getdoctoradhaar").val();
  let DoctorLoginPassword = $("#getdoctorpassword").val();

  var body = {"InputPassword":DoctorLoginPassword,"InputAdhaar":Doctoradhaarnum,"user":"Doctor","type":"shareaddress","UserID":DoctorInputId};

  $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(result) {
    console.log(result);
       jQuery(function(){
       	jQuery('#output').qrcode(result);
       })
       $('#writtenaddress').html(result);
},
  contentType: 'application/json'
});

}

window.getPatientDetail = function(candidate) {

  let DoctorInputId = $("#getdoctorid").val();
  let Doctoradhaarnum = $("#getdoctoradhaar").val();
  let DoctorLoginPassword = $("#getdoctorpassword").val();

    var body = {"InputPassword":DoctorLoginPassword,"InputAdhaar":Doctoradhaarnum,"user":"Doctor","type":"getpatientfromdoctor","UserID":DoctorInputId};

    $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(list) {
      console.log(list);
      if(list=='Null'){
        $("#displaypatientinfo").html("No Patient Details Available");
      }else{
      var result = JSON.parse(list);
                console.log(result);
                var firstname = result['entry'][0]['resource']['name'][0]['given'][0];
                var familysname = result['entry'][0]['resource']['name'][0]['family'];
                var patientsgender = result['entry'][0]['resource']['gender'];
                var patientsbirthdate = result['entry'][0]['resource']['birthDate'];
                var patientaddresslineone = result['entry'][0]['resource']['address'][0]['line'][0];
                var patientaddresslinetwo = result['entry'][0]['resource']['address'][0]['line'][1];
                var patientcity = result['entry'][0]['resource']['address'][0]['city'];
                var patientstate = result['entry'][0]['resource']['address'][0]['state'];
                var patientcode = result['entry'][0]['resource']['address'][0]['postalCode'];
                var patientscountry = result['entry'][0]['resource']['address'][0]['country'];
               var data = "<table> <tr>    <td>Given Name</td>    <td>" + firstname + "</td>  </tr>         <tr>    <td>Family Name</td>    <td>" + familysname + "</td>  </tr>         <tr>    <td>Gender</td>    <td>" + patientsgender + "</td>  </tr>         <tr>    <td>Birth Date</td>    <td>" + patientsbirthdate + "</td>  </tr>         <tr>    <td>Address Line 1</td>    <td>" + patientaddresslineone + "</td>  </tr>         <tr>    <td>Address Line 2</td>    <td>" + patientaddresslinetwo + "</td> </tr>         <tr>    <td>City</td>    <td>" + patientcity + "</td>  </tr>         <tr>    <td>State</td>    <td>" + patientstate + "</td>  </tr>        <tr>    <td>Postal Code</td>    <td>" + patientcode + "</td>  </tr>        <tr>    <td>Country</td>    <td>" + patientscountry + "</td>  </tr>  </table>";
               $("#displaypatientinfo").html(data);
      }
  },
    contentType: 'application/json'
  });
  }




window.insertPatientFile = function(candidate) {
  let PatientId = $("#getpatientid").val();
  let Patientadhaarnum = $("#getpatientadhaar").val();
  let PatientGivenPassword = $("#getpatientpassword").val();
  var Fileurl = downloadURL;
  $('#secondpost').removeClass("hide");

  var body = {"InputPassword":PatientGivenPassword,"InputAdhaar":Patientadhaarnum,"type":"insertfile","UserID":PatientId,"FileUrl":Fileurl};

  $.ajax({type: "POST",url: 'http://localhost:4445/ownership',data: JSON.stringify(body),success: function(result) {
    console.log(result);
    $('#secondpost').addClass("hide");
     $("#displayinsert").html("<p class='black-text'> Saved </p> <blockquote>");
  },
  contentType: 'application/json'
});
}

window.getPatientFile = function(candidate) {

  let PatientId = $("#getpatientid").val();
  let Patientadhaarnum = $("#getpatientadhaar").val();
  let PatientGivenPassword = $("#getpatientpassword").val();

  var body = {"InputPassword":PatientGivenPassword,"InputAdhaar":Patientadhaarnum,"user":"getpatientfile","UserID":PatientId};

  $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(data) {
    console.log(data);
    var url=data;
    try {
          window.open("http://localhost:8000?id="+url,"_blank");
          } catch (err) {
            console.log(err);
          }
  },
  contentType: 'application/json'
  });


}


window.getPatientFileDoctor = function(candidate) {

  let DoctorInputId = $("#getdoctorid").val();
  let Doctoradhaarnum = $("#getdoctoradhaar").val();
  let DoctorLoginPassword = $("#getdoctorpassword").val();

  var body = {"InputPassword":DoctorLoginPassword,"InputAdhaar":Doctoradhaarnum,"user":"Doctor","type":"getfilefromdoctor","UserID":DoctorInputId};

  $.ajax({type: "POST",url: 'http://localhost:4445/getaccount',data: JSON.stringify(body),success: function(result) {
    console.log(result);
    var url=result;
    try {
          window.open("http://localhost:8000?id="+url,"_blank");
          } catch (err) {
            console.log(err);
          }

},
  contentType: 'application/json'
});



}

//   var contract = new ethers.Contract(address, abi, provider);
//
//   try {
//     var callPromise = contract.getpatienthashdoctor(requestaddress);
//     callPromise.then(function(get_url){
//       var url=get_url;
//       console.log(url);
//           try {
//             window.open("http://localhost:8000?id="+url,"_blank");
//       } catch (err) {
//         console.log(err);
//       }
//     });
//   } catch (err) {
//     console.log(err);
//   }
// }
