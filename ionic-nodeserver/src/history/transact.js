const ethers = require('ethers');
const utils = require('ethers').utils;
const { forEach } = require('p-iteration');
const Interface = ethers.Interface;
import config from '../config';
import helper from "./helper";
const abi = config.abi;
const iface = new Interface(abi);
const provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');
// const provider = ethers.providers.getDefaultProvider('rinkeby');
const eventInfo = iface.events;
const contractAddress = config.contract_address;

module.exports = {

    getAllEventLogs: async (eventTopic) => {

        var filter = {
            fromBlock: 3391370,
            address: contractAddress,
            topics: [ eventTopic ]
         }

         var eventLogs = await provider.getLogs(filter);

          return eventLogs;
   
    },


    getEventsForAddress : async (address,eventLogs,eventName) => {

        let newEventInfo = eventInfo[eventName];
        var result = [];

        for(const i in eventLogs){

            var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);


            if(eventParsed.patient.toLowerCase() === address.toLowerCase()) {

                let a= {};

                 
                if(eventName == "patientGivenAccessToDoctor" || eventName == "patientRevokeAccessToDoctor" ) {

                    a.doctor = eventParsed.doctor;

                    const dbResult = await helper.getName(eventParsed.doctor);
                    a.name = dbResult[0].name;
                    

                } else if (eventName == "patientNominating") {

                    a.nominee = eventParsed.nominatingPerson;

                    const dbResult = await helper.getName(eventParsed.nominatingPerson);
                    a.name = dbResult[0].name;

                } else {

                    a.nominee = eventParsed.nominatedPerson;

                    const dbResult = await helper.getName(eventParsed.nominatedPerson);
                    a.name = dbResult[0].name;

                }
               
                a.time = new Date(eventParsed.time.toNumber()*1000).toLocaleString("en-US", {timeZone: "Asia/Calcutta"});

                result.push(a);

            }

           
        }

        return result;
      
    }
}