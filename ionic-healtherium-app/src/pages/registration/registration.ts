import {Component, OnInit} from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import v4 from 'uuid/v4';
import sha1 from 'sha1';
import { HealtheriumApi } from '../../providers/healtherium-api/healtherium-api';
import { ModalContentPage } from '../modal-content/modal-content';
import {Provider} from "../../models/provider";

@Component({
  selector: 'registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage implements OnInit{

  loader: any;
  user: any = {};
  userType: string;
  selectedProvider: Provider;
  providerList: Provider[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private api: HealtheriumApi,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController) {
      if(this.navParams.data && this.navParams.data.user) {
         switch (this.navParams.data.user) {
            case "patient" :
              this.userType = "patient";
              break;
            case "doctor" :
              this.userType = "doctor";
              break;
            case "research" :
              this.user.type = "researcher";
              break;
            case "insurance" :
              this.userType = "insurance";
              break;
            case "buyer" :
              this.userType = "buyer";
              break;
            default :
              this.userType = "patient";
              break;

         }
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientSignupPage');
  }

  ngOnInit() {
    this.getProviders();
  }

  getProviders(): void {
    this.api.getProviders()
      .subscribe(response => {
        this.providerList = response['providers'];
        this.selectedProvider = this.providerList[0];
      },error => {
        console.log(error);
      });
  }


  submitInfo() {
    this.user.user = this.userType;
    this.user.isRegistered = false;
    this.user.provider = this.selectedProvider.providerNumber;
    console.log(this.user);
    this.createLoader();
    console.log(this.user);
    this.api.generateOTP(this.user).subscribe(response => {
      if (response) {
        console.log(response);
        this.loader.dismiss();
        let modal = this.modalCtrl.create(ModalContentPage, { type: "otp", data:this.user,  action: "registration" });
        modal.present();
        console.log(response);
      }
    },
      error => {
        this.loader.dismiss();
        let info = {
          title: "Error",
          subTitle: error.error.message,
        };
        this.presentAlert(info);
      });
  }

  createLoader(): void {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

  presentAlert(info) {
    let alert = this.alertCtrl.create({
      title: info.title,
      subTitle: info.subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }
}
