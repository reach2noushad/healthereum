export class Notification {
  id: string;
  message: string;
  isRead: boolean;
  createdOn: any
}

export class NotificationsResponse {
  count: number;
  notifications: Array<Notification>;
}
