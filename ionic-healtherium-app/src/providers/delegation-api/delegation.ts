import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from '../../config';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the DelegationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DelegationProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DelegationProvider Provider');
  }

  delegate(data): Observable<any> {
    return this.http.post(`${BASE_URL}/api/delegate/add`, data)
      .map(response => {
        return response;
      });
  }

  getDelegationInfo(data): Observable<any> {
    const params = new HttpParams().set('userId', data);
    return this.http.get(`${BASE_URL}/api/delegate/nominees`, { params })
      .map(response => response);
  }

  getDelegators(data: any): any {
    const params = new HttpParams().set('userId', data);
    return this.http.get(`${BASE_URL}/api/delegate/nominators`, { params })
      .map(response => response);
  }


  revokeDelegation(data): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: data
  };
    console.log("revoke", data);
    return this.http.delete(`${BASE_URL}/api/delegate/revoke`, httpOptions)
      .map(response => response);
  }

  grantAccess(data) : Observable<any> {
    return this.http.post(`${BASE_URL}/api/delegate/grantAccess`, data)
      .map(response => {
        return response;
      });
  }


}
