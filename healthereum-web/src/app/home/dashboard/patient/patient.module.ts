import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientComponent } from './patient.component';
import { AngularMaterialModule } from 'src/app/angular-material/angular-material.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

@NgModule({
declarations: [PatientComponent],
imports: [
CommonModule, AngularMaterialModule,
NgxLoadingModule.forRoot({
animationType: ngxLoadingAnimationTypes.rectangleBounce,
backdropBackgroundColour: 'rgba(0,0,0,0.8)'
})
],
exports: [
PatientComponent
]
})
export class PatientModule { }