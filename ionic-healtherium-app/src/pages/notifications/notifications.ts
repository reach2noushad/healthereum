import { Component } from '@angular/core';
import { IonicPage, ViewController , NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  userDetails: any;
  constructor(public viewCtrl: ViewController, params: NavParams) {
    console.log(params.get('userDetails'));
    this.userDetails = params.get('userDetails');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
