import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorDashboardPage } from './doctor-dashoard';
import { ComponentsModule } from "../../components/components.module";
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  declarations: [
    DoctorDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorDashboardPage),
    ComponentsModule,
    QRCodeModule
  ],
})
export class DoctorDashboardPageModule {}
