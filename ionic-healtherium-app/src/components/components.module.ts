import { NgModule } from '@angular/core';
import { NotificationsComponent } from './notifications/notifications';
import { IonicModule} from 'ionic-angular';
import { PrescriptionComponent } from './prescription/prescription';
import { DelegationComponent } from './delegation/delegation';
@NgModule({
	declarations: [NotificationsComponent,
    PrescriptionComponent,
    DelegationComponent],
	imports: [IonicModule],
	exports: [NotificationsComponent,
    PrescriptionComponent,
    DelegationComponent]
})
export class ComponentsModule {}
