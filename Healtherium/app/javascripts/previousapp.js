
// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'


import voting_artifacts from '../../build/contracts/Voting.json'


//Initialisations

var Voting = contract(voting_artifacts);
var searchnumbertwo = "";
var get_value = [];
var requritwo = "";
var get_valuethree = 0;
var submitted = true;
var get_url = " ";
var returnuuid = "";
var delayInMilliseconds = 1000;
var blockchainoutidentifier="";
var searchnumber = "";
var requuid = "";
var requri="";
var requsername="";
var reqpassword="";
var reqdepartment="";
var reqhospital="";
var timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
var name ="";
var genger="";
var age="";
var patientvisitquery = "";
var dt=0;
var month=0;
var year=0;

//Initial Functions

var crypto = require('crypto'),
shasum = crypto.createHash('sha1');
console.log(timeStampInMs);
shasum.update("createnew"+timeStampInMs);
console.log(shasum.digest('hex'));



function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


//Executing Functions Called From HTML

$("#loader").hide();


window.createPatient = function(candidate) {
  let PatientFirstName = $("#patient_gname").val();
  let PatientSecondName = $("#patient_fname").val();
  let PatientGender = $("#patient_gender").val();
  let PatientBirthDate = $("#patient_birthdate").val();
  let PatientAddressLineOne = $("#patient_addresslineone").val();
  let PatientAddressLineTwo = $("#patient_addresslinetwo").val();
  let PatientCity = $("#patient_city").val();
  let PatientState = $("#patient_state").val();
  let PatientPostalCode = $("#patient_postalcode").val();
  let PatientCountry = $("#patient_country").val();
  let PatientAdhaar = $("#patient_adhaar").val();
  var a = parseFloat(PatientAdhaar);
  console.log("Adhaar"+" "+a);
  var b = timeStampInMs;
  console.log("Timestamp"+" "+b)
  var c = a+b;
  console.log("Total"+" "+c);
  let d = shasum.update(c);
  let e = shasum.digest("hex");
  console.log(e);
  console.log("blockinputtimeplusuuid"+" "+e);
  let PatientUID = uuidv4();
  var shauid = web3.sha3(PatientUID);
  var PatientUserId = shauid.toString();
  let PatientAddress = web3.eth.accounts[0];
  $('#newpost').removeClass("hide");
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.insertPatient(PatientAddress,PatientAdhaar,PatientUserId,e,{gas: 980806, from: web3.eth.accounts[0]}).then(function(return_value) {
        contractInstance.insertPatient.call(PatientAddress,PatientAdhaar,PatientUserId,e,{gas: 980806, from: web3.eth.accounts[0]}).then(function(get_value) {
        blockchainoutidentifier = get_value[2];
        requsername = get_value[0];
        reqpassword = get_value[1];
        console.log("blockout identifier"+" "+blockchainoutidentifier);
        var data = "<table><tr>    <td>Name</td>    <td>" + get_value[0] + "</td>  </tr>  <tr>    <td>Gender</td>    <td>" + get_value[1] + "</td>  </tr>  <tr>    <td>Age</td>    <td>" + get_value[2] + "</td>  </tr> </table>";
      }).then(function vote() {
        var body = { "resourceType": "Patient", "identifier": [ { "use": "usual", "system": "Old Identification Number", "value": blockchainoutidentifier } ], "name": [ { "use": "usual", "family": [ PatientSecondName ], "given": [ PatientFirstName ], "prefix": [ "Mr." ], "suffix": [ "Mr." ] } ], "gender": PatientGender, "birthDate": PatientBirthDate, "deceasedBoolean": false, "address": [ { "use": "home", "line": [ PatientAddressLineOne, PatientAddressLineTwo ], "city": PatientCity, "state": PatientState, "postalCode": PatientPostalCode, "country": PatientCountry  } ], "active": true };
        console.log(body);
        $.ajax({
            type: "POST",
            url: 'http://localhost:3115/postpatient',
            data: JSON.stringify(body),
            success: function(list) {
                console.log(list);
                $('#response2').html(list); // show the list
            },
            contentType: 'application/json'
          });
      });
      $('#newpost').addClass("hide");
      $("#patient_humanized").html("<p class='black-text'>Please note your Unique ID </p> <blockquote>"+ PatientUserId + "</blockquote> <p class='black-text'>This is required for future access to your data</p>");
    });
    });
  } catch (err) {
    console.log(err);
  }

};

window.getPatient = function(candidate) {
  let PatientInputAddress = web3.eth.accounts[0];
  let patientId = $("#getpatientid").val();
  let patientadhaarnum = $("#getpatientadhaar").val();
  try {
    Voting.deployed().then(function(contractInstance) {contractInstance.loginpatient(PatientInputAddress,patientId,patientadhaarnum).then(function(get_value) {
      console.log("Query Identifier is "+" "+get_value[2]);
      searchnumber = get_value[2];
      requsername = get_value[0];
      reqpassword = get_value[1];
      patientvisitquery = searchnumber;
      console.log("Transfer Query Parameter for Visitcall"+patientvisitquery);
      console.log("Searchnumber is  "+searchnumber);
    }).then(function getvote() {
      $.get('http://localhost:3115/requestpatient/' + searchnumber  , function(list) {
        $('#response').html(list);
        console.log(typeof(list));
         var result = JSON.parse(list);
         console.log(result);
         var firstname = result['entry'][0]['resource']['name'][0]['given'][0];
         var familysname = result['entry'][0]['resource']['name'][0]['family'];
         var patientsgender = result['entry'][0]['resource']['gender'];
         var patientsbirthdate = result['entry'][0]['resource']['birthDate'];
         var patientaddresslineone = result['entry'][0]['resource']['address'][0]['line'][0];
         var patientaddresslinetwo = result['entry'][0]['resource']['address'][0]['line'][1];
         var patientcity = result['entry'][0]['resource']['address'][0]['city'];
         var patientstate = result['entry'][0]['resource']['address'][0]['state'];
         var patientcode = result['entry'][0]['resource']['address'][0]['postalCode'];
         var patientscountry = result['entry'][0]['resource']['address'][0]['country'];
        var data = "<table> <tr>    <td>Given Name</td>    <td>" + firstname + "</td>  </tr>         <tr>    <td>Family Name</td>    <td>" + familysname + "</td>  </tr>         <tr>    <td>Gender</td>    <td>" + patientsgender + "</td>  </tr>         <tr>    <td>Birth Date</td>    <td>" + patientsbirthdate + "</td>  </tr>         <tr>    <td>Address Line 1</td>    <td>" + patientaddresslineone + "</td>  </tr>         <tr>    <td>Address Line 2</td>    <td>" + patientaddresslinetwo + "</td> </tr>         <tr>    <td>City</td>    <td>" + patientcity + "</td>  </tr>         <tr>    <td>State</td>    <td>" + patientstate + "</td>  </tr>        <tr>    <td>Postal Code</td>    <td>" + patientcode + "</td>  </tr>        <tr>    <td>Country</td>    <td>" + patientscountry + "</td>  </tr>  </table>";
        $("#display").html(data);
    });
    });
    });
  } catch (err) {
    console.log(err);
  }
};




window.doctorsignup = function(candidate) {
  let DoctorFirstName = $("#doctor_fname").val();
  let DoctorSecondName = $("#doctor_sname").val();
  let DoctorGender = $("#doctor_gender").val();
  let DoctorBirthDate = $("#doctor_age").val();
  let DoctorAddressLineOne = $("#doctor_addresslineone").val();
  let DoctorAddressLineTwo = $("#doctor_addresslinetwo").val();
  let DoctorCity = $("#doctor_city").val();
  let DoctorState = $("#doctor_state").val();
  let DoctorPostalCode = $("#doctor_postalcode").val();
  let DoctorCountry = $("#doctor_country").val();
  let DoctorDepartment = $("#doctor_department").val();
  let DoctorHospital = $("#doctor_hospital").val();
  let DoctorAdhaar = $("#doctor_adhaar").val();
  let DoctorUID = uuidv4();
  var shauid = web3.sha3(DoctorUID);
  var DoctorUserId = shauid.toString();
  var a = parseFloat(DoctorAdhaar);
  console.log("Adhaar"+" "+a);
  var b = timeStampInMs;
  console.log("Timestamp"+" "+b)
  var c = a+b;
  console.log("Total"+" "+c);
  let d = shasum.update(c);
  let e = shasum.digest("hex");
  console.log(e);
  console.log("blockinputtimeplusuuid"+" "+e);
  let DoctorAddress = web3.eth.accounts[0];
  $("#loader").show();
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.insertDoctor(DoctorAddress,DoctorAdhaar,DoctorUserId,e,DoctorDepartment,DoctorHospital,{gas: 980806, from: web3.eth.accounts[0]}).then(function(return_value) {
        contractInstance.insertDoctor.call(DoctorAddress,DoctorAdhaar,DoctorUserId,e,DoctorDepartment,DoctorHospital,{gas: 980806, from: web3.eth.accounts[0]}).then(function(get_value) {
        blockchainoutidentifier = get_value[2];
        requsername = get_value[0];
        reqpassword = get_value[1];
        console.log("blockout identifier"+" "+blockchainoutidentifier);
        var data = "<table><tr>    <td>Name</td>    <td>" + get_value[0] + "</td>  </tr>  <tr>    <td>Gender</td>    <td>" + get_value[1] + "</td>  </tr>  <tr>    <td>Age</td>    <td>" + get_value[2] + "</td>  </tr> </table>";
      }).then(function vote() {
        var body = { "resourceType": "Patient", "identifier": [ { "use": "usual", "system": "Old Identification Number", "value": blockchainoutidentifier } ], "name": [ { "use": "usual", "family": [ DoctorSecondName ], "given": [ DoctorFirstName ], "prefix": [ "Mr." ], "suffix": [ "Mr." ] } ], "gender": DoctorGender, "birthDate": DoctorBirthDate, "deceasedBoolean": false, "address": [ { "use": "home", "line": [ DoctorAddressLineOne, DoctorAddressLineTwo ], "city": DoctorCity, "state": DoctorState, "postalCode": DoctorPostalCode, "country": DoctorCountry  } ], "active": true };
        console.log(body);
        $.ajax({
            type: "POST",
            url: 'http://localhost:3115/postpatient',
            data: JSON.stringify(body),
            success: function(list) {
                console.log(list);
                $('#response2').html(list); // show the list
            },
            contentType: 'application/json'
          });
      });
      $("#doctor_humanized").html("<p class='black-text'>Please note your Unique ID </p> <blockquote>"+ DoctorUserId + "</blockquote> <p class='black-text'>This is required for future access to your data</p>");
      $("#loader").hide();
      });
    });
  } catch (err) {
    console.log(err);
  }
}




window.getDoctor = function(candidate) {
  let DoctorInputAddress = web3.eth.accounts[0];
  let DoctorInputId = $("#getdoctorid").val();
  let Doctoradhaarnum = $("#getdoctoradhaar").val();
  try {
    Voting.deployed().then(function(contractInstance) {contractInstance.loginDoctor(DoctorInputAddress,DoctorInputId,Doctoradhaarnum).then(function(get_value) {
    searchnumber = get_value[2];
    requsername = get_value[0];
    reqpassword = get_value[1];
    console.log("see getvalue3 "+get_value[3]);
    reqdepartment = web3.toUtf8(get_value[3]);
    console.log("see reqdepartment "+reqdepartment);
    reqhospital = web3.toUtf8(get_value[4]);
    console.log("Searchnumber is  "+searchnumber);
  }).then(function getvote() {
    $.get('http://localhost:3115/requestpatient/' + searchnumber  , function(list) {
      $('#response').html(list);
      console.log(typeof(list));
       var result = JSON.parse(list);
       console.log(result);
       var docfirstname = result['entry'][0]['resource']['name'][0]['given'][0];
       var docfamilysname = result['entry'][0]['resource']['name'][0]['family'];
       var docgender = result['entry'][0]['resource']['gender'];
       var docbirthdate = result['entry'][0]['resource']['birthDate'];
       var docaddresslineone = result['entry'][0]['resource']['address'][0]['line'][0];
       var docaddresslinetwo = result['entry'][0]['resource']['address'][0]['line'][1];
       var doccity = result['entry'][0]['resource']['address'][0]['city'];
       var docstate = result['entry'][0]['resource']['address'][0]['state'];
       var doccode = result['entry'][0]['resource']['address'][0]['postalCode'];
       var doccountry = result['entry'][0]['resource']['address'][0]['country'];
      var data = "<table> <tr>    <td>Given Name</td>    <td>" + docfirstname + "</td>  </tr> <tr>    <td>Family Name</td>    <td>" + docfamilysname + "</td>  </tr> <tr>    <td>Gender</td>    <td>" + docgender + "</td>  </tr> <tr>    <td>Birth Date</td>    <td>" + docbirthdate + "</td>  </tr> <tr>    <td>Address Line 1</td>    <td>" + docaddresslineone + "</td>  </tr> <tr>    <td>Address Line 2</td>    <td>" + docaddresslinetwo + "</td>  </tr> <tr>    <td>City</td>    <td>" + doccity + "</td>  </tr> <tr>   <td>State</td>    <td>" + docstate + "</td>  </tr> <tr>    <td>Postal Code</td>    <td>" + doccode + "</td>  </tr> <tr>    <td>Country</td>    <td>" + doccountry + "</td>  </tr> <tr>    <td>Department</td>    <td>" + reqdepartment + "</td>  </tr> <tr>    <td>Hospital</td>    <td>" + reqhospital + "</td>  </tr>  </table>";
      $("#displaydoctor").html(data);
  });
  });
    });
  } catch (err) {
    console.log(err);
  }
};




window.insertPatientFile = function(candidate) {
  let candidateNameSeven = web3.eth.accounts[0];
  var candidateNameEight = downloadURL;
  $('#secondpost').removeClass("hide");
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.addhashpatient(candidateNameSeven,candidateNameEight,{gas: 980806, from: web3.eth.accounts[0]}).then(function() {
      $('#secondpost').addClass("hide");
      $("#displayinsert").html("<p class='black-text'> Saved </p> <blockquote>");
      });
    });
  } catch (err) {
    console.log(err);
  }
}

window.getPatientFile = function(candidate) {
  let requestaddress = web3.eth.accounts[0];
  let candidateNameNine = $("#patientfileuuid").val();
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.gethashpatient.call(requestaddress,candidateNameNine).then(function(get_url) {
      var url=get_url;
      console.log(url);
          try {
            window.open("http://localhost:8000?id="+url,"_blank");
      } catch (err) {
        console.log(err);
      }
    });
    });
  } catch (err) {
    console.log(err);
  }
}



window.picture = function(candidate) {
  var pic = get_url;
  try {
    document.getElementById('bigpic').src = pic.replace('90x90', '125x125');
  } catch (err) {
    console.log(err);
  }
}



window.transferownership = function(candidate) {
  let selfaddress = web3.eth.accounts[0];
  let secondowneraddress = $("#doctorid").val();
  $('#acesspost').removeClass("hide");
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.secondOwner(selfaddress,secondowneraddress,{gas: 980806, from: web3.eth.accounts[0]}).then(function() {
      $('#acesspost').addClass("hide");
      $("#ownershipstatus").html("Access Granted");
      });
    });
  } catch (err) {
    console.log(err);
  }
}

window.cancelownership = function(candidate) {
  let selfaddress = web3.eth.accounts[0];
  let secondowneraddress = $("#doctorids").val();
  $('#cancelaccesspost').removeClass("hide");
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.cancelsecondowner(selfaddress,secondowneraddress,{gas: 980806, from: web3.eth.accounts[0]}).then(function() {
      $('#cancelaccesspost').addClass("hide");
      $("#ownershipstatuscancelled").html("Access Cancelled");
      });
    });
  } catch (err) {
    console.log(err);
  }
}


window.getPatientDetail = function(candidate) {
  let DoctorInputAddress = web3.eth.accounts[0];
  //var get_value = [" "," ", 0, 0];
  console.log(get_value);
  try {
    Voting.deployed().then(function(contractInstance) {contractInstance.getPatient(DoctorInputAddress).then(function(get_value) {
    var data = "<table><tr>    <td>Name</td>    <td>" + get_value[0] + "</td>  </tr>  <tr>    <td>Gender</td>    <td>" +get_value[1] + "</td>  </tr>  <tr>    <td>Age</td>    <td>" + get_value[2] + "</td>   </tr></table>";
    console.log("Query Identifier is "+" "+get_value[2]);
    searchnumbertwo = get_value[2];
    requsername = get_value[0];
    reqpassword = get_value[1];
    console.log("Searchnumber is  "+searchnumbertwo);
    $("#displaypatientinfo").html("No Patient Details Available");
  }).then(function getvote() {
    $.get('http://localhost:3115/requestpatient/' + searchnumbertwo  , function(list) {
      $('#response').html(list);
      console.log(typeof(list));
       var result = JSON.parse(list);
       console.log(result);
       var firstname = result['entry'][0]['resource']['name'][0]['given'][0];
       var familysname = result['entry'][0]['resource']['name'][0]['family'];
       var patientsgender = result['entry'][0]['resource']['gender'];
       var patientsbirthdate = result['entry'][0]['resource']['birthDate'];
       var patientaddresslineone = result['entry'][0]['resource']['address'][0]['line'][0];
       var patientaddresslinetwo = result['entry'][0]['resource']['address'][0]['line'][1];
       var patientcity = result['entry'][0]['resource']['address'][0]['city'];
       var patientstate = result['entry'][0]['resource']['address'][0]['state'];
       var patientcode = result['entry'][0]['resource']['address'][0]['postalCode'];
       var patientscountry = result['entry'][0]['resource']['address'][0]['country'];
      var data = "<table> <tr>    <td>Given Name</td>    <td>" + firstname + "</td>  </tr>         <tr>    <td>Family Name</td>    <td>" + familysname + "</td>  </tr>         <tr>    <td>Gender</td>    <td>" + patientsgender + "</td>  </tr>         <tr>    <td>Birth Date</td>    <td>" + patientsbirthdate + "</td>  </tr>         <tr>    <td>Address Line 1</td>    <td>" + patientaddresslineone + "</td>  </tr>         <tr>    <td>Address Line 2</td>    <td>" + patientaddresslinetwo + "</td> </tr>         <tr>    <td>City</td>    <td>" + patientcity + "</td>  </tr>         <tr>    <td>State</td>    <td>" + patientstate + "</td>  </tr>        <tr>    <td>Postal Code</td>    <td>" + patientcode + "</td>  </tr>        <tr>    <td>Country</td>    <td>" + patientscountry + "</td>  </tr>  </table>";
      $("#displaypatientinfo").html(data);
  });
  });
      });
  } catch (err) {
    console.log(err);
  }
}

window.getPatientFileDoctor = function(candidate) {
  let requestaddress = web3.eth.accounts[0];
  try {
    Voting.deployed().then(function(contractInstance) {
      contractInstance.getpatienthashdoctor.call(requestaddress).then(function(get_url) {
        var url=get_url;
        console.log(url);
      try {
        window.open("http://localhost:8000?id="+url,"_blank");
      } catch (err) {
        console.log(err);
      }
      // picture();
      // $("#displayurl").html(displayurl);
      });
    });
  } catch (err) {
    console.log(err);
  }
}

///View Recent Visits by Patient
var myresponse = 0;

window.getvisits = function(candidate) {
  console.log("availability of query parameter for visit call"+patientvisitquery);

  try {
    var url = 'http://localhost:3115/requestvisit/' + patientvisitquery
    console.log("the url is" + url);
    $.getJSON(url , function(data){
      var list =data['entry'];
      console.log(list);
      $.each( list , function(i) {
        if (list[i].resource.hasOwnProperty("partOf")) {
          console.log(i);
          $('#displaycollapsible').append('<li><div class="collapsible-header"><i style="color:green;" class="material-icons">local_hospital</i>'+new Date(list[i].resource.period.start)+'</div><div class="collapsible-body"><span>Start:</span><span> '+new Date(list[i].resource.period.start)+'<br>End: '+new Date(list[i].resource.period.end)+'<br>Location: '+list[i].resource.location[0].location.display+'<br>Type: '+list[i].resource.partOf.display+'<br>Doctor: '+list[0].resource.participant[0].individual.display+'</span></div></li>');
        }
        else {
          $('#displaycollapsible').append('<li><div class="collapsible-header"><i style="color:orange;" class="material-icons">local_hospital</i>'+new Date(list[i].resource.period.start)+'</div><div class="collapsible-body"><span>Start: '+new Date(list[i].resource.period.start)+'<br>End: '+new Date(list[i].resource.period.end)+'<br>Location: '+list[i].resource.location[0].location.display+'</span></div></li>');
        }
      });
      })
    } catch (err) {
      console.log(err);
    }
}

///Share doctor addresstogenerate

window.sharedoctoraddress=function(candidate){
  var addresstogenerate = web3.eth.accounts[0];;
 try {
   jQuery(function(){
   	jQuery('#output').qrcode(addresstogenerate);
   })
   $('#writtenaddress').html(addresstogenerate);
 }catch (err) {
   console.log(err);
 }


}








$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  Voting.setProvider(web3.currentProvider);

});
