import { HomeService } from './../../home.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalComponent } from 'src/app/modal/modal.component';
import { ToastrService } from 'ngx-toastr';

import {User} from 'src/app/authentication/services/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

  profileInfo: any;
  demographics: any;
  fileURL: string = "";
  user: any;
  updateUser: any = {};
  loading:boolean = false;
  dialogRef: MatDialogRef<ModalComponent, any>;
  files: any;
  constructor(public dialog: MatDialog, 
    private authService: AuthenticationService, 
    private homeService: HomeService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.loading = true;
    var profileInfo = this.getUserProfile();
    this.user = profileInfo;
    let user = profileInfo ? profileInfo.creds: null;
    const data = {
      ssn: user ? user.ssn : null,
      account: profileInfo? profileInfo.user.account: null,
      user: user ? user.user: null,
      provider: user ? user.emrDetails.number: null,
      id: (profileInfo.clinicalData.body.entry && profileInfo.clinicalData.body.entry.length > 0) ? profileInfo.clinicalData.body.entry[0].resource.id : null
    };
    this.profileInfo = profileInfo;
    if (this.profileInfo && this.profileInfo.clinicalData && this.profileInfo.clinicalData.body.entry[0])
        this.demographics = this.profileInfo.clinicalData.body.entry[0].resource;
    if(user && user.user !== "doctor"){
      this.homeService.getFile(data, user.emrDetails).subscribe(response => {
        this.loading = false;
        if(response && response.data) {
          this.fileURL = response.data;
        }
        else if(response && response.total) {
          this.files = response.entry;
        }
      },
      error => {
        this.loading = false;
        if(this.dialogRef) this.dialogRef.close();
        this.toastr.error("Unable To Get File", "Please try again after sometime.");
        console.log(error);
      });
    }
    else {
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
      
    }
  }

  getUserProfile() {
    console.log("here",this.authService.userProfile);
    return this.authService.userProfile;
  }

  uploadFile(file) {
    this.loading = true;
    this.user.creds.type = "insertfile";
    this.homeService.postFile(file).subscribe(response => {
      console.log(response);
      if(response && response.url) {
        this.user.creds.fileUrl = response.url;
        this.homeService.uploadFile(this.user.creds).subscribe(data => {
          this.loading = false;
          console.log(data);
          if(this.dialogRef) this.dialogRef.close();
          this.toastr.success("Success!!!", "Your file is uploaded succesfully");
        });
      }
    },
    error => {
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
      this.toastr.error("Error!!!", "File upload failed. Please try again after sometime");
    });
  }

  openModal(data) {
    this.dialogRef = this.dialog.open(ModalComponent, { data });
    const onFileUpload = this.dialogRef.componentInstance.onFileUpload.subscribe(file => {
      console.log(file);
      this.uploadFile(file);
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      onFileUpload.unsubscribe();
    });
  }

  downloadFile(resource) : void {
    if(resource) {
      console.log("emr",resource);
      console.log("emr2",resource.series[0].endpoint[0].reference);
      //window.open(resource.endpoint[0].reference,"_blank");
      window.open(`http://192.168.2.27:8082?input=`+encodeURIComponent(resource.series[0].endpoint[0].reference),"_blank");
    }
    else {
      console.log(this.fileURL);
    //   this.loading = true;
    //   const data = {
    //    ssn : this.user.creds.ssn,
    //    account: this.user.user.account,
    //    user: this.user.creds.user,
    //    provider:this.user.creds.emrDetails.number,
    //    id: profileInfo.clinicalData.body.entry[0].resource.id
    //  };
    //  this.homeService.getFile(data,this.user.creds.emrDetails).subscribe(response => {
    //    console.log(response);
    //    //window.open(`${DCOM_URL}?input=`+encodeURIComponent(response.data),"_blank");
    //    this.loading = false;
    //  },
    //  error => {
    //    this.loading = false;
    //    this.toastr.error("Error!!!", "Downloading file failed. Please try again after sometime");
    //  });
    }
    
  }

  updateProfile() {
    this.loading = true;
    console.log("calling submitting edited profile function");
    //this.updateUser.firstName = this.demographics.name[0].given[0].
    // console.log("patientResourceData",this.user.clinicalData.body.entry[0].resource);
    // console.log("userCreds",this.user);
    this.updateUser.patientResourceData = this.user.clinicalData.body.entry[0].resource;
    this.updateUser.userCredentials = this.user.creds;
    console.log("user to update",this.updateUser);
    this.homeService.updateProfile(this.updateUser).subscribe(response => {
      if(response.message == "success") {
        this.loading = false;
        this.toastr.success("Success!!!", "Your profile has been updated succesfully");
      }

    },
    error => {
    this.loading = false;
    this.toastr.error("Error!!!", "Updating Patient Profile failed. Please try again after sometime");
    });

  }

}
