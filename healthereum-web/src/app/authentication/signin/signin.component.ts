import { AuthenticationService } from './../services/authentication.service';
import { User, Provider } from './../services/user';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  user: User = <User>new Object();
  otp: number = null;
  showOtpform: boolean = false;
  providers: Array<Provider>;
  provider: Provider;
  loading:boolean = false;
  constructor(private router: Router,
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private toastr: ToastrService) {

  }

  ngOnInit() {
  }


  generateOtp(): void {
    this.user.isRegistered = true;
    console.log(this.user);
    this.loading = true;
    this.authService.generateOTP(this.user)
      .subscribe(response => {
        if (response) {
          this.authService.getRegisteredProviders(this.user)
            .subscribe(providers => {
              if (providers) {
                this.provider = providers[0];
                this.providers = providers;
                console.log(this.providers);
                this.showOtpform = true;
              }
              this.loading = false;
              this.toastr.success("Success!!!", "OTP generated successfully");
            })
        }
        else {
          this.loading = false;
        }
      },
        error => {
          this.loading = false;
          console.error('Oops:', error);
          this.toastr.error("OTP generation failed", "Please make sure you have entered all the data correctly");
        });
  }


  setuserProfile(value: any) {
    if (value && value.clinicalData && value.clinicalData.body) {
      value.clinicalData.body = JSON.parse(value.clinicalData.body);
    }
    value.creds = this.user;
    this.authService.userProfile = value;
  }


  signin(): void {
    this.loading = true;
    this.user.otp = this.otp;
    this.user.emrDetails = { hospitalName: this.provider.providerName, number: this.provider.providerNumber };
    this.authService.login(this.user)
      .subscribe(response => {
        console.log(response);
        this.loading = false;
        this.setuserProfile(response);
        this.toastr.success("Success!!!", "You are successfully logged in.");
        this.router.navigate(['/home']);
      },
        error => {
          this.loading = false;
          this.toastr.error("Login failed!!!", "Please make sure you have entered all details correctly.");
          console.error('Oops:', error);
        });
  }

}
