# README #

This repository contains Healtherium demo app, both web and mobile app with dicom server to view dicom images and a node server to communicate with Open MRS.

# STEPS TO START THE PROJECT #
STEP 1

- Step a -Install MySQL server in your machine

- Step b - OpenMRS
    - Install openMRS server. Refer to (https://github.com/openmrs/openmrs-core)
    - Run openMRS as mentioned in the above link
    - The openMRS server starts in port 8080 in your machine

- Step c- Run node server
    - Go to ionic-nodeserver/ folder 
    - Run npm install
    - Add mysql username and password in app.js 
    - Run command node app.js
    - The node server runs in port 4445

- Step d - Start healtherium web app (Old version)
    - Go to Healtherium folder
    - Run npm install
    - Run npm run dev
    - The browser window opens in port 8081
    NOTE : For the old web application to work make sure to perform the above Step c in folder Nodeserver/ folder

- Step e - Dicomserver
    - Go to Dicomserver folder 
    - Run python -m SimpleHTTPServer 8000 in terminal
Open Dicomserver folder and run python -m SimpleHTTPServer in command line to have your dicom running in port 8000.
NOTE : Enable CORS for Dicomserver session in browser.

- Step f - ionic-application
    - Go to ionic-healtherium-app/
    - Run ionic serve in terminal
    - The ionic application runs on port 8100 in the browser

BUILD THE APPLICATION FOR MOBILE:
    - Go to ionic-healtherium-app/
    - Comment - <!-- <base href="/"> -->
    - Uncomment - <base href="file:///android_asset/www/" />
    - Change the port address in healtherium-api.ts
    - Change the port address in patient-dashboard.ts and doctor-dashboard.ts
    - Run ionic cordova build android 
    - Run ionic cordova run android

