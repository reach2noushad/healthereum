import { ActivityLogComponent } from './dashboard/activity-log/activity-log.component';
import { AuthGuard } from './../authentication/auth.guard';
import { DelegateComponent } from './dashboard/delegate/delegate.component';
import { PatientComponent } from './dashboard/patient/patient.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DoctorComponent } from './dashboard/doctor/doctor.component';
import { DetailsComponent } from './dashboard/details/details.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { HomeComponent } from './home.component';
// import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    { path: 'home', component: HomeComponent,
    children: [
      {path: '', component: ProfileComponent},
      {path: 'details', component: DetailsComponent},
      {path: 'delegate', component: DelegateComponent},
      {path: 'patients', component: PatientComponent},
      {path: 'doctors', component: DoctorComponent},
      {path: 'activitylog', component: ActivityLogComponent}
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



export class HomeRoutingModule { }
