var keythereum = require("keythereum");
var request = require('request');
var web3 = require('web3');
var express = require('express');
var mkdirp = require('mkdirp');
var cors = require('cors');
var os = require('os');
const FS = require('fs');
var mysql = require('mysql');
const PATH = require('path');
var ethers = require('ethers');
var bodyParser = require('body-parser');
var app = express();
app.use(cors());
app.use(bodyParser.json());

// Initialisations

var provider = ethers.providers.getDefaultProvider('rinkeby');
var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
var abi = [{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"hash","type":"string"}],"name":"addhashpatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"firstowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"cancelsecondowner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"string"},{"name":"department","type":"bytes32"},{"name":"hospital","type":"bytes32"}],"name":"insertDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"string"}],"name":"insertPatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"presentowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"secondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"}],"name":"gethashpatient","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getPatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getpatienthashdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"bytes32"},{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginpatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];

var prefix = os.homedir();
var datadir= prefix+'/mobile_wallet/doctor/keystore';

//connection properties of mysql

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "wafi@2017",
  database: "mydb"
});

con.connect(function(err) {
      if (err) throw err;
      console.log("Connected!");
      });

app.post('/createaccount', function (req, res) {
  data = req.body;
  console.log(data.InputPassword);
  console.log(data.InputAdhaar);

  //Creating keystore
  var params = { keyBytes: 32, ivBytes: 16 };
  var dk = keythereum.create(params);
  var password = data.InputPassword;
  var kdf = "pbkdf2";
  var options = {kdf: "pbkdf2",cipher: "aes-128-ctr",kdfparams: {c: 262144,dklen: 32,prf: "hmac-sha256"}};
  var keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);

  console.log(datadir);

  async function myfirstfunction () {

    try {

     mkdirp(datadir, function (err) {
        if (err) console.error(err)
        else console.log('Directory Created');
        adhaar = data.InputAdhaar;
        mysqladdress = keyObject.address;
        var sql = "INSERT INTO users(Adhaar, Account) VALUES ?";
        var values = [[adhaar,mysqladdress]];

       con.query(sql,[values], function (err, result) {
          if (err){
             console.log (err);
             res.send('User Already Exist');
            }else {
            console.log("Successfully Inserted");
            keythereum.exportToFile(keyObject,datadir);
            console.log("KeyStore File Created");
            console.log(keyObject.address);
            var myaddress = "0x"+keyObject.address;
            //Private Key of Parent Account to Load Ethers
            var privateKey = '0x7dcd3fcd70f6df14b46749ae0fd491048d9c0743c3e20f59bb0363554a02caaf';
            var wallet = new ethers.Wallet(privateKey);
            wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
            //Amount of Ethers To Be Loaded(0.5 in this case)
            var amount = ethers.utils.parseEther('0.5');
            var sendPromise = wallet.send(myaddress, amount);
            sendPromise.then(function(transaction){
                  console.log(transaction);
                  provider.waitForTransaction(transaction.hash).then(function(transaction){
                      console.log("Transaction Mined: "+transaction.hash)
                      console.log(myaddress+" loaded with 0.5 Ethers");
                      var privateKey = keythereum.recover(data.InputPassword, keyObject);
                      console.log(privateKey.toString('hex'));

                      if(data.user=="Doctor"){
                        var addedprivatekey = privateKey.toString('hex');
                        var doctorprivateKey = "0x"+addedprivatekey;
                        var wallet = new ethers.Wallet(doctorprivateKey, provider);
                        var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                        var contract = new ethers.Contract(address, abi, wallet);
                        var createdoctor = contract.insertDoctor(myaddress,web3.utils.fromAscii(data.InputAdhaar),web3.utils.fromAscii(data.UserID),data.InputOutputIdentifier,web3.utils.fromAscii(data.Department),web3.utils.fromAscii(data.Hospital));
                        createdoctor.then(function(transaction){
                          console.log(transaction);
                          provider.waitForTransaction(transaction.hash).then(function(transaction){
                            console.log("Transaction Mined: "+transaction.hash);
                            var openmrsbody = { "resourceType": "Patient", "identifier": [ { "use": "usual", "system": "Old Identification Number", "value": data.InputOutputIdentifier} ], "name": [ { "use": "usual", "family": [ data.SecondName ], "given": [ data.FirstName ], "prefix": [ "Mr." ], "suffix": [ "Mr." ] } ], "gender": data.Gender, "birthDate": data.birthDate, "deceasedBoolean": false, "address": [ { "use": "home", "line": [ data.AddressLineOne, data.AddressLineTwo ], "city": data.City, "state": data.State, "postalCode": data.PostalCode, "country": data.Country  } ], "active": true };
                            var postPatient = {
                                  method: 'POST',
                                  url: 'http://localhost:8080/openmrs/ws/fhir/Patient/',
                                  headers: {
                                      'Cache-Control': 'no-cache',
                                      Authorization: 'Basic YWRtaW46QWRtaW4xMjM=',
                                      'Content-Type': 'application/json'
                                  },
                                  body: openmrsbody,
                                  json: true
                              };
                              request(postPatient, function(error, response, body) {
                                  if (error) throw new Error(error);
                                  console.log(body);
                                  res.send("Success");
                              });
                          });
                        })
                      } else if (data.user=="Patient"){
                        var addedprivatekey = privateKey.toString('hex');
                        var patientprivateKey = "0x"+addedprivatekey;
                        var provider = ethers.providers.getDefaultProvider('rinkeby');
                        var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                        var wallet = new ethers.Wallet(patientprivateKey, provider);
                        var contract = new ethers.Contract(address, abi, wallet);
                        var createpatient = contract.insertPatient(myaddress,web3.utils.fromAscii(data.InputAdhaar),web3.utils.fromAscii(data.UserID),data.InputOutputIdentifier);
                        createpatient.then(function(transaction){
                          console.log(transaction);
                          provider.waitForTransaction(transaction.hash).then(function(transaction){
                            console.log("Transaction Mined: "+transaction.hash);
                            var openmrsbody = { "resourceType": "Patient", "identifier": [ { "use": "usual", "system": "Old Identification Number", "value": data.InputOutputIdentifier} ], "name": [ { "use": "usual", "family": [ data.SecondName ], "given": [ data.FirstName ], "prefix": [ "Mr." ], "suffix": [ "Mr." ] } ], "gender": data.Gender, "birthDate": data.birthDate, "deceasedBoolean": false, "address": [ { "use": "home", "line": [ data.AddressLineOne, data.AddressLineTwo ], "city": data.City, "state": data.State, "postalCode": data.PostalCode, "country": data.Country  } ], "active": true };
                            var postPatient = {
                                  method: 'POST',
                                  url: 'http://localhost:8080/openmrs/ws/fhir/Patient/',
                                  headers: {
                                      'Cache-Control': 'no-cache',
                                      Authorization: 'Basic YWRtaW46QWRtaW4xMjM=',
                                      'Content-Type': 'application/json'
                                  },
                                  body: openmrsbody,
                                  json: true
                              };
                              request(postPatient, function(error, response, body) {
                                  if (error) throw new Error(error);
                                  console.log(body);
                                  res.send("Success");
                              });
                           });
                        });
                      }
                    });
                 })
               }
            });
         });
       }catch(err) {
      console.log(err);
       }
}

  myfirstfunction();

});


app.post('/getaccount', function (req, res) {
  data = req.body;
  console.log(data);
  console.log(data.InputPassword);
  var passpassword = data.InputPassword;
  console.log(data.InputAdhaar);
  var adr = data.InputAdhaar;
  var sql = "SELECT * FROM users where Adhaar = "+ mysql.escape(adr);
  con.query(sql, function (err, result) {
    if (err){
      console.log(err);
    }else{
    console.log(result);
    var addressfromdb=result[0].Account;
    console.log(addressfromdb);
    FS.readdir(datadir,function(err,files){
      if(err) throw err;
      for (var index in files) {
         console.log(files[index]);
         var fileaddress=files[index].split('--').pop();
         if (fileaddress==addressfromdb) {
           console.log("address found");
           console.log(datadir);
           console.log(files[index]);
           var mypath = datadir+'/'+files[index];
           console.log(mypath);
             FS.readFile(mypath, (err, mydata) => {
               if (err) throw err;
               console.log(mydata);
               var myfile = JSON.parse(mydata);
               console.log(myfile);
               var myaddress = myfile.address;
               console.log(address);
               var requiredaddress = "0x"+myaddress;
               console.log(requiredaddress);
               console.log(data);
                  if(data.user=='Doctor'){
                    console.log('reached doctor');
                    if(data.type=='doctordata'){
                      var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                      var contract = new ethers.Contract(address, abi, provider);
                      var callPromise = contract.loginDoctor(requiredaddress,web3.utils.fromAscii(data.UserID),web3.utils.fromAscii(data.InputAdhaar));
                      callPromise.then(function(result){
                        console.log(result);
                        var searchnumber = result[2];
                        reqdepartment = web3.utils.toUtf8(result[3]);
                        reqhospital = web3.utils.toUtf8(result[4]);
                                   var getPatient = {
                                        method: 'GET',
                                        url: 'http://localhost:8080/openmrs/ws/fhir/Patient',
                                        qs: {
                                            identifier: searchnumber,
                                        },
                                        headers: {
                                            'Cache-Control': 'no-cache',
                                            Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
                                        }
                                    };
                                      request(getPatient, function(error, response, body) {
                                          if (error) throw new Error(error);
                                          console.log(body);
                                          modifiedbody = {"openmrsbody":body,"department":reqdepartment,"hospital":reqhospital};
                                          res.send(modifiedbody);
                                      });

                                    });
                      }else if(data.type=='shareaddress') {
                        console.log(requiredaddress);
                        res.send(requiredaddress);
                      }else if(data.type=='getpatientfromdoctor'){
                        console.log("getpatient");
                        var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                        var contract = new ethers.Contract(address, abi, provider);
                        var callPromise = contract.getPatient(requiredaddress);
                        callPromise.then(function(result){
                          console.log(result);
                          var searchnumber = result[2];
                          if(searchnumber==''){
                            res.send('Null');
                          }else{
                          var getPatient = {
                               method: 'GET',
                               url: 'http://localhost:8080/openmrs/ws/fhir/Patient',
                               qs: {
                                   identifier: searchnumber,
                               },
                               headers: {
                                   'Cache-Control': 'no-cache',
                                   Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
                               }
                           };
                             request(getPatient, function(error, response, body) {
                                 if (error) throw new Error(error);
                                 console.log(body);
                                 res.send(body);
                             });
                           }
                        });

                      }else if (data.type='getfilefromdoctor'){
                        console.log('reached');
                        var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                        var contract = new ethers.Contract(address, abi, provider);
                        var getfilefromdoctor = contract.getpatienthashdoctor(requiredaddress);
                        getfilefromdoctor.then(function(result){
                          res.send(result);
                        });
                      }
                }else if (data.user=='Patient'){
                    var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                    var contract = new ethers.Contract(address, abi, provider);
                    var callPromise = contract.loginpatient(requiredaddress,web3.utils.fromAscii(data.UserID),web3.utils.fromAscii(data.InputAdhaar));
                    callPromise.then(function(result){
                      console.log(result);
                      var searchnumber = result[2];

                          if(data.type=='patientdata'){
                          var getPatient = {
                              method: 'GET',
                              url: 'http://localhost:8080/openmrs/ws/fhir/Patient',
                              qs: {
                                  identifier: searchnumber,
                              },
                              headers: {
                                  'Cache-Control': 'no-cache',
                                  Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
                              }
                          };
                            request(getPatient, function(error, response, body) {
                                if (error) throw new Error(error);
                                console.log(body);
                                res.send(body);
                            });
                          }else if(data.type=='patientvisits'){
                            console.log("checkingpatientvisit");
                            var getVisit = {
                                    method: 'GET',
                                    url: 'http://localhost:8080/openmrs/ws/fhir/Encounter',
                                    qs: {
                                        identifier: searchnumber,
                                    },
                                    headers: {
                                        'Cache-Control': 'no-cache',
                                        Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
                                    }
                                };
                                  request(getVisit, function(error, response, body) {
                                      if (error) throw new Error(error);
                                      console.log(body);
                                      res.send(body);
                                  });
                                }
                             });
                    }else if(data.user='getpatientfile'){
                      var address = '0xbf402e1ba2a36b3200bc503300037b8ebc604a1b';
                      var contract = new ethers.Contract(address, abi, provider);
                      var getpatientfile = contract.gethashpatient(requiredaddress,web3.utils.fromAscii(data.UserID));
                      getpatientfile.then(function(result){
                        console.log(result);
                        res.send(result);
                      });
                     }
             });
         }else{
           console.log("No Matching");
         }
       }
    })
   }
  });
});

app.post('/ownership', function (req, res) {

  data = req.body;
  console.log(data);
  var passpassword = data.InputPassword;
  var adr = data.InputAdhaar;
  var sql = "SELECT * FROM users where Adhaar = "+ mysql.escape(adr);

async function ownership(){

  try {
  con.query(sql, function (err, result) {
    if (err){
      console.log(err);
    }else{
    console.log(result);
    var addressfromdb=result[0].Account;
    console.log(addressfromdb);
    FS.readdir(datadir,function(err,files){
      if(err) throw err;
      for (var index in files) {
         console.log(files[index]);
         var fileaddress=files[index].split('--').pop();
         if (fileaddress==addressfromdb) {
           console.log("address found");
           console.log(datadir);
           console.log(files[index]);
           var mypath = datadir+'/'+files[index];
           console.log(mypath);
             FS.readFile(mypath, (err, mydata) => {
               if (err) throw err;
               console.log(mydata);
               var myfile = JSON.parse(mydata);
               console.log(myfile);
               var myaddress = myfile.address;
               var requiredaddress = "0x"+myaddress;
               console.log(requiredaddress);
               console.log(datadir);
               var str = datadir;
               var respo = str.replace("/keystore", "");
               console.log(respo);

               var keyObject = keythereum.importFromFile(myaddress, respo);
               console.log(keyObject);
               console.log(passpassword);
               var privateKey = keythereum.recover(passpassword, keyObject);
               console.log(privateKey.toString('hex'));
               var addedprivatekey = privateKey.toString('hex');
               var patientprivateKey = "0x"+addedprivatekey;
               console.log(patientprivateKey);
               var wallet = new ethers.Wallet(patientprivateKey, provider);
               var contract = new ethers.Contract(address, abi, wallet);
                  if(data.type=="grant"){
                    var transferowner = contract.secondOwner(myaddress,data.SecondAddress);
                    transferowner.then(function(transaction){
                      console.log(transaction);
                      provider.waitForTransaction(transaction.hash).then(function(transaction){
                        console.log("Transaction Mined: "+transaction.hash);
                        res.send("Success");
                      });
                    });
                  }else if (data.type=="cancel"){
                    var cancelowner = contract.cancelsecondowner(myaddress,data.SecondAddress);
                    cancelowner.then(function(transaction){
                      console.log(transaction);
                      provider.waitForTransaction(transaction.hash).then(function(transaction){
                        console.log("Transaction Mined: "+transaction.hash);
                        res.send("Success");
                      });
                    });
                  }else if (data.type=='insertfile'){
                    var insertfile = contract.addhashpatient(myaddress,data.FileUrl);
                    insertfile.then(function(transaction){
                      console.log(transaction);
                      provider.waitForTransaction(transaction.hash).then(function(transaction){
                        console.log("Transaction Mined: "+transaction.hash);
                        res.send("Success");
                      });
                    });

                  }
              });
            }
          }
       });
     }
   });

 }catch(err){
   console.log(err);
 }
}
ownership();
});
















// //Initialisations
//
// var provider = ethers.providers.getDefaultProvider('rinkeby');
//
// //function to readfile
// function readFiles(dirPath, processFile) {
//   FS.readdir(dirPath, (err, fileNames) => {
//     if (err) throw err;
//     fileNames.forEach((fileName) => {
//       let name = PATH.parse(fileName).name;
//       let ext = PATH.parse(fileName).ext;
//       let path = PATH.resolve(dirPath, fileName);
//       processFile(name, ext, path);
//     });
//   });
// }
//
//
//
// //To create account for doctor
// app.get('/requestaccountdoctor/:identifier', function(req, res) {
//   console.log("Availability of inputpassword in server side "+req.params.identifier);
//   var params = { keyBytes: 32, ivBytes: 16 };
//   var dk = keythereum.create(params);
//   var password = req.params.identifier;
//   var kdf = "pbkdf2"; // or "scrypt" to use the scrypt kdf
//   var options = {kdf: "pbkdf2",cipher: "aes-128-ctr",kdfparams: {c: 262144,dklen: 32,prf: "hmac-sha256"}};
//   var keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
//
//   var datadir = '/home/mohamednoushad/mobile_wallet/doctor/keystore';
//
//   mkdirp(datadir, function (err) {
//     if (err) console.error(err)
//     else console.log('Directory Created');
//     FS.readdir(datadir, (err, fileNames) => {
//     if (err) throw err;
//     var i=0;
//     fileNames.forEach((fileName) => {
//       console.log(fileName);
//       i++;
//     });
//     console.log(i);
//     if (i==0) {
//       keythereum.exportToFile(keyObject,datadir);
//       res.send("0x"+keyObject.address);
//     } else {
//       res.send("File already exists");
//       console.log("File already exists");
//     }
//   });
//    });
// });
//
// //To load ethers into the incoming address
// app.get('/loadethers/:identifier',function(req,res){
//   console.log("Availability of Account to Load Ethers: "+req.params.identifier);
//
//   //Private Key of Parent Account to Load Ethers
//   var privateKey = '0x7dcd3fcd70f6df14b46749ae0fd491048d9c0743c3e20f59bb0363554a02caaf';
//   var wallet = new ethers.Wallet(privateKey);
//   wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
//
//   //Amount of Ethers To Be Loaded(0.5 in this case)
//     var amount = ethers.utils.parseEther('0.5');
//     var sendPromise = wallet.send(req.params.identifier, amount);
//     sendPromise.then(function(transaction){
//       console.log(transaction);
//       provider.waitForTransaction(transaction.hash).then(function(transaction){
//           console.log("Transaction Mined: "+transaction.hash)
//           res.send(req.params.identifier);
//         });
//     })
// });
//
// //To get Privatekey of doctor account
// app.get('/requestprivatekeydoctor/:identifier', function(req, res) {
//   console.log("Availability of inputpassword in server side "+req.params.identifier);
//
//   var datadir = "/home/mohamednoushad/mobile_wallet/doctor";
//
//   readFiles(datadir+'/keystore', (name, ext, path) => {
//   var mypath = path;
//   console.log(mypath);
//   FS.readFile(mypath, (err, data) => {
//     if (err) throw err;
//     console.log(data);
//     var myfile = JSON.parse(data);
//     var address = myfile.address;
//     console.log(address);
//     var keyObject = keythereum.importFromFile(address, datadir);
//     var password = req.params.identifier;
//     var privateKey = keythereum.recover(password, keyObject);
//     console.log(privateKey.toString('hex'));
//     res.send(privateKey.toString('hex'));
//   });
// });
// });
//
// app.get('/requestaddressdoctor/:identifier', function(req, res) {
//   console.log("Availability of inputpassword in server side "+req.params.identifier);
//
//   var datadir = "/home/mohamednoushad/mobile_wallet/doctor";
//
//   readFiles(datadir+'/keystore', (name, ext, path) => {
//   var mypath = path;
//   console.log(mypath);
//   FS.readFile(mypath, (err, data) => {
//     if (err) throw err;
//     console.log(data);
//     var myfile = JSON.parse(data);
//     var address = myfile.address;
//     var keyObject = keythereum.importFromFile(address, datadir);
//     try{
//     var password = req.params.identifier;
//     var privateKey = keythereum.recover(password, keyObject);
//     console.log(address);
//     res.send(address);
//   }catch(error){
//     console.log(error);
//     res.send("Wrong Password!")
//   }
//
//   });
// });
// });
//
// app.get('/requestaccountpatient/:identifier', function(req, res) {
//   console.log("Availability of inputpassword in server side "+req.params.identifier);
//
//   var params = { keyBytes: 32, ivBytes: 16 };
//   var dk = keythereum.create(params);
//   var password = req.params.identifier;
//   var kdf = "pbkdf2"; // or "scrypt" to use the scrypt kdf
//   var options = {kdf: "pbkdf2",cipher: "aes-128-ctr",kdfparams: {c: 262144,dklen: 32,prf: "hmac-sha256"}};
//   var keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
//
//   var datadir = '/home/mohamednoushad/mobile_wallet/patient/keystore';
//
//   mkdirp(datadir, function (err) {
//     if (err) console.error(err)
//     else console.log('Directory Created');
//     FS.readdir(datadir, (err, fileNames) => {
//     if (err) throw err;
//     var i=0;
//     fileNames.forEach((fileName) => {
//       console.log(fileName);
//       i++;
//       });
//     console.log(i);
//     if (i==0) {
//       keythereum.exportToFile(keyObject,datadir);
//       res.send("0x"+keyObject.address);
//     } else {
//       res.send("File already exists");
//       console.log("Key Store File already exist");
//         }
//      });
//    });
// });
//
// app.get('/requestprivatekeypatient/:identifier', function(req, res) {
//   console.log("Availability of inputpassword in server side "+req.params.identifier);
//
//   var datadir = "/home/mohamednoushad/mobile_wallet/patient";
//
//   readFiles(datadir+'/keystore', (name, ext, path) => {
//   var mypath = path;
//   console.log(mypath);
//   FS.readFile(mypath, (err, data) => {
//     if (err) throw err;
//     console.log(data);
//     var myfile = JSON.parse(data);
//     var address = myfile.address;
//     console.log(address);
//     var keyObject = keythereum.importFromFile(address, datadir);
//     var password = req.params.identifier;
//     var privateKey = keythereum.recover(password, keyObject);
//     console.log(privateKey.toString('hex'));
//     res.send(privateKey.toString('hex'));
//   });
// });
// });
//
// app.get('/requestaddresspatient/:identifier', function(req, res) {
//   console.log("Availability of inputpassword in server side "+req.params.identifier);
//
//   var datadir = "/home/mohamednoushad/mobile_wallet/patient";
//
//   readFiles(datadir+'/keystore', (name, ext, path) => {
//   var mypath = path;
//   console.log(mypath);
//   FS.readFile(mypath, (err, data) => {
//     if (err) throw err;
//     console.log(data);
//     var myfile = JSON.parse(data);
//     var address = myfile.address;
//     var keyObject = keythereum.importFromFile(address, datadir);
//     try{
//     var password = req.params.identifier;
//     var privateKey = keythereum.recover(password, keyObject);
//     console.log(address);
//     res.send(address);
//   }catch(error){
//     console.log(error);
//     res.send("Wrong Password!")
//   }
//   });
// });
// });
//
//
//
app.listen(4445);
