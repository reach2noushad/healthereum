import { HealtheriumApi } from './../../providers/healtherium-api/healtherium-api';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Platform ,AlertController} from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import {DCOM_URL} from '../../config';
// import { File } from '@ionic-native/file';
// import { FileChooser } from '@ionic-native/file-chooser';

@Component({
  selector: 'page-patient-dashboard',
  templateUrl: 'patient-dashboard.html',
})
export class PatientDashboardPage {
  visitList: Array<any> = [];
  visited: boolean;
  fileToUpload: any;
  loader;
  visitsInfoFetched: boolean;
  clinicalData: any = {};
  scannedAddress: string = "";
  addressToRevoke: string = "";
  isApp: boolean = true;
  observationDetails: any;
  //shownGroup: any;
  showDetails: boolean;
  credentials: any;
  selectedEmr: any;
  user: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private qrScanner: QRScanner,
    public api: HealtheriumApi,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public platform: Platform) {

    if (this.platform.is('core') || this.platform.is('mobileweb')) {
      this.isApp = false;
    }
    else {
      this.isApp = true;
    }

    console.log(this.navParams.data);
    this.credentials = this.navParams.data.credentials;
    this.user = this.navParams.data.response.user;
    this.selectedEmr = this.credentials.emrDetails;
    if (this.navParams.data && this.navParams.data.response && this.navParams.data.response.clinicalData.body) {
      this.clinicalData.emrData = JSON.parse(this.navParams.data.response.clinicalData.body).entry[0].resource;
    }
    console.log(this.clinicalData.emrData)
  }

  ionViewDidLoad() {
    this.showDetails = false;
    console.log('ionViewDidLoad PatientDashboardPage');
  }

  scanQRCode(grant): void {
    console.log("hello I am here");
    console.log(this.qrScanner);
    var ionApp = <HTMLElement>document.getElementsByTagName("ion-app")[0];
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            if (grant) {
              this.addressToRevoke = "";
              this.scannedAddress = text;
            }
            else {
              this.scannedAddress = "";
              this.addressToRevoke = text;
            }
            this.qrScanner.hide(); // hide camera preview
            ionApp.style.display = "block";
            scanSub.unsubscribe(); // stop scanning
          });
          // show camera preview
          ionApp.style.display = "none";
          this.qrScanner.show();

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => {
        console.log('Error is', e);
        if (grant) {
          this.scannedAddress = "";
          this.addressToRevoke = "";
        }
        else {
          this.scannedAddress = "";
          this.addressToRevoke = "";
        }

      });
  }

  manageAccess(grant): void {
    let user = this.navParams.data.credentials;
    if (grant) {
      user.type = true;
      user.toAddress = this.scannedAddress;
    }
    else {
      user.type = false;
      user.toAddress = this.addressToRevoke;
    }
    this.createLoader();
    this.api.manageAccess(user).subscribe(response => {
      this.loader.dismiss();
      this.scannedAddress = "";
      this.addressToRevoke = "";
      console.log("address sharing/cancellation success");
      console.log(response);
      let info = {
        title: "Success",
        subTitle:response.message
        //subTitle: "Oops. Something went wrong. Please try again after some time"
      }
      this.presentAlert(info);
    },
    error => {
      this.loader.dismiss();
      console.error('Oops:', error);
      let info = {
        title: "Error",
        subTitle:error.error.message
        //subTitle: "Oops. Something went wrong. Please try again after some time"
      }
      this.presentAlert(info);
    });
  }

  getObservations(): void {
    let self = this;
    this.showDetails = true
    this.observationDetails = null;
    this.api.getObservations(this.clinicalData.emrData.id, this.selectedEmr.number)
      .subscribe(
        response => {
          console.log("response", response);
          self.observationDetails = response.entry;
          //self.toggleGroup(index);
        },
      error => {

      });
  }

  handleFileInput(files: FileList) : void {
    this.fileToUpload = files.item(0);
  }

  uploadFile() : void {
    let patientDetails = this.navParams.data.credentials;
    patientDetails.type = "insertfile";
    this.createLoader();
    this.api.postFile(this.fileToUpload).subscribe(response => {
      console.log(response);
      if(response && response.url) {
        patientDetails.fileUrl = response.url;
        this.api.uploadFile(patientDetails).subscribe(data => {
          this.loader.dismiss();
          console.log(data);
          let info = {
            title: "Success",
            subTitle:data.message
          }
          this.presentAlert(info);
        });
      }
    },
    error => {
      this.loader.dismiss();
      console.error('Oops:', error);
      let info = {
        title: "Error",
        subTitle:error.error.message
        //subTitle: "Oops. Something went wrong. Please try again after some time"
      }
      this.presentAlert(info);
    });
  }

  getFile(): void {
    console.log("hello");
    let patientDetails = this.navParams.data.credentials;
     const data = {
      ssn : this.credentials.ssn,
      account: this.user.account,
      user: this.credentials.user,
      provider:this.credentials.emrDetails.number
    };
    this.createLoader();
    this.api.getFile(data).subscribe(response => {
      console.log(response);
      this.loader.dismiss();
      window.open(`${DCOM_URL}?input=`+encodeURIComponent(response.data),"_blank");
    });
  }

  getVisits(): void {
    this.createLoader();
    this.api.getVisits(this.clinicalData.emrData.identifier[0].value, this.selectedEmr.number).subscribe(response => {
      this.visitsInfoFetched = true;
      console.log(response);
        if(response.total) {
          this.visitList = [];
          this.visited = true;
          let visits = response.entry;
          visits.forEach(visit => {
            visit.resource.period.start = new Date(visit.resource.period.start);
            visit.resource.period.end = new Date(visit.resource.period.end);
            this.visitList.push(visit.resource);
          })
        }
        else {
          this.visited = false;
        }
      this.loader.dismiss();
    });
  }

  // toggleGroup(group) {
  //   if (this.isGroupShown(group)) {
  //       this.shownGroup = null;
  //   } else {
  //       this.shownGroup = group;
  //   }
  // }
  // isGroupShown(group) {
  //     return this.shownGroup === group;
  // }

  presentAlert(info) {
    let alert = this.alertCtrl.create({
      title: info.title,
      subTitle: info.subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }

  createLoader(): void {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

}
