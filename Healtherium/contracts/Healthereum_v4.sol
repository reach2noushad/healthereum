pragma solidity ^0.4.18;

contract Healthcare {

  struct Patient {
    address patientAddress;
    address[] sharedAddresses;
    address[] nominees;
    address[] nominators;
    bytes32 globalId;
    bytes32[] encryptedUuid;

  }
  
  struct EmrDetails {
      string emrId;
      string hospital;
      string department;
      string fileUrl;
      
  }

    struct Doctor {
    address doctorAddress;
    bytes32 globalId;
    address[] patients;
    bytes32[] encryptedUuid;
  }


  mapping(address => Patient) private patients;
  mapping(address => Doctor) private doctors;
  mapping(bytes32 => EmrDetails) emrDetails;


   modifier patientUUIDOwner(address _userAddress, bytes32 _encryptedUuid, uint index) {
    require(patients[_userAddress].encryptedUuid[index] == _encryptedUuid);
    _;
  }

  modifier patientGloablIdOwner(address _userAddress, bytes32 _globalId) {
    require(patients[_userAddress].globalId == _globalId);
    _;
  }

  modifier doctorUUIDOwner(address _userAddress,bytes32 _encryptedUuid, uint index){
    require(doctors[_userAddress].encryptedUuid[index]==_encryptedUuid);
    _;
  }
  
   modifier docotrGlobalIdOwner(address _userAddress, bytes32 _globalId) {
       require(doctors[_userAddress].globalId==_globalId);
       _;
   }
   
    modifier validateNomination (address nominee, address currentUser) {
        require(nominee != currentUser);
        _;
    }
   
   
   event patientGivenAccessToDoctor(address patient, address doctor, uint time);
   event patientRevokeAccessToDoctor(address patient, address doctor, uint time);
   event patientNominating(address patient,address nominatingPerson, uint time);
   event patientRevokingNomination(address patient, address nominatedPerson, uint time);

   
  
  function insertPatient (address _userAddress,bytes32 _globalId,bytes32 _encryptedUuid, string _emrId)  public  {
       patients[_userAddress].patientAddress = _userAddress;
       patients[_userAddress].globalId   = _globalId;
       patients[_userAddress].encryptedUuid.push(_encryptedUuid);
       EmrDetails memory userDetails;
       userDetails.emrId = _emrId;
       emrDetails[_encryptedUuid] = userDetails;
  }

    function loginpatient  (address _userAddress, bytes32 _globalId, bytes32 _encryptedUuid, uint index) patientUUIDOwner(_userAddress,_encryptedUuid,index) patientGloablIdOwner(_userAddress,_globalId) constant
    public  returns(string)
  {
      return emrDetails[_encryptedUuid].emrId;
    }

    function insertDoctor (address _userAddress,bytes32 _globalId,bytes32 _encryptedUuid, string _emrId,string _department,string _hospital) public  {
       doctors[_userAddress].doctorAddress = _userAddress;
       doctors[_userAddress].globalId = _globalId;
       doctors[_userAddress].encryptedUuid.push(_encryptedUuid);
       EmrDetails memory userDetails;
       userDetails.emrId = _emrId;
       userDetails.department = _department;
       userDetails.hospital = _hospital;
       emrDetails[_encryptedUuid] = userDetails;
    }

     function loginDoctor (address _userAddress,bytes32 _encryptedUuid, bytes32 _gloablId, uint index) doctorUUIDOwner(_userAddress,_encryptedUuid,index) docotrGlobalIdOwner(_userAddress,_gloablId) constant
     public returns(string,string,string)
     {
         return(emrDetails[_encryptedUuid].emrId,emrDetails[_encryptedUuid].department,emrDetails[_encryptedUuid].hospital);
      }


     function addfilepatient (bytes32 _encryptedUuid, string _uploadFileUrl) public returns (bool success) {
       EmrDetails storage userDetails = emrDetails[_encryptedUuid];
       userDetails.fileUrl = _uploadFileUrl;
       return true;
     }

     function getfilepatient (address _userAddress, bytes32 _encryptedUuid, uint index) patientUUIDOwner(_userAddress,_encryptedUuid,index) public constant returns(string) {
      return(emrDetails[_encryptedUuid].fileUrl);
     }
     
    function giveAccessToDoctor(address _patientAddress,address _doctorAddress) public {
         doctors[_doctorAddress].patients.push(_patientAddress);
         patients[_patientAddress].sharedAddresses.push(_doctorAddress);
         patientGivenAccessToDoctor(_patientAddress, _doctorAddress,now);
     }

     
     function getGivenAccesslist(address _doctorAddress) constant public returns(address[]){
         return doctors[_doctorAddress].patients;
     }
     
     function getPatientSharedList(address _patientAddress) constant public returns(address[]){
         return patients[_patientAddress].sharedAddresses;
     }

    function revokeAccessFromDoctor(address _patientAddress,uint doctorIndex,uint patientIndex,address _doctorAddress) public {
        address[] storage _patients = doctors[_doctorAddress].patients;
        address[] storage _doctors = patients[_patientAddress].sharedAddresses;
        delete _patients[patientIndex];
        _patients[patientIndex] = _patients[_patients.length-1];
        delete _patients[_patients.length-1];
        _patients.length--;
        delete _doctors[doctorIndex];
        _doctors[doctorIndex] = _doctors[_doctors.length-1];
        delete _doctors[_doctors.length-1];
        _doctors.length--;
        patientRevokeAccessToDoctor(_patientAddress,_doctorAddress,now);
     }

     function getPatient(address _doctorAddress,uint d, uint pindex) constant public returns (string) {
     address patientAddress = doctors[_doctorAddress].patients[d];
     bytes32 uuID = patients[patientAddress].encryptedUuid[pindex];
   
      return(emrDetails[uuID].emrId);
     }


    function getPatientFileFromdoctor(address docPubKey,uint d, uint patientEmrIndex) constant public returns (string hash) {
      address patientAddress = doctors[docPubKey].patients[d];
      bytes32 encrytedUUID = patients[patientAddress].encryptedUuid[patientEmrIndex];
      return(emrDetails[encrytedUUID].fileUrl);
    }

 
    
    function nominate(address nominee, address currentUser)  validateNomination (nominee, currentUser) public payable returns (address[]) {
        patients[currentUser].nominees.push(nominee);
        patients[nominee].nominators.push(currentUser);
        patientNominating(currentUser,nominee,now);
        return (patients[currentUser].nominees);
        
    }
    
    function getNominators(address user) public constant returns (address[]) {
        return patients[user].nominators; 
    }
    
    function getNominees(address user) public constant returns (address[]) {
        return patients[user].nominees;
    }
    
    function revokeNomination(address nominee, address currentUser, uint index, uint nominee_index) validateNomination (nominee, currentUser) public payable returns (address[]) {
        address[] storage nominees = patients[currentUser].nominees;
        address[] storage nominators = patients[nominee].nominators;
        delete nominees[index];
        nominees[index] = nominees[nominees.length-1];
        delete nominees[nominees.length-1];
        nominees.length--;
        delete nominators[nominee_index];
        nominators[nominee_index] = nominators[nominators.length-1];
        delete nominators[nominators.length-1];
        nominators.length--;
        patientRevokingNomination(currentUser,nominee,now);
        return (nominees);
    }

}
