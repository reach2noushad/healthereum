import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { NavbarService } from 'src/app/shared/sevices/navbar/navbar.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './app-side-nav.component.html',
  styleUrls: ['./app-side-nav.component.scss']
})
export class AppSideNavComponent implements OnInit {
  
  isLoggedIn: boolean = true;
  isLoggedIn$: Observable<boolean>;
  userType$: Observable<string>;
  constructor(private _router: Router, private authService: AuthenticationService, private nav: NavbarService) { 
    console.log("asdasdas");
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.userType$ = this.authService.userInitialized;
    console.log(this.isLoggedIn$);
    console.log(this.userType$);
  }

}
