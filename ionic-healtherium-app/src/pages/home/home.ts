import { RegistrationPage } from './../registration/registration';
import { Component } from '@angular/core';
import { ModalController, Platform, NavController, NavParams } from 'ionic-angular';
import { ModalContentPage } from '../modal-content/modal-content';

@Component({
    templateUrl: 'home.html',
    selector: 'Home'
})

export class Home {

    role: any = null;
    isApp: boolean;
    constructor(public nav: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public platform: Platform) {
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isApp = false;
        } else {
            this.isApp = true;
        }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DoctorDashboardPage');
    }

    login(user) {
        let modal = this.modalCtrl.create(ModalContentPage, { user, action: "login" });
        modal.present();
    }

    register(user) {
        console.log(user);
        this.nav.push(RegistrationPage, {user});
    }
}
