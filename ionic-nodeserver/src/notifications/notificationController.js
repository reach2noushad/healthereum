const express = require('express');
const router = express.Router();
const path = require('path');
const notificationRepo = require(path.resolve('src/notifications/notificationRepo.js'));
const constants = require(path.resolve('src/constants.js'));

router.get('/', async function (req, res) {
    try {
        const userId = req.query.userId;
        if (req.query.field === 'count') {
            let notificationCount =  await notificationRepo.getUnreadNotificationsCountForUser(userId);
            res.status(200).send({count: notificationCount});
        }
        else{
            let notificationPromise =  notificationRepo.getUnreadNotificationsForUser(userId);
            let notificationCountPromise =  notificationRepo.getUnreadNotificationsCountForUser(userId);
            let result = await Promise.all([notificationPromise, notificationCountPromise]);
            res.status(200).send({count: result[1], notifications: result[0]});
        }
    }catch(err){
        res.status(500).send(err);
    }
});

router.post('/', async function (req, res) {
    try {
        let notifications = req.body.notifications;
        await notificationRepo.updateNotificationsReadStatus(notifications);
        res.status(200).send({message: constants.SUCCESS.NOTIFICATIONS_READ_SUCCESS.text});
    }catch(err){
        res.status(500).send(err);
    }
});

module.exports = router;
