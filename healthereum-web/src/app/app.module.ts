import { AuthenticationService } from './authentication/services/authentication.service';
import { AuthGuard } from './authentication/auth.guard';

import { AuthenticationModule } from './authentication/authentication.module';
//import { DashboardModule } from './home/dashboard/dashboard.module';
import { HomeModule } from './home/home.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { ModalComponent } from './modal/modal.component';
import { PatientModule } from './home/dashboard/patient/patient.module';
import {AppSideNavComponent} from './home/app-side-nav/app-side-nav.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from './modal/modal.module';
import { ToastrModule } from 'ngx-toastr';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import * as firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
// Initialize Firebase
var config = {
  apiKey: "AIzaSyDUHE2taoT6jG22Ja-k3zsxME74N7akyHY",
  authDomain: "healtherium-a1bba.firebaseapp.com",
  databaseURL: "https://healtherium-a1bba.firebaseio.com",
  projectId: "healtherium-a1bba",
  storageBucket: "healtherium-a1bba.appspot.com",
  messagingSenderId: "793468538300"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    AppComponent,
    AppSideNavComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    ModalModule,
    // DashboardModule,
    PatientModule,
    AuthenticationModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      closeButton: true
    }),
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.rectangleBounce,
        backdropBackgroundColour: 'rgba(0,0,0,0.8)', 
    }),
    //AngularFireModule.initializeApp(config),
    //AngularFireStorageModule,
  ],
  providers: [AuthenticationService, AuthGuard],
  bootstrap: [AppComponent],
  entryComponents:[ModalComponent]
})
export class AppModule { }
