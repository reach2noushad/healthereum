import { HealtheriumApi } from './../../providers/healtherium-api/healtherium-api';
import { Component } from '@angular/core';
import { /*Platform, */NavParams, ViewController, NavController, LoadingController, AlertController } from 'ionic-angular';
import { DoctorDashboardPage } from './../doctor-dashoard/doctor-dashoard';
import { PatientDashboardPage } from './../patient-dashboard/patient-dashboard';


@Component({
  selector: 'page-modal-content',
  templateUrl: 'modal-content.html',
})
export class ModalContentPage {

  loader;

  user: any = {};
  action: string;
  role: any;
  signUpInfo: any;
  otp: string;
  showOTPform: any = false;
  loginOTP: any;
  totalProviderList: any;
  patientProvider: any;
  doctorAddress:any;
  selectedPatientAddress:any;
  constructor(
    //private platform: Platform,
    private params: NavParams,
    private viewCtrl: ViewController,
    private nav: NavController,
    private api: HealtheriumApi,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.action = this.params.get("action");
    this.role = this.params.get("user");
    this.patientProvider = this.params.get("chooseProvider");
    this.doctorAddress = this.params.get("userAccount");
    this.selectedPatientAddress = this.params.get("selectedPatientAddress");
    console.log(this.signUpInfo, this.action);
    console.log(this.patientProvider);
  }

  register() {
    this.createLoader();
    let data = this.params.get("data");
    data.otp = this.otp;
    this.api.register(data)
      .subscribe(response => {
        if (response) {
          console.log(response);
          let info = {
            title: "Successful",
            subTitle: `Your account has been successfully created`
          }
          this.presentAlert(info);
          this.loader.dismiss();
          this.viewCtrl.dismiss();
        }
      },
        error => {
          this.loader.dismiss();
          let info = {
            title: "Error",
            subTitle: error.error.message,
          };
          this.presentAlert(info);
        });
  }

  grantedEmrData(selectedOption) {
    console.log(selectedOption);
    let selectedProvider = selectedOption;
    let selectedPatientAddress = this.selectedPatientAddress;
    console.log(this.doctorAddress);
    console.log(this.selectedPatientAddress);
    this.api.getUserInfo(this.selectedPatientAddress, this.doctorAddress,selectedOption.number).subscribe(response => {
      console.log("shared address");
      this.loader.dismiss();
      console.log(response);
      this.viewCtrl.dismiss();
      this.params.data.selectedOption = selectedProvider;
      let patientDetails = JSON.parse(response.body).entry[0].resource;
        patientDetails.selectedPatientAddress = selectedPatientAddress;
      console.log(this.params.data.selectedOption);
      this.nav.push(DoctorDashboardPage, {isEmrData: true, response, patientDetails: patientDetails, doctor: this.params.data });
    },
      error => {
        this.loader.dismiss();
        console.error('Oops:', error);
        let info = {
          title: "Error",
          subTitle: error.error.message
          //subTitle: "Oops. Something went wrong. Please try again after some time"
        }
        this.presentAlert(info);
      });

  }

  generateOTP(): void {
    var self = this;
    this.user.isRegistered = true;
    this.createLoader();
    console.log(this.user);
    // this.user.userName = "user011";
    // this.user.ssn = "161616161616";
    // this.user.password = "password";
    this.api.generateOTP(this.user)
      .subscribe(response => {
        if (response) {
          this.api.getProvidersOfUser(this.user)
          .subscribe(providerList => {
            if(providerList){
              console.log(providerList);
              this.totalProviderList = providerList;
              console.log(this.totalProviderList);
              this.showOTPform = true;
              this.loader.dismiss();
            }
          })
        }
      },
        error => {
          this.loader.dismiss();
          console.error('Oops:', error);
          let info = {
            title: "Error",
            subTitle: "Oops. Something went wrong. Please try again after some time"
          }
          this.presentAlert(info);
        });

  }

  login(selectedOption): void {
    this.createLoader();
    const user = this.params.get("user");
    this.user.user = user
    this.user.otp = this.otp;
    this.user.emrDetails = selectedOption;
    this.api.login(this.user)
      .subscribe(response => {
        if (user === "patient") {
          this.nav.push(PatientDashboardPage, { response, credentials: this.user });
        }
        else if(user === "doctor"){
          this.nav.push(DoctorDashboardPage, { response, credentials: this.user });
        }
        else {
          window.alert("Work in progress");
        }
        this.viewCtrl.dismiss();
      },
        error => {
          this.loader.dismiss();
          var message = error && error.message ? error.message : error;
          console.error('Oops:', message);
          let info = {
            title: "Error",
            subTitle: message || "Oops. Something went wrong. Please try again after some time"
          }
          this.presentAlert(info);
        });
  }

  dismiss(): void {
    this.viewCtrl.dismiss();
  }

  presentAlert(info) {
    let alert = this.alertCtrl.create({
      title: info.title,
      subTitle: info.subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }

  createLoader(): void {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

}
