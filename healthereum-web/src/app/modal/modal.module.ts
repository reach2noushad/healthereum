import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from './../angular-material/angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  declarations: [ModalComponent],
  imports: [
    CommonModule, FormsModule, AngularMaterialModule, QRCodeModule
  ]
})
export class ModalModule { }
