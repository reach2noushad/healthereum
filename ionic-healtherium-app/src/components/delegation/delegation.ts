import { DelegationProvider } from './../../providers/delegation-api/delegation';
import { Component, Input } from '@angular/core';
import { LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the DelegationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'delegation',
  templateUrl: 'delegation.html'
})
export class DelegationComponent {

  @Input()
  isProvider: boolean = false;
  nomineeAdhaar: string;
  error: string;
  nominees: Array<any>;
  nominators: Array<any>;
  @Input()
  user: any;
  @Input()
  userInfo: any;
  grantAddress: any;
  revokeAddress: any
  showItem = null;
  c: any;
  loader: any;
  constructor(
    private DelegationProvider: DelegationProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {
    console.log('Hello DelegationComponent Component');
  }

  getDelegationInfo(): void {
    const userId = this.user.ssn;
    this.createLoader();
    this.DelegationProvider.getDelegationInfo(userId)
      .subscribe(response => {
        this.loader.dismiss();
        this.showItem = "view"
        console.log("response", response);
        this.nominees = response;
      },
        error => {
          this.loader.dismiss();
          console.log("error", error);
        });
  }

  isArray(obj: any) {
    return Array.isArray(obj);
  }

  nominate(): void {
    this.showItem = "delegate"
    this.error = null;
    console.log("in nominate");
    console.log("in get Nominees", this.user);
    let params = {
      nominee_id: this.nomineeAdhaar,
      user_id: this.user.ssn,
    };
    this.createLoader();
    if (this.nomineeAdhaar) {
      this.DelegationProvider.delegate(params)
        .subscribe(response => {
          this.loader.dismiss();
          console.log("response", response);
          let info = {
            title: "Success",
            subTitle: "User with  "+ this.nomineeAdhaar +" given access to act on behalf of you."
          }
          this.presentAlert(info);
        },
          error => {
            let info = {
              title: "Error",
              subTitle: "Oops!!! Something went wrong. Please try again after sometime"
            }
            this.presentAlert(info);
            this.loader.dismiss();
            console.log("error", error);
          });
    }
    else {
      this.loader.dismiss();
      this.error = "Please provide user identification"
    }
  }

  revoke(nominee): void {
    console.log(nominee);
    let params = {
      nominee_id: nominee.Adhaar,
      user_id: this.user.ssn
    }
    this.createLoader();
    this.DelegationProvider.revokeDelegation(params)
      .subscribe(response => {
        this.loader.dismiss();
        console.log(response);
        if (response)
          this.nominees = response;
        let info = {
          title: "Success",
          subTitle: "Privelege of User with Adhaar "+ nominee.user_id +" revoked."
        }
        this.presentAlert(info);
      },
        error => {
          this.loader.dismiss();
          let info = {
            title: "Error",
            subTitle: "Oops!!! Something went wrong. Please try again after sometime"
          }
          this.presentAlert(info);
          console.log(error);
        });
  }

  getDelegators(): void {
    const userId = this.user.ssn;
    this.createLoader();
    this.DelegationProvider.getDelegators(userId)
      .subscribe(response => {
        this.loader.dismiss();
        this.showItem = "delegator";
        console.log("response", response);
        this.nominators = response;
      },
        error => {
          this.loader.dismiss();
          let info = {
            title: "Error",
            subTitle: "Oops!!! Something went wrong. Please try again after sometime"
          }
          this.presentAlert(info);
          console.log("error", error);
        });
  }

  manageAccess(nominator, grantAccess): void {
    let info;
    let data = {
      user_id: this.user.ssn,
      user_name: this.userInfo.name,
      nominator_id: nominator.Adhaar,
      doctorAddress: grantAccess ? this.grantAddress: this.revokeAddress,
      grantAccess
    }
    this.createLoader();
    this.DelegationProvider.grantAccess(data)
      .subscribe(response => {
        this.loader.dismiss();
        console.log("response", response);
        if (grantAccess) {
          info = {
            title: "Success",
            subTitle: "Access shared with doctor for " + nominator.name
          }

        }
        else {
          info = {
            title: "Success",
            subTitle: "Access revoked from doctorfor " + nominator.name
          }
        }
        this.presentAlert(info);
        this.nominators = response;
      },
        error => {
          this.loader.dismiss();
          let info = {
            title: "Error",
            subTitle: "Oops!!! Something went wrong. Please try again after sometime"
          }
          this.presentAlert(info);
          console.log("error", error);
        });
  }

  createLoader(): void {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

  presentAlert(info) {
    let alert = this.alertCtrl.create({
      title: info.title,
      subTitle: info.subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }

}
