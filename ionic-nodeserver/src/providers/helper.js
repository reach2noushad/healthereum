import DBService from '../services/dbservice';


module.exports = {
    getProviderList: async (data) => {
        try {
            const emrList =  await DBService.getAllRecordsFromTable({tableName:"providerTable"});
            return emrList;

        }
        catch (e) {
            throw e;
        }
    }

}