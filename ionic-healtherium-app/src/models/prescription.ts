export class Prescription {
  status: string;
  authoredOn: string;
  requester: Requester;
  recorder: Recorder;
  dispenseRequest: DispenseRequest;
  dosageInstruction: DosageInstruction;
}

class Requester {
  id: string;
  name: string;
}

class Recorder {
  id: string;
  name: string;
}

class DispenseRequest {
  quantity: Value;
  expectedSupplyDuration: Value;
}

class DosageInstruction {
  drugName: string;
  timing: string;
  route: string;
  doseQuantity: Value;
  maxDosePerAdministration: Value;
  dosageInstructionFreeText: String;
}

class Value {
  value: number;
  unit: string;
}

export class PrescriptionResponse {
  count: number;
  prescriptions: Array<Prescription>;
}
