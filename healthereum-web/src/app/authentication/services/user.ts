// export interface User {
//   username: string;
//   password: string;
//   aadhar: string;
//   type: string;
// }

export interface Provider {
  providerNumber: number;
  providerName: string;
}

export interface User {
  userName: string;
  password: string;
  aadhar: string;
  user: string;
  firstName: string,
  familyName: string,
  dob: string,
  gender: string,
  addressLine1: string,
  addressLine2: string,
  city: string,
  state: string,
  areaCode: string,
  country: string,
  ssn: string,
  department: string,
  hospital: string,
  phoneNumber: number,
  emrNumber: number,
  provider: string,
  isRegistered: boolean,
  existingEmrUser:boolean,
  existingEmrPatientId:any,
  otp: any,
  emrDetails: any
}