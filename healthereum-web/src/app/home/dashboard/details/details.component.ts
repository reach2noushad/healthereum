import { AuthenticationService } from './../../../authentication/services/authentication.service';
import { HomeService } from './../../home.service';
import { Component, OnInit } from "@angular/core";
import { ViewEncapsulation } from "@angular/core";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class DetailsComponent implements OnInit {

  visits: any;
  observations: any;
  prescriptions: any;
  loading:boolean = false;
  constructor(private userService: HomeService, private toastr: ToastrService, private authService: AuthenticationService) {
    
  }

  ngOnInit() {
    let profileInfo = this.getUserProfile();
    let user = profileInfo ? profileInfo.creds: null;
    let demographics = profileInfo ? profileInfo.clinicalData.body.entry[0].resource : {};
    let emrNumber = user.emrDetails.number;
    this.loading = true;
    this.userService.getVisits(demographics.identifier[0].value, emrNumber).subscribe(visits => {
      console.log("here",visits);
      this.visits = visits.entry;
      this.loading = false;
      this.userService.getObservations(demographics.id, emrNumber).subscribe(observations => {
        console.log(observations);
        this.observations = observations.entry;
        this.userService.getPrescriptions(demographics.id, emrNumber).subscribe(response => {
          console.log(response);
          this.prescriptions = response.entry;
        },
        error => {
          this.toastr.error("Unable To Get Prescriptions", "Please try again after sometime.");
          console.log("error", error);
        })
      },
      error => {
        this.toastr.error("Unable To Get Observations", "Please try again after sometime.");
        console.log("error", error);
      })
    },
    error => {
      this.loading = false;
      this.toastr.error("Unable To Get Visits", "Please try again after sometime.");
      console.log("error", error);
    })
  }

  getUserProfile() {
    return this.authService.userProfile;
  }
}
