import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { HomeService } from './../../home.service';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalComponent } from 'src/app/modal/modal.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {

  @Input() user;
  patients: any;
  providers: any;
  selectedAddress: any;
  patientDetails: any;
  provider: any;
  visits: any;
  observations: any;
  prescriptions: any;
  selectedPatient: any;
  loading:boolean = false;
  currentProvider: any;
  dialogRef: MatDialogRef<ModalComponent, any>;
  constructor(public dialog: MatDialog, private homeService: HomeService,  private toastr: ToastrService, private authService: AuthenticationService) {
    console.log(this.user);
   }

  ngOnInit() {
    this.user = this.authService.userProfile;
    this.user ? this.getPatients(this.user.creds.ssn): null;
  }

  openModal(data: any) {
    this.dialogRef = this.dialog.open(ModalComponent, {data});
    const onPatientDataFetch = this.dialogRef.componentInstance.onPatientDataFetch.subscribe(data => {
      this.currentProvider = { hospitalName: data.provider.providerName, number: data.provider.providerNumber };
      if(!data.isDownload){
        this.getDemographics(data.provider);
      }
      else {
        this.download();
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      this.selectedPatient.isShown = !this.selectedPatient.isShown ? false: true; 
      onPatientDataFetch.unsubscribe();
    });
  }

  download(): void  {
    this.loading = true;
    
    this.homeService.getDemographics(this.selectedAddress, this.user.user.account, this.currentProvider.number).subscribe(response => {      
      this.patientDetails = JSON.parse(response.body).entry[0].resource;
      this.patientDetails.selectedAddress = this.selectedAddress;
      const data = {
        user: this.user.creds.user,
        grantor: '0x'+ this.selectedPatient.Account,
        recipient: this.user.user.account,
        provider:this.currentProvider.number,
        id: this.patientDetails.id // this is patient provider number from which doctor needs to access file of the patient
  
      };
      this.homeService.getFile(data, this.currentProvider).subscribe(response => {
        this.loading = false;
        if(this.dialogRef) this.dialogRef.close();
        let file;
       if (response && response.total >= 1) {
          file = response.entry[0].resource.series[0].endpoint[0].reference;
          window.open(`http://192.168.2.27:8082?input=`+encodeURIComponent(file),"_blank");
        }
       else if (response && response.total == 0) {
          console.log("patient has no files");
          this.toastr.error("Patient has no Dicom File To Display", "Please try again after sometime.");
        }
       },
        error => {
          this.loading = false;
          if(this.dialogRef) this.dialogRef.close();
        });
    },
      error => {
        this.loading = false;
        if(this.dialogRef) this.dialogRef.close();
        this.toastr.error("Unable To Get Patient Basic Details", "Please try again after sometime.");
        console.error('Oops:', error);
      });

  }

  togglePanel(expanded) {
    expanded = !expanded;
  }

  getPatients(ssn): void {
    this.loading = true;
    this.homeService.getAllocatedUsers(ssn, null).subscribe(response => {
      this.patients = response;
      if(this.patients && this.patients.length) this.patients.map(p =>  {p.isShown = false});
      console.log(this.patients);
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
    },
    error => {
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
      this.toastr.error("Unable To Get Patients", "Please try again after sometime.");
      console.log(error);
    })
  }

  getProviders(address, patient, event, isDownload) : void {
    this.loading = true;
    this.selectedPatient = patient;
    if(!patient.isShown || isDownload) {
      event.stopPropagation();
      this.selectedAddress = address;
      this.homeService.getProvidersOfUser({ address }).subscribe(response => {
        console.log(response);
        this.providers = response;
        this.openModal({type:'providers', providers: this.providers, isDownload});
        this.loading = false;
      },
        error => {
          this.loading = false;
          this.toastr.error("Unable To Get Providers", "Please try again after sometime.");
          console.error('Oops:', error);
          
        });
    }
  }

  getDemographics(provider) : void {
    if(this.dialogRef) this.dialogRef.close();
    this.loading = true;
    this.provider = provider;
    this.homeService.getDemographics(this.selectedAddress, this.user.user.account, provider.providerNumber).subscribe(response => {
      console.log("shared address");
      console.log(response);
      this.selectedPatient.isShown = true;
      this.patientDetails = JSON.parse(response.body).entry[0].resource;
      this.patientDetails.selectedAddress = this.selectedAddress;
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
    },
      error => {
        this.loading = false;
        if(this.dialogRef) this.dialogRef.close();
        this.toastr.error("Unable To Get Patient Basic Details", "Please try again after sometime.");
        console.error('Oops:', error);
      });
  }

  getPatientVisits(patient) : void {
    this.loading = true;
    this.homeService.getVisits(patient.identifier[0].value, this.provider.providerNumber).subscribe(visits => {
      console.log(visits);
      this.visits = visits.entry;
      this.openModal({type:'visits', visits: this.visits});
      this.loading = false;
    },error => {
      this.loading = false;
      this.toastr.error("Unable To Get Patient Visits", "Please try again after sometime.");
      console.error('Oops:', error);
    })
  }

  getObservations(patient) {
    this.loading = true;
    this.homeService.getObservations(patient.id, this.provider.providerNumber).subscribe(observations => {
      console.log(observations);
      this.observations = observations.entry;
      this.openModal({type:'observations', observations: this.observations});
      this.loading = false;
    },error => {
      this.loading = false;
      this.toastr.error("Unable To Get Patient Observations", "Please try again after sometime.");
      console.error('Oops:', error);
    })
  }

  getPrescriptions(patient) {
    this.loading = true;
    this.homeService.getPrescriptions(patient.id, this.provider.providerNumber).subscribe(response => {
      console.log(response);
      this.prescriptions = response && response.entry.length  ? response.entry : null;
      this.openModal({type:'prescriptions', prescriptions: this.prescriptions});
      this.loading = false;
    },error => {
      this.loading = false;
      this.toastr.error("Unable To Get Patient Prescriptions", "Please try again after sometime.");
      console.error('Oops:', error);
    })
  }

}
