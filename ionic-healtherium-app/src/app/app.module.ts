import { RegistrationPageModule } from './../pages/registration/registration.module';
import { Home } from './../pages/home/home';
import { AboutUsPage } from './../pages/about-us/about-us';
import { QRCodeModule } from 'angularx-qrcode';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { QRScanner } from '@ionic-native/qr-scanner';
import { MyApp } from './app.component';
import { LandingPage } from '../pages/landing/landing';
import { ModalContentPage } from '../pages/modal-content/modal-content';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HealtheriumApi } from '../providers/healtherium-api/healtherium-api';

import firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { NotificationApiProvider } from '../providers/notification-api/notification-api';
import {NotificationsPageModule} from "../pages/notifications/notifications.module";
import { DelegationProvider } from '../providers/delegation-api/delegation';
//import { AppRoutingModule } from './app-routing.module';
import {PatientDashboardPageModule} from "../pages/patient-dashboard/patient-dashboard.module";
import {DoctorDashboardPageModule} from "../pages/doctor-dashoard/doctor-dashboard.module";
import { RegistrationPage } from '../pages/registration/registration';
// Initialize Firebase
var config = {
  apiKey: "AIzaSyDUHE2taoT6jG22Ja-k3zsxME74N7akyHY",
  authDomain: "healtherium-a1bba.firebaseapp.com",
  databaseURL: "https://healtherium-a1bba.firebaseio.com",
  projectId: "healtherium-a1bba",
  storageBucket: "healtherium-a1bba.appspot.com",
  messagingSenderId: "793468538300"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    MyApp,
    LandingPage,
    Home,
    AboutUsPage,
    ModalContentPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    //AppRoutingModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFireStorageModule,
    RegistrationPageModule,
    NotificationsPageModule,
    PatientDashboardPageModule,
    DoctorDashboardPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RegistrationPage,
    LandingPage,
    Home,
    AboutUsPage,
    ModalContentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    QRScanner,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HealtheriumApi,
    NotificationApiProvider,
    DelegationProvider
  ]
})
export class AppModule { }
