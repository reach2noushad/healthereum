const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ['./app/javascripts/app.js','./app/javascripts/jquery.qrcode.min.js','./app/javascripts/materialize.js','./app/javascripts/walletapp.js','./app/javascripts/materialize.min.js'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.js'
  },
  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html" },
      { from: './app/Doc_signup.html', to: "Doc_signup.html" },
      { from: './app/Doc_Dashboard.html', to: "Doc_Dashboard.html" },
      { from: './app/Patient_Dashboard.html', to: "Patient_Dashboard.html" },
      { from: './app/Patient_Signup.html', to: "Patient_Signup.html" }
    ])
  ],
  node: {
  fs: 'empty'
    },
  module: {
    rules: [
      {
       test: /\.css$/,
       use: [ 'style-loader', 'css-loader' ]
     },
     {
       test: /\.html$/,
       use: ['html-loader']
     },
     {
       test: /\.(jpg|png)$/,
       use: [
         {
           loader: 'file-loader',
           options : {
             name : '[name].[ext]',
             outputPath: 'img/',
             publicPath: 'img/'
           }
         }

       ]
     }
    ],
    loaders: [
      { test: /\.json$/, use: 'json-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  }
}
