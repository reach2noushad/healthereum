import express from "express";
import controller from "./controller";
const router = express.Router();

router.post('/add', async (req, res, next) => {
    try {
        const data = req.body;
        const transaction_result = await controller.add(data);
        res.status(200).send(transaction_result);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/nominees', async (req, res, next) => {
    try {
        const data = req.query;
        const response = await controller.getNominees(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/nominators', async (req, res, next) => {
    try {
        const data = req.query;
        const response = await controller.getNominators(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.put('/update', async (req, res, next) => {
    try {
        const response = await controller.update();
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/revoke', async (req, res, next) => {
    try {
        const data = req.body;
        const response = await controller.revoke(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/grantAccess', async (req, res, next) => {
    try {
        const data = req.body;
        const response = await controller.grantAccess(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;
