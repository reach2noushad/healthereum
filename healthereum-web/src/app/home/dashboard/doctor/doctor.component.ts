import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { HomeService } from './../../home.service';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalComponent } from 'src/app/modal/modal.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss']
})
export class DoctorComponent implements OnInit {

  doctors: any;
  user: any;
  doctor: any;
  loading: boolean = false;
  dialogRef: MatDialogRef<ModalComponent, any>;
  constructor(public dialog: MatDialog, 
    private homeService: HomeService, 
    private toastr: ToastrService, 
    private authService: AuthenticationService) {
    
   }

  ngOnInit() {
    this.user = this.authService.userProfile;
    this.getDoctors(this.user.creds.ssn);
  }

  getDoctors(ssn) {
    this.loading = true;
    this.homeService.getAllocatedUsers(ssn, "doctor").subscribe(response => {
      this.doctors = response;
      console.log(response);
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
    },
    error => {
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
      this.toastr.error("Unable To Fetch Doctors List", "Please try again after sometime.");
      console.log(error);
    });
  }

  manageAccess (address, mode) {
    if(this.dialogRef) this.dialogRef.close();
    this.loading = true;
    let data = this.user.creds;
    if (mode) {
      data.type = true;
    }
    else {
      data.type = false;
    }
    data.toAddress = address;
    this.homeService.manageAccess(data).subscribe(response => {
      this.doctor = "";
      console.log("address sharing/cancellation success");
      console.log(response);
      this.getDoctors(this.user.creds.ssn);
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
    },
    error => {
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
      console.error('Oops:', error);
      this.toastr.error("Unable To Manage Access / Revoke ", "Please try again after sometime.");
     });
  }

  openModal(data) {
    this.dialogRef = this.dialog.open(ModalComponent, { data });
    const onAccessGrant = this.dialogRef.componentInstance.onDoctorDelegation.subscribe(doctor => {
      console.log(doctor);
      this.manageAccess(doctor, true)
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      onAccessGrant.unsubscribe();
    });
  }
  

}
