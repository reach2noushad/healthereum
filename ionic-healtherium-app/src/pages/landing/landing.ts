import { Component } from '@angular/core';
import { ModalController, Platform, NavController, NavParams } from 'ionic-angular';

@Component({
    templateUrl: 'landing.html',
    selector: 'Landing'
})

export class LandingPage {

    role: any = null;
    isApp: boolean;
    constructor(public nav: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public platform: Platform) {
        if(this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isApp = false;
          } else {
            this.isApp = true;
          }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DoctorDashboardPage');
    }
}
