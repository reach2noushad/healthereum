import { AuthenticationComponent } from './authentication/authentication.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './authentication/auth.guard';
import { SignupComponent } from './authentication/signup/signup.component';

const routes: Routes = [
  {path:'signin', component: AuthenticationComponent},
  { path: '',  redirectTo: 'signin', pathMatch:'full' },
  {path: 'signup', component: SignupComponent}
  // {path:'dashboard', component: DashboardComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
