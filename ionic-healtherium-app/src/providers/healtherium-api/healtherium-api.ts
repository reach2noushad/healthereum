import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { BASE_URL } from '../../config';
import {Provider} from "../../models/provider";




@Injectable()
export class HealtheriumApi {

  patientData;

  //private apiUrl = '';
  constructor(private http: HttpClient) {
  }

  getDetails(data): Observable<any> {
    console.log("login")
    return this.http.post(`${BASE_URL}/fetchDetails`, data)
      .map(response => {
        return response;
      });
  }

  getAccessList(data): Observable<any> {
    console.log("getting access list")
    return this.http.post(`${BASE_URL}/getSharedPatientList`,data)
    .map(response=> {
      return response;
    });
  }

  login(data): Observable<any> {
    console.log("login", data)
    return this.http.post(`${BASE_URL}/api/user/login`, data)
      .map(response => {
        return response;
      });
  }

  fetchDetails(data): Observable<any> {
    console.log("fetchDetails")
    return this.http.post(`${BASE_URL}/fetchDetails`, data)
      .map(response => {
        return response;
      });
  }

  generateOTP(data): Observable<any> {
    console.log("registerUser")
    return this.http.post(`${BASE_URL}/api/user/generateotp`, data)
      .map(response => {
        return response;
      });
  }

  register(data): Observable<any> {
    console.log("addUser")
    return this.http.post(`${BASE_URL}/api/user/register`, data)
      .map(response => {
        return response;
      });
  }
  uploadFile(data): Observable<any> {
    console.log("addUser");
    return this.http.post(`${BASE_URL}/api/user/upload`, data)
      .map(response => {
        return response;
      });
  }

  manageAccess(data): Observable<any> {
    console.log("addUser");
    return this.http.post(`${BASE_URL}/api/ownership`, data)
      .map(response => {
        return response;
      });
  }

  postFile(fileToUpload): Observable<any> {
    var file = fileToUpload;
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${fileToUpload.name}`).put(fileToUpload);
    return Observable.create(observer => {
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          // upload in progress
          fileToUpload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        },
        (error) => {
          // upload failed
          console.log(error);
          observer.error(status);
        },
        () => {
          // upload success
          file.url = uploadTask.snapshot.downloadURL;
          console.log(file);
          observer.next(file);
          observer.complete();
        });
    });

  }

  getVisits(data, provider): any {
    const params = new HttpParams().set('identifier', data).set('provider', provider);
    return this.http.get(`${BASE_URL}/api/user/visits`, { params })
      .map(response => {
        return response;
      });
  }

  getObservations(data, provider): any {
    const params = new HttpParams().set('subject', data).set('provider', provider);
    return this.http.get(`${BASE_URL}/api/user/observations`, { params })
      .map(response => {
        return response;
      });
  }

  getPrescriptions(personId, provider): any {
    const params = new HttpParams().set('personId', personId).set('provider', provider);
    return this.http.get(`${BASE_URL}/prescriptions`, { params })
      .map(response => {
        return response;
      });
  }

  getAllocatedUsers(ssn): Observable<any> {
    const params = new HttpParams().set('userId', ssn);
    return this.http.get(`${BASE_URL}/api/user/allocated`,{ params })
    .map(response=> {
      return response;
    });
  }

  getUserInfo(grantor, recipient, provider): Observable<any> {
    const params = new HttpParams().set('recipient', recipient).set('grantor', grantor).set('providerNo',provider)
    console.log(params);
    return this.http.get(`${BASE_URL}/api/user/grantor`, { params })
    .map(response=> {
      this.patientData = response;
      return response;
    });
  }

  getFile(data) : Observable<any> {
    return this.http.post(`${BASE_URL}/api/user/file`, data)
    .map(response=> {
      return response;
    });
  }

  getProvidersOfUser(data) : Observable<any> {
    console.log("calling get providers api from front end");
    console.log(data);
    let queryParams='';
    if(data.ssn){
      queryParams = `?userId=${data.ssn}`;
    }
    else if(data.address){
      queryParams = `?address=${data.address}`;
    }
 
    return this.http.get(`${BASE_URL}/api/user/providerList`+ queryParams)
    .map(response=> {
      return response;
    });
  }

  getProviders(): Observable<Provider[]> {
    return this.http.get<Provider[]>(`${BASE_URL}/api/providers`)
      .map(response=> {
        return response;
      });
  }

}
