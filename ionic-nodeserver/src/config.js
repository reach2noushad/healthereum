exports.secretKey = "1234567890abcdefghijklmnopqrstuv";
exports.contract_address = '0x691940b40025df8d360ca7423d7a70ac473c06a7';
exports.admin_pk = '0x1bb4a50289de3c095afe5f40fa0ebb105aba0e917f3e4b36a74d847c683232da';
exports.cipherKeyIv = {
    iv: Buffer.from("1234567890abcdefghijklmnopqrstuv").toString('hex').slice(0, 16),
    key: `1234567890abcdefghijklmnopqrstuv`
};
exports.APP = "1b671a64-40d5-491e-99b0-da01ff1f3341";
exports.abi = [{"constant":false,"inputs":[{"name":"_encryptedUuid","type":"bytes32"},{"name":"_uploadFileUrl","type":"string"}],"name":"addfilepatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_patientAddress","type":"address"},{"name":"_doctorAddress","type":"address"}],"name":"giveAccessToDoctor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_userAddress","type":"address"},{"name":"_globalId","type":"bytes32"},{"name":"_encryptedUuid","type":"bytes32"},{"name":"_emrId","type":"string"},{"name":"_department","type":"string"},{"name":"_hospital","type":"string"}],"name":"insertDoctor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_userAddress","type":"address"},{"name":"_globalId","type":"bytes32"},{"name":"_encryptedUuid","type":"bytes32"},{"name":"_emrId","type":"string"}],"name":"insertPatient","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"nominee","type":"address"},{"name":"currentUser","type":"address"}],"name":"nominate","outputs":[{"name":"","type":"address[]"}],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"_patientAddress","type":"address"},{"name":"doctorIndex","type":"uint256"},{"name":"patientIndex","type":"uint256"},{"name":"_doctorAddress","type":"address"}],"name":"revokeAccessFromDoctor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"nominee","type":"address"},{"name":"currentUser","type":"address"},{"name":"index","type":"uint256"},{"name":"nominee_index","type":"uint256"}],"name":"revokeNomination","outputs":[{"name":"","type":"address[]"}],"payable":true,"stateMutability":"payable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"patient","type":"address"},{"indexed":false,"name":"doctor","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"patientGivenAccessToDoctor","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"patient","type":"address"},{"indexed":false,"name":"doctor","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"patientRevokeAccessToDoctor","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"patient","type":"address"},{"indexed":false,"name":"doctor","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"doctorAccessingPatient","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"patient","type":"address"},{"indexed":false,"name":"nominatingPerson","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"patientNominating","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"patient","type":"address"},{"indexed":false,"name":"nominatedPerson","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"patientRevokingNomination","type":"event"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"},{"name":"_encryptedUuid","type":"bytes32"},{"name":"index","type":"uint256"}],"name":"getfilepatient","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_doctorAddress","type":"address"}],"name":"getGivenAccesslist","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"user","type":"address"}],"name":"getNominators","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"user","type":"address"}],"name":"getNominees","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_doctorAddress","type":"address"},{"name":"d","type":"uint256"},{"name":"pindex","type":"uint256"}],"name":"getPatient","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"docPubKey","type":"address"},{"name":"d","type":"uint256"},{"name":"patientEmrIndex","type":"uint256"}],"name":"getPatientFileFromdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_patientAddress","type":"address"}],"name":"getPatientSharedList","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"},{"name":"_encryptedUuid","type":"bytes32"},{"name":"_gloablId","type":"bytes32"},{"name":"index","type":"uint256"}],"name":"loginDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"},{"name":"_globalId","type":"bytes32"},{"name":"_encryptedUuid","type":"bytes32"},{"name":"index","type":"uint256"}],"name":"loginpatient","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];

exports.events = {
    accessGranted : {
        eventName : "patientGivenAccessToDoctor",
        eventTopic : "0x071d0f40e6698e755d89ce45493bb1ca4ee448f11b31780de79fdbbcdca1be86"
    },
    accessRevoked : {
        eventName : "patientRevokeAccessToDoctor",
        eventTopic : "0x43ad743c8b8ad95a1a260856dab3a9e62bc48b335e0588c8d46a2b5a66d1f6ab"
    },
    nominationGranted : {
        eventName : "patientNominating",
        eventTopic : "0x2bfad2e48ccd03eb7c80e57f79c56a3254abec06c7bb2a814e86ae0ae0917cc7"
    },
    nominationRevoked : {
        eventName : "patientRevokingNomination",
        eventTopic : "0x6e5f6a07cadcb83c2c6c3b3d201614b00a3b9550e0ec17b50ad9c25ecc0a592a"
    }
}


exports.db = {
    host: "localhost",
    user: "root",
    password: "attinad@123",//add password here,
    database: "healthereum"
};

//const OPENMRS_FHIR_BASE_URL = "http://192.168.2.27:8081/openmrs-standalone/ws/fhir";
const OPENMRS_FHIR_BASE_URL = "http://localhost:3000/3_0_1";
exports.OPENMRS_FHIR_BASE_URL = OPENMRS_FHIR_BASE_URL;

const MONGO_FHIR_BASE_URL = "http://localhost:3000/3_0_1";
//const MONGO_FHIR_BASE_URL = "http://192.168.11.39:3000/3_0_1";
exports.MONGO_FHIR_BASE_URL = MONGO_FHIR_BASE_URL;

exports.keystoreFilePath = require('os').homedir() + '/mobile_wallet/doctor/keystore';
exports.CIPHERS = {
    "AES_128": "aes128",          //requires 16 byte key
    "AES_128_CBC": "aes-128-cbc", //requires 16 byte key
    "AES_192": "aes192",          //requires 24 byte key
    "AES_256": "aes256"           //requires 32 byte key
}

exports.healthereumProviderId = 1;

exports.getEmrUrl = function getEmr(providerId){
    switch(providerId) {
        case 1:
            return OPENMRS_FHIR_BASE_URL;
        case 2:
            return MONGO_FHIR_BASE_URL;
        default:
            return OPENMRS_FHIR_BASE_URL;
    }
};
