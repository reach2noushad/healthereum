const path = require('path');
const queries = require(path.resolve('src/notifications/queries.js'));
const db = require(path.resolve('src/mysql.js'));
const {uuid} = require(path.resolve('src/uuid.js'));

function getUnreadNotificationsForUser(userId) {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await db.query(queries.GET_UNREAD_NOTIFICATIONS_FOR_USER, [userId]);
            resolve(result);
        } catch (err) {
            reject(err);
        }
    });
}

function getUnreadNotificationsCountForUser(userId) {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await db.query(queries.GET_UNREAD_NOTIFICATIONS_COUNT_FOR_USER, [userId]);
            resolve(result[0].count);
        } catch (err) {
            reject(err);
        }
    });
}

function updateNotificationsReadStatus(notifications) {
    return new Promise(async (resolve, reject) => {
        try {
            if(notifications.length !== 0){
                await db.query(queries.markNotificationsAsReadQuery(notifications),[]);
            }
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

function createNotification(reqOptions){
    return new Promise(async (resolve, reject) => {
        try {
            let notificationMessage;
            if(reqOptions.isAccessGranted){
                notificationMessage = `${reqOptions.patientDetails.patientName} has granted ownership to you`;
            }
            else{
                notificationMessage = `${reqOptions.patientDetails.patientName} has cancelled your ownership`;
            }
             let params = [uuid(),notificationMessage,reqOptions.doctorDetails.doctorPublicKey, false, new Date()];
            await db.query(queries.CREATE_NOTIFICATION_QUERY,params);
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

module.exports = {
    getUnreadNotificationsForUser,
    getUnreadNotificationsCountForUser,
    updateNotificationsReadStatus,
    createNotification
};
