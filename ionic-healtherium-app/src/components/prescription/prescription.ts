import {Component, Input} from '@angular/core';
import { Prescription } from '../../models/prescription';
import { HealtheriumApi } from './../../providers/healtherium-api/healtherium-api';
import {LoadingController} from "ionic-angular";


/**
 * Generated class for the PrescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'prescription',
  templateUrl: 'prescription.html'
})
export class PrescriptionComponent {
  prescriptions: Prescription[];
  @Input() isDoctor;
  @Input() selectedEmr;
  count: number = 0;
  @Input() userDetails;
  showPrescriptionsView:boolean = false;
  public loader;
  constructor(private api: HealtheriumApi, public loadingCtrl: LoadingController) {
    this.prescriptions = [];
    console.log('Hello PrescriptionComponent Component');
  }

  ngOnInit() {
    if(this.isDoctor){
      this.getPrescriptions();
    }
  }

  getPrescriptions(): void {
    let self = this;
    this.showPrescriptionsView = false;
    let personId;
    if(this.isDoctor){
      personId = this.userDetails.id;
    }
    else{
      personId = this.userDetails.emrData.id;
      console.log(this.userDetails.emrData);
    }
    self.createLoader();
    this.api.getPrescriptions(personId, this.selectedEmr.number)
      .subscribe(response => {
        self.loader.dismiss();
        self.showPrescriptionsView = true;
        console.log(response);
        this.prescriptions = response['prescriptions'];
        this.count = response['count'];
      },error => {
        console.log(error);
      });
  }

  createLoader(): void {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

}
