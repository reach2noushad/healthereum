import { AboutUsPage } from './../pages/about-us/about-us';
import { DoctorDashboardPage } from './../pages/doctor-dashoard/doctor-dashoard';
import { PatientDashboardPage } from './../pages/patient-dashboard/patient-dashboard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Home } from '../pages/home/home';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'patientdashboard',
    component: PatientDashboardPage
  },
  {
    path: 'doctordashboard',
    component: DoctorDashboardPage
  },
  {
    path: 'login',
    component: Home
  },
  {
    path: 'about',
    component: AboutUsPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}