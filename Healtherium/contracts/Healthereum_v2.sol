pragma solidity ^0.4.18;

contract Healthereum {

  struct UserStruct {
    address instowner;
    address[] givenowner;
    address[] nominees;
    address[] nominators;
    bytes32 adhaar;
    string comb;
    bytes32 UUID;
    uint index;
    string hash;
  }

    struct DoctorStruct {
    address instowner;
    bytes32 adhaar;
    string comb;
    address[] patients;
    bytes32 department;
    bytes32 hospital;
    uint index;
    bytes32 UUID;
    string hash;
  }

  string constant username = "admin";
  string constant password = "Admin123";


  mapping(address => UserStruct) private userStructs;
  mapping(address => DoctorStruct) private doctorStructs;
  bytes32[] private userIndex;

   modifier patientUUIDOwner(address anyowners, bytes32 UUIDs) {
    require(userStructs[anyowners].UUID == UUIDs);
    _;
  }

  modifier patientAdharOwner(address anyowners, bytes32 adhaar) {
    require(userStructs[anyowners].adhaar == adhaar);
    _;
  }

  modifier doctorUUIDOwner(address anyowners,bytes32 UUIDs){
    require(doctorStructs[anyowners].UUID==UUIDs);
    _;
  }



  function insertPatient (address instowner,bytes32 adhaar,bytes32 UUID, string comb)  public returns (string, string,string) {
       userStructs[instowner].instowner = instowner;
       userStructs[instowner].adhaar   = adhaar;
       userStructs[instowner].UUID   = UUID;
       userStructs[instowner].comb   = comb;
       userStructs[instowner].index     = userIndex.push(UUID)-1;
       string storage ccomb=userStructs[instowner].comb;
        return (username,password,ccomb);
  }

    function loginpatient  (address instowner, bytes32 UUIDs, bytes32 aadhar) patientUUIDOwner(instowner,UUIDs) patientAdharOwner(instowner,aadhar) constant
    public  returns(string,string,string)
  {
    return(
      username,
      password,
      userStructs[instowner].comb);

    }

    function insertDoctor (address instowner,bytes32 adhaar,bytes32 UUID, string comb,bytes32 department,bytes32 hospital) public returns (string,string,string) {
       doctorStructs[instowner].instowner = instowner;
       doctorStructs[instowner].adhaar = adhaar;
       doctorStructs[instowner].UUID = UUID;
       doctorStructs[instowner].comb = comb;
       doctorStructs[instowner].index = userIndex.push(UUID)-1;
       doctorStructs[instowner].department = department;
       doctorStructs[instowner].hospital = hospital;
       string storage ccomb=doctorStructs[instowner].comb;
       return (username,password,ccomb);
    }

     function loginDoctor (address instowner,bytes32 UUIDs/*, bytes32 aadhar*/) doctorUUIDOwner(instowner,UUIDs) constant
     public returns(string,string,string,bytes32,bytes32)
     {
         bytes32 department = doctorStructs[instowner].department;
         bytes32 hospital = doctorStructs[instowner].hospital;
         return(username,password,doctorStructs[instowner].comb,department,hospital);
     }


     function addhashpatient (address instowner, string hash) public returns (bool success) {
       userStructs[instowner].hash   = hash;
       return true;
     }

     function gethashpatient (address instowner, bytes32 UUIDs) patientUUIDOwner(instowner,UUIDs) public constant returns(string hash) {
      return(userStructs[instowner].hash);
     }
     
     function giveAccessToDoctor(address patientaddress,address doctoraddress) public {
         doctorStructs[doctoraddress].patients.push(patientaddress);
         userStructs[patientaddress].givenowner.push(doctoraddress);
     }

     
     function getGivenAccesslist(address doctor) public constant returns(address[]){
         return doctorStructs[doctor].patients;
     }
     
     function getPatientSharedList(address patient) public constant returns(address[]){
         return userStructs[patient].givenowner;
     }

     function revokeAccessFromDoctor(address patient,uint doctorIndex,uint patientIndex,address doctor) public {
        address[] storage patients = doctorStructs[doctor].patients;
        address[] storage doctors = userStructs[patient].givenowner;
        delete patients[patientIndex];
        patients[patientIndex] = patients[patients.length-1];
        delete patients[patients.length-1];
        patients.length--;
        delete doctors[doctorIndex];
        doctors[doctorIndex] = doctors[doctors.length-1];
        delete doctors[doctors.length-1];
        doctors.length--;
     }

     function getPatient(address Doctor,uint index) constant public returns (string,string,string) {
      address ituser = doctorStructs[Doctor].patients[index];
       return(username,password,userStructs[ituser].comb);
     }


    function getpatienthashdoctor(address Doctor,uint index) constant public returns (string hash) {
      address ituser = doctorStructs[Doctor].patients[index];
       return(userStructs[ituser].hash);
    }
    
    modifier validateNomination (address nominee, address currentUser) {
        require(nominee != currentUser);
        _;
    }
    
    function nominate(address nominee, address currentUser)  validateNomination (nominee, currentUser) public payable returns (address[]) {
        userStructs[currentUser].nominees.push(nominee);
        userStructs[nominee].nominators.push(currentUser);
        return (userStructs[currentUser].nominees);
        
    }
    
    function getNominators(address user) public constant returns (address[]) {
        return userStructs[user].nominators; 
    }
    
    function getNominees(address user) public constant returns (address[]) {
        return userStructs[user].nominees;
    }
    
    function revokeNomination(address nominee, address currentUser, uint index, uint nominee_index) validateNomination (nominee, currentUser) public payable returns (address[]) {
        address[] storage nominees = userStructs[currentUser].nominees;
        address[] storage nominators = userStructs[nominee].nominators;
        delete nominees[index];
        nominees[index] = nominees[nominees.length-1];
        delete nominees[nominees.length-1];
        nominees.length--;
        delete nominators[nominee_index];
        nominators[nominee_index] = nominators[nominators.length-1];
        delete nominators[nominators.length-1];
        nominators.length--;
        return (nominees);
    }

}