import { Home } from './../pages/home/home';
import { AboutUsPage } from './../pages/about-us/about-us';
import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';

//import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { LandingPage } from '../pages/landing/landing';
//import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = LandingPage;
  pages: Array<{title: string, component: any, url: string}>;
  //pages: Array<{title: string, component: any, url: string}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Login/Sign up', component: Home, url: '/login' },
      { title: 'About Us', component: AboutUsPage, url: '/about' }
    ];
    // this.pages = [
    //   {
    //     title: 'Login/Sign up',
    //     url: '/app/tabs/(schedule:schedule)',
    //     component: Registration }
    // ]

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.push(page.component);
    //this.router.navigate(['/login'])
  }

  aboutUs() {
    this.nav.push(AboutUsPage);
    this.menu.close();
  }
}
