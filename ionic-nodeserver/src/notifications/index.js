const path = require('path');
const notificationController = require(path.resolve('src/notifications/notificationController.js'));
const notificationRepo = require(path.resolve('src/notifications/notificationRepo.js'));
module.exports = {
    controller: notificationController,
    repo: notificationRepo
};
