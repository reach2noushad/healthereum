import { AuthenticationService } from './../../../authentication/services/authentication.service';
import { HomeService } from './../../home.service';
import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss']
})
export class ActivityLogComponent implements OnInit {

  user: any;
  loading: boolean = false
  activity: any = null;
  constructor(private homeService: HomeService,
    private authService: AuthenticationService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.user = this.authService.userProfile;
    this.loading = true;
    const address = this.user && this.user.user ? this.user.user.account : null;
    if (address)
      this.getActivityLog(address).subscribe(response => {
        console.log(response);
        this.activity = {
          accessGranted: response[0],
          accessRevoked: response[1],
          nominationGranted: response[2],
          nominationRevoked: response[3]
        }
        this.loading = false;
      }, error => {
        this.loading = false;
        this.toastr.error("Unable To Get Acivity Logs", "Please try again after sometime.");
        console.log("error");
      })
  }

  getActivityLog(address) {
    return this.homeService.getActivityLog(address);
  }

}
