import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getFile(data, emrDetails): Observable<any> {
    if(emrDetails.hospitalName.toLowerCase() === "openmrs") {
      return this.http.post(`http://192.168.2.27:4445/api/user/file`, data)
      .pipe(map(response => {
        return response;
      }));
    }
    else {
      //new API for fetching file from openemr
        return this.http.get(`http://192.168.2.27:4445/api/user/${data.id}/imagingstudy?provider=${data.provider}`)
        .pipe(map(response => {
          return response;
        }));
    }
  }

  getVisits(data, provider): any {
    const params = new HttpParams().set('identifier', data).set('provider', provider);
    return this.http.get(`http://192.168.2.27:4445/api/user/visits`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getObservations(data, provider): any {
    const params = new HttpParams().set('subject', data).set('provider', provider);
    return this.http.get(`http://192.168.2.27:4445/api/user/observations`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getPrescriptions(personId, provider): any {
    const params = new HttpParams().set('personId', personId).set('provider', provider);
    return this.http.get(`http://192.168.2.27:4445/prescriptions`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getNominees(data): Observable<any> {
    const params = new HttpParams().set('userId', data);
    return this.http.get(`http://192.168.2.27:4445/api/delegate/nominees`, { params })
      .pipe(map(response => response));
  }

  delegate(data): Observable<any> {
    return this.http.post(`http://192.168.2.27:4445/api/delegate/add`, data)
      .pipe(map(response => response));
  }

  getGrantor(data: any): Observable<any> {
    const params = new HttpParams().set('userId', data);
    return this.http.get(`http://192.168.2.27:4445/api/delegate/nominators`, { params })
      .pipe(map(response => response));
  }


  revokeDelegation(data): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: data
    };
    console.log("revoke", data);
    return this.http.delete(`http://192.168.2.27:4445/api/delegate/revoke`, httpOptions)
      .pipe(map(response => response));
  }

  grantAccess(data): Observable<any> {
    return this.http.post(`http://192.168.2.27:4445/api/delegate/grantAccess`, data)
      .pipe(map(response => response));
  }

  getAllocatedUsers(ssn, user): Observable<any> {
    const params = new HttpParams().set('userId', ssn).set('user', user);
    return this.http.get(`http://192.168.2.27:4445/api/user/allocated`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  manageAccess(data): Observable<any> {
    return this.http.post(`http://192.168.2.27:4445/api/ownership`, data)
      .pipe(map(response => {
        return response;
      }));
  }

  getProvidersOfUser(data): Observable<any> {
    console.log(data);
    let queryParams = '';
    if (data.ssn) {
      queryParams = `?userId=${data.ssn}`;
    }
    else if (data.address) {
      queryParams = `?address=${data.address}`;
    }

    return this.http.get(`http://192.168.2.27:4445/api/user/providerList` + queryParams)
      .pipe(map(response => {
        return response;
      }));
  }

  getDemographics(grantor, recipient, provider): Observable<any> {
    const params = new HttpParams().set('recipient', recipient).set('grantor', grantor).set('providerNo', provider)
    console.log(params);
    return this.http.get(`http://192.168.2.27:4445/api/user/grantor`, { params })
      .pipe(map(response => {
        return response;
      }));
  }

  getActivityLog(data) {
    console.log(data);
    let queryParams = `&address=${data}`;
    let accessGranted = this.http.get(`http://192.168.2.27:4445/api/history?type=accessGranted` + queryParams);
    let accessRevoked = this.http.get(`http://192.168.2.27:4445/api/history?type=accessRevoked` + queryParams);
    let nominationGranted = this.http.get(`http://192.168.2.27:4445/api/history?type=nominationGranted` + queryParams);
    let nominationRevoked = this.http.get(`http://192.168.2.27:4445/api/history?type=nominationRevoked` + queryParams);
    return forkJoin([accessGranted, accessRevoked, nominationGranted, nominationRevoked])
      .pipe(map(response => {
        console.log(response);
        return response;
      }));
  }

  postFile(fileToUpload): Observable<any> {
    let file = fileToUpload;
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${fileToUpload.name}`).put(fileToUpload);
    return Observable.create(observer => {
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          // upload in progress
          fileToUpload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        },
        (error) => {
          // upload failed
          console.log(error);
          observer.error(status);
        },
        () => {
          // upload success - new method > 5.x version of firebase
          uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            file.url = downloadURL;
            console.log(file);
            observer.next(file);
            observer.complete();
          });

        });
    });

  }

  uploadFile(data): Observable<any> {
    return this.http.post(`http://192.168.2.27:4445/api/user/upload`, data)
      .pipe(map(response => {
        return response;
      }));
  }

  updateProfile(userData): Observable<any> {
    console.log("calling update profile api in home service");
    let patientId = userData.patientResourceData.id;
    console.log(patientId);
    return this.http.put(`http://192.168.2.27:4445/api/user/${patientId}`,userData)
      .pipe(map(response => {
        return response;
      }))
  }



}
