import { Component, OnInit, Input } from '@angular/core';
import { Notification } from '../../models/notification';
import { NotificationApiProvider } from '../../providers/notification-api/notification-api';

/**
 * Generated class for the NotificationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'notifications',
  templateUrl: 'notifications.html'
})
export class NotificationsComponent implements OnInit{
  notifications: Notification[];
  count: number = 0;
  @Input() userDetails;
  constructor(private notificationProvider: NotificationApiProvider) {
    console.log('Hello NotificationsComponent Component');
    this.notifications = [];
  }

  ngOnInit() {
    this.getNotifications();
  }

  getNotifications(): void {
    console.log('user details');
    console.log(this.userDetails);
    this.notificationProvider.getNotifications(this.userDetails.userPublicAddress)
      .subscribe(response => {
        this.notifications = response['notifications'];
        this.count = response['count'];
        if(this.notifications.length){
          this.updateNotificationsRead(this.notifications );
        }
      },error => {
        console.log(error);
      });
  }

  updateNotificationsRead(notifications): void {
    this.notificationProvider.updateNotificationsRead(notifications)
      .subscribe(response => {console.log('notification read updated successfully')},
          error => {
        console.log(error);
      });
  }

}
