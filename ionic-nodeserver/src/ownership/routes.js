import express from "express";
import PATH from "path";
import controller from "./controller";
const router = express.Router();

router.post('/', async (req, res, next) => {
    try {
        const data = req.body;
        console.log(data);
        const transaction_result = await controller.manage(data);
        res.status(200).send(transaction_result);
    }
    catch (e) {
        res.status(500).send(transaction_result);
    }
});

module.exports = router;