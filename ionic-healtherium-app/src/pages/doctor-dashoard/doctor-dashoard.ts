import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, PopoverController, ModalController, Platform } from 'ionic-angular';
import { HealtheriumApi } from '../../providers/healtherium-api/healtherium-api';
import { NotificationsPage } from "../notifications/notifications";
import { NotificationApiProvider } from "../../providers/notification-api/notification-api";
import { DCOM_URL } from '../../config';
import { ModalContentPage } from '../modal-content/modal-content';
@Component({
  selector: 'page-doctor-dashoard',
  templateUrl: 'doctor-dashoard.html',
})
export class DoctorDashboardPage implements OnInit{
  showDetails: boolean;
  observationDetails: any;
  showPrescriptions: boolean;
  patientIdentifier(arg0: any): any {
    throw new Error("Method not implemented.");
  }

  public clinicalData: any = {};
  public myAngularxQrCode: string = null;
  public qrCodeRequestInfo: any = {};
  public scannedAddress: string;
  public patientDetails: any;
  public doctorAccessList: any = null;
  public emptydetails: string;
  selectedEmr: any;
  public loader;
  public showQRCode: boolean = false;
  public notificationsCount: number = 0;
  private selectedPatientAddress: any = null;
  getVisits: boolean;
  visitList: any[];
  user: any = {};
  credentials;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: HealtheriumApi,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public platform: Platform,
    private notificationProvider: NotificationApiProvider) {
    this.showPrescriptions = false;
    this.showQRCode = false;
    console.log(this.navParams.data);

    if (this.navParams.data) {
      this.user = this.navParams.data.response.user;
      this.selectedEmr = this.navParams.data.selectedOption;
      if (!this.navParams.data.isEmrData && this.navParams.data.response && this.navParams.data.response.clinicalData.body) {
        this.clinicalData.emrData = JSON.parse(this.navParams.data.response.clinicalData.body).entry[0].resource;
        this.clinicalData.department = this.navParams.data.response.clinicalData.department;
        this.clinicalData.hospital = this.navParams.data.response.clinicalData.hospital;
        this.credentials = this.navParams.data.credentials;
        this.qrCodeRequestInfo = this.credentials;
      }
    }
    console.log(this.clinicalData.emrData);
  }

  ngOnInit() {
    if (this.navParams.data.isEmrData) {
      this.selectedEmr = this.navParams.data.doctor.selectedOption;
      this.clinicalData.emrData = this.navParams.data.doctor.doctorData.emrData;
      this.clinicalData.department = this.navParams.data.doctor.doctorData.department;
      this.clinicalData.hospital = this.navParams.data.doctor.doctorData.hospital;
      this.user = this.navParams.data.doctor.user;
      this.patientDetails = this.navParams.data.patientDetails;
      this.selectedPatientAddress = this.patientDetails.selectedPatientAddress;
      this.credentials = this.navParams.data.doctor.credentials;
      this.qrCodeRequestInfo = this.credentials;
      this.doctorAccessList = this.navParams.data.doctor.patients;
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorDashboardPage');
    this.showQRCode = false;
    this.showDetails = false;
    this.getVisits = false;
    document.getElementById("qrContainer").style.display = "none";
    this.getNotificationsCount();
  }

  //generate QR code
  getDoctorAddress(): void {
    this.createLoader();
    this.showQRCode = true;
    document.getElementById("qrContainer").style.display = "block";
    this.myAngularxQrCode = this.navParams.data.response.user ? this.navParams.data.response.user.account : this.navParams.data.doctor.user.account;
    this.loader.dismiss();
  }

  //show alert box
  presentAlert(info) {
    let alert = this.alertCtrl.create({
      title: info.title,
      subTitle: info.subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }

  //get shared patient data
  getPatientList(): void {
    this.createLoader();
    this.doctorAccessList = null;
    this.patientDetails = null;
    this.showDetails = false;
    this.getVisits = false;
    this.showPrescriptions = false;
    this.api.getAllocatedUsers(this.credentials.ssn).subscribe(response => {
      this.loader.dismiss();
      console.log(response);
      if (response && response && response.length) {
        this.doctorAccessList = response;
      }
      else {
        let info = {
          title: "No Data",
          subTitle: "No Patient Has Shared Data"
        }
        this.presentAlert(info);
      }

    },
      error => {
        this.loader.dismiss();
        console.error('Oops:', error);
        let info = {
          title: "Error",
          subTitle: error.error.message
        }
        this.presentAlert(info);
      });
  }

  getPatientInfo(address): void {
    console.log("get patient info");
    const self = this;
    let userAddress = this.doctorAccessList.find(function (user, index) {
      console.log('element' + user);
      return user.Account.toLowerCase() === address.toLowerCase();
    });
    console.log(userAddress);
    this.selectedPatientAddress = "0x" + userAddress.Account;
    console.log(this.selectedPatientAddress);
    var doctorInfo = this.credentials;
    this.createLoader();
    this.patientDetails = null;
    this.showDetails = false;
    this.getVisits = false;
    this.showPrescriptions = false;
    this.api.getProvidersOfUser({ address: this.selectedPatientAddress }).subscribe(response => {
      console.log(response);
      this.loader.dismiss();
      let modal = this.modalCtrl.create(ModalContentPage, { patients: this.doctorAccessList, type: "otp", doctorData: this.clinicalData, user: this.user, chooseProvider: response, selectedPatientAddress: this.selectedPatientAddress, userAccount: this.user.account, action: "multipleEmr", credentials: this.credentials });
      modal.present();
    },
      error => {
        this.loader.dismiss();
        console.error('Oops:', error);
        let info = {
          title: "Error",
          subTitle: error.error.message
          //subTitle: "Oops. Something went wrong. Please try again after some time"
        }
        this.presentAlert(info);
      });
  }

  getPatientVisits(): void {
    this.getVisits = true;
    this.visitList = [];
    this.api.getVisits(this.patientDetails.identifier[0].value, this.selectedEmr.number)
      .subscribe(response => {
        let visits = response.entry;
        if (visits && visits.length)
          visits.forEach(visit => {
            visit.resource.period.start = new Date(visit.resource.period.start);
            visit.resource.period.end = new Date(visit.resource.period.end);
            this.visitList.push(visit.resource);
          })
      },
        error => {
          this.loader.dismiss();
          let info = {
            title: "Error",
            subTitle: error.message
          };
          this.presentAlert(info);
        });
  }

  getPatientHistory(): void {
    let self = this;
    this.showDetails = true
    this.observationDetails = null;
    this.api.getObservations(this.patientDetails.id, this.selectedEmr.number)
      .subscribe(
        response => {
          console.log("response", response);
          self.observationDetails = response.entry;
        },
        error => {
          self.loader.dismiss();
          let info = {
            title: "Error",
            subTitle: error.message
          };
          self.presentAlert(info);
        });
  }

  showPrescriptionView(): void {
    this.showPrescriptions = true;
  }


  getPatientFile(): void {
    var self = this;
    self.createLoader();
    const data = {
      user: this.credentials.user,
      grantor: this.selectedPatientAddress,
      recipient: this.user.account,
      provider:this.navParams.data.doctor.selectedOption.number // this is patient provider number from which doctor needs to access file of the patient
    };
    self.api.getFile(data).subscribe(response => {
      self.loader.dismiss();
      console.log(response);
      window.open(`${DCOM_URL}?input=`+encodeURIComponent(response.data),"_blank");
    },
      error => {
        self.loader.dismiss();
        let info = {
          title: "Error",
          subTitle: error.error.message
        };
        self.presentAlert(info);
      });
  }

  createLoader(): void {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

  getNotificationsCount(): void {
    this.notificationProvider.getNotificationsCount(this.clinicalData.userPublicAddress)
      .subscribe(response => {
        this.notificationsCount = response['count'];
      }, error => {
        console.log(error);
      });
  }

  presentNotifications(myEvent) {
    if (this.platform.is('iphone') || this.platform.is('mobile')) {
      const modal = this.modalCtrl.create(NotificationsPage, { userDetails: this.clinicalData });
      modal.present();
      modal.onDidDismiss(() => { this.notificationsCount = 0 });
    }
    else {
      let popover = this.popoverCtrl.create(NotificationsPage, { userDetails: this.clinicalData }, { cssClass: 'contact-popover' });
      popover.present({
        ev: myEvent
      });
      popover.onDidDismiss(() => { this.notificationsCount = 0 });
    }
  }

};
