import { HomeService } from './../../home.service';
import { AuthenticationService } from './../../../authentication/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalComponent } from 'src/app/modal/modal.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-delegate',
  templateUrl: './delegate.component.html',
  styleUrls: ['./delegate.component.scss']
})
export class DelegateComponent implements OnInit {

  nominees: any;
  user: any
  givers: any;
  grantors: any;
  loading:boolean = false;
  dialogRef: MatDialogRef<ModalComponent, any>;
  constructor(
    private authService: AuthenticationService,
    private homeService: HomeService,
    private toastr: ToastrService,
    public dialog: MatDialog) { }

  ngOnInit() {
    let user = this.authService.userProfile;
    //temporary
    this.user = user;
    this.loading = true;
    this.getNominees(user.creds.ssn)
    this.getGrantor(user.creds.ssn);
    if(this.dialogRef) this.dialogRef.close();
  }

  getNominees(user = null): void {
    this.loading = true;
    this.homeService.getNominees(user).subscribe(response => {
      console.log("response", response);
      this.nominees = response;
      this.loading = false;
    },
      error => {
        this.loading = false;
        this.toastr.error("Unable To Get Nominees", "Please try again after sometime.");
        console.log("error", error);
      });
  }

  getGrantor(user = null): void {
    this.loading = true;
    this.homeService.getGrantor(user).subscribe(response => {
      console.log("response", response);
      this.grantors = response;
      this.loading = false;
    },
      error => {
        this.loading = false;
        this.toastr.error("Unable To Get Granted Users", "Please try again after sometime.");
        console.log("error", error);
      });
  }

  delegate(user, nominee): void {
    if(this.dialogRef) this.dialogRef.close();
    this.loading = true;
    const data = { user_id: this.user.creds.ssn, nominee_id: nominee }
    this.homeService.delegate(data).subscribe(response => {
      console.log("response", response);
      this.nominees = response;
      this.loading = false;
      if(this.dialogRef) this.dialogRef.close();
    },
      error => {
        this.loading = false;
        if(this.dialogRef) this.dialogRef.close();
        this.toastr.error("Unable to Delegate User", "Please try again after sometime.");
        console.log("error", error);
      });
  }

  revoke(nominee): void {
    if(this.dialogRef) this.dialogRef.close();
    this.loading = true;
    const data = { user_id: this.user.creds.ssn, nominee_id: nominee }
    this.homeService.revokeDelegation(data).subscribe(response => {
      console.log("response", response);
      this.nominees = response;
      this.loading = false;
    },
      error => {
        this.loading = false;
        this.toastr.error("Unable to Revoke User", "Please try again after sometime.");
        console.log("error", error);
      });
  }

  manageDoctorAccess(value, doctor): void {
    this.loading = true;
    let data = {
      user_id: this.user.creds.ssn,
      user_name: this.user.creds.name,
      nominator_id: value.grantor.Adhaar,
      doctorAddress: doctor,
      grantAccess: value.mode
    }
    this.loading = true;
    this.homeService.grantAccess(data)
      .subscribe(response => {
        console.log("response", response);
        this.loading = false;
        if (value.mode) {
          this.loading = false;
          if(this.dialogRef) this.dialogRef.close();
          // info = {
          //   title: "Success",
          //   subTitle: "Access shared with doctor for " + nominator.name
          // }

        }
        else {
          this.loading = false;
          if(this.dialogRef) this.dialogRef.close();
          // info = {
          //   title: "Success",
          //   subTitle: "Access revoked from doctorfor " + nominator.name
          // }
        }
      },
        error => {
          // let info = {
          //   title: "Error",
          //   subTitle: "Oops!!! Something went wrong. Please try again after sometime"
          // }
          console.log("error", error);
          if(this.dialogRef) this.dialogRef.close();
          this.toastr.error("Unable to Perform Operation On Behalf of User", "Please try again after sometime.");
        });
  }

  openModal(data) {
    this.dialogRef = this.dialog.open(ModalComponent, { data });
    const onDelegate = this.dialogRef.componentInstance.onDelegate.subscribe((nominee) => {
      this.delegate(this.user.creds.ssn, nominee)
    })
    const onDoctorDelegation = this.dialogRef.componentInstance.onDoctorDelegation.subscribe((doctor) => {
      console.log(data, doctor);
      this.manageDoctorAccess(data, doctor)
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      onDelegate.unsubscribe();
      onDoctorDelegation.unsubscribe();
    });
  }

}
