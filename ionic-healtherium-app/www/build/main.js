webpackJsonp([0],{

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__notifications_notifications__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__prescription_prescription__ = __webpack_require__(679);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__delegation_delegation__ = __webpack_require__(690);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__notifications_notifications__["a" /* NotificationsComponent */],
                __WEBPACK_IMPORTED_MODULE_3__prescription_prescription__["a" /* PrescriptionComponent */],
                __WEBPACK_IMPORTED_MODULE_4__delegation_delegation__["a" /* DelegationComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_1__notifications_notifications__["a" /* NotificationsComponent */],
                __WEBPACK_IMPORTED_MODULE_3__prescription_prescription__["a" /* PrescriptionComponent */],
                __WEBPACK_IMPORTED_MODULE_4__delegation_delegation__["a" /* DelegationComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
/*
  Generated class for the NotificationApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var NotificationApiProvider = /** @class */ (function () {
    function NotificationApiProvider(http) {
        this.http = http;
        this.notificationsUrl = __WEBPACK_IMPORTED_MODULE_2__config__["a" /* BASE_URL */] + '/api/notifications';
        console.log('Hello NotificationApiProvider Provider');
    }
    NotificationApiProvider.prototype.getNotifications = function (userId) {
        return this.http.get(this.notificationsUrl + ("?userId=" + userId)).pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["catchError"])(function (err) {
            return __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].throw(err);
        }));
    };
    NotificationApiProvider.prototype.getNotificationsCount = function (userId) {
        return this.http.get(this.notificationsUrl + ("?userId=" + userId + "&field=count")).pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["catchError"])(function (err) {
            return __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].throw(err);
        }));
    };
    NotificationApiProvider.prototype.updateNotificationsRead = function (notifications) {
        var notificationIds = [];
        for (var _i = 0, notifications_1 = notifications; _i < notifications_1.length; _i++) {
            var notification = notifications_1[_i];
            notificationIds.push(notification.id);
        }
        return this.http.post(this.notificationsUrl, { notifications: notificationIds }, httpOptions).pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["catchError"])(function (err) {
            return __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].throw(err);
        }));
    };
    NotificationApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], NotificationApiProvider);
    return NotificationApiProvider;
}());

//# sourceMappingURL=notification-api.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_healtherium_api_healtherium_api__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_content_modal_content__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegistrationPage = /** @class */ (function () {
    function RegistrationPage(navCtrl, navParams, alertCtrl, api, loadingCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.api = api;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.user = {};
        if (this.navParams.data && this.navParams.data.user) {
            switch (this.navParams.data.user) {
                case "patient":
                    this.userType = "patient";
                    break;
                case "doctor":
                    this.userType = "doctor";
                    break;
                case "research":
                    this.user.type = "researcher";
                    break;
                case "insurance":
                    this.userType = "insurance";
                    break;
                case "buyer":
                    this.userType = "buyer";
                    break;
                default:
                    this.userType = "patient";
                    break;
            }
        }
    }
    RegistrationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PatientSignupPage');
    };
    RegistrationPage.prototype.ngOnInit = function () {
        this.getProviders();
    };
    RegistrationPage.prototype.getProviders = function () {
        var _this = this;
        this.api.getProviders()
            .subscribe(function (response) {
            _this.providerList = response['providers'];
            _this.selectedProvider = _this.providerList[0];
        }, function (error) {
            console.log(error);
        });
    };
    RegistrationPage.prototype.submitInfo = function () {
        var _this = this;
        this.user.user = this.userType;
        this.user.isRegistered = false;
        this.user.provider = this.selectedProvider.providerNumber;
        console.log(this.user);
        this.createLoader();
        console.log(this.user);
        this.api.generateOTP(this.user).subscribe(function (response) {
            if (response) {
                console.log(response);
                _this.loader.dismiss();
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_content_modal_content__["a" /* ModalContentPage */], { type: "otp", data: _this.user, action: "registration" });
                modal.present();
                console.log(response);
            }
        }, function (error) {
            _this.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: error.error.message,
            };
            _this.presentAlert(info);
        });
    };
    RegistrationPage.prototype.createLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    RegistrationPage.prototype.presentAlert = function (info) {
        var alert = this.alertCtrl.create({
            title: info.title,
            subTitle: info.subTitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    RegistrationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'registration',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/registration/registration.html"*/'<!--\n  Generated template for the PatientSignupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>{{userType.toUpperCase()}} REGISTRATION</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding style="background: none transparent;">\n  <ion-row>\n    <ion-col col-12>\n      <form (ngSubmit)="submitInfo()">\n        <ion-card>\n          <ion-row>\n            <ion-col col-12>\n              <ion-row padding margin>\n                <ion-header padding>{{userType.toUpperCase()}} PROFILE</ion-header>\n              </ion-row>\n              <ion-row>\n                  <ion-col col-12>User name</ion-col>\n                  <ion-col col-12>\n                    <ion-input [(ngModel)]="user.userName" name ="userName" placeholder="User name"></ion-input>\n                  </ion-col>\n                </ion-row>\n              <ion-row>\n                <ion-col col-12>First name</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.firstName" name="firstName" placeholder="First name"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Family name</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.familyName" name="familyName" placeholder="Family name"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Gender</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.gender" name="gender" placeholder="Gender"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Date of Birth (YYYY-MM-DD)</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.dob" name="dob" placeholder="Date of Birth"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Address Line 1</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.addressLine1" name="addressLine1" placeholder="Address Line 1"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Address Line 2</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.addressLine2" name="addressLine2" placeholder="Address Line 2"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row *ngIf="userType === \'doctor\'">\n                <ion-col col-12>Department</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.department" name ="department" placeholder="Department"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row *ngIf="userType === \'doctor\'">\n                <ion-col col-12>Hospital</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.hospital" name ="hospital" placeholder="Hospital"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col col-12>Phone Number</ion-col>\n                  <ion-col col-12>\n                    <ion-input [(ngModel)]="user.phoneNumber" name ="phoneNumber" placeholder="Contact Address"></ion-input>\n                  </ion-col>\n                </ion-row>\n              <ion-row>\n                <ion-col col-12>City</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.city" name="city" placeholder="City"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>State</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.state" name="state" placeholder="State"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Area Code</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.areaCode" name="areaCode" placeholder="Area Code"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Country</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.country" name="country" placeholder="Country"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Secure Number/Aadhar</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.ssn" name="ssn" placeholder="Secure Number/Aadhar"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Password</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.password" name="password" type="password" placeholder="Password"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Provider</ion-col>\n                <ion-col col-12>\n                  <ion-select [(ngModel)]="selectedProvider" name="provider">\n                    <ion-option *ngFor="let provider of providerList" [value]="provider">{{provider.providerName}}</ion-option>\n                  </ion-select>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>Emr Number</ion-col>\n                <ion-col col-12>\n                  <ion-input [(ngModel)]="user.emrNumber" name="emrnumber" placeholder="Emr Number"></ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-12>\n                  <button type="submit" ion-button>Sign Up</button>\n                </ion-col>\n                <ion-col col-12></ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </form>\n    </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/registration/registration.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_healtherium_api_healtherium_api__["a" /* HealtheriumApi */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], RegistrationPage);
    return RegistrationPage;
}());

//# sourceMappingURL=registration.js.map

/***/ }),

/***/ 175:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 175;

/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/notifications/notifications.module": [
		220
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 219;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsPageModule", function() { return NotificationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notifications__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(119);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NotificationsPageModule = /** @class */ (function () {
    function NotificationsPageModule() {
    }
    NotificationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
            entryComponents: []
        })
    ], NotificationsPageModule);
    return NotificationsPageModule;
}());

//# sourceMappingURL=notifications.module.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(viewCtrl, params) {
        this.viewCtrl = viewCtrl;
        console.log(params.get('userDetails'));
        this.userDetails = params.get('userDetails');
    }
    NotificationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationsPage');
    };
    NotificationsPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    NotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notifications',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/notifications/notifications.html"*/'<!--\n  Generated template for the NotificationsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Notifications</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding-top>\n  <notifications [userDetails] = "userDetails"></notifications>\n</ion-content>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/notifications/notifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], NotificationsPage);
    return NotificationsPage;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DelegationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the DelegationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DelegationProvider = /** @class */ (function () {
    function DelegationProvider(http) {
        this.http = http;
        console.log('Hello DelegationProvider Provider');
    }
    DelegationProvider.prototype.delegate = function (data) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__config__["a" /* BASE_URL */] + "/api/delegate/add", data)
            .map(function (response) {
            return response;
        });
    };
    DelegationProvider.prototype.getDelegationInfo = function (data) {
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('userId', data);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__config__["a" /* BASE_URL */] + "/api/delegate/nominees", { params: params })
            .map(function (response) { return response; });
    };
    DelegationProvider.prototype.getDelegators = function (data) {
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('userId', data);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__config__["a" /* BASE_URL */] + "/api/delegate/nominators", { params: params })
            .map(function (response) { return response; });
    };
    DelegationProvider.prototype.revokeDelegation = function (data) {
        var httpOptions = {
            headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' }), body: data
        };
        console.log("revoke", data);
        return this.http.delete(__WEBPACK_IMPORTED_MODULE_2__config__["a" /* BASE_URL */] + "/api/delegate/revoke", httpOptions)
            .map(function (response) { return response; });
    };
    DelegationProvider.prototype.grantAccess = function (data) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__config__["a" /* BASE_URL */] + "/api/delegate/grantAccess", data)
            .map(function (response) {
            return response;
        });
    };
    DelegationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], DelegationProvider);
    return DelegationProvider;
}());

//# sourceMappingURL=delegation.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoctorDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_healtherium_api_healtherium_api__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__notifications_notifications__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notification_api_notification_api__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modal_content_modal_content__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DoctorDashboardPage = /** @class */ (function () {
    function DoctorDashboardPage(navCtrl, navParams, api, alertCtrl, loadingCtrl, popoverCtrl, modalCtrl, platform, notificationProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.notificationProvider = notificationProvider;
        this.clinicalData = {};
        this.myAngularxQrCode = null;
        this.qrCodeRequestInfo = {};
        this.doctorAccessList = null;
        this.showQRCode = false;
        this.notificationsCount = 0;
        this.selectedPatientAddress = null;
        this.user = {};
        this.showPrescriptions = false;
        this.showQRCode = false;
        console.log(this.navParams.data);
        if (this.navParams.data) {
            this.user = this.navParams.data.response.user;
            this.selectedEmr = this.navParams.data.selectedOption;
            if (!this.navParams.data.isEmrData && this.navParams.data.response && this.navParams.data.response.clinicalData.body) {
                this.clinicalData.emrData = JSON.parse(this.navParams.data.response.clinicalData.body).entry[0].resource;
                this.clinicalData.department = this.navParams.data.response.clinicalData.department;
                this.clinicalData.hospital = this.navParams.data.response.clinicalData.hospital;
                this.credentials = this.navParams.data.credentials;
                this.qrCodeRequestInfo = this.credentials;
            }
        }
        console.log(this.clinicalData.emrData);
    }
    DoctorDashboardPage.prototype.patientIdentifier = function (arg0) {
        throw new Error("Method not implemented.");
    };
    DoctorDashboardPage.prototype.ngOnInit = function () {
        if (this.navParams.data.isEmrData) {
            this.selectedEmr = this.navParams.data.doctor.selectedOption;
            this.clinicalData.emrData = this.navParams.data.doctor.doctorData.emrData;
            this.clinicalData.department = this.navParams.data.doctor.doctorData.department;
            this.clinicalData.hospital = this.navParams.data.doctor.doctorData.hospital;
            this.user = this.navParams.data.doctor.user;
            this.patientDetails = this.navParams.data.patientDetails;
            this.selectedPatientAddress = this.patientDetails.selectedPatientAddress;
            this.credentials = this.navParams.data.doctor.credentials;
            this.qrCodeRequestInfo = this.credentials;
            this.doctorAccessList = this.navParams.data.doctor.patients;
        }
    };
    DoctorDashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DoctorDashboardPage');
        this.showQRCode = false;
        this.showDetails = false;
        this.getVisits = false;
        document.getElementById("qrContainer").style.display = "none";
        this.getNotificationsCount();
    };
    //generate QR code
    DoctorDashboardPage.prototype.getDoctorAddress = function () {
        this.createLoader();
        this.showQRCode = true;
        document.getElementById("qrContainer").style.display = "block";
        this.myAngularxQrCode = this.navParams.data.response.user ? this.navParams.data.response.user.account : this.navParams.data.doctor.user.account;
        this.loader.dismiss();
    };
    //show alert box
    DoctorDashboardPage.prototype.presentAlert = function (info) {
        var alert = this.alertCtrl.create({
            title: info.title,
            subTitle: info.subTitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    //get shared patient data
    DoctorDashboardPage.prototype.getPatientList = function () {
        var _this = this;
        this.createLoader();
        this.doctorAccessList = null;
        this.patientDetails = null;
        this.showDetails = false;
        this.getVisits = false;
        this.showPrescriptions = false;
        this.api.getAllocatedUsers(this.credentials.ssn).subscribe(function (response) {
            _this.loader.dismiss();
            console.log(response);
            if (response && response && response.length) {
                _this.doctorAccessList = response;
            }
            else {
                var info = {
                    title: "No Data",
                    subTitle: "No Patient Has Shared Data"
                };
                _this.presentAlert(info);
            }
        }, function (error) {
            _this.loader.dismiss();
            console.error('Oops:', error);
            var info = {
                title: "Error",
                subTitle: error.error.message
            };
            _this.presentAlert(info);
        });
    };
    DoctorDashboardPage.prototype.getPatientInfo = function (address) {
        var _this = this;
        console.log("get patient info");
        var self = this;
        var userAddress = this.doctorAccessList.find(function (user, index) {
            console.log('element' + user);
            return user.Account.toLowerCase() === address.toLowerCase();
        });
        console.log(userAddress);
        this.selectedPatientAddress = "0x" + userAddress.Account;
        console.log(this.selectedPatientAddress);
        var doctorInfo = this.credentials;
        this.createLoader();
        this.patientDetails = null;
        this.showDetails = false;
        this.getVisits = false;
        this.showPrescriptions = false;
        this.api.getProvidersOfUser({ address: this.selectedPatientAddress }).subscribe(function (response) {
            console.log(response);
            _this.loader.dismiss();
            var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modal_content_modal_content__["a" /* ModalContentPage */], { patients: _this.doctorAccessList, type: "otp", doctorData: _this.clinicalData, user: _this.user, chooseProvider: response, selectedPatientAddress: _this.selectedPatientAddress, userAccount: _this.user.account, action: "multipleEmr", credentials: _this.credentials });
            modal.present();
        }, function (error) {
            _this.loader.dismiss();
            console.error('Oops:', error);
            var info = {
                title: "Error",
                subTitle: error.error.message
                //subTitle: "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        });
    };
    DoctorDashboardPage.prototype.getPatientVisits = function () {
        var _this = this;
        this.getVisits = true;
        this.visitList = [];
        this.api.getVisits(this.patientDetails.identifier[0].value, this.selectedEmr.number)
            .subscribe(function (response) {
            var visits = response.entry;
            if (visits && visits.length)
                visits.forEach(function (visit) {
                    visit.resource.period.start = new Date(visit.resource.period.start);
                    visit.resource.period.end = new Date(visit.resource.period.end);
                    _this.visitList.push(visit.resource);
                });
        }, function (error) {
            _this.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: error.message
            };
            _this.presentAlert(info);
        });
    };
    DoctorDashboardPage.prototype.getPatientHistory = function () {
        var self = this;
        this.showDetails = true;
        this.observationDetails = null;
        this.api.getObservations(this.patientDetails.id, this.selectedEmr.number)
            .subscribe(function (response) {
            console.log("response", response);
            self.observationDetails = response.entry;
        }, function (error) {
            self.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: error.message
            };
            self.presentAlert(info);
        });
    };
    DoctorDashboardPage.prototype.showPrescriptionView = function () {
        this.showPrescriptions = true;
    };
    DoctorDashboardPage.prototype.getPatientFile = function () {
        var self = this;
        self.createLoader();
        var data = {
            user: this.credentials.user,
            grantor: this.selectedPatientAddress,
            recipient: this.user.account,
            provider: this.navParams.data.doctor.selectedOption.number // this is patient provider number from which doctor needs to access file of the patient
        };
        self.api.getFile(data).subscribe(function (response) {
            self.loader.dismiss();
            console.log(response);
            window.open(__WEBPACK_IMPORTED_MODULE_5__config__["b" /* DCOM_URL */] + "?input=" + encodeURIComponent(response.data), "_blank");
        }, function (error) {
            self.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: error.error.message
            };
            self.presentAlert(info);
        });
    };
    DoctorDashboardPage.prototype.createLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    DoctorDashboardPage.prototype.getNotificationsCount = function () {
        var _this = this;
        this.notificationProvider.getNotificationsCount(this.clinicalData.userPublicAddress)
            .subscribe(function (response) {
            _this.notificationsCount = response['count'];
        }, function (error) {
            console.log(error);
        });
    };
    DoctorDashboardPage.prototype.presentNotifications = function (myEvent) {
        var _this = this;
        if (this.platform.is('iphone') || this.platform.is('mobile')) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__notifications_notifications__["a" /* NotificationsPage */], { userDetails: this.clinicalData });
            modal.present();
            modal.onDidDismiss(function () { _this.notificationsCount = 0; });
        }
        else {
            var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__notifications_notifications__["a" /* NotificationsPage */], { userDetails: this.clinicalData }, { cssClass: 'contact-popover' });
            popover.present({
                ev: myEvent
            });
            popover.onDidDismiss(function () { _this.notificationsCount = 0; });
        }
    };
    DoctorDashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-doctor-dashoard',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/doctor-dashoard/doctor-dashoard.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Doctor Dashoard</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="presentNotifications($event)">\n        <ion-icon name="notifications"></ion-icon>\n        <ion-badge *ngIf="notificationsCount > 0" style="position: absolute;background: none;color: white; top: 0px;left: 14px" >{{notificationsCount}}</ion-badge>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Doctor Profile</label>\n              </ion-header>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">First name</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.name[0].given[0]}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Family name</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.name[0].family}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Gender</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.gender}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Date of Birth</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.birthDate}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Address Line 1</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].line[0]}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Address Line 2</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].line[1]}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">City</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].city}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">State</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].state}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Area Code</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].postalCode}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Country</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].country}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Department</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.department}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Hospital</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.hospital}}</ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      <ion-card style="margin-top: 25px;">\n        <ion-row>\n          <ion-col col-12>\n            <ion-row margin>\n              <ion-header padding>\n                <label>Generate QR Code</label>\n              </ion-header>\n            </ion-row>\n            <ion-row style="padding-top:50px">\n              <ion-col col-12>\n                <div style="max-width: 60%">Click here to generate QR Code to provide access to the patients</div>\n                <button style="margin-top: -40px;" ion-button color="primary" (click)="getDoctorAddress()" class="qr-code-button">Generate</button>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col id="qrContainer" col-12 class="qr-code" ng-show="showQRCode">\n                <qrcode ng-show="showQRCode" [qrdata]="myAngularxQrCode" [size]="50" [level]="\'M\'"></qrcode>\n              </ion-col>\n              <ion-col col-12 style="margin-top: 20px;text-align: center;">\n                {{myAngularxQrCode}}\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Get Patient Details</label>\n                <button ion-button color="default" class="pull-right" (click)="getPatientList()">Get Patient List</button>\n              </ion-header>\n            </ion-row>\n\n            <ion-list *ngIf="doctorAccessList">\n              <ion-item text-wrap *ngFor="let user of doctorAccessList">\n                <div>\n                  {{user.name}}\n                  <button ion-button clear="true" color="default" class="pull-right" (click)="getPatientInfo(user.Account)">View\n                    Details\n                  </button>\n                </div>\n\n              </ion-item>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      <ion-card *ngIf="patientDetails">\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label> Patient Details</label>\n                <button ion-button color="default" class="pull-right" (click)="getPatientFile()">Get File</button>\n              </ion-header>\n            </ion-row>\n\n\n            <ion-row style="padding-top: 10px;">\n              <ion-col col-12>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">First name</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.name[0].given[0]}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Family name</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.name[0].family}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Gender</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.gender}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Date of Birth</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.birthDate}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Address Line 1</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.address[0].line[0]}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Address Line 2</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.address[0].line[1]}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">City</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.address[0].city}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">State</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.address[0].state}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Area Code</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.address[0].postalCode}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Country</ion-col>\n                  <ion-col col-12 col-md-6>{{patientDetails.address[0].country}}</ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Get Visit Details</ion-col>\n                  <ion-col col-12 col-md-6>\n                    <button style="padding: 0; height: auto;font-size: 1.2rem;" ion-button clear="true" color="default"  (click)="getPatientVisits(patientDetails)">Patient\n                      Visits</button>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">Get History</ion-col>\n                  <ion-col col-12 col-md-6>\n                    <button style="padding: 0; height: auto;font-size: 1.2rem;" ion-button clear="true" color="default" (click)="getPatientHistory()">Patient\n                      History</button>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col col-12 col-md-6 class="ionic-label">View Prescriptions</ion-col>\n                  <ion-col col-12 col-md-6>\n                    <button style="padding: 0; height: auto;font-size: 1.2rem;" ion-button clear="true" color="default" (click)="showPrescriptionView()">Prescriptions</button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf="getVisits">\n    <ion-col col-12>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Patient consultation history</label>\n              </ion-header>\n            </ion-row>\n            <ion-row *ngIf="getVisits" padding>\n              <ion-col col-12 *ngIf="!visitList || visitList.length == 0;else visits">\n                No records available on the patient visits.\n              </ion-col>\n              <ng-template #visits>\n                <ion-col col-3>\n                  <b>Start Date</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>End Date</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>Location</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>Consulted with:</b>\n                </ion-col>\n                <ion-row padding *ngFor="let visit of visitList; let i=index" style="border-bottom: 1px solid lightgray; padding-bottom:5px; width: 100%;">\n                  <ion-col col-3>\n                    {{visit.period.start}}\n                  </ion-col>\n                  <ion-col col-3>\n                    {{visit.period.end}}\n                  </ion-col>\n                  <ion-col col-3>\n                    {{visit.location && visit.location.length ? visit.location[0].location.display: \'\'}}\n                  </ion-col>\n                  <ion-col col-3>\n                    {{visit.participant && visit.participant.length ? visit.participant[0].individual.display.split(\'(\')[0]: \'\'}}\n                  </ion-col>\n                </ion-row>\n              </ng-template>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf="showDetails">\n    <ion-col col-12>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Get Patient Medical Treatment History</label>\n              </ion-header>\n            </ion-row>\n            <ion-col col-12 *ngIf="!observationDetails || observationDetails.length == 0">\n              <div style="padding: 10px 16px 0px 16px;">\n                No history data available.\n              </div>\n            </ion-col>\n            <ion-col col-12 *ngIf="observationDetails && observationDetails.length > 0">\n              <ion-row>\n                <ion-col col-2>\n                  <b>ACTIVITY</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>ANALYSIS</b>\n                </ion-col>\n                <ion-col col-2>\n                  <b>RESULTS</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>COMMENTS</b>\n                </ion-col>\n                <ion-col col-2>\n                  <b>DATE</b>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <ion-col col-12 *ngFor="let obs of observationDetails; let i=index;">\n              <ion-row>\n                <ion-col col-2>\n                  {{obs.resource.resourceType}}\n                </ion-col>\n                <ion-col col-3>\n                  <div *ngIf="obs.resource && obs.resource.code && obs.resource.code.coding && obs.resource.code.coding.length">\n                    <ion-col col-12>\n                      {{obs.resource.code.coding[obs.resource.code.coding.length-1].display}}\n                    </ion-col>\n                  </div>\n                </ion-col>\n                <ion-col col-2>\n                  <div *ngIf="obs.resource && obs.resource.valueCodeableConcept && obs.resource.valueCodeableConcept.coding && obs.resource.valueCodeableConcept.coding.length">\n                    <ion-col col-12>\n                      {{obs.resource.valueCodeableConcept.coding[obs.resource.valueCodeableConcept.coding.length-1].display}}\n                    </ion-col>\n                    </div>\n                  <div *ngIf="obs.resource && obs.resource.valueQuantity">\n                    <ion-col col-12>\n                      {{obs.resource.valueQuantity.value}} {{obs.resource.valueQuantity.unit}}\n                    </ion-col>\n                  </div>\n                </ion-col>\n                <ion-col col-3>\n                  <div>{{obs.resource.comment}}</div>\n                </ion-col>\n                <ion-col col-2>\n                  <div>{{obs.resource.effectiveDateTime}}</div>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <prescription *ngIf="showPrescriptions && patientDetails" [userDetails] = "patientDetails" [isDoctor]="true" [selectedEmr] = "selectedEmr"></prescription>\n</ion-content>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/doctor-dashoard/doctor-dashoard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_healtherium_api_healtherium_api__["a" /* HealtheriumApi */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__providers_notification_api_notification_api__["a" /* NotificationApiProvider */]])
    ], DoctorDashboardPage);
    return DoctorDashboardPage;
}());

;
//# sourceMappingURL=doctor-dashoard.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PatientDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_healtherium_api_healtherium_api__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_qr_scanner__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { File } from '@ionic-native/file';
// import { FileChooser } from '@ionic-native/file-chooser';
var PatientDashboardPage = /** @class */ (function () {
    function PatientDashboardPage(navCtrl, navParams, qrScanner, api, loadingCtrl, alertCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.qrScanner = qrScanner;
        this.api = api;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.visitList = [];
        this.clinicalData = {};
        this.scannedAddress = "";
        this.addressToRevoke = "";
        this.isApp = true;
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isApp = false;
        }
        else {
            this.isApp = true;
        }
        console.log(this.navParams.data);
        this.credentials = this.navParams.data.credentials;
        this.user = this.navParams.data.response.user;
        this.selectedEmr = this.credentials.emrDetails;
        if (this.navParams.data && this.navParams.data.response && this.navParams.data.response.clinicalData.body) {
            this.clinicalData.emrData = JSON.parse(this.navParams.data.response.clinicalData.body).entry[0].resource;
        }
        console.log(this.clinicalData.emrData);
    }
    PatientDashboardPage.prototype.ionViewDidLoad = function () {
        this.showDetails = false;
        console.log('ionViewDidLoad PatientDashboardPage');
    };
    PatientDashboardPage.prototype.scanQRCode = function (grant) {
        var _this = this;
        console.log("hello I am here");
        console.log(this.qrScanner);
        var ionApp = document.getElementsByTagName("ion-app")[0];
        this.qrScanner.prepare()
            .then(function (status) {
            if (status.authorized) {
                // camera permission was granted
                // start scanning
                var scanSub_1 = _this.qrScanner.scan().subscribe(function (text) {
                    console.log('Scanned something', text);
                    if (grant) {
                        _this.addressToRevoke = "";
                        _this.scannedAddress = text;
                    }
                    else {
                        _this.scannedAddress = "";
                        _this.addressToRevoke = text;
                    }
                    _this.qrScanner.hide(); // hide camera preview
                    ionApp.style.display = "block";
                    scanSub_1.unsubscribe(); // stop scanning
                });
                // show camera preview
                ionApp.style.display = "none";
                _this.qrScanner.show();
            }
            else if (status.denied) {
                // camera permission was permanently denied
                // you must use QRScanner.openSettings() method to guide the user to the settings page
                // then they can grant the permission from there
            }
            else {
                // permission was denied, but not permanently. You can ask for permission again at a later time.
            }
        })
            .catch(function (e) {
            console.log('Error is', e);
            if (grant) {
                _this.scannedAddress = "";
                _this.addressToRevoke = "";
            }
            else {
                _this.scannedAddress = "";
                _this.addressToRevoke = "";
            }
        });
    };
    PatientDashboardPage.prototype.manageAccess = function (grant) {
        var _this = this;
        var user = this.navParams.data.credentials;
        if (grant) {
            user.type = true;
            user.toAddress = this.scannedAddress;
        }
        else {
            user.type = false;
            user.toAddress = this.addressToRevoke;
        }
        this.createLoader();
        this.api.manageAccess(user).subscribe(function (response) {
            _this.loader.dismiss();
            _this.scannedAddress = "";
            _this.addressToRevoke = "";
            console.log("address sharing/cancellation success");
            console.log(response);
            var info = {
                title: "Success",
                subTitle: response.message
                //subTitle: "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        }, function (error) {
            _this.loader.dismiss();
            console.error('Oops:', error);
            var info = {
                title: "Error",
                subTitle: error.error.message
                //subTitle: "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        });
    };
    PatientDashboardPage.prototype.getObservations = function () {
        var self = this;
        this.showDetails = true;
        this.observationDetails = null;
        this.api.getObservations(this.clinicalData.emrData.id, this.selectedEmr.number)
            .subscribe(function (response) {
            console.log("response", response);
            self.observationDetails = response.entry;
            //self.toggleGroup(index);
        }, function (error) {
        });
    };
    PatientDashboardPage.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
    };
    PatientDashboardPage.prototype.uploadFile = function () {
        var _this = this;
        var patientDetails = this.navParams.data.credentials;
        patientDetails.type = "insertfile";
        this.createLoader();
        this.api.postFile(this.fileToUpload).subscribe(function (response) {
            console.log(response);
            if (response && response.url) {
                patientDetails.fileUrl = response.url;
                _this.api.uploadFile(patientDetails).subscribe(function (data) {
                    _this.loader.dismiss();
                    console.log(data);
                    var info = {
                        title: "Success",
                        subTitle: data.message
                    };
                    _this.presentAlert(info);
                });
            }
        }, function (error) {
            _this.loader.dismiss();
            console.error('Oops:', error);
            var info = {
                title: "Error",
                subTitle: error.error.message
                //subTitle: "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        });
    };
    PatientDashboardPage.prototype.getFile = function () {
        var _this = this;
        console.log("hello");
        var patientDetails = this.navParams.data.credentials;
        var data = {
            ssn: this.credentials.ssn,
            account: this.user.account,
            user: this.credentials.user,
            provider: this.credentials.emrDetails.number
        };
        this.createLoader();
        this.api.getFile(data).subscribe(function (response) {
            console.log(response);
            _this.loader.dismiss();
            window.open(__WEBPACK_IMPORTED_MODULE_4__config__["b" /* DCOM_URL */] + "?input=" + encodeURIComponent(response.data), "_blank");
        });
    };
    PatientDashboardPage.prototype.getVisits = function () {
        var _this = this;
        this.createLoader();
        this.api.getVisits(this.clinicalData.emrData.identifier[0].value, this.selectedEmr.number).subscribe(function (response) {
            _this.visitsInfoFetched = true;
            console.log(response);
            if (response.total) {
                _this.visitList = [];
                _this.visited = true;
                var visits = response.entry;
                visits.forEach(function (visit) {
                    visit.resource.period.start = new Date(visit.resource.period.start);
                    visit.resource.period.end = new Date(visit.resource.period.end);
                    _this.visitList.push(visit.resource);
                });
            }
            else {
                _this.visited = false;
            }
            _this.loader.dismiss();
        });
    };
    // toggleGroup(group) {
    //   if (this.isGroupShown(group)) {
    //       this.shownGroup = null;
    //   } else {
    //       this.shownGroup = group;
    //   }
    // }
    // isGroupShown(group) {
    //     return this.shownGroup === group;
    // }
    PatientDashboardPage.prototype.presentAlert = function (info) {
        var alert = this.alertCtrl.create({
            title: info.title,
            subTitle: info.subTitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    PatientDashboardPage.prototype.createLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    PatientDashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-patient-dashboard',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/patient-dashboard/patient-dashboard.html"*/'<!--\n  Generated template for the PatientDashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Patient Dashboard</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-col col-12 col-sm-9 col-md-6 col-lg-6 col-xl-6>\n      <ion-card style="min-height: 370px;">\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label no-margin>Patient Profile</label>\n              </ion-header>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">First name</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.name[0].given[0]}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Family name</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.name[0].family}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Gender</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.gender}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Date of Birth</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.birthDate}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Address Line 1</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].line[0]}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Address Line 2</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].line[1]}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">City</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].city}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">State</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].state}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Area Code</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].postalCode}}</ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 col-md-6 class="ionic-label">Country</ion-col>\n              <ion-col col-12 col-md-6>{{clinicalData.emrData.address[0].country}}</ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Get Patient Files</label>\n                <button ion-button color="default" class="pull-right" (click)="getFile()">Get Files</button>\n              </ion-header>\n            </ion-row>\n            <!-- <ion-row>\n                    <ion-col col-12>\n                      <button ion-button color="dark">Get all files</button>\n                    </ion-col>\n                    <ion-col col-12></ion-col>\n                  </ion-row> -->\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-12 col-sm-9 col-md-6 col-lg-6 col-xl-6>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Give Access To Doctor</label>\n                <button ion-button color="default" class="pull-right" (click)="scanQRCode(true)">Scan Code</button>\n              </ion-header>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 padding>\n                <ion-input [(ngModel)]="scannedAddress"></ion-input>\n                <button *ngIf="scannedAddress" ion-button color="default" (click)="manageAccess(true)">Give Access</button>\n              </ion-col>\n              <ion-col col-12></ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Revoke Access From</label>\n                <button ion-button color="default" class="pull-right" (click)="scanQRCode(false)">Scan Code</button>\n              </ion-header>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12 padding>\n                <ion-input [(ngModel)]="addressToRevoke"></ion-input>\n                <button *ngIf="addressToRevoke" ion-button color="default" (click)="manageAccess(false)">Revoke Access</button>\n              </ion-col>\n              <ion-col col-12></ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label no-margin>Add Patient Files</label>\n              </ion-header>\n            </ion-row>\n            <ion-row>\n              <ion-col col-12>\n                <input type="file" (change)="handleFileInput($event.target.files)" style="margin-top: 15px; width: 70%;" />\n                <button ion-button color="default" class="pull-right" (click)="uploadFile()">Upload</button>\n              </ion-col>\n            </ion-row>\n            <!-- <ion-row>\n                  <ion-col col-12>\n                    <button ion-button color="dark">Add File</button>\n                  </ion-col>\n                  <ion-col col-12></ion-col>\n                </ion-row> -->\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-12>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Get Recent Visits</label>\n                <button ion-button color="default" class="pull-right" (click)="getVisits()">Get Visits</button>\n              </ion-header>\n            </ion-row>\n            <ion-row *ngIf="visitsInfoFetched" padding>\n              <ion-col col-12 *ngIf="!visited;else visits">\n                <div style="padding-top: 10px;">\n                  No records available on the patient visits.\n                </div>\n              </ion-col>\n              <ng-template #visits>\n                <ion-col col-3>\n                  <b>Start Date</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>End Date</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>Location</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>Consulted with:</b>\n                </ion-col>\n                <ion-row padding *ngFor="let visit of visitList; let i=index" style="border-bottom: 1px solid lightgray; padding-bottom:5px; width: 100%;">\n                  <ion-col col-3>\n                    {{visit.period.start}}\n                  </ion-col>\n                  <ion-col col-3>\n                    {{visit.period.end}}\n                  </ion-col>\n                  <ion-col col-3>\n                    {{visit.location[0].location.display}}\n                  </ion-col>\n                  <ion-col col-3>\n                    {{visit.participant && visit.participant.length ? visit.participant[0].individual.display.split(\'(\')[0]: \'\'}}\n                  </ion-col>\n                </ion-row>\n              </ng-template>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-12>\n      <ion-card>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row padding margin>\n              <ion-header padding>\n                <label>Get Patient Observations</label>\n                <button ion-button color="default" class="pull-right" (click)="getObservations()">History</button>\n              </ion-header>\n            </ion-row>\n            <ion-col col-12 *ngIf="showDetails && (!observationDetails || !(observationDetails.length > 0))">\n              <div style="padding: 10px 16px 0px 16px;">\n                No history data available.\n              </div>\n            </ion-col>\n            <ion-col col-12 *ngIf="observationDetails && observationDetails.length > 0">\n              <ion-row>\n                <ion-col col-2>\n                  <b>ACTIVITY</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>ANALYSIS</b>\n                </ion-col>\n                <ion-col col-2>\n                  <b>RESULTS</b>\n                </ion-col>\n                <ion-col col-3>\n                  <b>COMMENTS</b>\n                </ion-col>\n                <ion-col col-2>\n                  <b>DATE</b>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n            <ion-col col-12 *ngFor="let obs of observationDetails; let i=index;">\n              <ion-row>\n                <ion-col col-2>\n                  {{obs.resource.resourceType}}\n                </ion-col>\n                <ion-col col-3>\n                  <div *ngIf="obs.resource && obs.resource.code && obs.resource.code.coding && obs.resource.code.coding.length">\n                    <ion-col col-12>\n                      {{obs.resource.code.coding[obs.resource.code.coding.length-1].display}}\n                    </ion-col>\n                  </div>\n                </ion-col>\n                <ion-col col-2>\n                  <div *ngIf="obs.resource && obs.resource.valueCodeableConcept && obs.resource.valueCodeableConcept.coding && obs.resource.valueCodeableConcept.coding.length">\n                    <ion-col col-12>\n                      {{obs.resource.valueCodeableConcept.coding[obs.resource.valueCodeableConcept.coding.length-1].display}}\n                    </ion-col>\n                  </div>\n                  <div *ngIf="obs.resource && obs.resource.valueQuantity">\n                    <ion-col col-12>\n                      {{obs.resource.valueQuantity.value}} {{obs.resource.valueQuantity.unit}}\n                    </ion-col>\n                  </div>\n                </ion-col>\n                <ion-col col-3>\n                  <div>{{obs.resource.comment}}</div>\n                </ion-col>\n                <ion-col col-2>\n                  <div>{{obs.resource.effectiveDateTime}}</div>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <delegation [user]="credentials" [userInfo]="user" [isProvider]="false"></delegation>\n  <prescription [userDetails] = "clinicalData" [isDoctor]="false" [selectedEmr] = "selectedEmr"></prescription>\n</ion-content>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/patient-dashboard/patient-dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_qr_scanner__["a" /* QRScanner */],
            __WEBPACK_IMPORTED_MODULE_0__providers_healtherium_api_healtherium_api__["a" /* HealtheriumApi */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* Platform */]])
    ], PatientDashboardPage);
    return PatientDashboardPage;
}());

//# sourceMappingURL=patient-dashboard.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Home; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__registration_registration__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_content_modal_content__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Home = /** @class */ (function () {
    function Home(nav, navParams, modalCtrl, platform) {
        this.nav = nav;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.role = null;
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isApp = false;
        }
        else {
            this.isApp = true;
        }
    }
    Home.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DoctorDashboardPage');
    };
    Home.prototype.login = function (user) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__modal_content_modal_content__["a" /* ModalContentPage */], { user: user, action: "login" });
        modal.present();
    };
    Home.prototype.register = function (user) {
        console.log(user);
        this.nav.push(__WEBPACK_IMPORTED_MODULE_0__registration_registration__["a" /* RegistrationPage */], { user: user });
    };
    Home = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Login/Sign up</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="card-background-page">\n\n  <ion-grid>\n    <ion-row class="hidden-md">\n      <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n        <ion-item>\n          <ion-label>\n            <label style="margin-top: 0px;">Who are you</label>\n          </ion-label>\n          <ion-select [(ngModel)]="role">\n            <ion-option value="doctor">Doctor</ion-option>\n            <ion-option value="patient">Patient</ion-option>\n            <ion-option value="researcher">Researcher</ion-option>\n            <ion-option value="insurance">Insurance</ion-option>\n            <ion-option value="buyer">Buyer</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n      <ion-col col-12 [hidden]="!(role === \'doctor\' )">\n        <ion-card class="doctor-card">\n          <ion-card-header class="card-title">\n            Doctors\n          </ion-card-header>\n          <img src="assets/imgs/doctor.jpg" />\n          <div class="card-title"></div>\n          <div class="card-subtitle">Healtherium Doctors can sign up/Log in at the doctor\'s hub and see the patient\'s\n            history</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'doctor\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'doctor\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 [hidden]="!(role === \'patient\')">\n        <ion-card>\n          <ion-card-header class="card-title">\n            Patients\n          </ion-card-header>\n          <img src="assets/imgs/patient.jpg" />\n          <div class="card-subtitle">Patients can create thier free account here and store their medical records on\n            blockchain</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'patient\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'patient\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 [hidden]="!(role === \'researcher\')">\n        <ion-card class="research-card">\n          <ion-card-header class="card-title">\n            Researchers\n          </ion-card-header>\n          <img src="assets/imgs/research.jpg" />\n          <div class="card-subtitle">Healthereum acts as global source of clinical & social data of people, which can\n            be utilized for clinical and non-clinical research purposes</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'research\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'research\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 [hidden]="!(role === \'insurance\')">\n        <ion-card class="insurance-card">\n          <ion-card-header class="card-title">\n            Insurance\n          </ion-card-header>\n          <img src="assets/imgs/insurance.jpg" />\n          <div class="card-subtitle">Insurance providers can get access to the comprehensive health record of\n            individuals at a single pit stop, Healthereum.</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'insurance\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'insurance\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12 [hidden]="!(role === \'buyer\')">\n          <ion-card class="buyer-card">\n            <ion-card-header class="card-title">\n              Buyers\n            </ion-card-header>\n            <img src="assets/imgs/buyer1.jpeg" />\n            <div class="card-subtitle">Healthereum acts as a global market place for buyers and sellers to buy/sell\n              confidential medical data securely.</div>\n            <ion-row class="text-center">\n              <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n                <button ion-button (click)="register(\'buyer\')">New Registration</button>\n              </ion-col>\n              <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n                <button ion-button color="secondary" (click)="login(\'buyer\')">Dashboard</button>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-col>\n    </ion-row>\n    <ion-row class="hidden-sm">\n      <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n        <ion-card class="doctor-card">\n          <ion-card-header class="card-title">\n            Doctors\n          </ion-card-header>\n          <img src="assets/imgs/doctor.jpg" />\n          <div class="card-title"></div>\n          <div class="card-subtitle">Healthereum Doctors can sign up/Log in at the doctor\'s hub and see the patient\'s\n            history</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'doctor\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'doctor\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n\n      <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n        <ion-card>\n          <ion-card-header class="card-title">\n            Patients\n          </ion-card-header>\n          <img src="assets/imgs/patient.jpg" />\n          <div class="card-subtitle">Patients can create thier free account here and store their medical records on\n            blockchain</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'patient\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'patient\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n\n      <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n        <ion-card class="research-card">\n          <ion-card-header class="card-title">\n            Researchers\n          </ion-card-header>\n          <img src="assets/imgs/research.jpg" />\n          <div class="card-subtitle">A global source of clinical & social data of people, which can be utilized for clinical and non-clinical research purposes</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'research\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'research\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n\n      <ion-col offset-md-2 col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n        <ion-card class="insurance-card">\n          <ion-card-header class="card-title">\n            Insurance\n          </ion-card-header>\n          <img src="assets/imgs/insurance.jpg" />\n          <div class="card-subtitle">Insurance providers can get access to the comprehensive health record of\n            individuals at a single pit stop, Healthereum.</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'insurance\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'insurance\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n\n      <ion-col col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4>\n        <ion-card class="buyer-card">\n          <ion-card-header class="card-title">\n            Buyers\n          </ion-card-header>\n          <img src="assets/imgs/buyer1.jpeg" />\n          <div class="card-subtitle">Healthereum acts as a global market place for buyers and sellers to buy/sell\n            confidential medical data securely.</div>\n          <ion-row class="text-center">\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button (click)="register(\'buyer\')">New Registration</button>\n            </ion-col>\n            <ion-col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6>\n              <button ion-button color="secondary" (click)="login(\'buyer\')">Dashboard</button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/home/home.html"*/,
            selector: 'Home'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* Platform */]])
    ], Home);
    return Home;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutUsPage = /** @class */ (function () {
    function AboutUsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutUsPage');
    };
    AboutUsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about-us',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/about-us/about-us.html"*/'<!--\n  Generated template for the AboutUsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>About Us</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h3>\n      Blockchain backboned access management\n  </h3>\n  <div>\n      Healthereum leverages the potential of blockchain to alleviate one of the major challenges of the industry: the transmission of patient data across geographies without compromising its privacy and security We have developed a system which ensures that the ownership of data resides with the person who owns it and can be shared only upon his or her consent and the system can be tweaked or tailored to match any requirement as well.\n  </div>\n  <div style="text-align: center; padding-top: 20px;">\n      <img src="assets/imgs/blockchain.svg" width="400"/>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/about-us/about-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], AboutUsPage);
    return AboutUsPage;
}());

//# sourceMappingURL=about-us.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LandingPage = /** @class */ (function () {
    function LandingPage(nav, navParams, modalCtrl, platform) {
        this.nav = nav;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.role = null;
        if (this.platform.is('core') || this.platform.is('mobileweb')) {
            this.isApp = false;
        }
        else {
            this.isApp = true;
        }
    }
    LandingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DoctorDashboardPage');
    };
    LandingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/landing/landing.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Healthereum</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="card-background-page">\n  <div style="height: 50%; text-align: center;margin: 0px 40px;padding-top: 100px;">\n    <img src="assets/imgs/dwellize.png" />\n    <div>\n      Using Data Science to Focus on Wellizing Healthcare data\n    </div>\n  </div>\n  <ion-grid style="background: linear-gradient(103deg, #645dd8 0%, #2dcca5 100%);margin-top: 50px; padding: 20px;">\n\n    <h3>Vision</h3>\n    <div>\n      DWellize will revolutionize the healthcare decision making by providing data science backboned software solutions\n      that account for the impacts of both clinical therapies, social determinants and other factors that impact\n      performance in a value-based healthcare environment.\n    </div>\n    <h3>Mission</h3>\n    <div>\n      Our mission is to address the need for data science based evidence to support assessments of impacts of clinical\n      therapies and social determinants on patient outcomes in a value-based healthcare environment.\n    </div>\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/landing/landing.html"*/,
            selector: 'Landing'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], LandingPage);
    return LandingPage;
}());

//# sourceMappingURL=landing.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(377);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_registration_registration_module__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_about_us_about_us__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(711);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(712);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_qr_scanner__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(713);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_landing_landing__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_modal_content_modal_content__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_healtherium_api_healtherium_api__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_firebase__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angularfire2__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_angularfire2_storage__ = __webpack_require__(716);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_notification_api_notification_api__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_notifications_notifications_module__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_delegation_api_delegation__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_patient_dashboard_patient_dashboard_module__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_doctor_dashoard_doctor_dashboard_module__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_registration_registration__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















//import { AppRoutingModule } from './app-routing.module';



// Initialize Firebase
var config = {
    apiKey: "AIzaSyDUHE2taoT6jG22Ja-k3zsxME74N7akyHY",
    authDomain: "healtherium-a1bba.firebaseapp.com",
    databaseURL: "https://healtherium-a1bba.firebaseio.com",
    projectId: "healtherium-a1bba",
    storageBucket: "healtherium-a1bba.appspot.com",
    messagingSenderId: "793468538300"
};
__WEBPACK_IMPORTED_MODULE_16_firebase___default.a.initializeApp(config);
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_landing_landing__["a" /* LandingPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_home_home__["a" /* Home */],
                __WEBPACK_IMPORTED_MODULE_2__pages_about_us_about_us__["a" /* AboutUsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_modal_content_modal_content__["a" /* ModalContentPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* HttpModule */],
                //AppRoutingModule,
                __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/notifications/notifications.module#NotificationsPageModule', name: 'NotificationsPage', segment: 'notifications', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_17_angularfire2__["a" /* AngularFireModule */].initializeApp(config),
                __WEBPACK_IMPORTED_MODULE_18_angularfire2_storage__["a" /* AngularFireStorageModule */],
                __WEBPACK_IMPORTED_MODULE_0__pages_registration_registration_module__["a" /* RegistrationPageModule */],
                __WEBPACK_IMPORTED_MODULE_20__pages_notifications_notifications_module__["NotificationsPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_patient_dashboard_patient_dashboard_module__["a" /* PatientDashboardPageModule */],
                __WEBPACK_IMPORTED_MODULE_23__pages_doctor_dashoard_doctor_dashboard_module__["a" /* DoctorDashboardPageModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_24__pages_registration_registration__["a" /* RegistrationPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_landing_landing__["a" /* LandingPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_home_home__["a" /* Home */],
                __WEBPACK_IMPORTED_MODULE_2__pages_about_us_about_us__["a" /* AboutUsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_modal_content_modal_content__["a" /* ModalContentPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_qr_scanner__["a" /* QRScanner */],
                { provide: __WEBPACK_IMPORTED_MODULE_4__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_15__providers_healtherium_api_healtherium_api__["a" /* HealtheriumApi */],
                __WEBPACK_IMPORTED_MODULE_19__providers_notification_api_notification_api__["a" /* NotificationApiProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_delegation_api_delegation__["a" /* DelegationProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registration__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegistrationPageModule = /** @class */ (function () {
    function RegistrationPageModule() {
    }
    RegistrationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__registration__["a" /* RegistrationPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registration__["a" /* RegistrationPage */]),
            ],
        })
    ], RegistrationPageModule);
    return RegistrationPageModule;
}());

//# sourceMappingURL=registration.module.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_notification_api_notification_api__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NotificationsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(notificationProvider) {
        this.notificationProvider = notificationProvider;
        this.count = 0;
        console.log('Hello NotificationsComponent Component');
        this.notifications = [];
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        this.getNotifications();
    };
    NotificationsComponent.prototype.getNotifications = function () {
        var _this = this;
        console.log('user details');
        console.log(this.userDetails);
        this.notificationProvider.getNotifications(this.userDetails.userPublicAddress)
            .subscribe(function (response) {
            _this.notifications = response['notifications'];
            _this.count = response['count'];
            if (_this.notifications.length) {
                _this.updateNotificationsRead(_this.notifications);
            }
        }, function (error) {
            console.log(error);
        });
    };
    NotificationsComponent.prototype.updateNotificationsRead = function (notifications) {
        this.notificationProvider.updateNotificationsRead(notifications)
            .subscribe(function (response) { console.log('notification read updated successfully'); }, function (error) {
            console.log(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NotificationsComponent.prototype, "userDetails", void 0);
    NotificationsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'notifications',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/components/notifications/notifications.html"*/'<ion-content>\n  <ion-list *ngIf="count > 0">\n    <ion-item text-wrap *ngFor="let notification of notifications">\n      <p>{{notification.message}}</p>\n      <p align="right">{{notification.createdOn | date:\'medium\'}}</p>\n    </ion-item>\n  </ion-list>\n  <ion-item  *ngIf="count === 0" align-items-center> No new notifications</ion-item>\n</ion-content>\n\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/components/notifications/notifications.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_notification_api_notification_api__["a" /* NotificationApiProvider */]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealtheriumApi; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HealtheriumApi = /** @class */ (function () {
    //private apiUrl = '';
    function HealtheriumApi(http) {
        this.http = http;
    }
    HealtheriumApi.prototype.getDetails = function (data) {
        console.log("login");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/fetchDetails", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getAccessList = function (data) {
        console.log("getting access list");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/getSharedPatientList", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.login = function (data) {
        console.log("login", data);
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/login", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.fetchDetails = function (data) {
        console.log("fetchDetails");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/fetchDetails", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.generateOTP = function (data) {
        console.log("registerUser");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/generateotp", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.register = function (data) {
        console.log("addUser");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/register", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.uploadFile = function (data) {
        console.log("addUser");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/upload", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.manageAccess = function (data) {
        console.log("addUser");
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/ownership", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.postFile = function (fileToUpload) {
        var file = fileToUpload;
        var storageRef = __WEBPACK_IMPORTED_MODULE_4_firebase__["storage"]().ref();
        var uploadTask = storageRef.child("" + fileToUpload.name).put(fileToUpload);
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].create(function (observer) {
            uploadTask.on(__WEBPACK_IMPORTED_MODULE_4_firebase__["storage"].TaskEvent.STATE_CHANGED, function (snapshot) {
                // upload in progress
                fileToUpload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            }, function (error) {
                // upload failed
                console.log(error);
                observer.error(status);
            }, function () {
                // upload success
                file.url = uploadTask.snapshot.downloadURL;
                console.log(file);
                observer.next(file);
                observer.complete();
            });
        });
    };
    HealtheriumApi.prototype.getVisits = function (data, provider) {
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('identifier', data).set('provider', provider);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/visits", { params: params })
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getObservations = function (data, provider) {
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('subject', data).set('provider', provider);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/observations", { params: params })
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getPrescriptions = function (personId, provider) {
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('personId', personId).set('provider', provider);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/prescriptions", { params: params })
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getAllocatedUsers = function (ssn) {
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('userId', ssn);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/allocated", { params: params })
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getUserInfo = function (grantor, recipient, provider) {
        var _this = this;
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]().set('recipient', recipient).set('grantor', grantor).set('providerNo', provider);
        console.log(params);
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/grantor", { params: params })
            .map(function (response) {
            _this.patientData = response;
            return response;
        });
    };
    HealtheriumApi.prototype.getFile = function (data) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/file", data)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getProvidersOfUser = function (data) {
        console.log("calling get providers api from front end");
        console.log(data);
        var queryParams = '';
        if (data.ssn) {
            queryParams = "?userId=" + data.ssn;
        }
        else if (data.address) {
            queryParams = "?address=" + data.address;
        }
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/user/providerList" + queryParams)
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi.prototype.getProviders = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_5__config__["a" /* BASE_URL */] + "/api/providers")
            .map(function (response) {
            return response;
        });
    };
    HealtheriumApi = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], HealtheriumApi);
    return HealtheriumApi;
}());

//# sourceMappingURL=healtherium-api.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BASE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DCOM_URL; });
var BASE_URL = 'http://localhost:4445';
var DCOM_URL = 'http://localhost:8080/index.html';
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrescriptionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_healtherium_api_healtherium_api__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PrescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var PrescriptionComponent = /** @class */ (function () {
    function PrescriptionComponent(api, loadingCtrl) {
        this.api = api;
        this.loadingCtrl = loadingCtrl;
        this.count = 0;
        this.showPrescriptionsView = false;
        this.prescriptions = [];
        console.log('Hello PrescriptionComponent Component');
    }
    PrescriptionComponent.prototype.ngOnInit = function () {
        if (this.isDoctor) {
            this.getPrescriptions();
        }
    };
    PrescriptionComponent.prototype.getPrescriptions = function () {
        var _this = this;
        var self = this;
        this.showPrescriptionsView = false;
        var personId;
        if (this.isDoctor) {
            personId = this.userDetails.id;
        }
        else {
            personId = this.userDetails.emrData.id;
            console.log(this.userDetails.emrData);
        }
        self.createLoader();
        this.api.getPrescriptions(personId, this.selectedEmr.number)
            .subscribe(function (response) {
            self.loader.dismiss();
            self.showPrescriptionsView = true;
            console.log(response);
            _this.prescriptions = response['prescriptions'];
            _this.count = response['count'];
        }, function (error) {
            console.log(error);
        });
    };
    PrescriptionComponent.prototype.createLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], PrescriptionComponent.prototype, "isDoctor", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], PrescriptionComponent.prototype, "selectedEmr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], PrescriptionComponent.prototype, "userDetails", void 0);
    PrescriptionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'prescription',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/components/prescription/prescription.html"*/'<!-- Generated template for the PrescriptionComponent component -->\n<ion-col col-12>\n  <ion-card>\n    <ion-row>\n      <ion-col col-12>\n        <ion-row padding margin>\n          <ion-header padding>\n            <label>Prescriptions</label>\n            <button *ngIf="!isDoctor" ion-button color="default" class="pull-right" (click)="getPrescriptions()">View</button>\n          </ion-header>\n        </ion-row>\n        <ion-col col-12 *ngIf="(showPrescriptionsView && (!prescriptions || (prescriptions.length <= 0)))">\n          <div style="padding: 10px 16px 0px 16px;">\n            No history data available.\n          </div>\n        </ion-col>\n        <ion-col col-12 *ngIf="prescriptions && prescriptions.length > 0">\n          <ion-row>\n            <ion-col col-2>\n              <b>DRUG NAME</b>\n            </ion-col>\n            <ion-col col-3>\n              <b>DOSAGE</b>\n            </ion-col>\n            <ion-col col-2>\n              <b>STATUS</b>\n            </ion-col>\n            <ion-col col-3>\n              <b>PRESCRIBED BY</b>\n            </ion-col>\n            <ion-col col-2>\n              <b>PRESCRIPTION DATE</b>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-12 *ngFor="let prescription of prescriptions; let i=index;">\n          <ion-row>\n            <ion-col col-2>\n              {{prescription.drugName}}\n            </ion-col>\n            <ion-col col-3>\n              <div *ngIf="prescription.dosageInstruction.length > 0 && prescription.dosageInstruction[0].dosageInstructionFreeText">\n                {{prescription.dosageInstruction[0].dosageInstructionFreeText}}\n            </div>\n              <div *ngIf="prescription.dosageInstruction && prescription.dosageInstruction.length > 0 && !prescription.dosageInstruction[0].dosageInstructionFreeText && prescription.dispenseRequest">\n              {{prescription.dosageInstruction[0].doseQuantity.value}} {{prescription.dosageInstruction[0].doseQuantity.unit}}, {{prescription.dosageInstruction[0].route}}, {{prescription.dosageInstruction[0].timing}}, {{prescription.dispenseRequest.expectedSupplyDuration.value}} {{prescription.dispenseRequest.expectedSupplyDuration.unit}}\n            </div>\n      </ion-col>\n\n            <ion-col col-2>\n              {{prescription.status}}\n            </ion-col>\n            <ion-col col-3>\n              {{prescription.recorder.name}}\n            </ion-col>\n            <ion-col col-2>\n              {{prescription.authoredOn}}\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-col>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/components/prescription/prescription.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_healtherium_api_healtherium_api__["a" /* HealtheriumApi */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */]])
    ], PrescriptionComponent);
    return PrescriptionComponent;
}());

//# sourceMappingURL=prescription.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DelegationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_delegation_api_delegation__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DelegationComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var DelegationComponent = /** @class */ (function () {
    function DelegationComponent(DelegationProvider, loadingCtrl, alertCtrl) {
        this.DelegationProvider = DelegationProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.isProvider = false;
        this.showItem = null;
        console.log('Hello DelegationComponent Component');
    }
    DelegationComponent.prototype.getDelegationInfo = function () {
        var _this = this;
        var userId = this.user.ssn;
        this.createLoader();
        this.DelegationProvider.getDelegationInfo(userId)
            .subscribe(function (response) {
            _this.loader.dismiss();
            _this.showItem = "view";
            console.log("response", response);
            _this.nominees = response;
        }, function (error) {
            _this.loader.dismiss();
            console.log("error", error);
        });
    };
    DelegationComponent.prototype.isArray = function (obj) {
        return Array.isArray(obj);
    };
    DelegationComponent.prototype.nominate = function () {
        var _this = this;
        this.showItem = "delegate";
        this.error = null;
        console.log("in nominate");
        console.log("in get Nominees", this.user);
        var params = {
            nominee_id: this.nomineeAdhaar,
            user_id: this.user.ssn,
        };
        this.createLoader();
        if (this.nomineeAdhaar) {
            this.DelegationProvider.delegate(params)
                .subscribe(function (response) {
                _this.loader.dismiss();
                console.log("response", response);
                var info = {
                    title: "Success",
                    subTitle: "User with  " + _this.nomineeAdhaar + " given access to act on behalf of you."
                };
                _this.presentAlert(info);
            }, function (error) {
                var info = {
                    title: "Error",
                    subTitle: "Oops!!! Something went wrong. Please try again after sometime"
                };
                _this.presentAlert(info);
                _this.loader.dismiss();
                console.log("error", error);
            });
        }
        else {
            this.loader.dismiss();
            this.error = "Please provide user identification";
        }
    };
    DelegationComponent.prototype.revoke = function (nominee) {
        var _this = this;
        console.log(nominee);
        var params = {
            nominee_id: nominee.Adhaar,
            user_id: this.user.ssn
        };
        this.createLoader();
        this.DelegationProvider.revokeDelegation(params)
            .subscribe(function (response) {
            _this.loader.dismiss();
            console.log(response);
            if (response)
                _this.nominees = response;
            var info = {
                title: "Success",
                subTitle: "Privelege of User with Adhaar " + nominee.user_id + " revoked."
            };
            _this.presentAlert(info);
        }, function (error) {
            _this.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: "Oops!!! Something went wrong. Please try again after sometime"
            };
            _this.presentAlert(info);
            console.log(error);
        });
    };
    DelegationComponent.prototype.getDelegators = function () {
        var _this = this;
        var userId = this.user.ssn;
        this.createLoader();
        this.DelegationProvider.getDelegators(userId)
            .subscribe(function (response) {
            _this.loader.dismiss();
            _this.showItem = "delegator";
            console.log("response", response);
            _this.nominators = response;
        }, function (error) {
            _this.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: "Oops!!! Something went wrong. Please try again after sometime"
            };
            _this.presentAlert(info);
            console.log("error", error);
        });
    };
    DelegationComponent.prototype.manageAccess = function (nominator, grantAccess) {
        var _this = this;
        var info;
        var data = {
            user_id: this.user.ssn,
            user_name: this.userInfo.name,
            nominator_id: nominator.Adhaar,
            doctorAddress: grantAccess ? this.grantAddress : this.revokeAddress,
            grantAccess: grantAccess
        };
        this.createLoader();
        this.DelegationProvider.grantAccess(data)
            .subscribe(function (response) {
            _this.loader.dismiss();
            console.log("response", response);
            if (grantAccess) {
                info = {
                    title: "Success",
                    subTitle: "Access shared with doctor for " + nominator.name
                };
            }
            else {
                info = {
                    title: "Success",
                    subTitle: "Access revoked from doctorfor " + nominator.name
                };
            }
            _this.presentAlert(info);
            _this.nominators = response;
        }, function (error) {
            _this.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: "Oops!!! Something went wrong. Please try again after sometime"
            };
            _this.presentAlert(info);
            console.log("error", error);
        });
    };
    DelegationComponent.prototype.createLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    DelegationComponent.prototype.presentAlert = function (info) {
        var alert = this.alertCtrl.create({
            title: info.title,
            subTitle: info.subTitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], DelegationComponent.prototype, "isProvider", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DelegationComponent.prototype, "user", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], DelegationComponent.prototype, "userInfo", void 0);
    DelegationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'delegation',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/components/delegation/delegation.html"*/'<!-- Generated template for the DelegationComponent component -->\n\n<ion-col col-6>\n  <ion-card>\n    <ion-row>\n      <ion-col col-12>\n        <ion-row padding margin>\n          <ion-header padding>\n            <label>Handle Delegation</label>\n          </ion-header>\n        </ion-row>\n        <ion-row style="border-bottom: 1px solid lightgrey">\n          <ion-col col-xs-6 col-md-4><label>Delegate a user on behalf of me </label></ion-col>\n          <ion-col col-xs-6 col-md-4>\n            <ion-input style="width: 60%; margin-top: 10px; display: inline-block;" [(ngModel)]="nomineeAdhaar" name="nomineeAdhaar"\n              placeholder="User ID" (ionChange)="error=null"></ion-input>\n            <button ion-button color="default" class="pull-right" (click)="nominate()">Go</button>\n            <div *ngIf="error">{{error}}</div>\n          </ion-col>\n        </ion-row>\n        <ion-row style="border-bottom: 1px solid lightgrey">\n          <ion-col col-xs-6 col-md-4><label>Show users to whom access is given to</label></ion-col>\n          <ion-col col-xs-6 col-md-4>\n            <button ion-button color="default" class="pull-right" (click)="getDelegationInfo()">Show</button>\n          </ion-col>\n          <ion-col col-12 *ngIf="nominees && isArray(nominees) && nominees.length && showItem == \'view\';else nomineesUnavailable">\n            <table style="width: 100%;text-align: center;">\n              <thead>\n                <th>Name</th>\n                <th>Adhaar</th>\n                <th>Revoke Access</th>\n              </thead>\n              <tbody>\n                <tr *ngFor="let nominee of nominees; let i=index;">\n                  <td>{{nominee.name}}</td>\n                  <td>{{nominee.Adhaar}}</td>\n                  <td>\n                    <button ion-button color="default" (click)="revoke(nominee)">Revoke</button>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </ion-col>\n          <ion-col col-12 #nomineesUnavailable *ngIf="(!nominees || (isArray(nominees) && nominees.length == 0)) && showItem == \'view\'">\n            <div style="padding: 10px 16px 0px 16px;">\n              No nominees.\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row style="border-bottom: 1px solid lightgrey">\n          <ion-col col-xs-6 col-md-4><label>Show users who has given access to me</label></ion-col>\n          <ion-col col-xs-6 col-md-4>\n            <button ion-button color="default" class="pull-right" (click)="getDelegators()">Delegators</button>\n          </ion-col>\n          <ion-col col-12 *ngIf="nominators && isArray(nominators) && nominators.length && showItem == \'delegator\' ;else nominatorUnavailable">\n            <table style="width: 100%;text-align: center;">\n              <thead>\n                <th>Name</th>\n                <th>Adhaar</th>\n                <th>Grant Access To Doctor</th>\n                <th>Revoke Access From Doctor</th>\n              </thead>\n              <tbody>\n                <tr *ngFor="let nominator of nominators; let i=index;">\n                  <td>{{nominator.name}}</td>\n                  <td>{{nominator.Adhaar}}</td>\n                  <td>\n                    <ion-input style="width: 70%;display: inline-block;" [(ngModel)]="grantAddress" name="grantAddress" placeholder="Doctor Address"\n                      (ionChange)="error=null"></ion-input>\n                    <button ion-button color="default" (click)="manageAccess(nominator, true)">Go</button>\n                  </td>\n                  <td>\n                    <ion-input style="width: 70%;display: inline-block;" [(ngModel)]="revokeAddress" name="revokeAddress" placeholder="Doctor Address"\n                      (ionChange)="error=null"></ion-input>\n                    <button ion-button color="default" (click)="manageAccess(nominator, false)">Go</button>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </ion-col>\n          <ion-col col-12 #nominatorUnavailable *ngIf="(!nominators || (isArray(nominators) && nominators.length == 0)) && showItem == \'delegator\'">\n            <div style="padding: 10px 16px 0px 16px;">\n              No one has given access to their profile.\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n</ion-col>'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/components/delegation/delegation.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_delegation_api_delegation__["a" /* DelegationProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
    ], DelegationComponent);
    return DelegationComponent;
}());

//# sourceMappingURL=delegation.js.map

/***/ }),

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_about_us_about_us__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_landing_landing__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(365);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';

//import { ListPage } from '../pages/list/list';


var MyApp = /** @class */ (function () {
    //pages: Array<{title: string, component: any, url: string}>;
    function MyApp(platform, menu, statusBar, splashScreen) {
        this.platform = platform;
        this.menu = menu;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        // make HelloIonicPage the root (or first) page
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_landing_landing__["a" /* LandingPage */];
        this.initializeApp();
        // set our app's pages
        this.pages = [
            { title: 'Login/Sign up', component: __WEBPACK_IMPORTED_MODULE_0__pages_home_home__["a" /* Home */], url: '/login' },
            { title: 'About Us', component: __WEBPACK_IMPORTED_MODULE_1__pages_about_us_about_us__["a" /* AboutUsPage */], url: '/about' }
        ];
        // this.pages = [
        //   {
        //     title: 'Login/Sign up',
        //     url: '/app/tabs/(schedule:schedule)',
        //     component: Registration }
        // ]
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        this.nav.push(page.component);
        //this.router.navigate(['/login'])
    };
    MyApp.prototype.aboutUs = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_1__pages_about_us_about_us__["a" /* AboutUsPage */]);
        this.menu.close();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/app/app.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n    <ion-toolbar color="primary">\n      <ion-title>Healthereum</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list-header>\n      Navigate\n    </ion-list-header>\n    <ion-list >\n      <button *ngFor="let p of pages" menu-close ion-item (click)="openPage(p)">{{p.title}}</button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PatientDashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__patient_dashboard__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(119);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PatientDashboardPageModule = /** @class */ (function () {
    function PatientDashboardPageModule() {
    }
    PatientDashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__patient_dashboard__["a" /* PatientDashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__patient_dashboard__["a" /* PatientDashboardPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], PatientDashboardPageModule);
    return PatientDashboardPageModule;
}());

//# sourceMappingURL=patient-dashboard.module.js.map

/***/ }),

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoctorDashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__doctor_dashoard__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularx_qrcode__ = __webpack_require__(721);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DoctorDashboardPageModule = /** @class */ (function () {
    function DoctorDashboardPageModule() {
    }
    DoctorDashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__doctor_dashoard__["a" /* DoctorDashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__doctor_dashoard__["a" /* DoctorDashboardPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4_angularx_qrcode__["a" /* QRCodeModule */]
            ],
        })
    ], DoctorDashboardPageModule);
    return DoctorDashboardPageModule;
}());

//# sourceMappingURL=doctor-dashboard.module.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_healtherium_api_healtherium_api__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__doctor_dashoard_doctor_dashoard__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__patient_dashboard_patient_dashboard__ = __webpack_require__(358);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModalContentPage = /** @class */ (function () {
    function ModalContentPage(
        //private platform: Platform,
        params, viewCtrl, nav, api, alertCtrl, loadingCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.nav = nav;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.user = {};
        this.showOTPform = false;
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.action = this.params.get("action");
        this.role = this.params.get("user");
        this.patientProvider = this.params.get("chooseProvider");
        this.doctorAddress = this.params.get("userAccount");
        this.selectedPatientAddress = this.params.get("selectedPatientAddress");
        console.log(this.signUpInfo, this.action);
        console.log(this.patientProvider);
    }
    ModalContentPage.prototype.register = function () {
        var _this = this;
        this.createLoader();
        var data = this.params.get("data");
        data.otp = this.otp;
        this.api.register(data)
            .subscribe(function (response) {
            if (response) {
                console.log(response);
                var info = {
                    title: "Successful",
                    subTitle: "Your account has been successfully created"
                };
                _this.presentAlert(info);
                _this.loader.dismiss();
                _this.viewCtrl.dismiss();
            }
        }, function (error) {
            _this.loader.dismiss();
            var info = {
                title: "Error",
                subTitle: error.error.message,
            };
            _this.presentAlert(info);
        });
    };
    ModalContentPage.prototype.grantedEmrData = function (selectedOption) {
        var _this = this;
        console.log(selectedOption);
        var selectedProvider = selectedOption;
        var selectedPatientAddress = this.selectedPatientAddress;
        console.log(this.doctorAddress);
        console.log(this.selectedPatientAddress);
        this.api.getUserInfo(this.selectedPatientAddress, this.doctorAddress, selectedOption.number).subscribe(function (response) {
            console.log("shared address");
            _this.loader.dismiss();
            console.log(response);
            _this.viewCtrl.dismiss();
            _this.params.data.selectedOption = selectedProvider;
            var patientDetails = JSON.parse(response.body).entry[0].resource;
            patientDetails.selectedPatientAddress = selectedPatientAddress;
            console.log(_this.params.data.selectedOption);
            _this.nav.push(__WEBPACK_IMPORTED_MODULE_3__doctor_dashoard_doctor_dashoard__["a" /* DoctorDashboardPage */], { isEmrData: true, response: response, patientDetails: patientDetails, doctor: _this.params.data });
        }, function (error) {
            _this.loader.dismiss();
            console.error('Oops:', error);
            var info = {
                title: "Error",
                subTitle: error.error.message
                //subTitle: "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        });
    };
    ModalContentPage.prototype.generateOTP = function () {
        var _this = this;
        var self = this;
        this.user.isRegistered = true;
        this.createLoader();
        console.log(this.user);
        // this.user.userName = "user011";
        // this.user.ssn = "161616161616";
        // this.user.password = "password";
        this.api.generateOTP(this.user)
            .subscribe(function (response) {
            if (response) {
                _this.api.getProvidersOfUser(_this.user)
                    .subscribe(function (providerList) {
                    if (providerList) {
                        console.log(providerList);
                        _this.totalProviderList = providerList;
                        console.log(_this.totalProviderList);
                        _this.showOTPform = true;
                        _this.loader.dismiss();
                    }
                });
            }
        }, function (error) {
            _this.loader.dismiss();
            console.error('Oops:', error);
            var info = {
                title: "Error",
                subTitle: "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        });
    };
    ModalContentPage.prototype.login = function (selectedOption) {
        var _this = this;
        this.createLoader();
        var user = this.params.get("user");
        this.user.user = user;
        this.user.otp = this.otp;
        this.user.emrDetails = selectedOption;
        this.api.login(this.user)
            .subscribe(function (response) {
            if (user === "patient") {
                _this.nav.push(__WEBPACK_IMPORTED_MODULE_4__patient_dashboard_patient_dashboard__["a" /* PatientDashboardPage */], { response: response, credentials: _this.user });
            }
            else if (user === "doctor") {
                _this.nav.push(__WEBPACK_IMPORTED_MODULE_3__doctor_dashoard_doctor_dashoard__["a" /* DoctorDashboardPage */], { response: response, credentials: _this.user });
            }
            else {
                window.alert("Work in progress");
            }
            _this.viewCtrl.dismiss();
        }, function (error) {
            _this.loader.dismiss();
            var message = error && error.message ? error.message : error;
            console.error('Oops:', message);
            var info = {
                title: "Error",
                subTitle: message || "Oops. Something went wrong. Please try again after some time"
            };
            _this.presentAlert(info);
        });
    };
    ModalContentPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalContentPage.prototype.presentAlert = function (info) {
        var alert = this.alertCtrl.create({
            title: info.title,
            subTitle: info.subTitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    ModalContentPage.prototype.createLoader = function () {
        this.loader = this.loadingCtrl.create({
            content: "Please wait...",
            dismissOnPageChange: true
        });
        this.loader.present();
    };
    ModalContentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-modal-content',template:/*ion-inline-start:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/modal-content/modal-content.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>\n      <div *ngIf="role == \'patient\'">Please enter your details</div>\n      <div *ngIf="role == \'doctor\'">Doctor - Please enter your details</div>\n      <div *ngIf="action == \'registration\'">Sign Up</div>\n    </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-row>\n        <ion-col col-12 *ngIf="(action === \'login\') && showOTPform !== true ">\n          <form (ngSubmit)="generateOTP()">\n              <ion-row>\n                <ion-col col-12>\n                  <ion-row>\n                    <ion-col col-12>User Name</ion-col>\n                    <ion-col col-12>\n                      <ion-input [(ngModel)]="user.userName" name ="userName" placeholder="User Name"></ion-input>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col col-12>Secure Number/Aadhar</ion-col>\n                    <ion-col col-12>\n                      <ion-input [(ngModel)]="user.ssn" name ="ssn" placeholder="Secure Number/Aadhar"></ion-input>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col col-12>Password</ion-col>\n                    <ion-col col-12>\n                      <ion-input [(ngModel)]="user.password" name ="password" type="password" placeholder="Password"></ion-input>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col col-12>\n                      <button type="submit" ion-button>Log In</button>\n                      <button type="button" ion-button (click)="dismiss()">Cancel</button>\n                    </ion-col>\n                    <ion-col col-12></ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n          </form>\n        </ion-col>\n        <ion-col col-12 *ngIf="action === \'registration\'">\n            <label>Please enter the OTP recieved in your registered phone number</label>\n            <ion-input [(ngModel)]="otp" name ="otp"  placeholder = "OTP"></ion-input>\n            <button ion-button (click)="register()">Verify</button>\n        </ion-col>\n        <ion-col col-12 *ngIf="action === \'multipleEmr\'">\n          <label>Please select your provider</label>\n          <ion-item>\n            <ion-select [(ngModel)]="selected_option">\n                <ion-option *ngFor="let x of patientProvider" [value]="{number: x.providerNumber, hospitalName: x.providerName}">{{x.providerName}}</ion-option>\n            </ion-select>\n          </ion-item>\n          <button ion-button (click)="grantedEmrData(selected_option)">Submit</button>\n      </ion-col>\n        <ion-col col-12 *ngIf="showOTPform === true">\n            <label>Please enter the OTP recieved in your registered phone number</label>\n            <ion-input [(ngModel)]="otp" name ="otp"  placeholder = "OTP"></ion-input>\n            <label>Please select your provider</label>\n          <ion-item>\n            <ion-select [(ngModel)]="selected_option">\n                <ion-option *ngFor="let x of totalProviderList" [value]="{number: x.providerNumber, hospitalName: x.providerName}">{{x.providerName}}</ion-option>\n            </ion-select>\n          </ion-item>\n          <button ion-button (click)="login(selected_option)">Submit</button>\n        </ion-col>\n      </ion-row>\n</ion-content>\n'/*ion-inline-end:"/home/healthereum/blockchain-healthereum/ionic-healtherium-app/src/pages/modal-content/modal-content.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_0__providers_healtherium_api_healtherium_api__["a" /* HealtheriumApi */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */]])
    ], ModalContentPage);
    return ModalContentPage;
}());

//# sourceMappingURL=modal-content.js.map

/***/ })

},[372]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL2NvbXBvbmVudHMubW9kdWxlLnRzIiwiLi4vLi4vc3JjL3Byb3ZpZGVycy9ub3RpZmljYXRpb24tYXBpL25vdGlmaWNhdGlvbi1hcGkudHMiLCIuLi8uLi9zcmMvcGFnZXMvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi50cyIsIi4uLy4uL25vZGVfbW9kdWxlcy9AYW5ndWxhci9jb3JlL2VzbTUgbGF6eSIsIi4uLy4uL3NyYyBsYXp5IiwiLi4vLi4vc3JjL3BhZ2VzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5tb2R1bGUudHMiLCIuLi8uLi9zcmMvcGFnZXMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLnRzIiwiLi4vLi4vc3JjL3Byb3ZpZGVycy9kZWxlZ2F0aW9uLWFwaS9kZWxlZ2F0aW9uLnRzIiwiLi4vLi4vc3JjL3BhZ2VzL2RvY3Rvci1kYXNob2FyZC9kb2N0b3ItZGFzaG9hcmQudHMiLCIuLi8uLi9zcmMvcGFnZXMvcGF0aWVudC1kYXNoYm9hcmQvcGF0aWVudC1kYXNoYm9hcmQudHMiLCIuLi8uLi9zcmMvcGFnZXMvaG9tZS9ob21lLnRzIiwiLi4vLi4vc3JjL3BhZ2VzL2Fib3V0LXVzL2Fib3V0LXVzLnRzIiwiLi4vLi4vc3JjL3BhZ2VzL2xhbmRpbmcvbGFuZGluZy50cyIsIi4uLy4uL3NyYy9hcHAvbWFpbi50cyIsIi4uLy4uL3NyYy9hcHAvYXBwLm1vZHVsZS50cyIsIi4uLy4uL3NyYy9wYWdlcy9yZWdpc3RyYXRpb24vcmVnaXN0cmF0aW9uLm1vZHVsZS50cyIsIi4uLy4uL3NyYy9jb21wb25lbnRzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy50cyIsIi4uLy4uL3NyYy9wcm92aWRlcnMvaGVhbHRoZXJpdW0tYXBpL2hlYWx0aGVyaXVtLWFwaS50cyIsIi4uLy4uL3NyYy9jb25maWcudHMiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9wcmVzY3JpcHRpb24vcHJlc2NyaXB0aW9uLnRzIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvZGVsZWdhdGlvbi9kZWxlZ2F0aW9uLnRzIiwiLi4vLi4vc3JjL2FwcC9hcHAuY29tcG9uZW50LnRzIiwiLi4vLi4vc3JjL3BhZ2VzL3BhdGllbnQtZGFzaGJvYXJkL3BhdGllbnQtZGFzaGJvYXJkLm1vZHVsZS50cyIsIi4uLy4uL3NyYy9wYWdlcy9kb2N0b3ItZGFzaG9hcmQvZG9jdG9yLWRhc2hib2FyZC5tb2R1bGUudHMiLCIuLi8uLi9zcmMvcGFnZXMvbW9kYWwtY29udGVudC9tb2RhbC1jb250ZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUF5QztBQUM4QjtBQUM1QjtBQUN5QjtBQUNOO0FBVTlEO0lBQUE7SUFBK0IsQ0FBQztJQUFuQixnQkFBZ0I7UUFUNUIsdUVBQVEsQ0FBQztZQUNULFlBQVksRUFBRSxDQUFDLDRGQUFzQjtnQkFDbEMseUZBQXFCO2dCQUNyQixtRkFBbUIsQ0FBQztZQUN2QixPQUFPLEVBQUUsQ0FBQyxrRUFBVyxDQUFDO1lBQ3RCLE9BQU8sRUFBRSxDQUFDLDRGQUFzQjtnQkFDN0IseUZBQXFCO2dCQUNyQixtRkFBbUIsQ0FBQztTQUN2QixDQUFDO09BQ1csZ0JBQWdCLENBQUc7SUFBRCx1QkFBQztDQUFBO0FBQUg7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZDhFO0FBQ2hFO0FBQ0g7QUFDTjtBQUVlO0FBRWpELElBQU0sV0FBVyxHQUFHO0lBQ2xCLE9BQU8sRUFBRSxJQUFJLHlFQUFXLENBQUMsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUUsQ0FBQztDQUNqRSxDQUFDO0FBRUY7Ozs7O0VBS0U7QUFFRjtJQUVFLGlDQUFtQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBRDNCLHFCQUFnQixHQUFHLHlEQUFRLEdBQUcsb0JBQW9CLENBQUM7UUFFekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCxrREFBZ0IsR0FBaEIsVUFBa0IsTUFBYztRQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQTBCLElBQUksQ0FBQyxnQkFBZ0IsSUFBRSxhQUFXLE1BQVEsRUFBQyxDQUFDLElBQUksQ0FDNUYsa0VBQVUsQ0FBQyxVQUFDLEdBQXNCO1lBQ2hDLE1BQU0sQ0FBQyxnREFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FDSCxDQUFDO0lBQ0osQ0FBQztJQUVELHVEQUFxQixHQUFyQixVQUF1QixNQUFjO1FBQ25DLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBMEIsSUFBSSxDQUFDLGdCQUFnQixJQUFFLGFBQVcsTUFBTSxpQkFBYyxFQUFDLENBQUMsSUFBSSxDQUN4RyxrRUFBVSxDQUFDLFVBQUMsR0FBc0I7WUFDaEMsTUFBTSxDQUFDLGdEQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUNILENBQUM7SUFDSixDQUFDO0lBRUQseURBQXVCLEdBQXZCLFVBQXlCLGFBQWtDO1FBQ3pELElBQUksZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUN6QixHQUFHLEVBQXFCLFVBQWEsRUFBYiwrQkFBYSxFQUFiLDJCQUFhLEVBQWIsSUFBYTtZQUFqQyxJQUFJLFlBQVk7WUFDbEIsZUFBZSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDdkM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUMsYUFBYSxFQUFFLGVBQWUsRUFBQyxFQUFFLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FDOUYsa0VBQVUsQ0FBQyxVQUFDLEdBQXNCO1lBQ2hDLE1BQU0sQ0FBQyxnREFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FDSCxDQUFDO0lBQ0osQ0FBQztJQWhDVSx1QkFBdUI7UUFEbkMseUVBQVUsRUFBRTt5Q0FHYyx3RUFBVTtPQUZ4Qix1QkFBdUIsQ0FrQ25DO0lBQUQsOEJBQUM7Q0FBQTtBQWxDbUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJZO0FBQzhEO0FBRzdCO0FBQ2Y7QUFPbEU7SUFPRSwwQkFDUyxPQUFzQixFQUN0QixTQUFvQixFQUNwQixTQUEwQixFQUN6QixHQUFtQixFQUNwQixXQUE4QixFQUM5QixTQUEwQjtRQUwxQixZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDekIsUUFBRyxHQUFILEdBQUcsQ0FBZ0I7UUFDcEIsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBQzlCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBVm5DLFNBQUksR0FBUSxFQUFFLENBQUM7UUFXWCxFQUFFLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNsRCxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxLQUFLLFNBQVM7b0JBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7b0JBQzFCLEtBQUssQ0FBQztnQkFDUixLQUFLLFFBQVE7b0JBQ1gsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7b0JBQ3pCLEtBQUssQ0FBQztnQkFDUixLQUFLLFVBQVU7b0JBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDO29CQUM5QixLQUFLLENBQUM7Z0JBQ1IsS0FBSyxXQUFXO29CQUNkLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDO29CQUM1QixLQUFLLENBQUM7Z0JBQ1IsS0FBSyxPQUFPO29CQUNWLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO29CQUN4QixLQUFLLENBQUM7Z0JBQ1I7b0JBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7b0JBQzFCLEtBQUssQ0FBQztZQUVYLENBQUM7UUFDSixDQUFDO0lBQ0wsQ0FBQztJQUVELHlDQUFjLEdBQWQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELG1DQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELHVDQUFZLEdBQVo7UUFBQSxpQkFRQztRQVBDLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFO2FBQ3BCLFNBQVMsQ0FBQyxrQkFBUTtZQUNqQixLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMxQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQyxDQUFDLEVBQUMsZUFBSztZQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0QscUNBQVUsR0FBVjtRQUFBLGlCQXdCQztRQXZCQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDO1FBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFRO1lBQ2hELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsc0ZBQWdCLEVBQUUsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQyxLQUFJLENBQUMsSUFBSSxFQUFHLE1BQU0sRUFBRSxjQUFjLEVBQUUsQ0FBQyxDQUFDO2dCQUM5RyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDeEIsQ0FBQztRQUNILENBQUMsRUFDQyxlQUFLO1lBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixJQUFJLElBQUksR0FBRztnQkFDVCxLQUFLLEVBQUUsT0FBTztnQkFDZCxRQUFRLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPO2FBQzlCLENBQUM7WUFDRixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ3BDLE9BQU8sRUFBRSxnQkFBZ0I7WUFDekIsbUJBQW1CLEVBQUUsSUFBSTtTQUMxQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCx1Q0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQ2hDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDO1NBQ2hCLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBbkdVLGdCQUFnQjtRQUo1Qix3RUFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGNBQWM7V0FDUTtTQUNqQyxDQUFDOzZFQVMrQjtZQUNYLHNFQUFTO1lBQ1QsbUdBQWU7WUFDcEIsd0VBQWM7WUFDUCx3RUFBaUI7WUFDbkIsYUFBZTtPQWJ4QixnQkFBZ0IsQ0FvRzVCO0lBQUQsQ0FBQztBQUFBO1NBcEdZLGdCQUFnQixlOzs7Ozs7O0FDWjdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSw0Q0FBNEMsV0FBVztBQUN2RDtBQUNBO0FBQ0Esa0M7Ozs7Ozs7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQnlDO0FBQ087QUFDSTtBQUNrQjtBQVl0RTtJQUFBO0lBQXNDLENBQUM7SUFBMUIsdUJBQXVCO1FBVm5DLHVFQUFRLENBQUM7WUFDUixZQUFZLEVBQUU7Z0JBQ1oseUVBQWlCO2FBQ2xCO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLHNFQUFlLENBQUMsUUFBUSxDQUFDLHlFQUFpQixDQUFDO2dCQUMzQyx1RkFBZ0I7YUFDakI7WUFDRCxlQUFlLEVBQUUsRUFBRTtTQUNwQixDQUFDO09BQ1csdUJBQXVCLENBQUc7SUFBRCw4QkFBQztDQUFBO0FBQUg7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZNO0FBQ2dCO0FBTzFEO0lBRUUsMkJBQW1CLFFBQXdCLEVBQUUsTUFBaUI7UUFBM0MsYUFBUSxHQUFSLFFBQVEsQ0FBZ0I7UUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCwwQ0FBYyxHQUFkO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxpQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBYlUsaUJBQWlCO1FBSjdCLHdFQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1dBQ0c7U0FDbEMsQ0FBQzswQkFHOEQ7T0FGbkQsaUJBQWlCLENBZTdCO0lBQUQsQ0FBQztBQUFBO1NBZlksaUJBQWlCLGU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1I2QztBQUNoQztBQUNIO0FBR3hDOzs7OztFQUtFO0FBRUY7SUFFRSw0QkFBbUIsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVELHFDQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFJLHlEQUFRLHNCQUFtQixFQUFFLElBQUksQ0FBQzthQUN4RCxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhDQUFpQixHQUFqQixVQUFrQixJQUFJO1FBQ3BCLElBQU0sTUFBTSxHQUFHLElBQUksd0VBQVUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLHlEQUFRLDJCQUF3QixFQUFFLEVBQUUsTUFBTSxVQUFFLENBQUM7YUFDbEUsR0FBRyxDQUFDLGtCQUFRLElBQUksZUFBUSxFQUFSLENBQVEsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCwwQ0FBYSxHQUFiLFVBQWMsSUFBUztRQUNyQixJQUFNLE1BQU0sR0FBRyxJQUFJLHdFQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3BELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSx5REFBUSw2QkFBMEIsRUFBRSxFQUFFLE1BQU0sVUFBRSxDQUFDO2FBQ3BFLEdBQUcsQ0FBQyxrQkFBUSxJQUFJLGVBQVEsRUFBUixDQUFRLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBR0QsNkNBQWdCLEdBQWhCLFVBQWlCLElBQUk7UUFDbkIsSUFBTSxXQUFXLEdBQUc7WUFDbEIsT0FBTyxFQUFFLElBQUkseUVBQVcsQ0FBQyxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUk7U0FDL0UsQ0FBQztRQUNBLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBSSx5REFBUSx5QkFBc0IsRUFBRSxXQUFXLENBQUM7YUFDcEUsR0FBRyxDQUFDLGtCQUFRLElBQUksZUFBUSxFQUFSLENBQVEsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCx3Q0FBVyxHQUFYLFVBQVksSUFBSTtRQUNkLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSx5REFBUSw4QkFBMkIsRUFBRSxJQUFJLENBQUM7YUFDaEUsR0FBRyxDQUFDLGtCQUFRO1lBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUF4Q1Usa0JBQWtCO1FBRDlCLHlFQUFVLEVBQUU7eUNBR2Msd0VBQVU7T0FGeEIsa0JBQWtCLENBMkM5QjtJQUFELHlCQUFDO0NBQUE7QUEzQzhCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1ptQjtBQUN5RjtBQUMxRDtBQUNkO0FBQ3lCO0FBQ3BEO0FBQzBCO0FBS2xFO0lBd0JFLDZCQUNTLE9BQXNCLEVBQ3RCLFNBQW9CLEVBQ3BCLEdBQW1CLEVBQ25CLFNBQTBCLEVBQzFCLFdBQThCLEVBQzlCLFdBQThCLEVBQzlCLFNBQTBCLEVBQzFCLFFBQWtCLEVBQ2pCLG9CQUE2QztRQVI5QyxZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsUUFBRyxHQUFILEdBQUcsQ0FBZ0I7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFtQjtRQUM5QixjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUMxQixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2pCLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBeUI7UUF6QmhELGlCQUFZLEdBQVEsRUFBRSxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFXLElBQUksQ0FBQztRQUNoQyxzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFHNUIscUJBQWdCLEdBQVEsSUFBSSxDQUFDO1FBSTdCLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFDNUIsdUJBQWtCLEdBQVcsQ0FBQyxDQUFDO1FBQzlCLDJCQUFzQixHQUFRLElBQUksQ0FBQztRQUczQyxTQUFJLEdBQVEsRUFBRSxDQUFDO1FBWWIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFakMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztZQUM5QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUN0RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNySCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFDekcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO2dCQUNoRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDNUMsQ0FBQztRQUNILENBQUM7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQTlDRCwrQ0FBaUIsR0FBakIsVUFBa0IsSUFBUztRQUN6QixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7SUFDN0MsQ0FBQztJQThDRCxzQ0FBUSxHQUFSO1FBQ0UsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUM7WUFDN0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDMUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDaEYsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDNUUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQzVDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQ3pELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDO1lBQ3pFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztZQUMxRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUMxQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUM5RCxDQUFDO0lBQ0gsQ0FBQztJQUdELDRDQUFjLEdBQWQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUM5RCxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsa0JBQWtCO0lBQ2xCLDhDQUFnQixHQUFoQjtRQUNFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQy9ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDaEosSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsZ0JBQWdCO0lBQ2hCLDBDQUFZLEdBQVosVUFBYSxJQUFJO1FBQ2YsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7WUFDaEMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUM7U0FDaEIsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCx5QkFBeUI7SUFDekIsNENBQWMsR0FBZDtRQUFBLGlCQStCQztRQTlCQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQVE7WUFDakUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzVDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7WUFDbkMsQ0FBQztZQUNELElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksSUFBSSxHQUFHO29CQUNULEtBQUssRUFBRSxTQUFTO29CQUNoQixRQUFRLEVBQUUsNEJBQTRCO2lCQUN2QztnQkFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFCLENBQUM7UUFFSCxDQUFDLEVBQ0MsZUFBSztZQUNILEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDOUIsSUFBSSxJQUFJLEdBQUc7Z0JBQ1QsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsUUFBUSxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTzthQUM5QjtZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNENBQWMsR0FBZCxVQUFlLE9BQU87UUFBdEIsaUJBZ0NDO1FBL0JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoQyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRSxLQUFLO1lBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO1FBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDekMsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNsQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFRO1lBQ3RGLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixJQUFJLEtBQUssR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxzRkFBZ0IsRUFBRSxFQUFFLFFBQVEsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsS0FBSSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsS0FBSSxDQUFDLElBQUksRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxXQUFXLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDM1QsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2xCLENBQUMsRUFDQyxlQUFLO1lBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5QixJQUFJLElBQUksR0FBRztnQkFDVCxLQUFLLEVBQUUsT0FBTztnQkFDZCxRQUFRLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPO2dCQUM3QiwwRUFBMEU7YUFDM0U7WUFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhDQUFnQixHQUFoQjtRQUFBLGlCQXFCQztRQXBCQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDakYsU0FBUyxDQUFDLGtCQUFRO1lBQ2pCLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7WUFDNUIsRUFBRSxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUM7Z0JBQzFCLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBSztvQkFDbEIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNwRSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2hFLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDO1FBQ04sQ0FBQyxFQUNDLGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSxLQUFLLENBQUMsT0FBTzthQUN4QixDQUFDO1lBQ0YsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCwrQ0FBaUIsR0FBakI7UUFDRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJO1FBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDdEUsU0FBUyxDQUNSLGtCQUFRO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDM0MsQ0FBQyxFQUNELGVBQUs7WUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSxLQUFLLENBQUMsT0FBTzthQUN4QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCxrREFBb0IsR0FBcEI7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUFHRCw0Q0FBYyxHQUFkO1FBQ0UsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFNLElBQUksR0FBRztZQUNYLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUk7WUFDM0IsT0FBTyxFQUFFLElBQUksQ0FBQyxzQkFBc0I7WUFDcEMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUM1QixRQUFRLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsd0ZBQXdGO1NBQ25KLENBQUM7UUFDRixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQVE7WUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUkseURBQVEsWUFBUyxHQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBQyxRQUFRLENBQUMsQ0FBQztRQUMvRSxDQUFDLEVBQ0MsZUFBSztZQUNILElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsSUFBSSxJQUFJLEdBQUc7Z0JBQ1QsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsUUFBUSxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTzthQUM5QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUNwQyxPQUFPLEVBQUUsZ0JBQWdCO1lBQ3pCLG1CQUFtQixFQUFFLElBQUk7U0FDMUIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsbURBQXFCLEdBQXJCO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsb0JBQW9CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQzthQUNqRixTQUFTLENBQUMsa0JBQVE7WUFDakIsS0FBSSxDQUFDLGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUUsZUFBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0RBQW9CLEdBQXBCLFVBQXFCLE9BQU87UUFBNUIsaUJBYUM7UUFaQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0QsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsdUZBQWlCLEVBQUUsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7WUFDM0YsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2hCLEtBQUssQ0FBQyxZQUFZLENBQUMsY0FBUSxLQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVELENBQUM7UUFDRCxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLHVGQUFpQixFQUFFLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUM7WUFDOUgsT0FBTyxDQUFDLE9BQU8sQ0FBQztnQkFDZCxFQUFFLEVBQUUsT0FBTzthQUNaLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxZQUFZLENBQUMsY0FBUSxLQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlELENBQUM7SUFDSCxDQUFDO0lBMVFVLG1CQUFtQjtRQUovQix3RUFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHNCQUFzQjtXQUNHO1NBQ3BDLENBQUM7NkVBMEIrQjtZQUNYLGtHQUFTO1lBQ2Ysc0VBQWM7WUFDUix3RUFBZTtZQUNiLHdFQUFpQjtZQUNqQix1RUFBaUI7WUFDbkIsZ0VBQWU7WUFDaEIsNkdBQVE7WUFDSyxnQkFBdUI7T0FqQzVDLG1CQUFtQixDQTRRL0I7SUFBRCxDQUFDO0FBQUE7Q0E1UStCO0FBNFEvQixDQUFDLDBDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZSaUY7QUFDekM7QUFDNEQ7QUFDaEM7QUFDaEM7QUFDdEMsNkNBQTZDO0FBQzdDLDREQUE0RDtBQU01RDtJQWdCRSw4QkFDUyxPQUFzQixFQUN0QixTQUFvQixFQUNuQixTQUFvQixFQUNyQixHQUFtQixFQUNuQixXQUE4QixFQUM5QixTQUEwQixFQUMxQixRQUFrQjtRQU5sQixZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUNyQixRQUFHLEdBQUgsR0FBRyxDQUFnQjtRQUNuQixnQkFBVyxHQUFYLFdBQVcsQ0FBbUI7UUFDOUIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQXRCM0IsY0FBUyxHQUFlLEVBQUUsQ0FBQztRQUszQixpQkFBWSxHQUFRLEVBQUUsQ0FBQztRQUN2QixtQkFBYyxHQUFXLEVBQUUsQ0FBQztRQUM1QixvQkFBZSxHQUFXLEVBQUUsQ0FBQztRQUM3QixVQUFLLEdBQVksSUFBSSxDQUFDO1FBZ0JwQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDcEIsQ0FBQztRQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNuRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztRQUMvQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFHLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQzNHLENBQUM7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDO0lBQ3hDLENBQUM7SUFFRCw2Q0FBYyxHQUFkO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFRCx5Q0FBVSxHQUFWLFVBQVcsS0FBSztRQUFoQixpQkErQ0M7UUE5Q0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVCLElBQUksTUFBTSxHQUFnQixRQUFRLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUU7YUFDckIsSUFBSSxDQUFDLFVBQUMsTUFBdUI7WUFDNUIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLGdDQUFnQztnQkFDaEMsaUJBQWlCO2dCQUNqQixJQUFJLFNBQU8sR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQVk7b0JBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3ZDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ1YsS0FBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7d0JBQzFCLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO29CQUM3QixDQUFDO29CQUNELElBQUksQ0FBQyxDQUFDO3dCQUNKLEtBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO3dCQUN6QixLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztvQkFDOUIsQ0FBQztvQkFDRCxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsc0JBQXNCO29CQUM3QyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7b0JBQy9CLFNBQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQjtnQkFDekMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsc0JBQXNCO2dCQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7Z0JBQzlCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFeEIsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDekIsMkNBQTJDO2dCQUMzQyxzRkFBc0Y7Z0JBQ3RGLGdEQUFnRDtZQUNsRCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sZ0dBQWdHO1lBQ2xHLENBQUM7UUFDSCxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQyxDQUFNO1lBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0IsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDVixLQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztnQkFDekIsS0FBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDNUIsQ0FBQztZQUNELElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixLQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztZQUM1QixDQUFDO1FBRUgsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMkNBQVksR0FBWixVQUFhLEtBQUs7UUFBbEIsaUJBa0NDO1FBakNDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMzQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ1YsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQ3ZDLENBQUM7UUFDRCxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUN4QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxrQkFBUTtZQUM1QyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxTQUFTO2dCQUNoQixRQUFRLEVBQUMsUUFBUSxDQUFDLE9BQU87Z0JBQ3pCLDBFQUEwRTthQUMzRTtZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUNELGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU87Z0JBQzVCLDBFQUEwRTthQUMzRTtZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsOENBQWUsR0FBZjtRQUNFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUk7UUFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDNUUsU0FBUyxDQUNSLGtCQUFRO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7WUFDekMsMEJBQTBCO1FBQzVCLENBQUMsRUFDSCxlQUFLO1FBRUwsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsOENBQWUsR0FBZixVQUFnQixLQUFlO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQseUNBQVUsR0FBVjtRQUFBLGlCQTZCQztRQTVCQyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDckQsY0FBYyxDQUFDLElBQUksR0FBRyxZQUFZLENBQUM7UUFDbkMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQVE7WUFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QixFQUFFLEVBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixjQUFjLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxjQUFJO29CQUNoRCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNsQixJQUFJLElBQUksR0FBRzt3QkFDVCxLQUFLLEVBQUUsU0FBUzt3QkFDaEIsUUFBUSxFQUFDLElBQUksQ0FBQyxPQUFPO3FCQUN0QjtvQkFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQztZQUNMLENBQUM7UUFDSCxDQUFDLEVBQ0QsZUFBSztZQUNILEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDOUIsSUFBSSxJQUFJLEdBQUc7Z0JBQ1QsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsUUFBUSxFQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTztnQkFDNUIsMEVBQTBFO2FBQzNFO1lBQ0QsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxzQ0FBTyxHQUFQO1FBQUEsaUJBZUM7UUFkQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNwRCxJQUFNLElBQUksR0FBRztZQUNaLEdBQUcsRUFBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUc7WUFDMUIsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUMxQixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJO1lBQzNCLFFBQVEsRUFBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNO1NBQzVDLENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFRO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixNQUFNLENBQUMsSUFBSSxDQUFJLHlEQUFRLFlBQVMsR0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUMsUUFBUSxDQUFDLENBQUM7UUFDL0UsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsd0NBQVMsR0FBVDtRQUFBLGlCQW9CQztRQW5CQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxrQkFBUTtZQUMzRyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDcEIsRUFBRSxFQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixLQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztnQkFDcEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7Z0JBQzVCLE1BQU0sQ0FBQyxPQUFPLENBQUMsZUFBSztvQkFDbEIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNwRSxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2hFLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDO1lBQ0osQ0FBQztZQUNELElBQUksQ0FBQyxDQUFDO2dCQUNKLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLENBQUM7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHVCQUF1QjtJQUN2QixvQ0FBb0M7SUFDcEMsZ0NBQWdDO0lBQ2hDLGFBQWE7SUFDYixpQ0FBaUM7SUFDakMsTUFBTTtJQUNOLElBQUk7SUFDSix3QkFBd0I7SUFDeEIsd0NBQXdDO0lBQ3hDLElBQUk7SUFFSiwyQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQ2hDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDO1NBQ2hCLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsMkNBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFDcEMsT0FBTyxFQUFFLGdCQUFnQjtZQUN6QixtQkFBbUIsRUFBRSxJQUFJO1NBQzFCLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQXhQVSxvQkFBb0I7UUFKaEMsd0VBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSx3QkFBd0I7V0FDRztTQUN0QyxDQUFDOzZFQWtCK0I7WUFDWCwyRUFBUztZQUNSLGtHQUFTO1lBQ2hCLHdFQUFjO1lBQ04sdUVBQWlCO1lBQ25CLGlFQUFlO1lBQ2hCLFFBQVE7T0F2QmhCLG9CQUFvQixDQTBQaEM7SUFBRCxDQUFDO0FBQUE7U0ExUFksb0JBQW9CLGdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWmlDO0FBQ3hCO0FBQzBDO0FBQ2xCO0FBT2xFO0lBSUksY0FBbUIsR0FBa0IsRUFDMUIsU0FBb0IsRUFDcEIsU0FBMEIsRUFDMUIsUUFBa0I7UUFIVixRQUFHLEdBQUgsR0FBRyxDQUFlO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUw3QixTQUFJLEdBQVEsSUFBSSxDQUFDO1FBTWIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLENBQUM7SUFDTCxDQUFDO0lBRUQsNkJBQWMsR0FBZDtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0NBQW9DLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsb0JBQUssR0FBTCxVQUFNLElBQUk7UUFDTixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxzRkFBZ0IsRUFBRSxFQUFFLElBQUksUUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUMvRSxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELHVCQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvRkFBZ0IsRUFBRSxFQUFDLElBQUksUUFBQyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQTNCUSxJQUFJO1FBTGhCLHdFQUFTLENBQUM7WUFDUCxXQUFXLEVBQUUsR0FBVztXQUNSO1NBQ25CLENBQUM7NkVBTXVDO1lBQ2Ysc0VBQVM7WUFDVCxpRUFBZTtZQUNoQixDQUFRO09BUHBCLElBQUksQ0E0QmhCO0lBQUQsQ0FBQztBQUFBO1NBNUJZLElBQUksbUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVnlCO0FBQ2U7QUFPekQ7SUFFRSxxQkFBbUIsT0FBc0IsRUFBUyxTQUFvQjtRQUFuRCxZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBVztJQUN0RSxDQUFDO0lBRUQsb0NBQWMsR0FBZDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBUFUsV0FBVztRQUp2Qix3RUFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGVBQWU7V0FDRztTQUM3QixDQUFDO29CQUdzRTtPQUYzRCxXQUFXLENBU3ZCO0lBQUQsQ0FBQztBQUFBO1NBVFksV0FBVyxnQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSa0I7QUFDMEM7QUFPcEY7SUFJSSxxQkFBbUIsR0FBa0IsRUFDMUIsU0FBb0IsRUFDcEIsU0FBMEIsRUFDMUIsUUFBa0I7UUFIVixRQUFHLEdBQUgsR0FBRyxDQUFlO1FBQzFCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUw3QixTQUFJLEdBQVEsSUFBSSxDQUFDO1FBTWIsRUFBRSxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNyQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNwQixDQUFDO0lBQ1AsQ0FBQztJQUVELG9DQUFjLEdBQWQ7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQWpCUSxXQUFXO1FBTHZCLHdFQUFTLENBQUM7WUFDUCxXQUFXLEVBQUUsTUFBYztXQUNSO1NBQ3RCLENBQUM7NkVBTXVDO1lBQ2Ysc0VBQVM7WUFDVCxpRUFBZTtZQUNoQixRQUFRO09BUHBCLFdBQVcsQ0FrQnZCO0lBQUQsQ0FBQztBQUFBO1NBbEJZLFdBQVcsZTs7Ozs7Ozs7Ozs7QUNSbUQ7QUFFbEM7QUFFekMseUdBQXNCLEVBQUUsQ0FBQyxlQUFlLENBQUMsOERBQVMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKaUM7QUFDekM7QUFDZTtBQUVEO0FBQ0g7QUFDWjtBQUNhO0FBQ2lCO0FBQzNCO0FBQ087QUFDYjtBQUNlO0FBQ2lCO0FBRW5CO0FBQ007QUFDbUI7QUFFOUM7QUFDaUI7QUFDZTtBQUN5QjtBQUNMO0FBQ1I7QUFDNUUsMERBQTBEO0FBQ3FDO0FBQ0o7QUFDckI7QUFDdEUsc0JBQXNCO0FBQ3RCLElBQUksTUFBTSxHQUFHO0lBQ1gsTUFBTSxFQUFFLHlDQUF5QztJQUNqRCxVQUFVLEVBQUUsbUNBQW1DO0lBQy9DLFdBQVcsRUFBRSwwQ0FBMEM7SUFDdkQsU0FBUyxFQUFFLG1CQUFtQjtJQUM5QixhQUFhLEVBQUUsK0JBQStCO0lBQzlDLGlCQUFpQixFQUFFLGNBQWM7Q0FDbEMsQ0FBQztBQUNGLGlEQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBZ0QvQjtJQUFBO0lBQXlCLENBQUM7SUFBYixTQUFTO1FBOUNyQix1RUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLDhEQUFLO2dCQUNMLDRFQUFXO2dCQUNYLDhEQUFJO2dCQUNKLDZFQUFXO2dCQUNYLDZGQUFnQjthQUNqQjtZQUNELE9BQU8sRUFBRTtnQkFDUCxnRkFBYTtnQkFDYixpRUFBVTtnQkFDVixtQkFBbUI7Z0JBQ25CLGtFQUFXLENBQUMsT0FBTyxDQUFDLDhEQUFLLEVBQUUsRUFBRSxFQUNqQztvQkFDRSxLQUFLLEVBQUU7d0JBQ0wsRUFBRSxZQUFZLEVBQUUscUVBQXFFLEVBQUUsSUFBSSxFQUFFLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFO3FCQUNsTDtpQkFDRixDQUFDO2dCQUNFLDhFQUFnQjtnQkFDaEIsd0VBQWlCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztnQkFDdkMsdUZBQXdCO2dCQUN4Qix1R0FBc0I7Z0JBQ3RCLG1HQUF1QjtnQkFDdkIsc0hBQTBCO2dCQUMxQixrSEFBeUI7YUFDMUI7WUFDRCxTQUFTLEVBQUUsQ0FBQywrREFBUSxDQUFDO1lBQ3JCLGVBQWUsRUFBRTtnQkFDZiw4REFBSztnQkFDTCwyRkFBZ0I7Z0JBQ2hCLDRFQUFXO2dCQUNYLDhEQUFJO2dCQUNKLDZFQUFXO2dCQUNYLDZGQUFnQjthQUNqQjtZQUNELFNBQVMsRUFBRTtnQkFDVCw0RUFBUztnQkFDVCxrRkFBWTtnQkFDWixvRUFBTTtnQkFDTiwyRUFBUztnQkFDVCxFQUFFLE9BQU8sRUFBRSxtRUFBWSxFQUFFLFFBQVEsRUFBRSx3RUFBaUIsRUFBRTtnQkFDdEQsbUdBQWM7Z0JBQ2QsOEdBQXVCO2dCQUN2QixpR0FBa0I7YUFDbkI7U0FDRixDQUFDO09BQ1csU0FBUyxDQUFJO0lBQUQsZ0JBQUM7Q0FBQTtBQUFKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEZtQjtBQUNPO0FBQ0U7QUFVbEQ7SUFBQTtJQUFxQyxDQUFDO0lBQXpCLHNCQUFzQjtRQVJsQyx1RUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLHVFQUFnQjthQUNqQjtZQUNELE9BQU8sRUFBRTtnQkFDUCxzRUFBZSxDQUFDLFFBQVEsQ0FBQyx1RUFBZ0IsQ0FBQzthQUMzQztTQUNGLENBQUM7T0FDVyxzQkFBc0IsQ0FBRztJQUFELDZCQUFDO0NBQUE7QUFBSDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWnNCO0FBRW1DO0FBRTVGOzs7OztHQUtHO0FBS0g7SUFJRSxnQ0FBb0Isb0JBQTZDO1FBQTdDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBeUI7UUFGakUsVUFBSyxHQUFXLENBQUMsQ0FBQztRQUdoQixPQUFPLENBQUMsR0FBRyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELHlDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsaURBQWdCLEdBQWhCO1FBQUEsaUJBYUM7UUFaQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDO2FBQzNFLFNBQVMsQ0FBQyxrQkFBUTtZQUNqQixLQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMvQyxLQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvQixFQUFFLEVBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBQztnQkFDNUIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUUsQ0FBQztZQUNwRCxDQUFDO1FBQ0gsQ0FBQyxFQUFDLGVBQUs7WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHdEQUF1QixHQUF2QixVQUF3QixhQUFhO1FBQ25DLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUM7YUFDN0QsU0FBUyxDQUFDLGtCQUFRLElBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyx3Q0FBd0MsQ0FBQyxHQUFDLEVBQzFFLGVBQUs7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQS9CUTtRQUFSLG9FQUFLLEVBQUU7OytEQUFhO0lBSFYsc0JBQXNCO1FBSmxDLHdFQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtXQUNRO1NBQ2xDLENBQUM7K0JBS2lFO09BSnRELHNCQUFzQixDQW9DbEM7SUFBRCxDQUFDO0FBQUE7U0FwQ1ksc0JBQXNCLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2R3QztBQUNoQztBQUNaO0FBQ2M7QUFDUjtBQUNHO0FBT3hDO0lBSUUsc0JBQXNCO0lBQ3RCLHdCQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO0lBQ3BDLENBQUM7SUFFRCxtQ0FBVSxHQUFWLFVBQVcsSUFBSTtRQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSx5REFBUSxrQkFBZSxFQUFFLElBQUksQ0FBQzthQUNwRCxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUM7UUFDbEMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFJLHlEQUFRLDBCQUF1QixFQUFDLElBQUksQ0FBQzthQUM3RCxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDhCQUFLLEdBQUwsVUFBTSxJQUFJO1FBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSx5REFBUSxvQkFBaUIsRUFBRSxJQUFJLENBQUM7YUFDdEQsR0FBRyxDQUFDLGtCQUFRO1lBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSx5REFBUSxrQkFBZSxFQUFFLElBQUksQ0FBQzthQUNwRCxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9DQUFXLEdBQVgsVUFBWSxJQUFJO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUM7UUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFJLHlEQUFRLDBCQUF1QixFQUFFLElBQUksQ0FBQzthQUM1RCxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlDQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUM7UUFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFJLHlEQUFRLHVCQUFvQixFQUFFLElBQUksQ0FBQzthQUN6RCxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELG1DQUFVLEdBQVYsVUFBVyxJQUFJO1FBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUkseURBQVEscUJBQWtCLEVBQUUsSUFBSSxDQUFDO2FBQ3ZELEdBQUcsQ0FBQyxrQkFBUTtZQUNYLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscUNBQVksR0FBWixVQUFhLElBQUk7UUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSx5REFBUSxtQkFBZ0IsRUFBRSxJQUFJLENBQUM7YUFDckQsR0FBRyxDQUFDLGtCQUFRO1lBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpQ0FBUSxHQUFSLFVBQVMsWUFBWTtRQUNuQixJQUFJLElBQUksR0FBRyxZQUFZLENBQUM7UUFDeEIsSUFBSSxVQUFVLEdBQUcsaURBQWdCLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMxQyxJQUFJLFVBQVUsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUcsWUFBWSxDQUFDLElBQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1RSxNQUFNLENBQUMsMkRBQVUsQ0FBQyxNQUFNLENBQUMsa0JBQVE7WUFDL0IsVUFBVSxDQUFDLEVBQUUsQ0FBQyxpREFBZ0IsQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUNwRCxVQUFDLFFBQWE7Z0JBQ1oscUJBQXFCO2dCQUNyQixZQUFZLENBQUMsUUFBUSxHQUFHLENBQUMsUUFBUSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHO1lBQ2pGLENBQUMsRUFDRCxVQUFDLEtBQUs7Z0JBQ0osZ0JBQWdCO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQixRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3pCLENBQUMsRUFDRDtnQkFDRSxpQkFBaUI7Z0JBQ2pCLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7Z0JBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVELGtDQUFTLEdBQVQsVUFBVSxJQUFJLEVBQUUsUUFBUTtRQUN0QixJQUFNLE1BQU0sR0FBRyxJQUFJLHdFQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbEYsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLHlEQUFRLHFCQUFrQixFQUFFLEVBQUUsTUFBTSxVQUFFLENBQUM7YUFDNUQsR0FBRyxDQUFDLGtCQUFRO1lBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3Q0FBZSxHQUFmLFVBQWdCLElBQUksRUFBRSxRQUFRO1FBQzVCLElBQU0sTUFBTSxHQUFHLElBQUksd0VBQVUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMvRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUkseURBQVEsMkJBQXdCLEVBQUUsRUFBRSxNQUFNLFVBQUUsQ0FBQzthQUNsRSxHQUFHLENBQUMsa0JBQVE7WUFDWCxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFnQixHQUFoQixVQUFpQixRQUFRLEVBQUUsUUFBUTtRQUNqQyxJQUFNLE1BQU0sR0FBRyxJQUFJLHdFQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDcEYsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLHlEQUFRLG1CQUFnQixFQUFFLEVBQUUsTUFBTSxVQUFFLENBQUM7YUFDMUQsR0FBRyxDQUFDLGtCQUFRO1lBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBaUIsR0FBakIsVUFBa0IsR0FBRztRQUNuQixJQUFNLE1BQU0sR0FBRyxJQUFJLHdFQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ25ELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSx5REFBUSx3QkFBcUIsRUFBQyxFQUFFLE1BQU0sVUFBRSxDQUFDO2FBQ2hFLEdBQUcsQ0FBQyxrQkFBUTtZQUNYLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsb0NBQVcsR0FBWCxVQUFZLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUTtRQUF4QyxpQkFRQztRQVBDLElBQU0sTUFBTSxHQUFHLElBQUksd0VBQVUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFDLFFBQVEsQ0FBQztRQUM5RyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSx5REFBUSxzQkFBbUIsRUFBRSxFQUFFLE1BQU0sVUFBRSxDQUFDO2FBQy9ELEdBQUcsQ0FBQyxrQkFBUTtZQUNYLEtBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0NBQU8sR0FBUCxVQUFRLElBQUk7UUFDVixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUkseURBQVEsbUJBQWdCLEVBQUUsSUFBSSxDQUFDO2FBQ3ZELEdBQUcsQ0FBQyxrQkFBUTtZQUNYLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsMkNBQWtCLEdBQWxCLFVBQW1CLElBQUk7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1FBQ3hELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEIsSUFBSSxXQUFXLEdBQUMsRUFBRSxDQUFDO1FBQ25CLEVBQUUsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUM7WUFDWCxXQUFXLEdBQUcsYUFBVyxJQUFJLENBQUMsR0FBSyxDQUFDO1FBQ3RDLENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBQztZQUNwQixXQUFXLEdBQUcsY0FBWSxJQUFJLENBQUMsT0FBUyxDQUFDO1FBQzNDLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUkseURBQVEsMkJBQXdCLEdBQUUsV0FBVyxDQUFDO2FBQ3JFLEdBQUcsQ0FBQyxrQkFBUTtZQUNYLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQscUNBQVksR0FBWjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBZ0IseURBQVEsbUJBQWdCLENBQUM7YUFDMUQsR0FBRyxDQUFDLGtCQUFRO1lBQ1gsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUF4S1UsY0FBYztRQUQxQix5RUFBVSxFQUFFO3lDQU1lLHdFQUFVO09BTHpCLGNBQWMsQ0EwSzFCO0lBQUQscUJBQUM7Q0FBQTtBQTFLMEI7Ozs7Ozs7Ozs7O0FDWnBCLElBQU0sUUFBUSxHQUFHLHVCQUF1QixDQUFDO0FBQ3pDLElBQU0sUUFBUSxHQUFHLGtDQUFrQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRFo7QUFFb0M7QUFDbkM7QUFHaEQ7Ozs7O0dBS0c7QUFLSDtJQVFFLCtCQUFvQixHQUFtQixFQUFTLFdBQThCO1FBQTFELFFBQUcsR0FBSCxHQUFHLENBQWdCO1FBQVMsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBSjlFLFVBQUssR0FBVyxDQUFDLENBQUM7UUFFbEIsMEJBQXFCLEdBQVcsS0FBSyxDQUFDO1FBR3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUNBQXVDLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUNFLEVBQUUsRUFBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUM7WUFDaEIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDMUIsQ0FBQztJQUNILENBQUM7SUFFRCxnREFBZ0IsR0FBaEI7UUFBQSxpQkFzQkM7UUFyQkMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxRQUFRLENBQUM7UUFDYixFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDO1lBQ2hCLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQztRQUNqQyxDQUFDO1FBQ0QsSUFBSSxFQUFDO1lBQ0gsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEMsQ0FBQztRQUNELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUN6RCxTQUFTLENBQUMsa0JBQVE7WUFDakIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO1lBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsS0FBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDL0MsS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDakMsQ0FBQyxFQUFDLGVBQUs7WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ3BDLE9BQU8sRUFBRSxnQkFBZ0I7WUFDekIsbUJBQW1CLEVBQUUsSUFBSTtTQUMxQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUEvQ1E7UUFBUixvRUFBSyxFQUFFOzsyREFBVTtJQUNUO1FBQVIsb0VBQUssRUFBRTs7OERBQWE7SUFFWjtRQUFSLG9FQUFLLEVBQUU7OzhEQUFhO0lBTFYscUJBQXFCO1FBSmpDLHdFQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsY0FBYztXQUNRO1NBQ2pDLENBQUM7OEJBUzhFO09BUm5FLHFCQUFxQixDQW1EakM7SUFBRCxDQUFDO0FBQUE7U0FuRFkscUJBQXFCLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hCK0M7QUFDaEM7QUFDa0I7QUFFbkU7Ozs7O0dBS0c7QUFLSDtJQWlCRSw2QkFDVSxrQkFBc0MsRUFDdkMsV0FBOEIsRUFDOUIsU0FBMEI7UUFGekIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN2QyxnQkFBVyxHQUFYLFdBQVcsQ0FBbUI7UUFDOUIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFqQm5DLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFXNUIsYUFBUSxHQUFHLElBQUksQ0FBQztRQU9kLE9BQU8sQ0FBQyxHQUFHLENBQUMscUNBQXFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCO1FBQUEsaUJBY0M7UUFiQyxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQzthQUM5QyxTQUFTLENBQUMsa0JBQVE7WUFDakIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixLQUFJLENBQUMsUUFBUSxHQUFHLE1BQU07WUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbEMsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxFQUNDLGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELHFDQUFPLEdBQVAsVUFBUSxHQUFRO1FBQ2QsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVELHNDQUFRLEdBQVI7UUFBQSxpQkFtQ0M7UUFsQ0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxVQUFVO1FBQzFCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUMsSUFBSSxNQUFNLEdBQUc7WUFDWCxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWE7WUFDOUIsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRztTQUN2QixDQUFDO1FBQ0YsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO2lCQUNyQyxTQUFTLENBQUMsa0JBQVE7Z0JBQ2pCLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLElBQUksR0FBRztvQkFDVCxLQUFLLEVBQUUsU0FBUztvQkFDaEIsUUFBUSxFQUFFLGFBQWEsR0FBRSxLQUFJLENBQUMsYUFBYSxHQUFFLHdDQUF3QztpQkFDdEY7Z0JBQ0QsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixDQUFDLEVBQ0MsZUFBSztnQkFDSCxJQUFJLElBQUksR0FBRztvQkFDVCxLQUFLLEVBQUUsT0FBTztvQkFDZCxRQUFRLEVBQUUsK0RBQStEO2lCQUMxRTtnQkFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN4QixLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNULENBQUM7UUFDRCxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLEtBQUssR0FBRyxvQ0FBb0M7UUFDbkQsQ0FBQztJQUNILENBQUM7SUFFRCxvQ0FBTSxHQUFOLFVBQU8sT0FBTztRQUFkLGlCQTRCQztRQTNCQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLElBQUksTUFBTSxHQUFHO1lBQ1gsVUFBVSxFQUFFLE9BQU8sQ0FBQyxNQUFNO1lBQzFCLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUc7U0FDdkI7UUFDRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQzthQUM3QyxTQUFTLENBQUMsa0JBQVE7WUFDakIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFDWCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUMzQixJQUFJLElBQUksR0FBRztnQkFDVCxLQUFLLEVBQUUsU0FBUztnQkFDaEIsUUFBUSxFQUFFLGdDQUFnQyxHQUFFLE9BQU8sQ0FBQyxPQUFPLEdBQUUsV0FBVzthQUN6RTtZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUNDLGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSwrREFBK0Q7YUFDMUU7WUFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsMkNBQWEsR0FBYjtRQUFBLGlCQW1CQztRQWxCQyxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7YUFDMUMsU0FBUyxDQUFDLGtCQUFRO1lBQ2pCLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDdEIsS0FBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDbEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDN0IsQ0FBQyxFQUNDLGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSwrREFBK0Q7YUFDMUU7WUFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDBDQUFZLEdBQVosVUFBYSxTQUFTLEVBQUUsV0FBVztRQUFuQyxpQkF1Q0M7UUF0Q0MsSUFBSSxJQUFJLENBQUM7UUFDVCxJQUFJLElBQUksR0FBRztZQUNULE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUc7WUFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSTtZQUM3QixZQUFZLEVBQUUsU0FBUyxDQUFDLE1BQU07WUFDOUIsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQyxDQUFDLElBQUksQ0FBQyxhQUFhO1lBQ2xFLFdBQVc7U0FDWjtRQUNELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQzthQUN0QyxTQUFTLENBQUMsa0JBQVE7WUFDakIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUNsQyxFQUFFLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixJQUFJLEdBQUc7b0JBQ0wsS0FBSyxFQUFFLFNBQVM7b0JBQ2hCLFFBQVEsRUFBRSxnQ0FBZ0MsR0FBRyxTQUFTLENBQUMsSUFBSTtpQkFDNUQ7WUFFSCxDQUFDO1lBQ0QsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxHQUFHO29CQUNMLEtBQUssRUFBRSxTQUFTO29CQUNoQixRQUFRLEVBQUUsZ0NBQWdDLEdBQUcsU0FBUyxDQUFDLElBQUk7aUJBQzVEO1lBQ0gsQ0FBQztZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEIsS0FBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDN0IsQ0FBQyxFQUNDLGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSwrREFBK0Q7YUFDMUU7WUFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDBDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ3BDLE9BQU8sRUFBRSxnQkFBZ0I7WUFDekIsbUJBQW1CLEVBQUUsSUFBSTtTQUMxQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCwwQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQ2hDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDO1NBQ2hCLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBekxEO1FBREMsb0VBQUssRUFBRTs7MkRBQ29CO0lBTTVCO1FBREMsb0VBQUssRUFBRTs7cURBQ0U7SUFFVjtRQURDLG9FQUFLLEVBQUU7O3lEQUNNO0lBWEgsbUJBQW1CO1FBSi9CLHdFQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsWUFBWTtXQUNRO1NBQy9CLENBQUM7cUZBbUJnRDtZQUMxQix3RUFBaUI7WUFDbkIsZUFBZTtPQXBCeEIsbUJBQW1CLENBOEwvQjtJQUFELENBQUM7QUFBQTtTQTlMWSxtQkFBbUIsVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2RZO0FBQ2U7QUFDTjtBQUNTO0FBRTlELG9FQUFvRTtBQUNiO0FBQ3ZELGdEQUFnRDtBQUVLO0FBQ007QUFNM0Q7SUFNRSw2REFBNkQ7SUFFN0QsZUFDUyxRQUFrQixFQUNsQixJQUFvQixFQUNwQixTQUFvQixFQUNwQixZQUEwQjtRQUgxQixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLFNBQUksR0FBSixJQUFJLENBQWdCO1FBQ3BCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFUbkMsK0NBQStDO1FBQy9DLGFBQVEsR0FBRywyRUFBVyxDQUFDO1FBVXJCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVyQixzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLEtBQUssR0FBRztZQUNYLEVBQUUsS0FBSyxFQUFFLGVBQWUsRUFBRSxTQUFTLEVBQUUsOERBQUksRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFO1lBQzFELEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsNkVBQVcsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFO1NBQzdELENBQUM7UUFDRixpQkFBaUI7UUFDakIsTUFBTTtRQUNOLDhCQUE4QjtRQUM5Qiw0Q0FBNEM7UUFDNUMsZ0NBQWdDO1FBQ2hDLElBQUk7SUFFTixDQUFDO0lBRUQsNkJBQWEsR0FBYjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDekIsZ0VBQWdFO1lBQ2hFLGlFQUFpRTtZQUNqRSxLQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQzlCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsd0JBQVEsR0FBUixVQUFTLElBQUk7UUFDWCxvREFBb0Q7UUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQix5REFBeUQ7UUFDekQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlCLGtDQUFrQztJQUNwQyxDQUFDO0lBRUQsdUJBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLDZFQUFXLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFqRGU7UUFBZix5RUFBUyxDQUFDLDBEQUFHLENBQUM7a0NBQU0sMERBQUc7c0NBQUM7SUFEZCxLQUFLO1FBSGpCLHdFQUFTLENBQUM7V0FDYztTQUN4QixDQUFDO2tGQVUyQjtZQUNaLDRFQUFjO1lBQ1QsaUZBQVM7WUFDTixFQUFZO09BWnhCLEtBQUssQ0FtRGpCO0lBQUQsQ0FBQztBQUFBO1NBbkRZLEtBQUssMkI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQnVCO0FBQ087QUFDVztBQUNXO0FBV3RFO0lBQUE7SUFBeUMsQ0FBQztJQUE3QiwwQkFBMEI7UUFUdEMsdUVBQVEsQ0FBQztZQUNSLFlBQVksRUFBRTtnQkFDWixnRkFBb0I7YUFDckI7WUFDRCxPQUFPLEVBQUU7Z0JBQ1Asc0VBQWUsQ0FBQyxRQUFRLENBQUMsZ0ZBQW9CLENBQUM7Z0JBQzlDLHVGQUFnQjthQUNqQjtTQUNGLENBQUM7T0FDVywwQkFBMEIsQ0FBRztJQUFELGlDQUFDO0NBQUE7QUFBSDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZEU7QUFDTztBQUNRO0FBQ2M7QUFDdkI7QUFZL0M7SUFBQTtJQUF3QyxDQUFDO0lBQTVCLHlCQUF5QjtRQVZyQyx1RUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLDZFQUFtQjthQUNwQjtZQUNELE9BQU8sRUFBRTtnQkFDUCxzRUFBZSxDQUFDLFFBQVEsQ0FBQyw2RUFBbUIsQ0FBQztnQkFDN0MsdUZBQWdCO2dCQUNoQixxRUFBWTthQUNiO1NBQ0YsQ0FBQztPQUNXLHlCQUF5QixDQUFHO0lBQUQsZ0NBQUM7Q0FBQTtBQUFIOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQjZDO0FBQ3pDO0FBQ2lGO0FBQ2hEO0FBQ0s7QUFPaEY7SUFlRTtRQUNFLDZCQUE2QjtRQUNyQixNQUFpQixFQUNqQixRQUF3QixFQUN4QixHQUFrQixFQUNsQixHQUFtQixFQUNwQixTQUEwQixFQUMxQixXQUE4QjtRQUw3QixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLGFBQVEsR0FBUixRQUFRLENBQWdCO1FBQ3hCLFFBQUcsR0FBSCxHQUFHLENBQWU7UUFDbEIsUUFBRyxHQUFILEdBQUcsQ0FBZ0I7UUFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsZ0JBQVcsR0FBWCxXQUFXLENBQW1CO1FBbEJ2QyxTQUFJLEdBQVEsRUFBRSxDQUFDO1FBS2YsZ0JBQVcsR0FBUSxLQUFLLENBQUM7UUFjdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUNwQyxPQUFPLEVBQUUsZ0JBQWdCO1lBQ3pCLG1CQUFtQixFQUFFLElBQUk7U0FDMUIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELG1DQUFRLEdBQVI7UUFBQSxpQkF5QkM7UUF4QkMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7YUFDcEIsU0FBUyxDQUFDLGtCQUFRO1lBQ2pCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxJQUFJLEdBQUc7b0JBQ1QsS0FBSyxFQUFFLFlBQVk7b0JBQ25CLFFBQVEsRUFBRSw0Q0FBNEM7aUJBQ3ZEO2dCQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ3RCLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUIsQ0FBQztRQUNILENBQUMsRUFDQyxlQUFLO1lBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixJQUFJLElBQUksR0FBRztnQkFDVCxLQUFLLEVBQUUsT0FBTztnQkFDZCxRQUFRLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPO2FBQzlCLENBQUM7WUFDRixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELHlDQUFjLEdBQWQsVUFBZSxjQUFjO1FBQTdCLGlCQTRCQztRQTNCQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzVCLElBQUksZ0JBQWdCLEdBQUcsY0FBYyxDQUFDO1FBQ3RDLElBQUksc0JBQXNCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDO1FBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxrQkFBUTtZQUM1RyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDeEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLGdCQUFnQixDQUFDO1lBQ25ELElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDL0QsY0FBYyxDQUFDLHNCQUFzQixHQUFHLHNCQUFzQixDQUFDO1lBQ2pFLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDN0MsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsNkZBQW1CLEVBQUUsRUFBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFFBQVEsWUFBRSxjQUFjLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7UUFDN0gsQ0FBQyxFQUNDLGVBQUs7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU87Z0JBQzdCLDBFQUEwRTthQUMzRTtZQUNELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQsc0NBQVcsR0FBWDtRQUFBLGlCQWlDQztRQWhDQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixrQ0FBa0M7UUFDbEMsa0NBQWtDO1FBQ2xDLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQzVCLFNBQVMsQ0FBQyxrQkFBUTtZQUNqQixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNiLEtBQUksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQztxQkFDckMsU0FBUyxDQUFDLHNCQUFZO29CQUNyQixFQUFFLEVBQUMsWUFBWSxDQUFDLEVBQUM7d0JBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLFlBQVksQ0FBQzt3QkFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzt3QkFDcEMsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0JBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ3hCLENBQUM7Z0JBQ0gsQ0FBQyxDQUFDO1lBQ0osQ0FBQztRQUNILENBQUMsRUFDQyxlQUFLO1lBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5QixJQUFJLElBQUksR0FBRztnQkFDVCxLQUFLLEVBQUUsT0FBTztnQkFDZCxRQUFRLEVBQUUsOERBQThEO2FBQ3pFO1lBQ0QsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUVULENBQUM7SUFFRCxnQ0FBSyxHQUFMLFVBQU0sY0FBYztRQUFwQixpQkE2QkM7UUE1QkMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUk7UUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUM7UUFDdEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUN0QixTQUFTLENBQUMsa0JBQVE7WUFDakIsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGtHQUFvQixFQUFFLEVBQUUsUUFBUSxZQUFFLFdBQVcsRUFBRSxLQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUM1RSxDQUFDO1lBQ0QsSUFBSSxDQUFDLEVBQUUsRUFBQyxJQUFJLEtBQUssUUFBUSxDQUFDLEVBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLDZGQUFtQixFQUFFLEVBQUUsUUFBUSxZQUFFLFdBQVcsRUFBRSxLQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUMzRSxDQUFDO1lBQ0QsSUFBSSxDQUFDLENBQUM7Z0JBQ0osTUFBTSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ25DLENBQUM7WUFDRCxLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFDQyxlQUFLO1lBQ0gsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0QixJQUFJLE9BQU8sR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzdELE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2hDLElBQUksSUFBSSxHQUFHO2dCQUNULEtBQUssRUFBRSxPQUFPO2dCQUNkLFFBQVEsRUFBRSxPQUFPLElBQUksOERBQThEO2FBQ3BGO1lBQ0QsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCxrQ0FBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsdUNBQVksR0FBWixVQUFhLElBQUk7UUFDZixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztZQUNoQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQztTQUNoQixDQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVELHVDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ3BDLE9BQU8sRUFBRSxnQkFBZ0I7WUFDekIsbUJBQW1CLEVBQUUsSUFBSTtTQUMxQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFsTFUsZ0JBQWdCO1FBSjVCLHdFQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1dBQ0c7U0FDbEMsQ0FBQztrRkFrQjJCO1lBQ1AscUVBQWM7WUFDbkIsa0dBQWE7WUFDYixzRUFBYztZQUNULHdFQUFlO1lBQ2IsYUFBaUI7T0F0QjVCLGdCQUFnQixDQW9MNUI7SUFBRCxDQUFDO0FBQUE7U0FwTFksZ0JBQWdCLGdCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uc0NvbXBvbmVudCB9IGZyb20gJy4vbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zJztcbmltcG9ydCB7IElvbmljTW9kdWxlfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB7IFByZXNjcmlwdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vcHJlc2NyaXB0aW9uL3ByZXNjcmlwdGlvbic7XG5pbXBvcnQgeyBEZWxlZ2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9kZWxlZ2F0aW9uL2RlbGVnYXRpb24nO1xuQE5nTW9kdWxlKHtcblx0ZGVjbGFyYXRpb25zOiBbTm90aWZpY2F0aW9uc0NvbXBvbmVudCxcbiAgICBQcmVzY3JpcHRpb25Db21wb25lbnQsXG4gICAgRGVsZWdhdGlvbkNvbXBvbmVudF0sXG5cdGltcG9ydHM6IFtJb25pY01vZHVsZV0sXG5cdGV4cG9ydHM6IFtOb3RpZmljYXRpb25zQ29tcG9uZW50LFxuICAgIFByZXNjcmlwdGlvbkNvbXBvbmVudCxcbiAgICBEZWxlZ2F0aW9uQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBDb21wb25lbnRzTW9kdWxlIHt9XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY29tcG9uZW50cy9jb21wb25lbnRzLm1vZHVsZS50cyIsImltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzLCBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cEV2ZW50ICwgSHR0cFJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCQVNFX1VSTCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb24sIE5vdGlmaWNhdGlvbnNSZXNwb25zZX0gZnJvbSAnLi4vLi4vbW9kZWxzL25vdGlmaWNhdGlvbic7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmNvbnN0IGh0dHBPcHRpb25zID0ge1xuICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH0pXG59O1xuXG4vKlxuICBHZW5lcmF0ZWQgY2xhc3MgZm9yIHRoZSBOb3RpZmljYXRpb25BcGlQcm92aWRlciBwcm92aWRlci5cblxuICBTZWUgaHR0cHM6Ly9hbmd1bGFyLmlvL2d1aWRlL2RlcGVuZGVuY3ktaW5qZWN0aW9uIGZvciBtb3JlIGluZm8gb24gcHJvdmlkZXJzXG4gIGFuZCBBbmd1bGFyIERJLlxuKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25BcGlQcm92aWRlciB7XG4gIHByaXZhdGUgbm90aWZpY2F0aW9uc1VybCA9IEJBU0VfVVJMICsgJy9hcGkvbm90aWZpY2F0aW9ucyc7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgY29uc29sZS5sb2coJ0hlbGxvIE5vdGlmaWNhdGlvbkFwaVByb3ZpZGVyIFByb3ZpZGVyJyk7XG4gIH1cblxuICBnZXROb3RpZmljYXRpb25zICh1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Tm90aWZpY2F0aW9uc1Jlc3BvbnNlW10+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxOb3RpZmljYXRpb25zUmVzcG9uc2VbXT4odGhpcy5ub3RpZmljYXRpb25zVXJsKyBgP3VzZXJJZD0ke3VzZXJJZH1gKS5waXBlKFxuICAgICAgY2F0Y2hFcnJvcigoZXJyOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4ge1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnIpO1xuICAgICAgfSlcbiAgICApO1xuICB9XG5cbiAgZ2V0Tm90aWZpY2F0aW9uc0NvdW50ICh1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Tm90aWZpY2F0aW9uc1Jlc3BvbnNlW10+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxOb3RpZmljYXRpb25zUmVzcG9uc2VbXT4odGhpcy5ub3RpZmljYXRpb25zVXJsKyBgP3VzZXJJZD0ke3VzZXJJZH0mZmllbGQ9Y291bnRgKS5waXBlKFxuICAgICAgY2F0Y2hFcnJvcigoZXJyOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4ge1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnIpO1xuICAgICAgfSlcbiAgICApO1xuICB9XG5cbiAgdXBkYXRlTm90aWZpY2F0aW9uc1JlYWQgKG5vdGlmaWNhdGlvbnM6IEFycmF5PE5vdGlmaWNhdGlvbj4pOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGxldCBub3RpZmljYXRpb25JZHMgPSBbXTtcbiAgICBmb3IobGV0IG5vdGlmaWNhdGlvbiBvZiBub3RpZmljYXRpb25zKXtcbiAgICAgIG5vdGlmaWNhdGlvbklkcy5wdXNoKG5vdGlmaWNhdGlvbi5pZCk7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLm5vdGlmaWNhdGlvbnNVcmwsIHtub3RpZmljYXRpb25zOiBub3RpZmljYXRpb25JZHN9LCBodHRwT3B0aW9ucykucGlwZShcbiAgICAgIGNhdGNoRXJyb3IoKGVycjogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcbiAgICAgICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyKTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcHJvdmlkZXJzL25vdGlmaWNhdGlvbi1hcGkvbm90aWZpY2F0aW9uLWFwaS50cyIsImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2Q29udHJvbGxlciwgTmF2UGFyYW1zLCBBbGVydENvbnRyb2xsZXIsIExvYWRpbmdDb250cm9sbGVyLCBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB2NCBmcm9tICd1dWlkL3Y0JztcbmltcG9ydCBzaGExIGZyb20gJ3NoYTEnO1xuaW1wb3J0IHsgSGVhbHRoZXJpdW1BcGkgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvaGVhbHRoZXJpdW0tYXBpL2hlYWx0aGVyaXVtLWFwaSc7XG5pbXBvcnQgeyBNb2RhbENvbnRlbnRQYWdlIH0gZnJvbSAnLi4vbW9kYWwtY29udGVudC9tb2RhbC1jb250ZW50JztcbmltcG9ydCB7UHJvdmlkZXJ9IGZyb20gXCIuLi8uLi9tb2RlbHMvcHJvdmlkZXJcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncmVnaXN0cmF0aW9uJyxcbiAgdGVtcGxhdGVVcmw6ICdyZWdpc3RyYXRpb24uaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIFJlZ2lzdHJhdGlvblBhZ2UgaW1wbGVtZW50cyBPbkluaXR7XG5cbiAgbG9hZGVyOiBhbnk7XG4gIHVzZXI6IGFueSA9IHt9O1xuICB1c2VyVHlwZTogc3RyaW5nO1xuICBzZWxlY3RlZFByb3ZpZGVyOiBQcm92aWRlcjtcbiAgcHJvdmlkZXJMaXN0OiBQcm92aWRlcltdO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgbmF2Q3RybDogTmF2Q29udHJvbGxlcixcbiAgICBwdWJsaWMgbmF2UGFyYW1zOiBOYXZQYXJhbXMsXG4gICAgcHVibGljIGFsZXJ0Q3RybDogQWxlcnRDb250cm9sbGVyLFxuICAgIHByaXZhdGUgYXBpOiBIZWFsdGhlcml1bUFwaSxcbiAgICBwdWJsaWMgbG9hZGluZ0N0cmw6IExvYWRpbmdDb250cm9sbGVyLFxuICAgIHB1YmxpYyBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcikge1xuICAgICAgaWYodGhpcy5uYXZQYXJhbXMuZGF0YSAmJiB0aGlzLm5hdlBhcmFtcy5kYXRhLnVzZXIpIHtcbiAgICAgICAgIHN3aXRjaCAodGhpcy5uYXZQYXJhbXMuZGF0YS51c2VyKSB7XG4gICAgICAgICAgICBjYXNlIFwicGF0aWVudFwiIDpcbiAgICAgICAgICAgICAgdGhpcy51c2VyVHlwZSA9IFwicGF0aWVudFwiO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJkb2N0b3JcIiA6XG4gICAgICAgICAgICAgIHRoaXMudXNlclR5cGUgPSBcImRvY3RvclwiO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJyZXNlYXJjaFwiIDpcbiAgICAgICAgICAgICAgdGhpcy51c2VyLnR5cGUgPSBcInJlc2VhcmNoZXJcIjtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiaW5zdXJhbmNlXCIgOlxuICAgICAgICAgICAgICB0aGlzLnVzZXJUeXBlID0gXCJpbnN1cmFuY2VcIjtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiYnV5ZXJcIiA6XG4gICAgICAgICAgICAgIHRoaXMudXNlclR5cGUgPSBcImJ1eWVyXCI7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdCA6XG4gICAgICAgICAgICAgIHRoaXMudXNlclR5cGUgPSBcInBhdGllbnRcIjtcbiAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgIH1cbiAgICAgIH1cbiAgfVxuXG4gIGlvblZpZXdEaWRMb2FkKCkge1xuICAgIGNvbnNvbGUubG9nKCdpb25WaWV3RGlkTG9hZCBQYXRpZW50U2lnbnVwUGFnZScpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5nZXRQcm92aWRlcnMoKTtcbiAgfVxuXG4gIGdldFByb3ZpZGVycygpOiB2b2lkIHtcbiAgICB0aGlzLmFwaS5nZXRQcm92aWRlcnMoKVxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICAgIHRoaXMucHJvdmlkZXJMaXN0ID0gcmVzcG9uc2VbJ3Byb3ZpZGVycyddO1xuICAgICAgICB0aGlzLnNlbGVjdGVkUHJvdmlkZXIgPSB0aGlzLnByb3ZpZGVyTGlzdFswXTtcbiAgICAgIH0sZXJyb3IgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG5cbiAgc3VibWl0SW5mbygpIHtcbiAgICB0aGlzLnVzZXIudXNlciA9IHRoaXMudXNlclR5cGU7XG4gICAgdGhpcy51c2VyLmlzUmVnaXN0ZXJlZCA9IGZhbHNlO1xuICAgIHRoaXMudXNlci5wcm92aWRlciA9IHRoaXMuc2VsZWN0ZWRQcm92aWRlci5wcm92aWRlck51bWJlcjtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnVzZXIpO1xuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgY29uc29sZS5sb2codGhpcy51c2VyKTtcbiAgICB0aGlzLmFwaS5nZW5lcmF0ZU9UUCh0aGlzLnVzZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICBpZiAocmVzcG9uc2UpIHtcbiAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgIGxldCBtb2RhbCA9IHRoaXMubW9kYWxDdHJsLmNyZWF0ZShNb2RhbENvbnRlbnRQYWdlLCB7IHR5cGU6IFwib3RwXCIsIGRhdGE6dGhpcy51c2VyLCAgYWN0aW9uOiBcInJlZ2lzdHJhdGlvblwiIH0pO1xuICAgICAgICBtb2RhbC5wcmVzZW50KCk7XG4gICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAgIH1cbiAgICB9LFxuICAgICAgZXJyb3IgPT4ge1xuICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgICAgc3ViVGl0bGU6IGVycm9yLmVycm9yLm1lc3NhZ2UsXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgfSk7XG4gIH1cblxuICBjcmVhdGVMb2FkZXIoKTogdm9pZCB7XG4gICAgdGhpcy5sb2FkZXIgPSB0aGlzLmxvYWRpbmdDdHJsLmNyZWF0ZSh7XG4gICAgICBjb250ZW50OiBcIlBsZWFzZSB3YWl0Li4uXCIsXG4gICAgICBkaXNtaXNzT25QYWdlQ2hhbmdlOiB0cnVlXG4gICAgfSk7XG4gICAgdGhpcy5sb2FkZXIucHJlc2VudCgpO1xuICB9XG5cbiAgcHJlc2VudEFsZXJ0KGluZm8pIHtcbiAgICBsZXQgYWxlcnQgPSB0aGlzLmFsZXJ0Q3RybC5jcmVhdGUoe1xuICAgICAgdGl0bGU6IGluZm8udGl0bGUsXG4gICAgICBzdWJUaXRsZTogaW5mby5zdWJUaXRsZSxcbiAgICAgIGJ1dHRvbnM6IFsnT2snXVxuICAgIH0pO1xuICAgIGFsZXJ0LnByZXNlbnQoKTtcbiAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24udHMiLCJmdW5jdGlvbiB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQocmVxKSB7XG5cdC8vIEhlcmUgUHJvbWlzZS5yZXNvbHZlKCkudGhlbigpIGlzIHVzZWQgaW5zdGVhZCBvZiBuZXcgUHJvbWlzZSgpIHRvIHByZXZlbnRcblx0Ly8gdW5jYXRjaGVkIGV4Y2VwdGlvbiBwb3BwaW5nIHVwIGluIGRldnRvb2xzXG5cdHJldHVybiBQcm9taXNlLnJlc29sdmUoKS50aGVuKGZ1bmN0aW9uKCkge1xuXHRcdHRocm93IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIicuXCIpO1xuXHR9KTtcbn1cbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5rZXlzID0gZnVuY3Rpb24oKSB7IHJldHVybiBbXTsgfTtcbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0VtcHR5QXN5bmNDb250ZXh0O1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQ7XG53ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQuaWQgPSAxNzU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQGFuZ3VsYXIvY29yZS9lc201IGxhenlcbi8vIG1vZHVsZSBpZCA9IDE3NVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJ2YXIgbWFwID0ge1xuXHRcIi4uL3BhZ2VzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5tb2R1bGVcIjogW1xuXHRcdDIyMFxuXHRdXG59O1xuZnVuY3Rpb24gd2VicGFja0FzeW5jQ29udGV4dChyZXEpIHtcblx0dmFyIGlkcyA9IG1hcFtyZXFdO1xuXHRpZighaWRzKVxuXHRcdHJldHVybiBQcm9taXNlLnJlamVjdChuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInLlwiKSk7XG5cdHJldHVybiBQcm9taXNlLmFsbChpZHMuc2xpY2UoMSkubWFwKF9fd2VicGFja19yZXF1aXJlX18uZSkpLnRoZW4oZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWRzWzBdKTtcblx0fSk7XG59O1xud2VicGFja0FzeW5jQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0FzeW5jQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tBc3luY0NvbnRleHQuaWQgPSAyMTk7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tBc3luY0NvbnRleHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMgbGF6eVxuLy8gbW9kdWxlIGlkID0gMjE5XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJb25pY1BhZ2VNb2R1bGUgfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvbnNQYWdlIH0gZnJvbSAnLi9ub3RpZmljYXRpb25zJztcbmltcG9ydCB7IENvbXBvbmVudHNNb2R1bGUgfSBmcm9tIFwiLi4vLi4vY29tcG9uZW50cy9jb21wb25lbnRzLm1vZHVsZVwiO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBOb3RpZmljYXRpb25zUGFnZVxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgSW9uaWNQYWdlTW9kdWxlLmZvckNoaWxkKE5vdGlmaWNhdGlvbnNQYWdlKSxcbiAgICBDb21wb25lbnRzTW9kdWxlXG4gIF0sXG4gIGVudHJ5Q29tcG9uZW50czogW11cbn0pXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uc1BhZ2VNb2R1bGUge31cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMubW9kdWxlLnRzIiwiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJb25pY1BhZ2UsIFZpZXdDb250cm9sbGVyICwgTmF2UGFyYW1zfSBmcm9tICdpb25pYy1hbmd1bGFyJztcblxuQElvbmljUGFnZSgpXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwYWdlLW5vdGlmaWNhdGlvbnMnLFxuICB0ZW1wbGF0ZVVybDogJ25vdGlmaWNhdGlvbnMuaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbnNQYWdlIHtcbiAgdXNlckRldGFpbHM6IGFueTtcbiAgY29uc3RydWN0b3IocHVibGljIHZpZXdDdHJsOiBWaWV3Q29udHJvbGxlciwgcGFyYW1zOiBOYXZQYXJhbXMpIHtcbiAgICBjb25zb2xlLmxvZyhwYXJhbXMuZ2V0KCd1c2VyRGV0YWlscycpKTtcbiAgICB0aGlzLnVzZXJEZXRhaWxzID0gcGFyYW1zLmdldCgndXNlckRldGFpbHMnKTtcbiAgfVxuXG4gIGlvblZpZXdEaWRMb2FkKCkge1xuICAgIGNvbnNvbGUubG9nKCdpb25WaWV3RGlkTG9hZCBOb3RpZmljYXRpb25zUGFnZScpO1xuICB9XG5cbiAgY2xvc2UoKSB7XG4gICAgdGhpcy52aWV3Q3RybC5kaXNtaXNzKCk7XG4gIH1cblxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy50cyIsImltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzLCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQkFTRV9VUkwgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG5cbi8qXG4gIEdlbmVyYXRlZCBjbGFzcyBmb3IgdGhlIERlbGVnYXRpb25Qcm92aWRlciBwcm92aWRlci5cblxuICBTZWUgaHR0cHM6Ly9hbmd1bGFyLmlvL2d1aWRlL2RlcGVuZGVuY3ktaW5qZWN0aW9uIGZvciBtb3JlIGluZm8gb24gcHJvdmlkZXJzXG4gIGFuZCBBbmd1bGFyIERJLlxuKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEZWxlZ2F0aW9uUHJvdmlkZXIge1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgY29uc29sZS5sb2coJ0hlbGxvIERlbGVnYXRpb25Qcm92aWRlciBQcm92aWRlcicpO1xuICB9XG5cbiAgZGVsZWdhdGUoZGF0YSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke0JBU0VfVVJMfS9hcGkvZGVsZWdhdGUvYWRkYCwgZGF0YSlcbiAgICAgIC5tYXAocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldERlbGVnYXRpb25JbmZvKGRhdGEpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCkuc2V0KCd1c2VySWQnLCBkYXRhKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldChgJHtCQVNFX1VSTH0vYXBpL2RlbGVnYXRlL25vbWluZWVzYCwgeyBwYXJhbXMgfSlcbiAgICAgIC5tYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UpO1xuICB9XG5cbiAgZ2V0RGVsZWdhdG9ycyhkYXRhOiBhbnkpOiBhbnkge1xuICAgIGNvbnN0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCkuc2V0KCd1c2VySWQnLCBkYXRhKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldChgJHtCQVNFX1VSTH0vYXBpL2RlbGVnYXRlL25vbWluYXRvcnNgLCB7IHBhcmFtcyB9KVxuICAgICAgLm1hcChyZXNwb25zZSA9PiByZXNwb25zZSk7XG4gIH1cblxuXG4gIHJldm9rZURlbGVnYXRpb24oZGF0YSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XG4gICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH0pLCBib2R5OiBkYXRhXG4gIH07XG4gICAgY29uc29sZS5sb2coXCJyZXZva2VcIiwgZGF0YSk7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5kZWxldGUoYCR7QkFTRV9VUkx9L2FwaS9kZWxlZ2F0ZS9yZXZva2VgLCBodHRwT3B0aW9ucylcbiAgICAgIC5tYXAocmVzcG9uc2UgPT4gcmVzcG9uc2UpO1xuICB9XG5cbiAgZ3JhbnRBY2Nlc3MoZGF0YSkgOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChgJHtCQVNFX1VSTH0vYXBpL2RlbGVnYXRlL2dyYW50QWNjZXNzYCwgZGF0YSlcbiAgICAgIC5tYXAocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICB9KTtcbiAgfVxuXG5cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wcm92aWRlcnMvZGVsZWdhdGlvbi1hcGkvZGVsZWdhdGlvbi50cyIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOYXZDb250cm9sbGVyLCBOYXZQYXJhbXMsIExvYWRpbmdDb250cm9sbGVyLCBBbGVydENvbnRyb2xsZXIsIFBvcG92ZXJDb250cm9sbGVyLCBNb2RhbENvbnRyb2xsZXIsIFBsYXRmb3JtIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBIZWFsdGhlcml1bUFwaSB9IGZyb20gJy4uLy4uL3Byb3ZpZGVycy9oZWFsdGhlcml1bS1hcGkvaGVhbHRoZXJpdW0tYXBpJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvbnNQYWdlIH0gZnJvbSBcIi4uL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9uc1wiO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uQXBpUHJvdmlkZXIgfSBmcm9tIFwiLi4vLi4vcHJvdmlkZXJzL25vdGlmaWNhdGlvbi1hcGkvbm90aWZpY2F0aW9uLWFwaVwiO1xuaW1wb3J0IHsgRENPTV9VUkwgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuaW1wb3J0IHsgTW9kYWxDb250ZW50UGFnZSB9IGZyb20gJy4uL21vZGFsLWNvbnRlbnQvbW9kYWwtY29udGVudCc7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwYWdlLWRvY3Rvci1kYXNob2FyZCcsXG4gIHRlbXBsYXRlVXJsOiAnZG9jdG9yLWRhc2hvYXJkLmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBEb2N0b3JEYXNoYm9hcmRQYWdlIGltcGxlbWVudHMgT25Jbml0e1xuICBzaG93RGV0YWlsczogYm9vbGVhbjtcbiAgb2JzZXJ2YXRpb25EZXRhaWxzOiBhbnk7XG4gIHNob3dQcmVzY3JpcHRpb25zOiBib29sZWFuO1xuICBwYXRpZW50SWRlbnRpZmllcihhcmcwOiBhbnkpOiBhbnkge1xuICAgIHRocm93IG5ldyBFcnJvcihcIk1ldGhvZCBub3QgaW1wbGVtZW50ZWQuXCIpO1xuICB9XG5cbiAgcHVibGljIGNsaW5pY2FsRGF0YTogYW55ID0ge307XG4gIHB1YmxpYyBteUFuZ3VsYXJ4UXJDb2RlOiBzdHJpbmcgPSBudWxsO1xuICBwdWJsaWMgcXJDb2RlUmVxdWVzdEluZm86IGFueSA9IHt9O1xuICBwdWJsaWMgc2Nhbm5lZEFkZHJlc3M6IHN0cmluZztcbiAgcHVibGljIHBhdGllbnREZXRhaWxzOiBhbnk7XG4gIHB1YmxpYyBkb2N0b3JBY2Nlc3NMaXN0OiBhbnkgPSBudWxsO1xuICBwdWJsaWMgZW1wdHlkZXRhaWxzOiBzdHJpbmc7XG4gIHNlbGVjdGVkRW1yOiBhbnk7XG4gIHB1YmxpYyBsb2FkZXI7XG4gIHB1YmxpYyBzaG93UVJDb2RlOiBib29sZWFuID0gZmFsc2U7XG4gIHB1YmxpYyBub3RpZmljYXRpb25zQ291bnQ6IG51bWJlciA9IDA7XG4gIHByaXZhdGUgc2VsZWN0ZWRQYXRpZW50QWRkcmVzczogYW55ID0gbnVsbDtcbiAgZ2V0VmlzaXRzOiBib29sZWFuO1xuICB2aXNpdExpc3Q6IGFueVtdO1xuICB1c2VyOiBhbnkgPSB7fTtcbiAgY3JlZGVudGlhbHM7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBuYXZDdHJsOiBOYXZDb250cm9sbGVyLFxuICAgIHB1YmxpYyBuYXZQYXJhbXM6IE5hdlBhcmFtcyxcbiAgICBwdWJsaWMgYXBpOiBIZWFsdGhlcml1bUFwaSxcbiAgICBwdWJsaWMgYWxlcnRDdHJsOiBBbGVydENvbnRyb2xsZXIsXG4gICAgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICBwdWJsaWMgcG9wb3ZlckN0cmw6IFBvcG92ZXJDb250cm9sbGVyLFxuICAgIHB1YmxpYyBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcixcbiAgICBwdWJsaWMgcGxhdGZvcm06IFBsYXRmb3JtLFxuICAgIHByaXZhdGUgbm90aWZpY2F0aW9uUHJvdmlkZXI6IE5vdGlmaWNhdGlvbkFwaVByb3ZpZGVyKSB7XG4gICAgdGhpcy5zaG93UHJlc2NyaXB0aW9ucyA9IGZhbHNlO1xuICAgIHRoaXMuc2hvd1FSQ29kZSA9IGZhbHNlO1xuICAgIGNvbnNvbGUubG9nKHRoaXMubmF2UGFyYW1zLmRhdGEpO1xuXG4gICAgaWYgKHRoaXMubmF2UGFyYW1zLmRhdGEpIHtcbiAgICAgIHRoaXMudXNlciA9IHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UudXNlcjtcbiAgICAgIHRoaXMuc2VsZWN0ZWRFbXIgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLnNlbGVjdGVkT3B0aW9uO1xuICAgICAgaWYgKCF0aGlzLm5hdlBhcmFtcy5kYXRhLmlzRW1yRGF0YSAmJiB0aGlzLm5hdlBhcmFtcy5kYXRhLnJlc3BvbnNlICYmIHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UuY2xpbmljYWxEYXRhLmJvZHkpIHtcbiAgICAgICAgdGhpcy5jbGluaWNhbERhdGEuZW1yRGF0YSA9IEpTT04ucGFyc2UodGhpcy5uYXZQYXJhbXMuZGF0YS5yZXNwb25zZS5jbGluaWNhbERhdGEuYm9keSkuZW50cnlbMF0ucmVzb3VyY2U7XG4gICAgICAgIHRoaXMuY2xpbmljYWxEYXRhLmRlcGFydG1lbnQgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLnJlc3BvbnNlLmNsaW5pY2FsRGF0YS5kZXBhcnRtZW50O1xuICAgICAgICB0aGlzLmNsaW5pY2FsRGF0YS5ob3NwaXRhbCA9IHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UuY2xpbmljYWxEYXRhLmhvc3BpdGFsO1xuICAgICAgICB0aGlzLmNyZWRlbnRpYWxzID0gdGhpcy5uYXZQYXJhbXMuZGF0YS5jcmVkZW50aWFscztcbiAgICAgICAgdGhpcy5xckNvZGVSZXF1ZXN0SW5mbyA9IHRoaXMuY3JlZGVudGlhbHM7XG4gICAgICB9XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKHRoaXMuY2xpbmljYWxEYXRhLmVtckRhdGEpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgaWYgKHRoaXMubmF2UGFyYW1zLmRhdGEuaXNFbXJEYXRhKSB7XG4gICAgICB0aGlzLnNlbGVjdGVkRW1yID0gdGhpcy5uYXZQYXJhbXMuZGF0YS5kb2N0b3Iuc2VsZWN0ZWRPcHRpb247XG4gICAgICB0aGlzLmNsaW5pY2FsRGF0YS5lbXJEYXRhID0gdGhpcy5uYXZQYXJhbXMuZGF0YS5kb2N0b3IuZG9jdG9yRGF0YS5lbXJEYXRhO1xuICAgICAgdGhpcy5jbGluaWNhbERhdGEuZGVwYXJ0bWVudCA9IHRoaXMubmF2UGFyYW1zLmRhdGEuZG9jdG9yLmRvY3RvckRhdGEuZGVwYXJ0bWVudDtcbiAgICAgIHRoaXMuY2xpbmljYWxEYXRhLmhvc3BpdGFsID0gdGhpcy5uYXZQYXJhbXMuZGF0YS5kb2N0b3IuZG9jdG9yRGF0YS5ob3NwaXRhbDtcbiAgICAgIHRoaXMudXNlciA9IHRoaXMubmF2UGFyYW1zLmRhdGEuZG9jdG9yLnVzZXI7XG4gICAgICB0aGlzLnBhdGllbnREZXRhaWxzID0gdGhpcy5uYXZQYXJhbXMuZGF0YS5wYXRpZW50RGV0YWlscztcbiAgICAgIHRoaXMuc2VsZWN0ZWRQYXRpZW50QWRkcmVzcyA9IHRoaXMucGF0aWVudERldGFpbHMuc2VsZWN0ZWRQYXRpZW50QWRkcmVzcztcbiAgICAgIHRoaXMuY3JlZGVudGlhbHMgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLmRvY3Rvci5jcmVkZW50aWFscztcbiAgICAgIHRoaXMucXJDb2RlUmVxdWVzdEluZm8gPSB0aGlzLmNyZWRlbnRpYWxzO1xuICAgICAgdGhpcy5kb2N0b3JBY2Nlc3NMaXN0ID0gdGhpcy5uYXZQYXJhbXMuZGF0YS5kb2N0b3IucGF0aWVudHM7XG4gICAgfVxuICB9XG5cblxuICBpb25WaWV3RGlkTG9hZCgpIHtcbiAgICBjb25zb2xlLmxvZygnaW9uVmlld0RpZExvYWQgRG9jdG9yRGFzaGJvYXJkUGFnZScpO1xuICAgIHRoaXMuc2hvd1FSQ29kZSA9IGZhbHNlO1xuICAgIHRoaXMuc2hvd0RldGFpbHMgPSBmYWxzZTtcbiAgICB0aGlzLmdldFZpc2l0cyA9IGZhbHNlO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicXJDb250YWluZXJcIikuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9uc0NvdW50KCk7XG4gIH1cblxuICAvL2dlbmVyYXRlIFFSIGNvZGVcbiAgZ2V0RG9jdG9yQWRkcmVzcygpOiB2b2lkIHtcbiAgICB0aGlzLmNyZWF0ZUxvYWRlcigpO1xuICAgIHRoaXMuc2hvd1FSQ29kZSA9IHRydWU7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJxckNvbnRhaW5lclwiKS5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xuICAgIHRoaXMubXlBbmd1bGFyeFFyQ29kZSA9IHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UudXNlciA/IHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UudXNlci5hY2NvdW50IDogdGhpcy5uYXZQYXJhbXMuZGF0YS5kb2N0b3IudXNlci5hY2NvdW50O1xuICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgfVxuXG4gIC8vc2hvdyBhbGVydCBib3hcbiAgcHJlc2VudEFsZXJ0KGluZm8pIHtcbiAgICBsZXQgYWxlcnQgPSB0aGlzLmFsZXJ0Q3RybC5jcmVhdGUoe1xuICAgICAgdGl0bGU6IGluZm8udGl0bGUsXG4gICAgICBzdWJUaXRsZTogaW5mby5zdWJUaXRsZSxcbiAgICAgIGJ1dHRvbnM6IFsnT2snXVxuICAgIH0pO1xuICAgIGFsZXJ0LnByZXNlbnQoKTtcbiAgfVxuXG4gIC8vZ2V0IHNoYXJlZCBwYXRpZW50IGRhdGFcbiAgZ2V0UGF0aWVudExpc3QoKTogdm9pZCB7XG4gICAgdGhpcy5jcmVhdGVMb2FkZXIoKTtcbiAgICB0aGlzLmRvY3RvckFjY2Vzc0xpc3QgPSBudWxsO1xuICAgIHRoaXMucGF0aWVudERldGFpbHMgPSBudWxsO1xuICAgIHRoaXMuc2hvd0RldGFpbHMgPSBmYWxzZTtcbiAgICB0aGlzLmdldFZpc2l0cyA9IGZhbHNlO1xuICAgIHRoaXMuc2hvd1ByZXNjcmlwdGlvbnMgPSBmYWxzZTtcbiAgICB0aGlzLmFwaS5nZXRBbGxvY2F0ZWRVc2Vycyh0aGlzLmNyZWRlbnRpYWxzLnNzbikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZSAmJiByZXNwb25zZS5sZW5ndGgpIHtcbiAgICAgICAgdGhpcy5kb2N0b3JBY2Nlc3NMaXN0ID0gcmVzcG9uc2U7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgdGl0bGU6IFwiTm8gRGF0YVwiLFxuICAgICAgICAgIHN1YlRpdGxlOiBcIk5vIFBhdGllbnQgSGFzIFNoYXJlZCBEYXRhXCJcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICAgIH1cblxuICAgIH0sXG4gICAgICBlcnJvciA9PiB7XG4gICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgY29uc29sZS5lcnJvcignT29wczonLCBlcnJvcik7XG4gICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgICAgc3ViVGl0bGU6IGVycm9yLmVycm9yLm1lc3NhZ2VcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UGF0aWVudEluZm8oYWRkcmVzcyk6IHZvaWQge1xuICAgIGNvbnNvbGUubG9nKFwiZ2V0IHBhdGllbnQgaW5mb1wiKTtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICBsZXQgdXNlckFkZHJlc3MgPSB0aGlzLmRvY3RvckFjY2Vzc0xpc3QuZmluZChmdW5jdGlvbiAodXNlciwgaW5kZXgpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdlbGVtZW50JyArIHVzZXIpO1xuICAgICAgcmV0dXJuIHVzZXIuQWNjb3VudC50b0xvd2VyQ2FzZSgpID09PSBhZGRyZXNzLnRvTG93ZXJDYXNlKCk7XG4gICAgfSk7XG4gICAgY29uc29sZS5sb2codXNlckFkZHJlc3MpO1xuICAgIHRoaXMuc2VsZWN0ZWRQYXRpZW50QWRkcmVzcyA9IFwiMHhcIiArIHVzZXJBZGRyZXNzLkFjY291bnQ7XG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZFBhdGllbnRBZGRyZXNzKTtcbiAgICB2YXIgZG9jdG9ySW5mbyA9IHRoaXMuY3JlZGVudGlhbHM7XG4gICAgdGhpcy5jcmVhdGVMb2FkZXIoKTtcbiAgICB0aGlzLnBhdGllbnREZXRhaWxzID0gbnVsbDtcbiAgICB0aGlzLnNob3dEZXRhaWxzID0gZmFsc2U7XG4gICAgdGhpcy5nZXRWaXNpdHMgPSBmYWxzZTtcbiAgICB0aGlzLnNob3dQcmVzY3JpcHRpb25zID0gZmFsc2U7XG4gICAgdGhpcy5hcGkuZ2V0UHJvdmlkZXJzT2ZVc2VyKHsgYWRkcmVzczogdGhpcy5zZWxlY3RlZFBhdGllbnRBZGRyZXNzIH0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICBsZXQgbW9kYWwgPSB0aGlzLm1vZGFsQ3RybC5jcmVhdGUoTW9kYWxDb250ZW50UGFnZSwgeyBwYXRpZW50czogdGhpcy5kb2N0b3JBY2Nlc3NMaXN0LCB0eXBlOiBcIm90cFwiLCBkb2N0b3JEYXRhOiB0aGlzLmNsaW5pY2FsRGF0YSwgdXNlcjogdGhpcy51c2VyLCBjaG9vc2VQcm92aWRlcjogcmVzcG9uc2UsIHNlbGVjdGVkUGF0aWVudEFkZHJlc3M6IHRoaXMuc2VsZWN0ZWRQYXRpZW50QWRkcmVzcywgdXNlckFjY291bnQ6IHRoaXMudXNlci5hY2NvdW50LCBhY3Rpb246IFwibXVsdGlwbGVFbXJcIiwgY3JlZGVudGlhbHM6IHRoaXMuY3JlZGVudGlhbHMgfSk7XG4gICAgICBtb2RhbC5wcmVzZW50KCk7XG4gICAgfSxcbiAgICAgIGVycm9yID0+IHtcbiAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICBjb25zb2xlLmVycm9yKCdPb3BzOicsIGVycm9yKTtcbiAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgICBzdWJUaXRsZTogZXJyb3IuZXJyb3IubWVzc2FnZVxuICAgICAgICAgIC8vc3ViVGl0bGU6IFwiT29wcy4gU29tZXRoaW5nIHdlbnQgd3JvbmcuIFBsZWFzZSB0cnkgYWdhaW4gYWZ0ZXIgc29tZSB0aW1lXCJcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0UGF0aWVudFZpc2l0cygpOiB2b2lkIHtcbiAgICB0aGlzLmdldFZpc2l0cyA9IHRydWU7XG4gICAgdGhpcy52aXNpdExpc3QgPSBbXTtcbiAgICB0aGlzLmFwaS5nZXRWaXNpdHModGhpcy5wYXRpZW50RGV0YWlscy5pZGVudGlmaWVyWzBdLnZhbHVlLCB0aGlzLnNlbGVjdGVkRW1yLm51bWJlcilcbiAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICBsZXQgdmlzaXRzID0gcmVzcG9uc2UuZW50cnk7XG4gICAgICAgIGlmICh2aXNpdHMgJiYgdmlzaXRzLmxlbmd0aClcbiAgICAgICAgICB2aXNpdHMuZm9yRWFjaCh2aXNpdCA9PiB7XG4gICAgICAgICAgICB2aXNpdC5yZXNvdXJjZS5wZXJpb2Quc3RhcnQgPSBuZXcgRGF0ZSh2aXNpdC5yZXNvdXJjZS5wZXJpb2Quc3RhcnQpO1xuICAgICAgICAgICAgdmlzaXQucmVzb3VyY2UucGVyaW9kLmVuZCA9IG5ldyBEYXRlKHZpc2l0LnJlc291cmNlLnBlcmlvZC5lbmQpO1xuICAgICAgICAgICAgdGhpcy52aXNpdExpc3QucHVzaCh2aXNpdC5yZXNvdXJjZSk7XG4gICAgICAgICAgfSlcbiAgICAgIH0sXG4gICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgICB0aXRsZTogXCJFcnJvclwiLFxuICAgICAgICAgICAgc3ViVGl0bGU6IGVycm9yLm1lc3NhZ2VcbiAgICAgICAgICB9O1xuICAgICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgICB9KTtcbiAgfVxuXG4gIGdldFBhdGllbnRIaXN0b3J5KCk6IHZvaWQge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICB0aGlzLnNob3dEZXRhaWxzID0gdHJ1ZVxuICAgIHRoaXMub2JzZXJ2YXRpb25EZXRhaWxzID0gbnVsbDtcbiAgICB0aGlzLmFwaS5nZXRPYnNlcnZhdGlvbnModGhpcy5wYXRpZW50RGV0YWlscy5pZCwgdGhpcy5zZWxlY3RlZEVtci5udW1iZXIpXG4gICAgICAuc3Vic2NyaWJlKFxuICAgICAgICByZXNwb25zZSA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJyZXNwb25zZVwiLCByZXNwb25zZSk7XG4gICAgICAgICAgc2VsZi5vYnNlcnZhdGlvbkRldGFpbHMgPSByZXNwb25zZS5lbnRyeTtcbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3IgPT4ge1xuICAgICAgICAgIHNlbGYubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgICBsZXQgaW5mbyA9IHtcbiAgICAgICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgICAgICBzdWJUaXRsZTogZXJyb3IubWVzc2FnZVxuICAgICAgICAgIH07XG4gICAgICAgICAgc2VsZi5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgc2hvd1ByZXNjcmlwdGlvblZpZXcoKTogdm9pZCB7XG4gICAgdGhpcy5zaG93UHJlc2NyaXB0aW9ucyA9IHRydWU7XG4gIH1cblxuXG4gIGdldFBhdGllbnRGaWxlKCk6IHZvaWQge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICBzZWxmLmNyZWF0ZUxvYWRlcigpO1xuICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICB1c2VyOiB0aGlzLmNyZWRlbnRpYWxzLnVzZXIsXG4gICAgICBncmFudG9yOiB0aGlzLnNlbGVjdGVkUGF0aWVudEFkZHJlc3MsXG4gICAgICByZWNpcGllbnQ6IHRoaXMudXNlci5hY2NvdW50LFxuICAgICAgcHJvdmlkZXI6dGhpcy5uYXZQYXJhbXMuZGF0YS5kb2N0b3Iuc2VsZWN0ZWRPcHRpb24ubnVtYmVyIC8vIHRoaXMgaXMgcGF0aWVudCBwcm92aWRlciBudW1iZXIgZnJvbSB3aGljaCBkb2N0b3IgbmVlZHMgdG8gYWNjZXNzIGZpbGUgb2YgdGhlIHBhdGllbnRcbiAgICB9O1xuICAgIHNlbGYuYXBpLmdldEZpbGUoZGF0YSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgIHNlbGYubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAgIHdpbmRvdy5vcGVuKGAke0RDT01fVVJMfT9pbnB1dD1gK2VuY29kZVVSSUNvbXBvbmVudChyZXNwb25zZS5kYXRhKSxcIl9ibGFua1wiKTtcbiAgICB9LFxuICAgICAgZXJyb3IgPT4ge1xuICAgICAgICBzZWxmLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgICAgc3ViVGl0bGU6IGVycm9yLmVycm9yLm1lc3NhZ2VcbiAgICAgICAgfTtcbiAgICAgICAgc2VsZi5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICB9KTtcbiAgfVxuXG4gIGNyZWF0ZUxvYWRlcigpOiB2b2lkIHtcbiAgICB0aGlzLmxvYWRlciA9IHRoaXMubG9hZGluZ0N0cmwuY3JlYXRlKHtcbiAgICAgIGNvbnRlbnQ6IFwiUGxlYXNlIHdhaXQuLi5cIixcbiAgICAgIGRpc21pc3NPblBhZ2VDaGFuZ2U6IHRydWVcbiAgICB9KTtcbiAgICB0aGlzLmxvYWRlci5wcmVzZW50KCk7XG4gIH1cblxuICBnZXROb3RpZmljYXRpb25zQ291bnQoKTogdm9pZCB7XG4gICAgdGhpcy5ub3RpZmljYXRpb25Qcm92aWRlci5nZXROb3RpZmljYXRpb25zQ291bnQodGhpcy5jbGluaWNhbERhdGEudXNlclB1YmxpY0FkZHJlc3MpXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zQ291bnQgPSByZXNwb25zZVsnY291bnQnXTtcbiAgICAgIH0sIGVycm9yID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBwcmVzZW50Tm90aWZpY2F0aW9ucyhteUV2ZW50KSB7XG4gICAgaWYgKHRoaXMucGxhdGZvcm0uaXMoJ2lwaG9uZScpIHx8IHRoaXMucGxhdGZvcm0uaXMoJ21vYmlsZScpKSB7XG4gICAgICBjb25zdCBtb2RhbCA9IHRoaXMubW9kYWxDdHJsLmNyZWF0ZShOb3RpZmljYXRpb25zUGFnZSwgeyB1c2VyRGV0YWlsczogdGhpcy5jbGluaWNhbERhdGEgfSk7XG4gICAgICBtb2RhbC5wcmVzZW50KCk7XG4gICAgICBtb2RhbC5vbkRpZERpc21pc3MoKCkgPT4geyB0aGlzLm5vdGlmaWNhdGlvbnNDb3VudCA9IDAgfSk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgbGV0IHBvcG92ZXIgPSB0aGlzLnBvcG92ZXJDdHJsLmNyZWF0ZShOb3RpZmljYXRpb25zUGFnZSwgeyB1c2VyRGV0YWlsczogdGhpcy5jbGluaWNhbERhdGEgfSwgeyBjc3NDbGFzczogJ2NvbnRhY3QtcG9wb3ZlcicgfSk7XG4gICAgICBwb3BvdmVyLnByZXNlbnQoe1xuICAgICAgICBldjogbXlFdmVudFxuICAgICAgfSk7XG4gICAgICBwb3BvdmVyLm9uRGlkRGlzbWlzcygoKSA9PiB7IHRoaXMubm90aWZpY2F0aW9uc0NvdW50ID0gMCB9KTtcbiAgICB9XG4gIH1cblxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9kb2N0b3ItZGFzaG9hcmQvZG9jdG9yLWRhc2hvYXJkLnRzIiwiaW1wb3J0IHsgSGVhbHRoZXJpdW1BcGkgfSBmcm9tICcuLy4uLy4uL3Byb3ZpZGVycy9oZWFsdGhlcml1bS1hcGkvaGVhbHRoZXJpdW0tYXBpJztcbmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2Q29udHJvbGxlciwgTmF2UGFyYW1zLCBMb2FkaW5nQ29udHJvbGxlciwgUGxhdGZvcm0gLEFsZXJ0Q29udHJvbGxlcn0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBRUlNjYW5uZXIsIFFSU2Nhbm5lclN0YXR1cyB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvcXItc2Nhbm5lcic7XG5pbXBvcnQge0RDT01fVVJMfSBmcm9tICcuLi8uLi9jb25maWcnO1xuLy8gaW1wb3J0IHsgRmlsZSB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvZmlsZSc7XG4vLyBpbXBvcnQgeyBGaWxlQ2hvb3NlciB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvZmlsZS1jaG9vc2VyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGFnZS1wYXRpZW50LWRhc2hib2FyZCcsXG4gIHRlbXBsYXRlVXJsOiAncGF0aWVudC1kYXNoYm9hcmQuaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIFBhdGllbnREYXNoYm9hcmRQYWdlIHtcbiAgdmlzaXRMaXN0OiBBcnJheTxhbnk+ID0gW107XG4gIHZpc2l0ZWQ6IGJvb2xlYW47XG4gIGZpbGVUb1VwbG9hZDogYW55O1xuICBsb2FkZXI7XG4gIHZpc2l0c0luZm9GZXRjaGVkOiBib29sZWFuO1xuICBjbGluaWNhbERhdGE6IGFueSA9IHt9O1xuICBzY2FubmVkQWRkcmVzczogc3RyaW5nID0gXCJcIjtcbiAgYWRkcmVzc1RvUmV2b2tlOiBzdHJpbmcgPSBcIlwiO1xuICBpc0FwcDogYm9vbGVhbiA9IHRydWU7XG4gIG9ic2VydmF0aW9uRGV0YWlsczogYW55O1xuICAvL3Nob3duR3JvdXA6IGFueTtcbiAgc2hvd0RldGFpbHM6IGJvb2xlYW47XG4gIGNyZWRlbnRpYWxzOiBhbnk7XG4gIHNlbGVjdGVkRW1yOiBhbnk7XG4gIHVzZXI6IGFueTtcbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIG5hdkN0cmw6IE5hdkNvbnRyb2xsZXIsXG4gICAgcHVibGljIG5hdlBhcmFtczogTmF2UGFyYW1zLFxuICAgIHByaXZhdGUgcXJTY2FubmVyOiBRUlNjYW5uZXIsXG4gICAgcHVibGljIGFwaTogSGVhbHRoZXJpdW1BcGksXG4gICAgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICBwdWJsaWMgYWxlcnRDdHJsOiBBbGVydENvbnRyb2xsZXIsXG4gICAgcHVibGljIHBsYXRmb3JtOiBQbGF0Zm9ybSkge1xuXG4gICAgaWYgKHRoaXMucGxhdGZvcm0uaXMoJ2NvcmUnKSB8fCB0aGlzLnBsYXRmb3JtLmlzKCdtb2JpbGV3ZWInKSkge1xuICAgICAgdGhpcy5pc0FwcCA9IGZhbHNlO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHRoaXMuaXNBcHAgPSB0cnVlO1xuICAgIH1cblxuICAgIGNvbnNvbGUubG9nKHRoaXMubmF2UGFyYW1zLmRhdGEpO1xuICAgIHRoaXMuY3JlZGVudGlhbHMgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLmNyZWRlbnRpYWxzO1xuICAgIHRoaXMudXNlciA9IHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UudXNlcjtcbiAgICB0aGlzLnNlbGVjdGVkRW1yID0gdGhpcy5jcmVkZW50aWFscy5lbXJEZXRhaWxzO1xuICAgIGlmICh0aGlzLm5hdlBhcmFtcy5kYXRhICYmIHRoaXMubmF2UGFyYW1zLmRhdGEucmVzcG9uc2UgJiYgdGhpcy5uYXZQYXJhbXMuZGF0YS5yZXNwb25zZS5jbGluaWNhbERhdGEuYm9keSkge1xuICAgICAgdGhpcy5jbGluaWNhbERhdGEuZW1yRGF0YSA9IEpTT04ucGFyc2UodGhpcy5uYXZQYXJhbXMuZGF0YS5yZXNwb25zZS5jbGluaWNhbERhdGEuYm9keSkuZW50cnlbMF0ucmVzb3VyY2U7XG4gICAgfVxuICAgIGNvbnNvbGUubG9nKHRoaXMuY2xpbmljYWxEYXRhLmVtckRhdGEpXG4gIH1cblxuICBpb25WaWV3RGlkTG9hZCgpIHtcbiAgICB0aGlzLnNob3dEZXRhaWxzID0gZmFsc2U7XG4gICAgY29uc29sZS5sb2coJ2lvblZpZXdEaWRMb2FkIFBhdGllbnREYXNoYm9hcmRQYWdlJyk7XG4gIH1cblxuICBzY2FuUVJDb2RlKGdyYW50KTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coXCJoZWxsbyBJIGFtIGhlcmVcIik7XG4gICAgY29uc29sZS5sb2codGhpcy5xclNjYW5uZXIpO1xuICAgIHZhciBpb25BcHAgPSA8SFRNTEVsZW1lbnQ+ZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJpb24tYXBwXCIpWzBdO1xuICAgIHRoaXMucXJTY2FubmVyLnByZXBhcmUoKVxuICAgICAgLnRoZW4oKHN0YXR1czogUVJTY2FubmVyU3RhdHVzKSA9PiB7XG4gICAgICAgIGlmIChzdGF0dXMuYXV0aG9yaXplZCkge1xuICAgICAgICAgIC8vIGNhbWVyYSBwZXJtaXNzaW9uIHdhcyBncmFudGVkXG4gICAgICAgICAgLy8gc3RhcnQgc2Nhbm5pbmdcbiAgICAgICAgICBsZXQgc2NhblN1YiA9IHRoaXMucXJTY2FubmVyLnNjYW4oKS5zdWJzY3JpYmUoKHRleHQ6IHN0cmluZykgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ1NjYW5uZWQgc29tZXRoaW5nJywgdGV4dCk7XG4gICAgICAgICAgICBpZiAoZ3JhbnQpIHtcbiAgICAgICAgICAgICAgdGhpcy5hZGRyZXNzVG9SZXZva2UgPSBcIlwiO1xuICAgICAgICAgICAgICB0aGlzLnNjYW5uZWRBZGRyZXNzID0gdGV4dDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLnNjYW5uZWRBZGRyZXNzID0gXCJcIjtcbiAgICAgICAgICAgICAgdGhpcy5hZGRyZXNzVG9SZXZva2UgPSB0ZXh0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5xclNjYW5uZXIuaGlkZSgpOyAvLyBoaWRlIGNhbWVyYSBwcmV2aWV3XG4gICAgICAgICAgICBpb25BcHAuc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICAgICAgICAgIHNjYW5TdWIudW5zdWJzY3JpYmUoKTsgLy8gc3RvcCBzY2FubmluZ1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8vIHNob3cgY2FtZXJhIHByZXZpZXdcbiAgICAgICAgICBpb25BcHAuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgIHRoaXMucXJTY2FubmVyLnNob3coKTtcblxuICAgICAgICB9IGVsc2UgaWYgKHN0YXR1cy5kZW5pZWQpIHtcbiAgICAgICAgICAvLyBjYW1lcmEgcGVybWlzc2lvbiB3YXMgcGVybWFuZW50bHkgZGVuaWVkXG4gICAgICAgICAgLy8geW91IG11c3QgdXNlIFFSU2Nhbm5lci5vcGVuU2V0dGluZ3MoKSBtZXRob2QgdG8gZ3VpZGUgdGhlIHVzZXIgdG8gdGhlIHNldHRpbmdzIHBhZ2VcbiAgICAgICAgICAvLyB0aGVuIHRoZXkgY2FuIGdyYW50IHRoZSBwZXJtaXNzaW9uIGZyb20gdGhlcmVcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBwZXJtaXNzaW9uIHdhcyBkZW5pZWQsIGJ1dCBub3QgcGVybWFuZW50bHkuIFlvdSBjYW4gYXNrIGZvciBwZXJtaXNzaW9uIGFnYWluIGF0IGEgbGF0ZXIgdGltZS5cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5jYXRjaCgoZTogYW55KSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdFcnJvciBpcycsIGUpO1xuICAgICAgICBpZiAoZ3JhbnQpIHtcbiAgICAgICAgICB0aGlzLnNjYW5uZWRBZGRyZXNzID0gXCJcIjtcbiAgICAgICAgICB0aGlzLmFkZHJlc3NUb1Jldm9rZSA9IFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgdGhpcy5zY2FubmVkQWRkcmVzcyA9IFwiXCI7XG4gICAgICAgICAgdGhpcy5hZGRyZXNzVG9SZXZva2UgPSBcIlwiO1xuICAgICAgICB9XG5cbiAgICAgIH0pO1xuICB9XG5cbiAgbWFuYWdlQWNjZXNzKGdyYW50KTogdm9pZCB7XG4gICAgbGV0IHVzZXIgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLmNyZWRlbnRpYWxzO1xuICAgIGlmIChncmFudCkge1xuICAgICAgdXNlci50eXBlID0gdHJ1ZTtcbiAgICAgIHVzZXIudG9BZGRyZXNzID0gdGhpcy5zY2FubmVkQWRkcmVzcztcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICB1c2VyLnR5cGUgPSBmYWxzZTtcbiAgICAgIHVzZXIudG9BZGRyZXNzID0gdGhpcy5hZGRyZXNzVG9SZXZva2U7XG4gICAgfVxuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgdGhpcy5hcGkubWFuYWdlQWNjZXNzKHVzZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICB0aGlzLnNjYW5uZWRBZGRyZXNzID0gXCJcIjtcbiAgICAgIHRoaXMuYWRkcmVzc1RvUmV2b2tlID0gXCJcIjtcbiAgICAgIGNvbnNvbGUubG9nKFwiYWRkcmVzcyBzaGFyaW5nL2NhbmNlbGxhdGlvbiBzdWNjZXNzXCIpO1xuICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3NcIixcbiAgICAgICAgc3ViVGl0bGU6cmVzcG9uc2UubWVzc2FnZVxuICAgICAgICAvL3N1YlRpdGxlOiBcIk9vcHMuIFNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGFnYWluIGFmdGVyIHNvbWUgdGltZVwiXG4gICAgICB9XG4gICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICB9LFxuICAgIGVycm9yID0+IHtcbiAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ09vcHM6JywgZXJyb3IpO1xuICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgIHN1YlRpdGxlOmVycm9yLmVycm9yLm1lc3NhZ2VcbiAgICAgICAgLy9zdWJUaXRsZTogXCJPb3BzLiBTb21ldGhpbmcgd2VudCB3cm9uZy4gUGxlYXNlIHRyeSBhZ2FpbiBhZnRlciBzb21lIHRpbWVcIlxuICAgICAgfVxuICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgfSk7XG4gIH1cblxuICBnZXRPYnNlcnZhdGlvbnMoKTogdm9pZCB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIHRoaXMuc2hvd0RldGFpbHMgPSB0cnVlXG4gICAgdGhpcy5vYnNlcnZhdGlvbkRldGFpbHMgPSBudWxsO1xuICAgIHRoaXMuYXBpLmdldE9ic2VydmF0aW9ucyh0aGlzLmNsaW5pY2FsRGF0YS5lbXJEYXRhLmlkLCB0aGlzLnNlbGVjdGVkRW1yLm51bWJlcilcbiAgICAgIC5zdWJzY3JpYmUoXG4gICAgICAgIHJlc3BvbnNlID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcInJlc3BvbnNlXCIsIHJlc3BvbnNlKTtcbiAgICAgICAgICBzZWxmLm9ic2VydmF0aW9uRGV0YWlscyA9IHJlc3BvbnNlLmVudHJ5O1xuICAgICAgICAgIC8vc2VsZi50b2dnbGVHcm91cChpbmRleCk7XG4gICAgICAgIH0sXG4gICAgICBlcnJvciA9PiB7XG5cbiAgICAgIH0pO1xuICB9XG5cbiAgaGFuZGxlRmlsZUlucHV0KGZpbGVzOiBGaWxlTGlzdCkgOiB2b2lkIHtcbiAgICB0aGlzLmZpbGVUb1VwbG9hZCA9IGZpbGVzLml0ZW0oMCk7XG4gIH1cblxuICB1cGxvYWRGaWxlKCkgOiB2b2lkIHtcbiAgICBsZXQgcGF0aWVudERldGFpbHMgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLmNyZWRlbnRpYWxzO1xuICAgIHBhdGllbnREZXRhaWxzLnR5cGUgPSBcImluc2VydGZpbGVcIjtcbiAgICB0aGlzLmNyZWF0ZUxvYWRlcigpO1xuICAgIHRoaXMuYXBpLnBvc3RGaWxlKHRoaXMuZmlsZVRvVXBsb2FkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgICAgaWYocmVzcG9uc2UgJiYgcmVzcG9uc2UudXJsKSB7XG4gICAgICAgIHBhdGllbnREZXRhaWxzLmZpbGVVcmwgPSByZXNwb25zZS51cmw7XG4gICAgICAgIHRoaXMuYXBpLnVwbG9hZEZpbGUocGF0aWVudERldGFpbHMpLnN1YnNjcmliZShkYXRhID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgICB0aXRsZTogXCJTdWNjZXNzXCIsXG4gICAgICAgICAgICBzdWJUaXRsZTpkYXRhLm1lc3NhZ2VcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0sXG4gICAgZXJyb3IgPT4ge1xuICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgY29uc29sZS5lcnJvcignT29wczonLCBlcnJvcik7XG4gICAgICBsZXQgaW5mbyA9IHtcbiAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgc3ViVGl0bGU6ZXJyb3IuZXJyb3IubWVzc2FnZVxuICAgICAgICAvL3N1YlRpdGxlOiBcIk9vcHMuIFNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGFnYWluIGFmdGVyIHNvbWUgdGltZVwiXG4gICAgICB9XG4gICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICB9KTtcbiAgfVxuXG4gIGdldEZpbGUoKTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coXCJoZWxsb1wiKTtcbiAgICBsZXQgcGF0aWVudERldGFpbHMgPSB0aGlzLm5hdlBhcmFtcy5kYXRhLmNyZWRlbnRpYWxzO1xuICAgICBjb25zdCBkYXRhID0ge1xuICAgICAgc3NuIDogdGhpcy5jcmVkZW50aWFscy5zc24sXG4gICAgICBhY2NvdW50OiB0aGlzLnVzZXIuYWNjb3VudCxcbiAgICAgIHVzZXI6IHRoaXMuY3JlZGVudGlhbHMudXNlcixcbiAgICAgIHByb3ZpZGVyOnRoaXMuY3JlZGVudGlhbHMuZW1yRGV0YWlscy5udW1iZXJcbiAgICB9O1xuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgdGhpcy5hcGkuZ2V0RmlsZShkYXRhKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgd2luZG93Lm9wZW4oYCR7RENPTV9VUkx9P2lucHV0PWArZW5jb2RlVVJJQ29tcG9uZW50KHJlc3BvbnNlLmRhdGEpLFwiX2JsYW5rXCIpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0VmlzaXRzKCk6IHZvaWQge1xuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgdGhpcy5hcGkuZ2V0VmlzaXRzKHRoaXMuY2xpbmljYWxEYXRhLmVtckRhdGEuaWRlbnRpZmllclswXS52YWx1ZSwgdGhpcy5zZWxlY3RlZEVtci5udW1iZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICB0aGlzLnZpc2l0c0luZm9GZXRjaGVkID0gdHJ1ZTtcbiAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAgICAgaWYocmVzcG9uc2UudG90YWwpIHtcbiAgICAgICAgICB0aGlzLnZpc2l0TGlzdCA9IFtdO1xuICAgICAgICAgIHRoaXMudmlzaXRlZCA9IHRydWU7XG4gICAgICAgICAgbGV0IHZpc2l0cyA9IHJlc3BvbnNlLmVudHJ5O1xuICAgICAgICAgIHZpc2l0cy5mb3JFYWNoKHZpc2l0ID0+IHtcbiAgICAgICAgICAgIHZpc2l0LnJlc291cmNlLnBlcmlvZC5zdGFydCA9IG5ldyBEYXRlKHZpc2l0LnJlc291cmNlLnBlcmlvZC5zdGFydCk7XG4gICAgICAgICAgICB2aXNpdC5yZXNvdXJjZS5wZXJpb2QuZW5kID0gbmV3IERhdGUodmlzaXQucmVzb3VyY2UucGVyaW9kLmVuZCk7XG4gICAgICAgICAgICB0aGlzLnZpc2l0TGlzdC5wdXNoKHZpc2l0LnJlc291cmNlKTtcbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHRoaXMudmlzaXRlZCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgfSk7XG4gIH1cblxuICAvLyB0b2dnbGVHcm91cChncm91cCkge1xuICAvLyAgIGlmICh0aGlzLmlzR3JvdXBTaG93bihncm91cCkpIHtcbiAgLy8gICAgICAgdGhpcy5zaG93bkdyb3VwID0gbnVsbDtcbiAgLy8gICB9IGVsc2Uge1xuICAvLyAgICAgICB0aGlzLnNob3duR3JvdXAgPSBncm91cDtcbiAgLy8gICB9XG4gIC8vIH1cbiAgLy8gaXNHcm91cFNob3duKGdyb3VwKSB7XG4gIC8vICAgICByZXR1cm4gdGhpcy5zaG93bkdyb3VwID09PSBncm91cDtcbiAgLy8gfVxuXG4gIHByZXNlbnRBbGVydChpbmZvKSB7XG4gICAgbGV0IGFsZXJ0ID0gdGhpcy5hbGVydEN0cmwuY3JlYXRlKHtcbiAgICAgIHRpdGxlOiBpbmZvLnRpdGxlLFxuICAgICAgc3ViVGl0bGU6IGluZm8uc3ViVGl0bGUsXG4gICAgICBidXR0b25zOiBbJ09rJ11cbiAgICB9KTtcbiAgICBhbGVydC5wcmVzZW50KCk7XG4gIH1cblxuICBjcmVhdGVMb2FkZXIoKTogdm9pZCB7XG4gICAgdGhpcy5sb2FkZXIgPSB0aGlzLmxvYWRpbmdDdHJsLmNyZWF0ZSh7XG4gICAgICBjb250ZW50OiBcIlBsZWFzZSB3YWl0Li4uXCIsXG4gICAgICBkaXNtaXNzT25QYWdlQ2hhbmdlOiB0cnVlXG4gICAgfSk7XG4gICAgdGhpcy5sb2FkZXIucHJlc2VudCgpO1xuICB9XG5cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9wYXRpZW50LWRhc2hib2FyZC9wYXRpZW50LWRhc2hib2FyZC50cyIsImltcG9ydCB7IFJlZ2lzdHJhdGlvblBhZ2UgfSBmcm9tICcuLy4uL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24nO1xuaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbENvbnRyb2xsZXIsIFBsYXRmb3JtLCBOYXZDb250cm9sbGVyLCBOYXZQYXJhbXMgfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB7IE1vZGFsQ29udGVudFBhZ2UgfSBmcm9tICcuLi9tb2RhbC1jb250ZW50L21vZGFsLWNvbnRlbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICB0ZW1wbGF0ZVVybDogJ2hvbWUuaHRtbCcsXG4gICAgc2VsZWN0b3I6ICdIb21lJ1xufSlcblxuZXhwb3J0IGNsYXNzIEhvbWUge1xuXG4gICAgcm9sZTogYW55ID0gbnVsbDtcbiAgICBpc0FwcDogYm9vbGVhbjtcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgbmF2OiBOYXZDb250cm9sbGVyLFxuICAgICAgICBwdWJsaWMgbmF2UGFyYW1zOiBOYXZQYXJhbXMsXG4gICAgICAgIHB1YmxpYyBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcixcbiAgICAgICAgcHVibGljIHBsYXRmb3JtOiBQbGF0Zm9ybSkge1xuICAgICAgICBpZiAodGhpcy5wbGF0Zm9ybS5pcygnY29yZScpIHx8IHRoaXMucGxhdGZvcm0uaXMoJ21vYmlsZXdlYicpKSB7XG4gICAgICAgICAgICB0aGlzLmlzQXBwID0gZmFsc2U7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmlzQXBwID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlvblZpZXdEaWRMb2FkKCkge1xuICAgICAgICBjb25zb2xlLmxvZygnaW9uVmlld0RpZExvYWQgRG9jdG9yRGFzaGJvYXJkUGFnZScpO1xuICAgIH1cblxuICAgIGxvZ2luKHVzZXIpIHtcbiAgICAgICAgbGV0IG1vZGFsID0gdGhpcy5tb2RhbEN0cmwuY3JlYXRlKE1vZGFsQ29udGVudFBhZ2UsIHsgdXNlciwgYWN0aW9uOiBcImxvZ2luXCIgfSk7XG4gICAgICAgIG1vZGFsLnByZXNlbnQoKTtcbiAgICB9XG5cbiAgICByZWdpc3Rlcih1c2VyKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKHVzZXIpO1xuICAgICAgICB0aGlzLm5hdi5wdXNoKFJlZ2lzdHJhdGlvblBhZ2UsIHt1c2VyfSk7XG4gICAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL2hvbWUvaG9tZS50cyIsImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2Q29udHJvbGxlciwgTmF2UGFyYW1zIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGFnZS1hYm91dC11cycsXG4gIHRlbXBsYXRlVXJsOiAnYWJvdXQtdXMuaHRtbCcsXG59KVxuZXhwb3J0IGNsYXNzIEFib3V0VXNQYWdlIHtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgbmF2Q3RybDogTmF2Q29udHJvbGxlciwgcHVibGljIG5hdlBhcmFtczogTmF2UGFyYW1zKSB7XG4gIH1cblxuICBpb25WaWV3RGlkTG9hZCgpIHtcbiAgICBjb25zb2xlLmxvZygnaW9uVmlld0RpZExvYWQgQWJvdXRVc1BhZ2UnKTtcbiAgfVxuXG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcGFnZXMvYWJvdXQtdXMvYWJvdXQtdXMudHMiLCJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1vZGFsQ29udHJvbGxlciwgUGxhdGZvcm0sIE5hdkNvbnRyb2xsZXIsIE5hdlBhcmFtcyB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuXG5AQ29tcG9uZW50KHtcbiAgICB0ZW1wbGF0ZVVybDogJ2xhbmRpbmcuaHRtbCcsXG4gICAgc2VsZWN0b3I6ICdMYW5kaW5nJ1xufSlcblxuZXhwb3J0IGNsYXNzIExhbmRpbmdQYWdlIHtcblxuICAgIHJvbGU6IGFueSA9IG51bGw7XG4gICAgaXNBcHA6IGJvb2xlYW47XG4gICAgY29uc3RydWN0b3IocHVibGljIG5hdjogTmF2Q29udHJvbGxlcixcbiAgICAgICAgcHVibGljIG5hdlBhcmFtczogTmF2UGFyYW1zLFxuICAgICAgICBwdWJsaWMgbW9kYWxDdHJsOiBNb2RhbENvbnRyb2xsZXIsXG4gICAgICAgIHB1YmxpYyBwbGF0Zm9ybTogUGxhdGZvcm0pIHtcbiAgICAgICAgaWYodGhpcy5wbGF0Zm9ybS5pcygnY29yZScpIHx8IHRoaXMucGxhdGZvcm0uaXMoJ21vYmlsZXdlYicpKSB7XG4gICAgICAgICAgICB0aGlzLmlzQXBwID0gZmFsc2U7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuaXNBcHAgPSB0cnVlO1xuICAgICAgICAgIH1cbiAgICB9XG5cbiAgICBpb25WaWV3RGlkTG9hZCgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ2lvblZpZXdEaWRMb2FkIERvY3RvckRhc2hib2FyZFBhZ2UnKTtcbiAgICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcGFnZXMvbGFuZGluZy9sYW5kaW5nLnRzIiwiaW1wb3J0IHsgcGxhdGZvcm1Ccm93c2VyRHluYW1pYyB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXItZHluYW1pYyc7XG5cbmltcG9ydCB7IEFwcE1vZHVsZSB9IGZyb20gJy4vYXBwLm1vZHVsZSc7XG5cbnBsYXRmb3JtQnJvd3NlckR5bmFtaWMoKS5ib290c3RyYXBNb2R1bGUoQXBwTW9kdWxlKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9hcHAvbWFpbi50cyIsImltcG9ydCB7IFJlZ2lzdHJhdGlvblBhZ2VNb2R1bGUgfSBmcm9tICcuLy4uL3BhZ2VzL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24ubW9kdWxlJztcbmltcG9ydCB7IEhvbWUgfSBmcm9tICcuLy4uL3BhZ2VzL2hvbWUvaG9tZSc7XG5pbXBvcnQgeyBBYm91dFVzUGFnZSB9IGZyb20gJy4vLi4vcGFnZXMvYWJvdXQtdXMvYWJvdXQtdXMnO1xuaW1wb3J0IHsgUVJDb2RlTW9kdWxlIH0gZnJvbSAnYW5ndWxhcngtcXJjb2RlJztcbmltcG9ydCB7IEJyb3dzZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7IE5nTW9kdWxlLCBFcnJvckhhbmRsZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJb25pY0FwcCwgSW9uaWNNb2R1bGUsIElvbmljRXJyb3JIYW5kbGVyIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBDYW1lcmEgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NhbWVyYSc7XG5pbXBvcnQgeyBRUlNjYW5uZXIgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3FyLXNjYW5uZXInO1xuaW1wb3J0IHsgTXlBcHAgfSBmcm9tICcuL2FwcC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTGFuZGluZ1BhZ2UgfSBmcm9tICcuLi9wYWdlcy9sYW5kaW5nL2xhbmRpbmcnO1xuaW1wb3J0IHsgTW9kYWxDb250ZW50UGFnZSB9IGZyb20gJy4uL3BhZ2VzL21vZGFsLWNvbnRlbnQvbW9kYWwtY29udGVudCc7XG5cbmltcG9ydCB7IFN0YXR1c0JhciB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvc3RhdHVzLWJhcic7XG5pbXBvcnQgeyBTcGxhc2hTY3JlZW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL3NwbGFzaC1zY3JlZW4nO1xuaW1wb3J0IHsgSGVhbHRoZXJpdW1BcGkgfSBmcm9tICcuLi9wcm92aWRlcnMvaGVhbHRoZXJpdW0tYXBpL2hlYWx0aGVyaXVtLWFwaSc7XG5cbmltcG9ydCBmaXJlYmFzZSBmcm9tICdmaXJlYmFzZSc7XG5pbXBvcnQgeyBBbmd1bGFyRmlyZU1vZHVsZSB9IGZyb20gJ2FuZ3VsYXJmaXJlMic7XG5pbXBvcnQgeyBBbmd1bGFyRmlyZVN0b3JhZ2VNb2R1bGUgfSBmcm9tICdhbmd1bGFyZmlyZTIvc3RvcmFnZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25BcGlQcm92aWRlciB9IGZyb20gJy4uL3Byb3ZpZGVycy9ub3RpZmljYXRpb24tYXBpL25vdGlmaWNhdGlvbi1hcGknO1xuaW1wb3J0IHtOb3RpZmljYXRpb25zUGFnZU1vZHVsZX0gZnJvbSBcIi4uL3BhZ2VzL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5tb2R1bGVcIjtcbmltcG9ydCB7IERlbGVnYXRpb25Qcm92aWRlciB9IGZyb20gJy4uL3Byb3ZpZGVycy9kZWxlZ2F0aW9uLWFwaS9kZWxlZ2F0aW9uJztcbi8vaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gJy4vYXBwLXJvdXRpbmcubW9kdWxlJztcbmltcG9ydCB7UGF0aWVudERhc2hib2FyZFBhZ2VNb2R1bGV9IGZyb20gXCIuLi9wYWdlcy9wYXRpZW50LWRhc2hib2FyZC9wYXRpZW50LWRhc2hib2FyZC5tb2R1bGVcIjtcbmltcG9ydCB7RG9jdG9yRGFzaGJvYXJkUGFnZU1vZHVsZX0gZnJvbSBcIi4uL3BhZ2VzL2RvY3Rvci1kYXNob2FyZC9kb2N0b3ItZGFzaGJvYXJkLm1vZHVsZVwiO1xuaW1wb3J0IHsgUmVnaXN0cmF0aW9uUGFnZSB9IGZyb20gJy4uL3BhZ2VzL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24nO1xuLy8gSW5pdGlhbGl6ZSBGaXJlYmFzZVxudmFyIGNvbmZpZyA9IHtcbiAgYXBpS2V5OiBcIkFJemFTeURVSEUydGFvVDZqRzIySmEtazN6c3hNRTc0Tjdha3lIWVwiLFxuICBhdXRoRG9tYWluOiBcImhlYWx0aGVyaXVtLWExYmJhLmZpcmViYXNlYXBwLmNvbVwiLFxuICBkYXRhYmFzZVVSTDogXCJodHRwczovL2hlYWx0aGVyaXVtLWExYmJhLmZpcmViYXNlaW8uY29tXCIsXG4gIHByb2plY3RJZDogXCJoZWFsdGhlcml1bS1hMWJiYVwiLFxuICBzdG9yYWdlQnVja2V0OiBcImhlYWx0aGVyaXVtLWExYmJhLmFwcHNwb3QuY29tXCIsXG4gIG1lc3NhZ2luZ1NlbmRlcklkOiBcIjc5MzQ2ODUzODMwMFwiXG59O1xuZmlyZWJhc2UuaW5pdGlhbGl6ZUFwcChjb25maWcpO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBNeUFwcCxcbiAgICBMYW5kaW5nUGFnZSxcbiAgICBIb21lLFxuICAgIEFib3V0VXNQYWdlLFxuICAgIE1vZGFsQ29udGVudFBhZ2VcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIEJyb3dzZXJNb2R1bGUsXG4gICAgSHR0cE1vZHVsZSxcbiAgICAvL0FwcFJvdXRpbmdNb2R1bGUsXG4gICAgSW9uaWNNb2R1bGUuZm9yUm9vdChNeUFwcCksXG4gICAgSHR0cENsaWVudE1vZHVsZSxcbiAgICBBbmd1bGFyRmlyZU1vZHVsZS5pbml0aWFsaXplQXBwKGNvbmZpZyksXG4gICAgQW5ndWxhckZpcmVTdG9yYWdlTW9kdWxlLFxuICAgIFJlZ2lzdHJhdGlvblBhZ2VNb2R1bGUsXG4gICAgTm90aWZpY2F0aW9uc1BhZ2VNb2R1bGUsXG4gICAgUGF0aWVudERhc2hib2FyZFBhZ2VNb2R1bGUsXG4gICAgRG9jdG9yRGFzaGJvYXJkUGFnZU1vZHVsZVxuICBdLFxuICBib290c3RyYXA6IFtJb25pY0FwcF0sXG4gIGVudHJ5Q29tcG9uZW50czogW1xuICAgIE15QXBwLFxuICAgIFJlZ2lzdHJhdGlvblBhZ2UsXG4gICAgTGFuZGluZ1BhZ2UsXG4gICAgSG9tZSxcbiAgICBBYm91dFVzUGFnZSxcbiAgICBNb2RhbENvbnRlbnRQYWdlXG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIFN0YXR1c0JhcixcbiAgICBTcGxhc2hTY3JlZW4sXG4gICAgQ2FtZXJhLFxuICAgIFFSU2Nhbm5lcixcbiAgICB7IHByb3ZpZGU6IEVycm9ySGFuZGxlciwgdXNlQ2xhc3M6IElvbmljRXJyb3JIYW5kbGVyIH0sXG4gICAgSGVhbHRoZXJpdW1BcGksXG4gICAgTm90aWZpY2F0aW9uQXBpUHJvdmlkZXIsXG4gICAgRGVsZWdhdGlvblByb3ZpZGVyXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2FwcC9hcHAubW9kdWxlLnRzIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElvbmljUGFnZU1vZHVsZSB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuaW1wb3J0IHsgUmVnaXN0cmF0aW9uUGFnZSB9IGZyb20gJy4vcmVnaXN0cmF0aW9uJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgUmVnaXN0cmF0aW9uUGFnZVxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgSW9uaWNQYWdlTW9kdWxlLmZvckNoaWxkKFJlZ2lzdHJhdGlvblBhZ2UpLFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBSZWdpc3RyYXRpb25QYWdlTW9kdWxlIHt9XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcGFnZXMvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi5tb2R1bGUudHMiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvbiB9IGZyb20gJy4uLy4uL21vZGVscy9ub3RpZmljYXRpb24nO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uQXBpUHJvdmlkZXIgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvbm90aWZpY2F0aW9uLWFwaS9ub3RpZmljYXRpb24tYXBpJztcblxuLyoqXG4gKiBHZW5lcmF0ZWQgY2xhc3MgZm9yIHRoZSBOb3RpZmljYXRpb25zQ29tcG9uZW50IGNvbXBvbmVudC5cbiAqXG4gKiBTZWUgaHR0cHM6Ly9hbmd1bGFyLmlvL2FwaS9jb3JlL0NvbXBvbmVudCBmb3IgbW9yZSBpbmZvIG9uIEFuZ3VsYXJcbiAqIENvbXBvbmVudHMuXG4gKi9cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25vdGlmaWNhdGlvbnMnLFxuICB0ZW1wbGF0ZVVybDogJ25vdGlmaWNhdGlvbnMuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcbiAgbm90aWZpY2F0aW9uczogTm90aWZpY2F0aW9uW107XG4gIGNvdW50OiBudW1iZXIgPSAwO1xuICBASW5wdXQoKSB1c2VyRGV0YWlscztcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBub3RpZmljYXRpb25Qcm92aWRlcjogTm90aWZpY2F0aW9uQXBpUHJvdmlkZXIpIHtcbiAgICBjb25zb2xlLmxvZygnSGVsbG8gTm90aWZpY2F0aW9uc0NvbXBvbmVudCBDb21wb25lbnQnKTtcbiAgICB0aGlzLm5vdGlmaWNhdGlvbnMgPSBbXTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9ucygpO1xuICB9XG5cbiAgZ2V0Tm90aWZpY2F0aW9ucygpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygndXNlciBkZXRhaWxzJyk7XG4gICAgY29uc29sZS5sb2codGhpcy51c2VyRGV0YWlscyk7XG4gICAgdGhpcy5ub3RpZmljYXRpb25Qcm92aWRlci5nZXROb3RpZmljYXRpb25zKHRoaXMudXNlckRldGFpbHMudXNlclB1YmxpY0FkZHJlc3MpXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gcmVzcG9uc2VbJ25vdGlmaWNhdGlvbnMnXTtcbiAgICAgICAgdGhpcy5jb3VudCA9IHJlc3BvbnNlWydjb3VudCddO1xuICAgICAgICBpZih0aGlzLm5vdGlmaWNhdGlvbnMubGVuZ3RoKXtcbiAgICAgICAgICB0aGlzLnVwZGF0ZU5vdGlmaWNhdGlvbnNSZWFkKHRoaXMubm90aWZpY2F0aW9ucyApO1xuICAgICAgICB9XG4gICAgICB9LGVycm9yID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICB1cGRhdGVOb3RpZmljYXRpb25zUmVhZChub3RpZmljYXRpb25zKTogdm9pZCB7XG4gICAgdGhpcy5ub3RpZmljYXRpb25Qcm92aWRlci51cGRhdGVOb3RpZmljYXRpb25zUmVhZChub3RpZmljYXRpb25zKVxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7Y29uc29sZS5sb2coJ25vdGlmaWNhdGlvbiByZWFkIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5Jyl9LFxuICAgICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NvbXBvbmVudHMvbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLnRzIiwiaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCAqIGFzIGZpcmViYXNlIGZyb20gJ2ZpcmViYXNlJztcbmltcG9ydCB7IEJBU0VfVVJMIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcbmltcG9ydCB7UHJvdmlkZXJ9IGZyb20gXCIuLi8uLi9tb2RlbHMvcHJvdmlkZXJcIjtcblxuXG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEhlYWx0aGVyaXVtQXBpIHtcblxuICBwYXRpZW50RGF0YTtcblxuICAvL3ByaXZhdGUgYXBpVXJsID0gJyc7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkge1xuICB9XG5cbiAgZ2V0RGV0YWlscyhkYXRhKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zb2xlLmxvZyhcImxvZ2luXCIpXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke0JBU0VfVVJMfS9mZXRjaERldGFpbHNgLCBkYXRhKVxuICAgICAgLm1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0QWNjZXNzTGlzdChkYXRhKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zb2xlLmxvZyhcImdldHRpbmcgYWNjZXNzIGxpc3RcIilcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoYCR7QkFTRV9VUkx9L2dldFNoYXJlZFBhdGllbnRMaXN0YCxkYXRhKVxuICAgIC5tYXAocmVzcG9uc2U9PiB7XG4gICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfSk7XG4gIH1cblxuICBsb2dpbihkYXRhKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zb2xlLmxvZyhcImxvZ2luXCIsIGRhdGEpXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke0JBU0VfVVJMfS9hcGkvdXNlci9sb2dpbmAsIGRhdGEpXG4gICAgICAubWFwKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgfSk7XG4gIH1cblxuICBmZXRjaERldGFpbHMoZGF0YSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc29sZS5sb2coXCJmZXRjaERldGFpbHNcIilcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoYCR7QkFTRV9VUkx9L2ZldGNoRGV0YWlsc2AsIGRhdGEpXG4gICAgICAubWFwKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgfSk7XG4gIH1cblxuICBnZW5lcmF0ZU9UUChkYXRhKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zb2xlLmxvZyhcInJlZ2lzdGVyVXNlclwiKVxuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChgJHtCQVNFX1VSTH0vYXBpL3VzZXIvZ2VuZXJhdGVvdHBgLCBkYXRhKVxuICAgICAgLm1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcmVnaXN0ZXIoZGF0YSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc29sZS5sb2coXCJhZGRVc2VyXCIpXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke0JBU0VfVVJMfS9hcGkvdXNlci9yZWdpc3RlcmAsIGRhdGEpXG4gICAgICAubWFwKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgfSk7XG4gIH1cbiAgdXBsb2FkRmlsZShkYXRhKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zb2xlLmxvZyhcImFkZFVzZXJcIik7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KGAke0JBU0VfVVJMfS9hcGkvdXNlci91cGxvYWRgLCBkYXRhKVxuICAgICAgLm1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbiAgbWFuYWdlQWNjZXNzKGRhdGEpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnNvbGUubG9nKFwiYWRkVXNlclwiKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoYCR7QkFTRV9VUkx9L2FwaS9vd25lcnNoaXBgLCBkYXRhKVxuICAgICAgLm1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcG9zdEZpbGUoZmlsZVRvVXBsb2FkKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICB2YXIgZmlsZSA9IGZpbGVUb1VwbG9hZDtcbiAgICBsZXQgc3RvcmFnZVJlZiA9IGZpcmViYXNlLnN0b3JhZ2UoKS5yZWYoKTtcbiAgICBsZXQgdXBsb2FkVGFzayA9IHN0b3JhZ2VSZWYuY2hpbGQoYCR7ZmlsZVRvVXBsb2FkLm5hbWV9YCkucHV0KGZpbGVUb1VwbG9hZCk7XG4gICAgcmV0dXJuIE9ic2VydmFibGUuY3JlYXRlKG9ic2VydmVyID0+IHtcbiAgICAgIHVwbG9hZFRhc2sub24oZmlyZWJhc2Uuc3RvcmFnZS5UYXNrRXZlbnQuU1RBVEVfQ0hBTkdFRCxcbiAgICAgICAgKHNuYXBzaG90OiBhbnkpID0+IHtcbiAgICAgICAgICAvLyB1cGxvYWQgaW4gcHJvZ3Jlc3NcbiAgICAgICAgICBmaWxlVG9VcGxvYWQucHJvZ3Jlc3MgPSAoc25hcHNob3QuYnl0ZXNUcmFuc2ZlcnJlZCAvIHNuYXBzaG90LnRvdGFsQnl0ZXMpICogMTAwXG4gICAgICAgIH0sXG4gICAgICAgIChlcnJvcikgPT4ge1xuICAgICAgICAgIC8vIHVwbG9hZCBmYWlsZWRcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgb2JzZXJ2ZXIuZXJyb3Ioc3RhdHVzKTtcbiAgICAgICAgfSxcbiAgICAgICAgKCkgPT4ge1xuICAgICAgICAgIC8vIHVwbG9hZCBzdWNjZXNzXG4gICAgICAgICAgZmlsZS51cmwgPSB1cGxvYWRUYXNrLnNuYXBzaG90LmRvd25sb2FkVVJMO1xuICAgICAgICAgIGNvbnNvbGUubG9nKGZpbGUpO1xuICAgICAgICAgIG9ic2VydmVyLm5leHQoZmlsZSk7XG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG5cbiAgfVxuXG4gIGdldFZpc2l0cyhkYXRhLCBwcm92aWRlcik6IGFueSB7XG4gICAgY29uc3QgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKS5zZXQoJ2lkZW50aWZpZXInLCBkYXRhKS5zZXQoJ3Byb3ZpZGVyJywgcHJvdmlkZXIpO1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke0JBU0VfVVJMfS9hcGkvdXNlci92aXNpdHNgLCB7IHBhcmFtcyB9KVxuICAgICAgLm1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0T2JzZXJ2YXRpb25zKGRhdGEsIHByb3ZpZGVyKTogYW55IHtcbiAgICBjb25zdCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpLnNldCgnc3ViamVjdCcsIGRhdGEpLnNldCgncHJvdmlkZXInLCBwcm92aWRlcik7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7QkFTRV9VUkx9L2FwaS91c2VyL29ic2VydmF0aW9uc2AsIHsgcGFyYW1zIH0pXG4gICAgICAubWFwKHJlc3BvbnNlID0+IHtcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgfSk7XG4gIH1cblxuICBnZXRQcmVzY3JpcHRpb25zKHBlcnNvbklkLCBwcm92aWRlcik6IGFueSB7XG4gICAgY29uc3QgcGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKS5zZXQoJ3BlcnNvbklkJywgcGVyc29uSWQpLnNldCgncHJvdmlkZXInLCBwcm92aWRlcik7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7QkFTRV9VUkx9L3ByZXNjcmlwdGlvbnNgLCB7IHBhcmFtcyB9KVxuICAgICAgLm1hcChyZXNwb25zZSA9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZ2V0QWxsb2NhdGVkVXNlcnMoc3NuKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpLnNldCgndXNlcklkJywgc3NuKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldChgJHtCQVNFX1VSTH0vYXBpL3VzZXIvYWxsb2NhdGVkYCx7IHBhcmFtcyB9KVxuICAgIC5tYXAocmVzcG9uc2U9PiB7XG4gICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfSk7XG4gIH1cblxuICBnZXRVc2VySW5mbyhncmFudG9yLCByZWNpcGllbnQsIHByb3ZpZGVyKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpLnNldCgncmVjaXBpZW50JywgcmVjaXBpZW50KS5zZXQoJ2dyYW50b3InLCBncmFudG9yKS5zZXQoJ3Byb3ZpZGVyTm8nLHByb3ZpZGVyKVxuICAgIGNvbnNvbGUubG9nKHBhcmFtcyk7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7QkFTRV9VUkx9L2FwaS91c2VyL2dyYW50b3JgLCB7IHBhcmFtcyB9KVxuICAgIC5tYXAocmVzcG9uc2U9PiB7XG4gICAgICB0aGlzLnBhdGllbnREYXRhID0gcmVzcG9uc2U7XG4gICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfSk7XG4gIH1cblxuICBnZXRGaWxlKGRhdGEpIDogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoYCR7QkFTRV9VUkx9L2FwaS91c2VyL2ZpbGVgLCBkYXRhKVxuICAgIC5tYXAocmVzcG9uc2U9PiB7XG4gICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgfSk7XG4gIH1cblxuICBnZXRQcm92aWRlcnNPZlVzZXIoZGF0YSkgOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnNvbGUubG9nKFwiY2FsbGluZyBnZXQgcHJvdmlkZXJzIGFwaSBmcm9tIGZyb250IGVuZFwiKTtcbiAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICBsZXQgcXVlcnlQYXJhbXM9Jyc7XG4gICAgaWYoZGF0YS5zc24pe1xuICAgICAgcXVlcnlQYXJhbXMgPSBgP3VzZXJJZD0ke2RhdGEuc3NufWA7XG4gICAgfVxuICAgIGVsc2UgaWYoZGF0YS5hZGRyZXNzKXtcbiAgICAgIHF1ZXJ5UGFyYW1zID0gYD9hZGRyZXNzPSR7ZGF0YS5hZGRyZXNzfWA7XG4gICAgfVxuIFxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke0JBU0VfVVJMfS9hcGkvdXNlci9wcm92aWRlckxpc3RgKyBxdWVyeVBhcmFtcylcbiAgICAubWFwKHJlc3BvbnNlPT4ge1xuICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0UHJvdmlkZXJzKCk6IE9ic2VydmFibGU8UHJvdmlkZXJbXT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PFByb3ZpZGVyW10+KGAke0JBU0VfVVJMfS9hcGkvcHJvdmlkZXJzYClcbiAgICAgIC5tYXAocmVzcG9uc2U9PiB7XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pO1xuICB9XG5cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wcm92aWRlcnMvaGVhbHRoZXJpdW0tYXBpL2hlYWx0aGVyaXVtLWFwaS50cyIsImV4cG9ydCBjb25zdCBCQVNFX1VSTCA9ICdodHRwOi8vbG9jYWxob3N0OjQ0NDUnO1xuZXhwb3J0IGNvbnN0IERDT01fVVJMID0gJ2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9pbmRleC5odG1sJztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jb25maWcudHMiLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUHJlc2NyaXB0aW9uIH0gZnJvbSAnLi4vLi4vbW9kZWxzL3ByZXNjcmlwdGlvbic7XG5pbXBvcnQgeyBIZWFsdGhlcml1bUFwaSB9IGZyb20gJy4vLi4vLi4vcHJvdmlkZXJzL2hlYWx0aGVyaXVtLWFwaS9oZWFsdGhlcml1bS1hcGknO1xuaW1wb3J0IHtMb2FkaW5nQ29udHJvbGxlcn0gZnJvbSBcImlvbmljLWFuZ3VsYXJcIjtcblxuXG4vKipcbiAqIEdlbmVyYXRlZCBjbGFzcyBmb3IgdGhlIFByZXNjcmlwdGlvbkNvbXBvbmVudCBjb21wb25lbnQuXG4gKlxuICogU2VlIGh0dHBzOi8vYW5ndWxhci5pby9hcGkvY29yZS9Db21wb25lbnQgZm9yIG1vcmUgaW5mbyBvbiBBbmd1bGFyXG4gKiBDb21wb25lbnRzLlxuICovXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwcmVzY3JpcHRpb24nLFxuICB0ZW1wbGF0ZVVybDogJ3ByZXNjcmlwdGlvbi5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBQcmVzY3JpcHRpb25Db21wb25lbnQge1xuICBwcmVzY3JpcHRpb25zOiBQcmVzY3JpcHRpb25bXTtcbiAgQElucHV0KCkgaXNEb2N0b3I7XG4gIEBJbnB1dCgpIHNlbGVjdGVkRW1yO1xuICBjb3VudDogbnVtYmVyID0gMDtcbiAgQElucHV0KCkgdXNlckRldGFpbHM7XG4gIHNob3dQcmVzY3JpcHRpb25zVmlldzpib29sZWFuID0gZmFsc2U7XG4gIHB1YmxpYyBsb2FkZXI7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpOiBIZWFsdGhlcml1bUFwaSwgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcikge1xuICAgIHRoaXMucHJlc2NyaXB0aW9ucyA9IFtdO1xuICAgIGNvbnNvbGUubG9nKCdIZWxsbyBQcmVzY3JpcHRpb25Db21wb25lbnQgQ29tcG9uZW50Jyk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZih0aGlzLmlzRG9jdG9yKXtcbiAgICAgIHRoaXMuZ2V0UHJlc2NyaXB0aW9ucygpO1xuICAgIH1cbiAgfVxuXG4gIGdldFByZXNjcmlwdGlvbnMoKTogdm9pZCB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIHRoaXMuc2hvd1ByZXNjcmlwdGlvbnNWaWV3ID0gZmFsc2U7XG4gICAgbGV0IHBlcnNvbklkO1xuICAgIGlmKHRoaXMuaXNEb2N0b3Ipe1xuICAgICAgcGVyc29uSWQgPSB0aGlzLnVzZXJEZXRhaWxzLmlkO1xuICAgIH1cbiAgICBlbHNle1xuICAgICAgcGVyc29uSWQgPSB0aGlzLnVzZXJEZXRhaWxzLmVtckRhdGEuaWQ7XG4gICAgICBjb25zb2xlLmxvZyh0aGlzLnVzZXJEZXRhaWxzLmVtckRhdGEpO1xuICAgIH1cbiAgICBzZWxmLmNyZWF0ZUxvYWRlcigpO1xuICAgIHRoaXMuYXBpLmdldFByZXNjcmlwdGlvbnMocGVyc29uSWQsIHRoaXMuc2VsZWN0ZWRFbXIubnVtYmVyKVxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XG4gICAgICAgIHNlbGYubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgc2VsZi5zaG93UHJlc2NyaXB0aW9uc1ZpZXcgPSB0cnVlO1xuICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICAgIHRoaXMucHJlc2NyaXB0aW9ucyA9IHJlc3BvbnNlWydwcmVzY3JpcHRpb25zJ107XG4gICAgICAgIHRoaXMuY291bnQgPSByZXNwb25zZVsnY291bnQnXTtcbiAgICAgIH0sZXJyb3IgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGNyZWF0ZUxvYWRlcigpOiB2b2lkIHtcbiAgICB0aGlzLmxvYWRlciA9IHRoaXMubG9hZGluZ0N0cmwuY3JlYXRlKHtcbiAgICAgIGNvbnRlbnQ6IFwiUGxlYXNlIHdhaXQuLi5cIixcbiAgICAgIGRpc21pc3NPblBhZ2VDaGFuZ2U6IHRydWVcbiAgICB9KTtcbiAgICB0aGlzLmxvYWRlci5wcmVzZW50KCk7XG4gIH1cblxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NvbXBvbmVudHMvcHJlc2NyaXB0aW9uL3ByZXNjcmlwdGlvbi50cyIsImltcG9ydCB7IERlbGVnYXRpb25Qcm92aWRlciB9IGZyb20gJy4vLi4vLi4vcHJvdmlkZXJzL2RlbGVnYXRpb24tYXBpL2RlbGVnYXRpb24nO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTG9hZGluZ0NvbnRyb2xsZXIsIEFsZXJ0Q29udHJvbGxlciB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuXG4vKipcbiAqIEdlbmVyYXRlZCBjbGFzcyBmb3IgdGhlIERlbGVnYXRpb25Db21wb25lbnQgY29tcG9uZW50LlxuICpcbiAqIFNlZSBodHRwczovL2FuZ3VsYXIuaW8vYXBpL2NvcmUvQ29tcG9uZW50IGZvciBtb3JlIGluZm8gb24gQW5ndWxhclxuICogQ29tcG9uZW50cy5cbiAqL1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGVsZWdhdGlvbicsXG4gIHRlbXBsYXRlVXJsOiAnZGVsZWdhdGlvbi5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBEZWxlZ2F0aW9uQ29tcG9uZW50IHtcblxuICBASW5wdXQoKVxuICBpc1Byb3ZpZGVyOiBib29sZWFuID0gZmFsc2U7XG4gIG5vbWluZWVBZGhhYXI6IHN0cmluZztcbiAgZXJyb3I6IHN0cmluZztcbiAgbm9taW5lZXM6IEFycmF5PGFueT47XG4gIG5vbWluYXRvcnM6IEFycmF5PGFueT47XG4gIEBJbnB1dCgpXG4gIHVzZXI6IGFueTtcbiAgQElucHV0KClcbiAgdXNlckluZm86IGFueTtcbiAgZ3JhbnRBZGRyZXNzOiBhbnk7XG4gIHJldm9rZUFkZHJlc3M6IGFueVxuICBzaG93SXRlbSA9IG51bGw7XG4gIGM6IGFueTtcbiAgbG9hZGVyOiBhbnk7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgRGVsZWdhdGlvblByb3ZpZGVyOiBEZWxlZ2F0aW9uUHJvdmlkZXIsXG4gICAgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcixcbiAgICBwdWJsaWMgYWxlcnRDdHJsOiBBbGVydENvbnRyb2xsZXIpIHtcbiAgICBjb25zb2xlLmxvZygnSGVsbG8gRGVsZWdhdGlvbkNvbXBvbmVudCBDb21wb25lbnQnKTtcbiAgfVxuXG4gIGdldERlbGVnYXRpb25JbmZvKCk6IHZvaWQge1xuICAgIGNvbnN0IHVzZXJJZCA9IHRoaXMudXNlci5zc247XG4gICAgdGhpcy5jcmVhdGVMb2FkZXIoKTtcbiAgICB0aGlzLkRlbGVnYXRpb25Qcm92aWRlci5nZXREZWxlZ2F0aW9uSW5mbyh1c2VySWQpXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICB0aGlzLnNob3dJdGVtID0gXCJ2aWV3XCJcbiAgICAgICAgY29uc29sZS5sb2coXCJyZXNwb25zZVwiLCByZXNwb25zZSk7XG4gICAgICAgIHRoaXMubm9taW5lZXMgPSByZXNwb25zZTtcbiAgICAgIH0sXG4gICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJlcnJvclwiLCBlcnJvcik7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgaXNBcnJheShvYmo6IGFueSkge1xuICAgIHJldHVybiBBcnJheS5pc0FycmF5KG9iaik7XG4gIH1cblxuICBub21pbmF0ZSgpOiB2b2lkIHtcbiAgICB0aGlzLnNob3dJdGVtID0gXCJkZWxlZ2F0ZVwiXG4gICAgdGhpcy5lcnJvciA9IG51bGw7XG4gICAgY29uc29sZS5sb2coXCJpbiBub21pbmF0ZVwiKTtcbiAgICBjb25zb2xlLmxvZyhcImluIGdldCBOb21pbmVlc1wiLCB0aGlzLnVzZXIpO1xuICAgIGxldCBwYXJhbXMgPSB7XG4gICAgICBub21pbmVlX2lkOiB0aGlzLm5vbWluZWVBZGhhYXIsXG4gICAgICB1c2VyX2lkOiB0aGlzLnVzZXIuc3NuLFxuICAgIH07XG4gICAgdGhpcy5jcmVhdGVMb2FkZXIoKTtcbiAgICBpZiAodGhpcy5ub21pbmVlQWRoYWFyKSB7XG4gICAgICB0aGlzLkRlbGVnYXRpb25Qcm92aWRlci5kZWxlZ2F0ZShwYXJhbXMpXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcInJlc3BvbnNlXCIsIHJlc3BvbnNlKTtcbiAgICAgICAgICBsZXQgaW5mbyA9IHtcbiAgICAgICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3NcIixcbiAgICAgICAgICAgIHN1YlRpdGxlOiBcIlVzZXIgd2l0aCAgXCIrIHRoaXMubm9taW5lZUFkaGFhciArXCIgZ2l2ZW4gYWNjZXNzIHRvIGFjdCBvbiBiZWhhbGYgb2YgeW91LlwiXG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgICB9LFxuICAgICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgICAgICB0aXRsZTogXCJFcnJvclwiLFxuICAgICAgICAgICAgICBzdWJUaXRsZTogXCJPb3BzISEhIFNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGFnYWluIGFmdGVyIHNvbWV0aW1lXCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJlcnJvclwiLCBlcnJvcik7XG4gICAgICAgICAgfSk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgdGhpcy5lcnJvciA9IFwiUGxlYXNlIHByb3ZpZGUgdXNlciBpZGVudGlmaWNhdGlvblwiXG4gICAgfVxuICB9XG5cbiAgcmV2b2tlKG5vbWluZWUpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZyhub21pbmVlKTtcbiAgICBsZXQgcGFyYW1zID0ge1xuICAgICAgbm9taW5lZV9pZDogbm9taW5lZS5BZGhhYXIsXG4gICAgICB1c2VyX2lkOiB0aGlzLnVzZXIuc3NuXG4gICAgfVxuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgdGhpcy5EZWxlZ2F0aW9uUHJvdmlkZXIucmV2b2tlRGVsZWdhdGlvbihwYXJhbXMpXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICAgIGlmIChyZXNwb25zZSlcbiAgICAgICAgICB0aGlzLm5vbWluZWVzID0gcmVzcG9uc2U7XG4gICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3NcIixcbiAgICAgICAgICBzdWJUaXRsZTogXCJQcml2ZWxlZ2Ugb2YgVXNlciB3aXRoIEFkaGFhciBcIisgbm9taW5lZS51c2VyX2lkICtcIiByZXZva2VkLlwiXG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICB9LFxuICAgICAgICBlcnJvciA9PiB7XG4gICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgICAgIHN1YlRpdGxlOiBcIk9vcHMhISEgU29tZXRoaW5nIHdlbnQgd3JvbmcuIFBsZWFzZSB0cnkgYWdhaW4gYWZ0ZXIgc29tZXRpbWVcIlxuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgZ2V0RGVsZWdhdG9ycygpOiB2b2lkIHtcbiAgICBjb25zdCB1c2VySWQgPSB0aGlzLnVzZXIuc3NuO1xuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgdGhpcy5EZWxlZ2F0aW9uUHJvdmlkZXIuZ2V0RGVsZWdhdG9ycyh1c2VySWQpXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICB0aGlzLnNob3dJdGVtID0gXCJkZWxlZ2F0b3JcIjtcbiAgICAgICAgY29uc29sZS5sb2coXCJyZXNwb25zZVwiLCByZXNwb25zZSk7XG4gICAgICAgIHRoaXMubm9taW5hdG9ycyA9IHJlc3BvbnNlO1xuICAgICAgfSxcbiAgICAgICAgZXJyb3IgPT4ge1xuICAgICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgICBsZXQgaW5mbyA9IHtcbiAgICAgICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgICAgICBzdWJUaXRsZTogXCJPb3BzISEhIFNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGFnYWluIGFmdGVyIHNvbWV0aW1lXCJcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJlcnJvclwiLCBlcnJvcik7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgbWFuYWdlQWNjZXNzKG5vbWluYXRvciwgZ3JhbnRBY2Nlc3MpOiB2b2lkIHtcbiAgICBsZXQgaW5mbztcbiAgICBsZXQgZGF0YSA9IHtcbiAgICAgIHVzZXJfaWQ6IHRoaXMudXNlci5zc24sXG4gICAgICB1c2VyX25hbWU6IHRoaXMudXNlckluZm8ubmFtZSxcbiAgICAgIG5vbWluYXRvcl9pZDogbm9taW5hdG9yLkFkaGFhcixcbiAgICAgIGRvY3RvckFkZHJlc3M6IGdyYW50QWNjZXNzID8gdGhpcy5ncmFudEFkZHJlc3M6IHRoaXMucmV2b2tlQWRkcmVzcyxcbiAgICAgIGdyYW50QWNjZXNzXG4gICAgfVxuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgdGhpcy5EZWxlZ2F0aW9uUHJvdmlkZXIuZ3JhbnRBY2Nlc3MoZGF0YSlcbiAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwicmVzcG9uc2VcIiwgcmVzcG9uc2UpO1xuICAgICAgICBpZiAoZ3JhbnRBY2Nlc3MpIHtcbiAgICAgICAgICBpbmZvID0ge1xuICAgICAgICAgICAgdGl0bGU6IFwiU3VjY2Vzc1wiLFxuICAgICAgICAgICAgc3ViVGl0bGU6IFwiQWNjZXNzIHNoYXJlZCB3aXRoIGRvY3RvciBmb3IgXCIgKyBub21pbmF0b3IubmFtZVxuICAgICAgICAgIH1cblxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGluZm8gPSB7XG4gICAgICAgICAgICB0aXRsZTogXCJTdWNjZXNzXCIsXG4gICAgICAgICAgICBzdWJUaXRsZTogXCJBY2Nlc3MgcmV2b2tlZCBmcm9tIGRvY3RvcmZvciBcIiArIG5vbWluYXRvci5uYW1lXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgICB0aGlzLm5vbWluYXRvcnMgPSByZXNwb25zZTtcbiAgICAgIH0sXG4gICAgICAgIGVycm9yID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgICB0aXRsZTogXCJFcnJvclwiLFxuICAgICAgICAgICAgc3ViVGl0bGU6IFwiT29wcyEhISBTb21ldGhpbmcgd2VudCB3cm9uZy4gUGxlYXNlIHRyeSBhZ2FpbiBhZnRlciBzb21ldGltZVwiXG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiZXJyb3JcIiwgZXJyb3IpO1xuICAgICAgICB9KTtcbiAgfVxuXG4gIGNyZWF0ZUxvYWRlcigpOiB2b2lkIHtcbiAgICB0aGlzLmxvYWRlciA9IHRoaXMubG9hZGluZ0N0cmwuY3JlYXRlKHtcbiAgICAgIGNvbnRlbnQ6IFwiUGxlYXNlIHdhaXQuLi5cIixcbiAgICAgIGRpc21pc3NPblBhZ2VDaGFuZ2U6IHRydWVcbiAgICB9KTtcbiAgICB0aGlzLmxvYWRlci5wcmVzZW50KCk7XG4gIH1cblxuICBwcmVzZW50QWxlcnQoaW5mbykge1xuICAgIGxldCBhbGVydCA9IHRoaXMuYWxlcnRDdHJsLmNyZWF0ZSh7XG4gICAgICB0aXRsZTogaW5mby50aXRsZSxcbiAgICAgIHN1YlRpdGxlOiBpbmZvLnN1YlRpdGxlLFxuICAgICAgYnV0dG9uczogWydPayddXG4gICAgfSk7XG4gICAgYWxlcnQucHJlc2VudCgpO1xuICB9XG5cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jb21wb25lbnRzL2RlbGVnYXRpb24vZGVsZWdhdGlvbi50cyIsImltcG9ydCB7IEhvbWUgfSBmcm9tICcuLy4uL3BhZ2VzL2hvbWUvaG9tZSc7XG5pbXBvcnQgeyBBYm91dFVzUGFnZSB9IGZyb20gJy4vLi4vcGFnZXMvYWJvdXQtdXMvYWJvdXQtdXMnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBsYXRmb3JtLCBNZW51Q29udHJvbGxlciwgTmF2IH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5cbi8vaW1wb3J0IHsgSGVsbG9Jb25pY1BhZ2UgfSBmcm9tICcuLi9wYWdlcy9oZWxsby1pb25pYy9oZWxsby1pb25pYyc7XG5pbXBvcnQgeyBMYW5kaW5nUGFnZSB9IGZyb20gJy4uL3BhZ2VzL2xhbmRpbmcvbGFuZGluZyc7XG4vL2ltcG9ydCB7IExpc3RQYWdlIH0gZnJvbSAnLi4vcGFnZXMvbGlzdC9saXN0JztcblxuaW1wb3J0IHsgU3RhdHVzQmFyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9zdGF0dXMtYmFyJztcbmltcG9ydCB7IFNwbGFzaFNjcmVlbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvc3BsYXNoLXNjcmVlbic7XG5cblxuQENvbXBvbmVudCh7XG4gIHRlbXBsYXRlVXJsOiAnYXBwLmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIE15QXBwIHtcbiAgQFZpZXdDaGlsZChOYXYpIG5hdjogTmF2O1xuXG4gIC8vIG1ha2UgSGVsbG9Jb25pY1BhZ2UgdGhlIHJvb3QgKG9yIGZpcnN0KSBwYWdlXG4gIHJvb3RQYWdlID0gTGFuZGluZ1BhZ2U7XG4gIHBhZ2VzOiBBcnJheTx7dGl0bGU6IHN0cmluZywgY29tcG9uZW50OiBhbnksIHVybDogc3RyaW5nfT47XG4gIC8vcGFnZXM6IEFycmF5PHt0aXRsZTogc3RyaW5nLCBjb21wb25lbnQ6IGFueSwgdXJsOiBzdHJpbmd9PjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgcGxhdGZvcm06IFBsYXRmb3JtLFxuICAgIHB1YmxpYyBtZW51OiBNZW51Q29udHJvbGxlcixcbiAgICBwdWJsaWMgc3RhdHVzQmFyOiBTdGF0dXNCYXIsXG4gICAgcHVibGljIHNwbGFzaFNjcmVlbjogU3BsYXNoU2NyZWVuXG4gICkge1xuICAgIHRoaXMuaW5pdGlhbGl6ZUFwcCgpO1xuXG4gICAgLy8gc2V0IG91ciBhcHAncyBwYWdlc1xuICAgIHRoaXMucGFnZXMgPSBbXG4gICAgICB7IHRpdGxlOiAnTG9naW4vU2lnbiB1cCcsIGNvbXBvbmVudDogSG9tZSwgdXJsOiAnL2xvZ2luJyB9LFxuICAgICAgeyB0aXRsZTogJ0Fib3V0IFVzJywgY29tcG9uZW50OiBBYm91dFVzUGFnZSwgdXJsOiAnL2Fib3V0JyB9XG4gICAgXTtcbiAgICAvLyB0aGlzLnBhZ2VzID0gW1xuICAgIC8vICAge1xuICAgIC8vICAgICB0aXRsZTogJ0xvZ2luL1NpZ24gdXAnLFxuICAgIC8vICAgICB1cmw6ICcvYXBwL3RhYnMvKHNjaGVkdWxlOnNjaGVkdWxlKScsXG4gICAgLy8gICAgIGNvbXBvbmVudDogUmVnaXN0cmF0aW9uIH1cbiAgICAvLyBdXG5cbiAgfVxuXG4gIGluaXRpYWxpemVBcHAoKSB7XG4gICAgdGhpcy5wbGF0Zm9ybS5yZWFkeSgpLnRoZW4oKCkgPT4ge1xuICAgICAgLy8gT2theSwgc28gdGhlIHBsYXRmb3JtIGlzIHJlYWR5IGFuZCBvdXIgcGx1Z2lucyBhcmUgYXZhaWxhYmxlLlxuICAgICAgLy8gSGVyZSB5b3UgY2FuIGRvIGFueSBoaWdoZXIgbGV2ZWwgbmF0aXZlIHRoaW5ncyB5b3UgbWlnaHQgbmVlZC5cbiAgICAgIHRoaXMuc3RhdHVzQmFyLnN0eWxlRGVmYXVsdCgpO1xuICAgICAgdGhpcy5zcGxhc2hTY3JlZW4uaGlkZSgpO1xuICAgIH0pO1xuICB9XG5cbiAgb3BlblBhZ2UocGFnZSkge1xuICAgIC8vIGNsb3NlIHRoZSBtZW51IHdoZW4gY2xpY2tpbmcgYSBsaW5rIGZyb20gdGhlIG1lbnVcbiAgICB0aGlzLm1lbnUuY2xvc2UoKTtcbiAgICAvLyBuYXZpZ2F0ZSB0byB0aGUgbmV3IHBhZ2UgaWYgaXQgaXMgbm90IHRoZSBjdXJyZW50IHBhZ2VcbiAgICB0aGlzLm5hdi5wdXNoKHBhZ2UuY29tcG9uZW50KTtcbiAgICAvL3RoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pXG4gIH1cblxuICBhYm91dFVzKCkge1xuICAgIHRoaXMubmF2LnB1c2goQWJvdXRVc1BhZ2UpO1xuICAgIHRoaXMubWVudS5jbG9zZSgpO1xuICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvYXBwL2FwcC5jb21wb25lbnQudHMiLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW9uaWNQYWdlTW9kdWxlIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBQYXRpZW50RGFzaGJvYXJkUGFnZSB9IGZyb20gJy4vcGF0aWVudC1kYXNoYm9hcmQnO1xuaW1wb3J0IHsgQ29tcG9uZW50c01vZHVsZSB9IGZyb20gXCIuLi8uLi9jb21wb25lbnRzL2NvbXBvbmVudHMubW9kdWxlXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFBhdGllbnREYXNoYm9hcmRQYWdlLFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgSW9uaWNQYWdlTW9kdWxlLmZvckNoaWxkKFBhdGllbnREYXNoYm9hcmRQYWdlKSxcbiAgICBDb21wb25lbnRzTW9kdWxlXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFBhdGllbnREYXNoYm9hcmRQYWdlTW9kdWxlIHt9XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcGFnZXMvcGF0aWVudC1kYXNoYm9hcmQvcGF0aWVudC1kYXNoYm9hcmQubW9kdWxlLnRzIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElvbmljUGFnZU1vZHVsZSB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuaW1wb3J0IHsgRG9jdG9yRGFzaGJvYXJkUGFnZSB9IGZyb20gJy4vZG9jdG9yLWRhc2hvYXJkJztcbmltcG9ydCB7IENvbXBvbmVudHNNb2R1bGUgfSBmcm9tIFwiLi4vLi4vY29tcG9uZW50cy9jb21wb25lbnRzLm1vZHVsZVwiO1xuaW1wb3J0IHsgUVJDb2RlTW9kdWxlIH0gZnJvbSAnYW5ndWxhcngtcXJjb2RlJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRG9jdG9yRGFzaGJvYXJkUGFnZSxcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIElvbmljUGFnZU1vZHVsZS5mb3JDaGlsZChEb2N0b3JEYXNoYm9hcmRQYWdlKSxcbiAgICBDb21wb25lbnRzTW9kdWxlLFxuICAgIFFSQ29kZU1vZHVsZVxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBEb2N0b3JEYXNoYm9hcmRQYWdlTW9kdWxlIHt9XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcGFnZXMvZG9jdG9yLWRhc2hvYXJkL2RvY3Rvci1kYXNoYm9hcmQubW9kdWxlLnRzIiwiaW1wb3J0IHsgSGVhbHRoZXJpdW1BcGkgfSBmcm9tICcuLy4uLy4uL3Byb3ZpZGVycy9oZWFsdGhlcml1bS1hcGkvaGVhbHRoZXJpdW0tYXBpJztcbmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgLypQbGF0Zm9ybSwgKi9OYXZQYXJhbXMsIFZpZXdDb250cm9sbGVyLCBOYXZDb250cm9sbGVyLCBMb2FkaW5nQ29udHJvbGxlciwgQWxlcnRDb250cm9sbGVyIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBEb2N0b3JEYXNoYm9hcmRQYWdlIH0gZnJvbSAnLi8uLi9kb2N0b3ItZGFzaG9hcmQvZG9jdG9yLWRhc2hvYXJkJztcbmltcG9ydCB7IFBhdGllbnREYXNoYm9hcmRQYWdlIH0gZnJvbSAnLi8uLi9wYXRpZW50LWRhc2hib2FyZC9wYXRpZW50LWRhc2hib2FyZCc7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGFnZS1tb2RhbC1jb250ZW50JyxcbiAgdGVtcGxhdGVVcmw6ICdtb2RhbC1jb250ZW50Lmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBNb2RhbENvbnRlbnRQYWdlIHtcblxuICBsb2FkZXI7XG5cbiAgdXNlcjogYW55ID0ge307XG4gIGFjdGlvbjogc3RyaW5nO1xuICByb2xlOiBhbnk7XG4gIHNpZ25VcEluZm86IGFueTtcbiAgb3RwOiBzdHJpbmc7XG4gIHNob3dPVFBmb3JtOiBhbnkgPSBmYWxzZTtcbiAgbG9naW5PVFA6IGFueTtcbiAgdG90YWxQcm92aWRlckxpc3Q6IGFueTtcbiAgcGF0aWVudFByb3ZpZGVyOiBhbnk7XG4gIGRvY3RvckFkZHJlc3M6YW55O1xuICBzZWxlY3RlZFBhdGllbnRBZGRyZXNzOmFueTtcbiAgY29uc3RydWN0b3IoXG4gICAgLy9wcml2YXRlIHBsYXRmb3JtOiBQbGF0Zm9ybSxcbiAgICBwcml2YXRlIHBhcmFtczogTmF2UGFyYW1zLFxuICAgIHByaXZhdGUgdmlld0N0cmw6IFZpZXdDb250cm9sbGVyLFxuICAgIHByaXZhdGUgbmF2OiBOYXZDb250cm9sbGVyLFxuICAgIHByaXZhdGUgYXBpOiBIZWFsdGhlcml1bUFwaSxcbiAgICBwdWJsaWMgYWxlcnRDdHJsOiBBbGVydENvbnRyb2xsZXIsXG4gICAgcHVibGljIGxvYWRpbmdDdHJsOiBMb2FkaW5nQ29udHJvbGxlcikge1xuICAgIHRoaXMubG9hZGVyID0gdGhpcy5sb2FkaW5nQ3RybC5jcmVhdGUoe1xuICAgICAgY29udGVudDogXCJQbGVhc2Ugd2FpdC4uLlwiLFxuICAgICAgZGlzbWlzc09uUGFnZUNoYW5nZTogdHJ1ZVxuICAgIH0pO1xuICAgIHRoaXMuYWN0aW9uID0gdGhpcy5wYXJhbXMuZ2V0KFwiYWN0aW9uXCIpO1xuICAgIHRoaXMucm9sZSA9IHRoaXMucGFyYW1zLmdldChcInVzZXJcIik7XG4gICAgdGhpcy5wYXRpZW50UHJvdmlkZXIgPSB0aGlzLnBhcmFtcy5nZXQoXCJjaG9vc2VQcm92aWRlclwiKTtcbiAgICB0aGlzLmRvY3RvckFkZHJlc3MgPSB0aGlzLnBhcmFtcy5nZXQoXCJ1c2VyQWNjb3VudFwiKTtcbiAgICB0aGlzLnNlbGVjdGVkUGF0aWVudEFkZHJlc3MgPSB0aGlzLnBhcmFtcy5nZXQoXCJzZWxlY3RlZFBhdGllbnRBZGRyZXNzXCIpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMuc2lnblVwSW5mbywgdGhpcy5hY3Rpb24pO1xuICAgIGNvbnNvbGUubG9nKHRoaXMucGF0aWVudFByb3ZpZGVyKTtcbiAgfVxuXG4gIHJlZ2lzdGVyKCkge1xuICAgIHRoaXMuY3JlYXRlTG9hZGVyKCk7XG4gICAgbGV0IGRhdGEgPSB0aGlzLnBhcmFtcy5nZXQoXCJkYXRhXCIpO1xuICAgIGRhdGEub3RwID0gdGhpcy5vdHA7XG4gICAgdGhpcy5hcGkucmVnaXN0ZXIoZGF0YSlcbiAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UpIHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgICB0aXRsZTogXCJTdWNjZXNzZnVsXCIsXG4gICAgICAgICAgICBzdWJUaXRsZTogYFlvdXIgYWNjb3VudCBoYXMgYmVlbiBzdWNjZXNzZnVsbHkgY3JlYXRlZGBcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgIHRoaXMudmlld0N0cmwuZGlzbWlzcygpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgICBlcnJvciA9PiB7XG4gICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgICAgIHN1YlRpdGxlOiBlcnJvci5lcnJvci5tZXNzYWdlLFxuICAgICAgICAgIH07XG4gICAgICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgZ3JhbnRlZEVtckRhdGEoc2VsZWN0ZWRPcHRpb24pIHtcbiAgICBjb25zb2xlLmxvZyhzZWxlY3RlZE9wdGlvbik7XG4gICAgbGV0IHNlbGVjdGVkUHJvdmlkZXIgPSBzZWxlY3RlZE9wdGlvbjtcbiAgICBsZXQgc2VsZWN0ZWRQYXRpZW50QWRkcmVzcyA9IHRoaXMuc2VsZWN0ZWRQYXRpZW50QWRkcmVzcztcbiAgICBjb25zb2xlLmxvZyh0aGlzLmRvY3RvckFkZHJlc3MpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRQYXRpZW50QWRkcmVzcyk7XG4gICAgdGhpcy5hcGkuZ2V0VXNlckluZm8odGhpcy5zZWxlY3RlZFBhdGllbnRBZGRyZXNzLCB0aGlzLmRvY3RvckFkZHJlc3Msc2VsZWN0ZWRPcHRpb24ubnVtYmVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgY29uc29sZS5sb2coXCJzaGFyZWQgYWRkcmVzc1wiKTtcbiAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAgIHRoaXMudmlld0N0cmwuZGlzbWlzcygpO1xuICAgICAgdGhpcy5wYXJhbXMuZGF0YS5zZWxlY3RlZE9wdGlvbiA9IHNlbGVjdGVkUHJvdmlkZXI7XG4gICAgICBsZXQgcGF0aWVudERldGFpbHMgPSBKU09OLnBhcnNlKHJlc3BvbnNlLmJvZHkpLmVudHJ5WzBdLnJlc291cmNlO1xuICAgICAgICBwYXRpZW50RGV0YWlscy5zZWxlY3RlZFBhdGllbnRBZGRyZXNzID0gc2VsZWN0ZWRQYXRpZW50QWRkcmVzcztcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMucGFyYW1zLmRhdGEuc2VsZWN0ZWRPcHRpb24pO1xuICAgICAgdGhpcy5uYXYucHVzaChEb2N0b3JEYXNoYm9hcmRQYWdlLCB7aXNFbXJEYXRhOiB0cnVlLCByZXNwb25zZSwgcGF0aWVudERldGFpbHM6IHBhdGllbnREZXRhaWxzLCBkb2N0b3I6IHRoaXMucGFyYW1zLmRhdGEgfSk7XG4gICAgfSxcbiAgICAgIGVycm9yID0+IHtcbiAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICBjb25zb2xlLmVycm9yKCdPb3BzOicsIGVycm9yKTtcbiAgICAgICAgbGV0IGluZm8gPSB7XG4gICAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgICBzdWJUaXRsZTogZXJyb3IuZXJyb3IubWVzc2FnZVxuICAgICAgICAgIC8vc3ViVGl0bGU6IFwiT29wcy4gU29tZXRoaW5nIHdlbnQgd3JvbmcuIFBsZWFzZSB0cnkgYWdhaW4gYWZ0ZXIgc29tZSB0aW1lXCJcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnByZXNlbnRBbGVydChpbmZvKTtcbiAgICAgIH0pO1xuXG4gIH1cblxuICBnZW5lcmF0ZU9UUCgpOiB2b2lkIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy51c2VyLmlzUmVnaXN0ZXJlZCA9IHRydWU7XG4gICAgdGhpcy5jcmVhdGVMb2FkZXIoKTtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnVzZXIpO1xuICAgIC8vIHRoaXMudXNlci51c2VyTmFtZSA9IFwidXNlcjAxMVwiO1xuICAgIC8vIHRoaXMudXNlci5zc24gPSBcIjE2MTYxNjE2MTYxNlwiO1xuICAgIC8vIHRoaXMudXNlci5wYXNzd29yZCA9IFwicGFzc3dvcmRcIjtcbiAgICB0aGlzLmFwaS5nZW5lcmF0ZU9UUCh0aGlzLnVzZXIpXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlKSB7XG4gICAgICAgICAgdGhpcy5hcGkuZ2V0UHJvdmlkZXJzT2ZVc2VyKHRoaXMudXNlcilcbiAgICAgICAgICAuc3Vic2NyaWJlKHByb3ZpZGVyTGlzdCA9PiB7XG4gICAgICAgICAgICBpZihwcm92aWRlckxpc3Qpe1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcm92aWRlckxpc3QpO1xuICAgICAgICAgICAgICB0aGlzLnRvdGFsUHJvdmlkZXJMaXN0ID0gcHJvdmlkZXJMaXN0O1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnRvdGFsUHJvdmlkZXJMaXN0KTtcbiAgICAgICAgICAgICAgdGhpcy5zaG93T1RQZm9ybSA9IHRydWU7XG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgICBlcnJvciA9PiB7XG4gICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ09vcHM6JywgZXJyb3IpO1xuICAgICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgICAgIHN1YlRpdGxlOiBcIk9vcHMuIFNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGFnYWluIGFmdGVyIHNvbWUgdGltZVwiXG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMucHJlc2VudEFsZXJ0KGluZm8pO1xuICAgICAgICB9KTtcblxuICB9XG5cbiAgbG9naW4oc2VsZWN0ZWRPcHRpb24pOiB2b2lkIHtcbiAgICB0aGlzLmNyZWF0ZUxvYWRlcigpO1xuICAgIGNvbnN0IHVzZXIgPSB0aGlzLnBhcmFtcy5nZXQoXCJ1c2VyXCIpO1xuICAgIHRoaXMudXNlci51c2VyID0gdXNlclxuICAgIHRoaXMudXNlci5vdHAgPSB0aGlzLm90cDtcbiAgICB0aGlzLnVzZXIuZW1yRGV0YWlscyA9IHNlbGVjdGVkT3B0aW9uO1xuICAgIHRoaXMuYXBpLmxvZ2luKHRoaXMudXNlcilcbiAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xuICAgICAgICBpZiAodXNlciA9PT0gXCJwYXRpZW50XCIpIHtcbiAgICAgICAgICB0aGlzLm5hdi5wdXNoKFBhdGllbnREYXNoYm9hcmRQYWdlLCB7IHJlc3BvbnNlLCBjcmVkZW50aWFsczogdGhpcy51c2VyIH0pO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYodXNlciA9PT0gXCJkb2N0b3JcIil7XG4gICAgICAgICAgdGhpcy5uYXYucHVzaChEb2N0b3JEYXNoYm9hcmRQYWdlLCB7IHJlc3BvbnNlLCBjcmVkZW50aWFsczogdGhpcy51c2VyIH0pO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHdpbmRvdy5hbGVydChcIldvcmsgaW4gcHJvZ3Jlc3NcIik7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy52aWV3Q3RybC5kaXNtaXNzKCk7XG4gICAgICB9LFxuICAgICAgICBlcnJvciA9PiB7XG4gICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgICAgIHZhciBtZXNzYWdlID0gZXJyb3IgJiYgZXJyb3IubWVzc2FnZSA/IGVycm9yLm1lc3NhZ2UgOiBlcnJvcjtcbiAgICAgICAgICBjb25zb2xlLmVycm9yKCdPb3BzOicsIG1lc3NhZ2UpO1xuICAgICAgICAgIGxldCBpbmZvID0ge1xuICAgICAgICAgICAgdGl0bGU6IFwiRXJyb3JcIixcbiAgICAgICAgICAgIHN1YlRpdGxlOiBtZXNzYWdlIHx8IFwiT29wcy4gU29tZXRoaW5nIHdlbnQgd3JvbmcuIFBsZWFzZSB0cnkgYWdhaW4gYWZ0ZXIgc29tZSB0aW1lXCJcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5wcmVzZW50QWxlcnQoaW5mbyk7XG4gICAgICAgIH0pO1xuICB9XG5cbiAgZGlzbWlzcygpOiB2b2lkIHtcbiAgICB0aGlzLnZpZXdDdHJsLmRpc21pc3MoKTtcbiAgfVxuXG4gIHByZXNlbnRBbGVydChpbmZvKSB7XG4gICAgbGV0IGFsZXJ0ID0gdGhpcy5hbGVydEN0cmwuY3JlYXRlKHtcbiAgICAgIHRpdGxlOiBpbmZvLnRpdGxlLFxuICAgICAgc3ViVGl0bGU6IGluZm8uc3ViVGl0bGUsXG4gICAgICBidXR0b25zOiBbJ09rJ11cbiAgICB9KTtcbiAgICBhbGVydC5wcmVzZW50KCk7XG4gIH1cblxuICBjcmVhdGVMb2FkZXIoKTogdm9pZCB7XG4gICAgdGhpcy5sb2FkZXIgPSB0aGlzLmxvYWRpbmdDdHJsLmNyZWF0ZSh7XG4gICAgICBjb250ZW50OiBcIlBsZWFzZSB3YWl0Li4uXCIsXG4gICAgICBkaXNtaXNzT25QYWdlQ2hhbmdlOiB0cnVlXG4gICAgfSk7XG4gICAgdGhpcy5sb2FkZXIucHJlc2VudCgpO1xuICB9XG5cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9tb2RhbC1jb250ZW50L21vZGFsLWNvbnRlbnQudHMiXSwic291cmNlUm9vdCI6IiJ9