var ethers = require('ethers');
var config = require('./config');
var web3 = require('web3');

function instantiateWallet(privateKey) {
    var wallet = new ethers.Wallet(privateKey);
    wallet.provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');
    return wallet;
}

const provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');

module.exports = {
    provider: new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby'),

    transferEthers: async (masterPrivateKey, userAddress) => {
        try {
            const amount = ethers.utils.parseEther('0.5');
            const wallet = instantiateWallet(masterPrivateKey);
            const tx = await wallet.send(userAddress, amount);
            if(tx && tx.hash) {
                await provider.waitForTransaction(tx.hash);
                const status = await provider.getTransactionReceipt(tx.hash);
                if (status.status === 1) {
                    return {
                        status: 200,
                        message: "Transaction mined. Execution success"
                    };
                }
                else if (status.status === 0){
                    throw {
                        status: 500,
                        message: "Transaction mined but execution failed."
                    }
                }
            }
            else {
                throw {
                    status: 500,
                    message: "Transaction failed.",
                    error: tx
                }
            }
        }
        catch (e) {
            throw {
                status : 500,
                error:e
            }
        }
    },

    insertUser: function (publicKey, privateKey, comb, data) {

        console.log("Insert User to Blockchain", comb, web3.utils.fromAscii(comb));
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        console.log("this is data before blockchian call",data);
        if (data.user === "Doctor") {
            console.log(web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier, data.Department, data.Hospital);
            return contract.insertDoctor(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier, data.Department, data.Hospital);
        }
        else if (data.user === "Patient") {
            console.log("patient sign up");
            //as per the contract it is uuid and then combination
            //but what we are passing from js is combination and then uuid
            console.log(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier);
            return contract.insertPatient(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier);
        }
    },

    insertUser2: function (publicKey, privateKey, comb, data) {

        console.log("Insert User to Blockchain", comb, web3.utils.fromAscii(comb));
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        console.log(data);
        if (data.user === "Doctor") {
            console.log(web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier, web3.utils.fromAscii(data.Department), web3.utils.fromAscii(data.Hospital));
            return contract.insertDoctor(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier, web3.utils.fromAscii(data.Department), web3.utils.fromAscii(data.Hospital));
        }
        else if (data.user === "Patient") {
            console.log("patient sign up");
            //as per the contract it is uuid and then combination
            //but what we are passing from js is combination and then uuid
            console.log(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier);
            return contract.insertPatient(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(comb), data.InputOutputIdentifier);
        }
    },

    getDoctorAccessList: function (publicKey) {

        console.log("Getting the Details of Patient Addresses accessible to Doctor");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getGivenAccesslist(publicKey);

    },

    getPatientSharedList: function (publicKey) {

        console.log("Getting the Details of Doctors whom the patient has shared the data with");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getPatientSharedList(publicKey);

    },

    giveOwnership: function (patientPubKey, doctorPubKey, privateKey) {

        console.log("Patient Giving Access To Doctor");
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        return contract.giveAccessToDoctor(patientPubKey, doctorPubKey);

    },

    addPatientFile: function (patientPubKey, url, privateKey) {

        console.log("Patient Adding Dicom File");
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        return contract.addhashpatient(patientPubKey, url);

    },

    cancelOwnership: function (patientPubKey, p, doctorPubKey, d, privateKey) {

        console.log("Patient Cancelling Accessing Given To Doctor");
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        return contract.revokeAccessFromDoctor(patientPubKey, p, d, doctorPubKey);

    },

    getPatientFromDoctor: function (doctorPubKey, patient_index) {

        console.log("Doctor Accessing Patient Data");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getPatient(doctorPubKey, patient_index);

    },

    getPatientFileFromDoctor: function (doctorPubKey, patient_index) {

        console.log("Doctor Accessing Patient Dicom File");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getpatienthashdoctor(doctorPubKey, patient_index);

    },

    callBlockchainTransaction: async (fnName, params, privatekey) => {
        let wallet;
        let contract;
        try {
            if (privatekey) {
                wallet = instantiateWallet(privatekey);
                contract = new ethers.Contract(config.contract_address, config.abi, wallet);
            }
            else {
                contract = new ethers.Contract(config.contract_address, config.abi, provider);
            }

             const contract_fn = contract[fnName];
            console.log(fnName);
             console.log(...params);
             const tx = await contract_fn(...params);
             console.log(tx, "in parent transaction");
            if (tx && tx.hash) {
                console.log("hello",tx.hash);
                await provider.waitForTransaction(tx.hash);
                const status = await provider.getTransactionReceipt(tx.hash);
                console.log(status);
                if (status.status === 1) {
                    return {
                        status: 200,
                        message: "Transaction mined. Execution success"
                    };
                }
                else if (status.status === 0){
                    throw {
                        status: 500,
                        message: "Transaction mined but execution failed."
                    }
                }
            }
            else {
                return {
                    status: 200,
                    message: "success",
                    data : tx
                };
            }
        }
        catch (e) {
            throw {
                status : 500,
                error:e
            }
        }
    }
}
