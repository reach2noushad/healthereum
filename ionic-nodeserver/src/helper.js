import config from './config';
import encryptionHelper from './encryptionHelper';
const uuidv1 = require('uuid/v1');

function decrypt(cipher) {
    const decipheredText = encryptionHelper.decryptText(config.CIPHERS.AES_256, config.cipherKeyIv.key, config.cipherKeyIv.iv, cipher, "hex");
    const userCredentials = JSON.parse(decipheredText);
    return userCredentials;
}

function encrypt(content) {
    return encryptionHelper.encryptText(config.CIPHERS.AES_256,  config.cipherKeyIv.key, config.cipherKeyIv.iv, content, "hex");
 }

function uuid() {
    return uuidv1();
}


module.exports = {
    decrypt: (cipher) => decrypt(cipher),
    encrypt: (content) => encrypt(content),
    uuid
}

