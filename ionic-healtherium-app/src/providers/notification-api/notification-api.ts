import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent , HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from '../../config';
import { Observable } from 'rxjs';
import { Notification, NotificationsResponse} from '../../models/notification';
import { catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

/*
  Generated class for the NotificationApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationApiProvider {
  private notificationsUrl = BASE_URL + '/api/notifications';
  constructor(public http: HttpClient) {
    console.log('Hello NotificationApiProvider Provider');
  }

  getNotifications (userId: string): Observable<NotificationsResponse[]> {
    return this.http.get<NotificationsResponse[]>(this.notificationsUrl+ `?userId=${userId}`).pipe(
      catchError((err: HttpErrorResponse) => {
        return Observable.throw(err);
      })
    );
  }

  getNotificationsCount (userId: string): Observable<NotificationsResponse[]> {
    return this.http.get<NotificationsResponse[]>(this.notificationsUrl+ `?userId=${userId}&field=count`).pipe(
      catchError((err: HttpErrorResponse) => {
        return Observable.throw(err);
      })
    );
  }

  updateNotificationsRead (notifications: Array<Notification>): Observable<any> {
    let notificationIds = [];
    for(let notification of notifications){
      notificationIds.push(notification.id);
    }
    return this.http.post(this.notificationsUrl, {notifications: notificationIds}, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        return Observable.throw(err);
      })
    );
  }

}
