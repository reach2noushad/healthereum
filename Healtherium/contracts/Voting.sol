pragma solidity ^0.4.18;

contract Voting {

  struct UserStruct {
    address instowner;
    bytes32 adhaar;
    string comb;
    bytes32 UUID;
    uint index;
    string hash;
  }

    struct DoctorStruct {
    address instowner;
    bytes32 adhaar;
    string comb;
    address secondowner;
    bytes32 department;
    bytes32 hospital;
    uint index;
    bytes32 UUID;
    string hash;
  }

  string constant username = "admin";
  string constant password = "Admin123";


  mapping(address => UserStruct) private userStructs;
  mapping(address => DoctorStruct) private doctorStructs;
  bytes32[] private userIndex;

   modifier patientUUIDOwner(address anyowners, bytes32 UUIDs) {
    require(userStructs[anyowners].UUID == UUIDs);
    _;
  }

  modifier patientAdharOwner(address anyowners, bytes32 adhaar) {
    require(userStructs[anyowners].adhaar == adhaar);
    _;
  }

  modifier doctorUUIDOwner(address anyowners,bytes32 UUIDs){
    require(doctorStructs[anyowners].UUID==UUIDs);
    _;
  }



  function insertPatient (address instowner,bytes32 adhaar,bytes32 UUID, string comb)  public returns (string, string,string) {
       userStructs[instowner].instowner = instowner;
       userStructs[instowner].adhaar   = adhaar;
       userStructs[instowner].UUID   = UUID;
       userStructs[instowner].comb   = comb;
       userStructs[instowner].index     = userIndex.push(UUID)-1;
       string ccomb=userStructs[instowner].comb;
        return (username,password,ccomb);
  }

    function loginpatient  (address instowner, bytes32 UUIDs, bytes32 aadhar) patientUUIDOwner(instowner,UUIDs) patientAdharOwner(instowner,aadhar) constant
    public  returns(string,string,string)
  {
    return(
      username,
      password,
      userStructs[instowner].comb);

    }

    function insertDoctor (address instowner,bytes32 adhaar,bytes32 UUID, string comb,bytes32 department,bytes32 hospital) public returns (string,string,string) {
       doctorStructs[instowner].instowner = instowner;
       doctorStructs[instowner].adhaar = adhaar;
       doctorStructs[instowner].UUID = UUID;
       doctorStructs[instowner].comb = comb;
       doctorStructs[instowner].index = userIndex.push(UUID)-1;
       doctorStructs[instowner].department = department;
       doctorStructs[instowner].hospital = hospital;
       string ccomb=doctorStructs[instowner].comb;
       return (username,password,ccomb);
    }

     function loginDoctor (address instowner,bytes32 UUIDs, bytes32 aadhar) doctorUUIDOwner(instowner,UUIDs) constant
     public returns(string,string,string,bytes32,bytes32)
     {
         var department = doctorStructs[instowner].department;
         var hospital = doctorStructs[instowner].hospital;
         return(username,password,doctorStructs[instowner].comb,department,hospital);
     }


     function addhashpatient (address instowner, string hash) public returns (bool success) {
       userStructs[instowner].hash   = hash;
       return true;
     }

     function gethashpatient (address instowner, bytes32 UUIDs) patientUUIDOwner(instowner,UUIDs) public constant returns(string hash) {
      return(userStructs[instowner].hash);
     }

     function secondOwner(address presentowner, address secondowner) public {
        doctorStructs[secondowner].secondowner =  presentowner;

     }

     function cancelsecondowner(address firstowner,address secondowner) public {
      doctorStructs[secondowner].secondowner= 0x00;
     }

     function getPatient(address Doctor) constant public returns (string,string,string) {
      var ituser = doctorStructs[Doctor].secondowner;
       return(username,password,userStructs[ituser].comb);
     }


    function getpatienthashdoctor(address Doctor) constant public returns (string hash) {
      var ituser = doctorStructs[Doctor].secondowner;
       return(userStructs[ituser].hash);
    }

}
