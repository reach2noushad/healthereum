import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User, Provider } from './user';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private userType: BehaviorSubject<string> = new BehaviorSubject<string>("");
  userProfile: any;
  constructor(private router: Router, private http: HttpClient) { }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  get userInitialized() {
    return this.userType.asObservable();
  }

  generateOTP(data): Observable<any> {
    console.log("registerUser")
    return this.http.post(`http://192.168.2.27:4445/api/user/generateotp`, data)
      .pipe(map(response => { console.log(response); return response; }))
  }

  login(user: User): Observable<any> {
    console.log("login", user)
    return this.http.post(`http://192.168.2.27:4445/api/user/login`, user)
      .pipe(map(response => {
        this.loggedIn.next(true);
        this.userType.next(user.user);
        return response;
      })
      );
  }

logout() {
  this.loggedIn.next(false);
  this.router.navigate(['/signin']);
}

getProviders(): Observable < Provider[] > {
  return this.http.get<Provider[]>(`http://192.168.2.27:4445/api/providers`)
    .pipe(map(response => { console.log(response); return response }))
}

signup(data): Observable < any > {
  return this.http.post(`http://192.168.2.27:4445/api/user/register`, data)
    .pipe(map(response => { console.log(response); return response }))
}

searchUser(data) : Observable <any> {

  let queryParams = `?emrId=${data.emrNumber}&providerId=${data.provider}`;

  return this.http.get(`http://192.168.2.27:4445/api/user`+ queryParams)
  .pipe(map(response => {
      return response;
  }))

}

getRegisteredProviders(data) : Observable < any > {
  console.log("calling get providers api from front end");
  console.log(data);
  let queryParams='';
  if(data.ssn){
  queryParams = `?userId=${data.ssn}`;
}
    else if (data.address) {
  queryParams = `?address=${data.address}`;
}

return this.http.get(`http://192.168.2.27:4445/api/user/providerList` + queryParams)
  .pipe(map(response => {
    console.log(response);
    return response;
  }));
  }
}

