import DBService from '../services/dbservice';


module.exports = {
    getName: async (address) => {
        try {
            var val = address.toLowerCase().substring(2);
            var sql = 'SELECT name FROM users WHERE Account = ?';
            const name =  await DBService.executeQuery({query:sql,values:val});
            return  name;

        }
        catch (e) {
            throw e;
        }
    }

}